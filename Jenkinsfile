/* ========================================================================== *
 * Jenkins Pipeline for the Big Flexible Inverter (BFI) FPGA project.         *
 * ========================================================================== */

/* Include libraries (the '_' at the end is not a type, rather mandatory syntax because no import follows so '_' is annotated) */
@Library('rimac-pipelines@v2.4.8')_

/* Project setup. */
def REPO = "git@bitbucket.org:rimacautomobili/bfi_fpga.git"
def AWS_DEV  = [
               id: "1",
               team: "ESD-INVERTER",
               folder: "Inverter/C_Two/GL-PRE/BFI_FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]

def AWS_R_REL  = [
               id: "2",
               team: "ESD-INVERTER",
               model: "C_Two",
               market: "GL",
               car_type: "PRE",
               hw_comp: "BFI_R",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]

def AWS_L_REL  = [
               id: "3",
               team: "ESD-INVERTER",
               model: "C_Two",
               market: "GL",
               car_type: "PRE",
               hw_comp: "BFI_L",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]

def AWS_R_REL_PF  = [
               id: "4",
               team: "ESD-INVERTER",
               model: "Battista",
               market: "GL",
               car_type: "PRE",
               hw_comp: "BFI_R",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]

def AWS_L_REL_PF  = [
               id: "5",
               team: "ESD-INVERTER",
               model: "Battista",
               market: "GL",
               car_type: "PRE",
               hw_comp: "BFI_L",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]

def AWS_R_REL_PROTO  = [
               id: "6",
               team: "ESD-INVERTER",
               model: "C_Two",
               market: "GL",
               car_type: "PRO",
               hw_comp: "BFI_R",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]

def AWS_L_REL_PROTO  = [
               id: "7",
               team: "ESD-INVERTER",
               model: "C_Two",
               market: "GL",
               car_type: "PRO",
               hw_comp: "BFI_L",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]

def AWS_R_REL_PROTO_PF  = [
               id: "8",
               team: "ESD-INVERTER",
               model: "Battista",
               market: "GL",
               car_type: "PRO",
               hw_comp: "BFI_R",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]

def AWS_L_REL_PROTO_PF  = [
               id: "9",
               team: "ESD-INVERTER",
               model: "Battista",
               market: "GL",
               car_type: "PRO",
               hw_comp: "BFI_L",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]
		   
def AWS_R_REL_S5  = [
               id: "10",
               team: "ESD-INVERTER",
               model: "C_Two",
               market: "GL",
               car_type: "COU",
               hw_comp: "BFI_R",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]		 

def AWS_L_REL_S5  = [
               id: "11",
               team: "ESD-INVERTER",
               model: "C_Two",
               market: "GL",
               car_type: "COU",
               hw_comp: "BFI_L",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]	

def AWS_R_REL_S5_PF  = [
               id: "12",
               team: "ESD-INVERTER",
               model: "Battista",
               market: "GL",
               car_type: "COU",
               hw_comp: "BFI_R",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]		 

def AWS_L_REL_S5_PF  = [
               id: "13",
               team: "ESD-INVERTER",
               model: "Battista",
               market: "GL",
               car_type: "COU",
               hw_comp: "BFI_L",
               software: "FPGA",
               files: "BFI/BFI.runs/impl_1/bfi_top.bit, BFI/BFI.runs/impl_1/bfi_top.mcs, BFI/BFI.runs/impl_1/bfi_top.prm, \
                                          repo/004_Documents/Changelog.md"
           ]		   

/* Jenkins CI/CD scripts repository information. */
def JENKINS_CFG = [
                    repo     : "git@bitbucket.org:rimacautomobili/ci_scripts.git",
                    branch   : "v1.0.0",
                    folder   : "ci_scripts",
                    cfg_file : "cfg.ini"
                  ]

/* Call Vivado*/
def RunVivado(vivado_script)
{
    env.VIVADO_SCRIPT = vivado_script
    script {
            bat label: 'Vivado Build', script: '''
            CALL C:\\Xilinx\\Vivado\\2018.3\\bin\\vivado.bat -nolog -nojournal -mode batch -source %VIVADO_SCRIPT%
            IF "%errorlevel%" == "0" (
                echo "Build passed"
            ) ELSE (
                error("Vivado build failed")
            )
            '''
        }
}


/* Pipeline */
pipeline {
    agent none
    options {
        skipDefaultCheckout();
    }

    stages {
        // All of the stages have to be sequential!
        // "Build project" => "Check Syntax" => "Synthesis" => "Implementation" => "Generate bitstream" => "Generate MCS file" => "Copy Configuration Files" => "Make Backup"
        stage("Build project") {
            agent { label 'slave_build_win' }
            steps {
                cleanWs()
                echo "Build project using BFI.tcl script."
                gitCheckout(REPO)
                RunVivado("BFI.tcl")
            }
        }

        stage("Check Syntax") {
            agent { label 'slave_build_win' }
            steps {
                echo "Check Syntax of the code."
                RunVivado("repo\\007_Automation\\check_syntax.tcl")
            }
        }

        stage("Synthesis") {
            agent { label 'slave_build_win' }
            steps {
                echo "Run Synthesis of the design."
                RunVivado("repo\\007_Automation\\run_synthesis.tcl")
            }
        }

        stage("Implementation") {
            agent { label 'slave_build_win' }
            steps {
                echo "Run Implementation of the design."
                RunVivado("repo\\007_Automation\\run_implementation.tcl")
            }
        }

        stage("Generate bitstream") {
            agent { label 'slave_build_win' }
            steps {
                echo "Generate bitstream."
                RunVivado("repo\\007_Automation\\generate_bitstream.tcl")
            }
        }

        stage("Generate MCS") {
            agent { label 'slave_build_win' }
            steps {
                echo "Generate MCS and PRM files."
                RunVivado("repo\\007_Automation\\generate_mcs.tcl")

                // Save files for upload in the last step
                awsSaveForUpload(AWS_DEV)
                awsSaveForUpload(AWS_R_REL)
                awsSaveForUpload(AWS_L_REL)
                awsSaveForUpload(AWS_R_REL_PF)
                awsSaveForUpload(AWS_L_REL_PF)
                awsSaveForUpload(AWS_R_REL_PROTO)
                awsSaveForUpload(AWS_L_REL_PROTO)
                awsSaveForUpload(AWS_R_REL_PROTO_PF)
                awsSaveForUpload(AWS_L_REL_PROTO_PF)
                awsSaveForUpload(AWS_R_REL_S5)
                awsSaveForUpload(AWS_L_REL_S5) 
                awsSaveForUpload(AWS_R_REL_S5_PF)
                awsSaveForUpload(AWS_L_REL_S5_PF)				
            }
        }

        /* ====================================== *
         * AWS UPLOAD                             *
         * ====================================== */
        stage("AWS Upload Development") {
            agent {
                label 'slave_build_win'
            }
            /* Upload to esd-binaries-dev will run for every commit */
            steps {
                script{

                    /* Setting filename to Git Tag or Branch */

                    if(env.TAG_NAME) {
                        filename = env.TAG_NAME
                    } else {
                        if (env.CHANGE_BRANCH) {
                            filename = env.CHANGE_BRANCH
                        } else if (env.BRANCH_NAME) {
                            filename = env.BRANCH_NAME
                        }
                    }
                    AWS_DEV.filename = filename
                }
                /* Development */
                awsDeployDev(AWS_DEV)
            }
        }

        stage("AWS Upload Release") {
            agent {
                label 'slave_build_win'
            }
            // Run when PR is merged to development branch and if commit is taged
            // Upload files to esd-binaries-dev and esd-binaries
            // Commit has to be taged to be uploaded to esd-binaries (official release location)
            when {
                anyOf {
                    branch 'development'
                    buildingTag()
                }
            }

            steps {

                /* Release */
                awsDeployRel(AWS_R_REL)
                awsDeployRel(AWS_L_REL)
                awsDeployRel(AWS_R_REL_PF)
                awsDeployRel(AWS_L_REL_PF)
                awsDeployRel(AWS_R_REL_PROTO)
                awsDeployRel(AWS_L_REL_PROTO)
                awsDeployRel(AWS_R_REL_PROTO_PF)
                awsDeployRel(AWS_L_REL_PROTO_PF)
            }
        }
		
        stage("AWS Upload - step 5") {
		    agent {
                label 'slave_build_win'
            }
            // Run when s5-vx.x.x tag is made

            when { tag "s5-*" }
            steps {
                awsDeployRel(AWS_R_REL_S5)
                awsDeployRel(AWS_L_REL_S5)
                awsDeployRel(AWS_R_REL_S5_PF)
                awsDeployRel(AWS_L_REL_S5_PF)				
            }
        }
		
    } // end stages
    post{
        cleanup{
            node ('slave_build_win'){
                echo 'Cleaning workspace after job is done!'
                cleanWs()
            }
        } // end post condition
    }
}

/* ========================================================================== *
 * END OF FILE                                                                *
 * ========================================================================== */