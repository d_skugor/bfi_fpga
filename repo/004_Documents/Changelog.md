# Big Flexible Inverter FPGA (BFI_FPGA) changelog

## Development

### Bugfix

-   C2BFIESW-2200: Spike in Speed Estimator

### Updated

-   C2BFIESW-934: Substitute all counters in gray code counters
-   C2BFIESW-1427: HV ADC Interface clock signals update according to ALTAM's review
-   C2BFIESW-1989: PWM feedback monitor FSM goes to error state - fix
-   C2BFIESW-2049: Refactor diagnostics module
-   C2BFIESW-2159: Speed Estimator counter substitute

### Added

-   C2BFIESW-1614: Connect PCI error signals to diagnostics module
-   C2BFIESW-2051: Version info prebuild script latest tag and commit hash
-   C2BFIESW-2107: Add commit hash information to diagnostics module
-   C2BFIESW-2260: PWM signals pull-down resistors

## [v4.0.0] - 2021-07-22

### Updated

-   C2BFIESW-2096: Bitstream version update to v4.0.0
-   C2BFIESW-1973: Heartbeat module update
-   C2BFIESW-1883: Refactor gray code counter
-   C2BFIESW-1843, C2BFIESW-1872: Synchronous Filter and Resolver Update
-   C2BFIESW-1837: Fix diagnostics bug when GDRV faults don't initialize completely
-   C2BFIESW-1880: Clean up of FPGA code

### Added

-   C2BFIESW-1516: Added jenkins support for building configuration files

## [v3.4.0] - 2021-05-31

## [v3.3.0] - 2021-04-22

### Updated

-   C2BFIESW-1827: Speed estimation and resolver driver
-   C2BFIESW-1826: FPGA bitstream version update to v3.3.0-rc1
-   C2BFIESW-1783: Fix PWM and GRDV fault monitors
-   C2BFIESW-1733: Changed number of filter samples from 7 to 15
-   C2BFIESW-1733: FPGA metadata support
-   C2BFIESW-1617: Timeout counter decreased from 5s to 0.5s
-   C2BFIESW-1883: Refactor gray code counter

## [v3.2.0-rc1] - 2021-02-19

### Added

-   Phase Current synchronus sampling with averaging
-   Variable switching frequency HV filtering

### Updated

-   ADC Driver
-   PCI Interface Buffer
-   Jenkins script