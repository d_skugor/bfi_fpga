##############################################################################################
##                                   Rimac Automobili d.o.o.
##                             Ljubljanska 7, 10431 Sveta Nedelja
##
##            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
##                                     All rights reserved
##############################################################################################
## file             control_board.xdc
## author           Danijela Skugor Markovic (skugor.danijela@rimac-automobili.com)
## project name     BFI
## target device    XA7A100T-1CSG324Q
## brief            Control board C sample constraint file
##############################################################################################
## create date      16.10.2019
## Revision:
##      Revision 0.01 - File Created
##               0.02 - Added constraints to the current measurement ADC ports
##               0.03 - Added ADC PWM Synchronization port, of mode IN
##               0.04 - Pins R6 and U9 ware swapped
##############################################################################################

## System clock signal
set_property -dict { PACKAGE_PIN P15   IOSTANDARD LVCMOS33} [get_ports { p_top_clk_in }];   #FPGA_OSC_100MHZ -> L13P_14_P15

## Reset signal
set_property -dict { PACKAGE_PIN U2   IOSTANDARD LVCMOS33 } [get_ports { p_top_reset_n_in }];   #RST# -> L9P_34_U2

## LEDs
set_property -dict { PACKAGE_PIN V12   IOSTANDARD LVCMOS33 } [get_ports { p_top_led1_out }];    #LED4 <- L20N_14_V12
set_property -dict { PACKAGE_PIN U12   IOSTANDARD LVCMOS33 } [get_ports { p_top_led2_out }];    #LED5 <- L20P_14_U12
set_property -dict { PACKAGE_PIN V14   IOSTANDARD LVCMOS33 } [get_ports { p_top_led3_out }];    #LED6 <- L22N_14_V14

## Buttons
set_property -dict { PACKAGE_PIN U14   IOSTANDARD LVCMOS33 } [get_ports { p_top_sw1_in }];     #SW3 <- L22P_14_U14
#set_property -dict { PACKAGE_PIN V15   IOSTANDARD LVCMOS33 } [get_ports { p_top_sw2_in }];     #SW4 <- L16P_14_V1

## SPI
set_property -dict { PACKAGE_PIN D14   IOSTANDARD LVCMOS33 }                   [get_ports { p_top_spi_miso_out }]; #D01-MISO   <- L1P_15_D14
set_property -dict { PACKAGE_PIN C14   IOSTANDARD LVCMOS33 }                   [get_ports { p_top_spi_mosi_in }];  #D00-MOSI   <- L1N_15_C14
set_property -dict { PACKAGE_PIN B13   IOSTANDARD LVCMOS33 PULLTYPE PULLDOWN } [get_ports { p_top_spi_sck_in  }];  #CLK        <- L2P_15_B13
set_property -dict { PACKAGE_PIN B14   IOSTANDARD LVCMOS33 }                   [get_ports { p_top_spi_ss_n_in }];  #CS1#       <- L2N_15_B14
#set_property -dict { PACKAGE_PIN L16   IOSTANDARD LVCMOS33 } [get_ports {  }];    #FPGA_SPI_OSC_100MHZ# <- L3N_14_L16

# PCI signals - Bank 35
set_property -dict { PACKAGE_PIN D5  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[0] }];       #MCU_PCI_D15 <- L11P_35_D5
set_property -dict { PACKAGE_PIN C1  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[1] }];       #MCU_PCI_D14 <- L16N_35_C1
set_property -dict { PACKAGE_PIN D4  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[2] }];       #MCU_PCI_D13 <- L11N_35_D4
set_property -dict { PACKAGE_PIN B1  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[3] }];       #MCU_PCI_D12 <- L9P_35_B1
set_property -dict { PACKAGE_PIN A4  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[4] }];       #MCU_PCI_D11 <- L8P_35_A4
set_property -dict { PACKAGE_PIN A3  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[5] }];       #MCU_PCI_D10 <- L8N_35_A3
set_property -dict { PACKAGE_PIN B3  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[6] }];       #MCU_PCI_D9  <- L10P_35_B3
set_property -dict { PACKAGE_PIN A1  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[7] }];       #MCU_PCI_D8  <- L9N_35_A1
set_property -dict { PACKAGE_PIN B2  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[8] }];       #MCU_PCI_D7  <- L10N_35_B2
set_property -dict { PACKAGE_PIN D3  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[9] }];       #MCU_PCI_D6  <- L12N_35_D3
set_property -dict { PACKAGE_PIN E1  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[10] }];      #MCU_PCI_D5  <- L18N_35_E1
set_property -dict { PACKAGE_PIN C4  IOSTANDARD LVCMOS33 } [get_ports { p_top_data_out[11] }];      #MCU_PCI_D4  <- L7P_35_C4
set_property -dict { PACKAGE_PIN F3  IOSTANDARD LVCMOS33 } [get_ports { p_top_address_out[0] }];    #MCU_PCI_D3  <- L13N_35_F3
set_property -dict { PACKAGE_PIN F4  IOSTANDARD LVCMOS33 } [get_ports { p_top_address_out[1] }];    #MCU_PCI_D2  <- L13P_35_F4
set_property -dict { PACKAGE_PIN B4  IOSTANDARD LVCMOS33 } [get_ports { p_top_address_out[2] }];    #MCU_PCI_D1  <- L7N_35_B4
set_property -dict { PACKAGE_PIN C2  IOSTANDARD LVCMOS33 } [get_ports { p_top_address_out[3] }];    #MCU_PCI_D0  <- L16P_35_C2

set_property -dict { PACKAGE_PIN H2   IOSTANDARD LVCMOS33 } [get_ports { p_top_clk_out }];  #MCU_PCI_CLKO <- L15P_35_H2
set_property -dict { PACKAGE_PIN E2   IOSTANDARD LVCMOS33 } [get_ports { p_top_req_in }];   #MCU_PCI_CLK  -> L14P_35_E2

## Intra-Inverter Fault Detection (pulldown on any input signal)
set_property -dict { PACKAGE_PIN J4   IOSTANDARD LVCMOS33 PULLTYPE PULLDOWN } [get_ports { p_top_iInvErrDet_inv_pwmi_in   }];    #INV_PWMI  -> L21P_35_J4
set_property -dict { PACKAGE_PIN H4   IOSTANDARD LVCMOS33 }                   [get_ports { p_top_iInvErrDet_inv_pwmo_out  }];    #INV_PWMO  -> L21N_35_H4
set_property -dict { PACKAGE_PIN J3   IOSTANDARD LVCMOS33 PULLTYPE PULLDOWN } [get_ports { p_top_iInvErrDet_inv_synci_in  }];    #INV_SYNCI -> L22P_35_J3
set_property -dict { PACKAGE_PIN J2   IOSTANDARD LVCMOS33 }                   [get_ports { p_top_iInvErrDet_inv_synco_out }];    #INV_SYNCO -> L22N_35_J2
set_property -dict { PACKAGE_PIN F1   IOSTANDARD LVCMOS33 PULLTYPE PULLDOWN } [get_ports { p_top_iInvErrDet_mcu_pwmi_in   }];    #MCU_DIO9  <- L18P_35_F1
set_property -dict { PACKAGE_PIN E3   IOSTANDARD LVCMOS33 }                   [get_ports { p_top_iInvErrDet_mcu_pwmo_out  }];    #MCU_DIO10 <- L12P_35_E3
set_property -dict { PACKAGE_PIN G1   IOSTANDARD LVCMOS33 PULLTYPE PULLDOWN } [get_ports { p_top_iInvErrDet_mcu_synci_in  }];    #MCU_DIO11 <- L17N_35_G1
set_property -dict { PACKAGE_PIN G2   IOSTANDARD LVCMOS33 }                   [get_ports { p_top_iInvErrDet_mcu_synco_out }];    #MCU_DIO12 <- L15N_35_G2

## Intra-Inverter PWM Synchronization
#set_property -dict { PACKAGE_PIN U1   IOSTANDARD LVCMOS33 }                    [get_ports { p_top_iInvPWMSync_flt_out }];         #MCU_PWM_SYNC <- L7P_34_U1
set_property -dict {PACKAGE_PIN P5 IOSTANDARD LVCMOS33}      [get_ports p_top_iInvPWMSync_flt_in];                                #MCU_PWM_SYNC <- L13N_34_P5

## PWM Monitor input signals
set_property -dict { PACKAGE_PIN M1    IOSTANDARD LVCMOS33  PULLTYPE PULLDOWN } [get_ports { p_top_aHs_in }];    #MCU_PWM -> L1N_34_M1
set_property -dict { PACKAGE_PIN R5    IOSTANDARD LVCMOS33  PULLTYPE PULLDOWN } [get_ports { p_top_aLs_in }];    #MCU_PWM -> L19N_34_R5
set_property -dict { PACKAGE_PIN L3    IOSTANDARD LVCMOS33  PULLTYPE PULLDOWN } [get_ports { p_top_bHs_in }];    #MCU_PWM -> L2N_34_L3
set_property -dict { PACKAGE_PIN T1    IOSTANDARD LVCMOS33  PULLTYPE PULLDOWN } [get_ports { p_top_bLs_in }];    #MCU_PWM -> L17N_34_T1
set_property -dict { PACKAGE_PIN P4    IOSTANDARD LVCMOS33  PULLTYPE PULLDOWN } [get_ports { p_top_cHs_in }];    #MCU_PWM -> L14P_34_P4
set_property -dict { PACKAGE_PIN M6    IOSTANDARD LVCMOS33  PULLTYPE PULLDOWN } [get_ports { p_top_cLs_in }];    #MCU_PWM -> L18P_34_M6

set_property -dict { PACKAGE_PIN B18   IOSTANDARD LVCMOS33 } [get_ports { p_top_aHs_fb_in }]; #PWM_FB [HS_A] -> L10P_15_B18
set_property -dict { PACKAGE_PIN A18   IOSTANDARD LVCMOS33 } [get_ports { p_top_aLs_fb_in }]; #PWM_FB [LS_A] -> L10N_15_A18
set_property -dict { PACKAGE_PIN A14   IOSTANDARD LVCMOS33 } [get_ports { p_top_bHs_fb_in }]; #PWM_FB [HS_B] -> L9N_15_A14
set_property -dict { PACKAGE_PIN A13   IOSTANDARD LVCMOS33 } [get_ports { p_top_bLs_fb_in }]; #PWM_FB [LS_B] -> L9P_15_A13
set_property -dict { PACKAGE_PIN A16   IOSTANDARD LVCMOS33 } [get_ports { p_top_cHs_fb_in }]; #PWM_FB [HS_C] -> L8N_15_A16
set_property -dict { PACKAGE_PIN A15   IOSTANDARD LVCMOS33 } [get_ports { p_top_cLs_fb_in }]; #PWM_FB [LS_C] -> L8P_15_A15

## PWM Monitor output signals - Bank 15
set_property -dict { PACKAGE_PIN E17   IOSTANDARD LVCMOS33 } [get_ports { p_top_aHs_out }];   #GDRV_PWM <- L16P_15_E17
set_property -dict { PACKAGE_PIN D17   IOSTANDARD LVCMOS33 } [get_ports { p_top_aLs_out }];   #GDRV_PWM <- L16N_15_D17
set_property -dict { PACKAGE_PIN C15   IOSTANDARD LVCMOS33 } [get_ports { p_top_bHs_out }];   #GDRV_PWM <- L12N_15_C15
set_property -dict { PACKAGE_PIN D15   IOSTANDARD LVCMOS33 } [get_ports { p_top_bLs_out }];   #GDRV_PWM <- L12P_15_D15
set_property -dict { PACKAGE_PIN C16   IOSTANDARD LVCMOS33 } [get_ports { p_top_cHs_out }];   #GDRV_PWM <- L20P_15_C16
set_property -dict { PACKAGE_PIN C17   IOSTANDARD LVCMOS33 } [get_ports { p_top_cLs_out }];   #GDRV_PWM <- L20N_15_C17

## Gate Drivers Monitor input ports
set_property -dict { PACKAGE_PIN E7    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_hs_in      }];   #FLT# [HS]   -> L6P_35_E7
set_property -dict { PACKAGE_PIN D7    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_ls_in      }];   #FLT# [LS]   -> L6N_35_D7
set_property -dict { PACKAGE_PIN A6    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_dcdc_in    }];   #FLT# [DCDC] -> L3P_35_A6
set_property -dict { PACKAGE_PIN A5    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_vin_in     }];   #FLT# [Vin]  -> L3N_35_A5
set_property -dict { PACKAGE_PIN D8    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_5v_in      }];   #FLT# [+5V]  -> L4P_35_D8

## Gate Drivers Monitor output ports
set_property -dict { PACKAGE_PIN M2    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_psup_n_out }];   #MCU_DIO6      -> L4N_34_M2
set_property -dict { PACKAGE_PIN R1    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_vin_n_out  }];   #MCU_DIO1      <- L17P_34_R1
set_property -dict { PACKAGE_PIN P2    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_dcdc_n_out }];   #MCU_DIO2      <- L15P_34_P2
set_property -dict { PACKAGE_PIN N2    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_5v_n_out   }];   #MCU_DIO3      <- L3P_34_N2
set_property -dict { PACKAGE_PIN K3    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_hs_n_out   }];   #MCU_DIO7      -> L2P_34_K3
set_property -dict { PACKAGE_PIN N1    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_ls_n_out   }];   #MCU_DIO8      -> L3N_34_N1
set_property -dict { PACKAGE_PIN V1    IOSTANDARD LVCMOS33 } [get_ports { p_top_gdrv_flt_gdrv_n_out }];   #MCU_GDRV_FLT# <- L7N_34_V1

## Diagnostic signals
set_property -dict { PACKAGE_PIN L1    IOSTANDARD LVCMOS33 } [get_ports { p_top_diag_flt_gen_n_out  }];   #MCU_DIO5  <- L1P_34_L1
set_property -dict { PACKAGE_PIN C7    IOSTANDARD LVCMOS33 } [get_ports { p_top_diag_flt_fpga_n_out }];   #FPGA_FLT# -> L4N_35_C7

## MCU DIO ports (unused)
#set_property -dict { PACKAGE_PIN M4    IOSTANDARD LVCMOS33 } [get_ports { }];   #MCU_DIO4 <- L16P_34_M4

## JTAG
#set_property -dict { PACKAGE_PIN J18   IOSTANDARD LVCMOS33 } [get_ports { }]; #LVDS [TDO]   -> L23N_15_J18
#set_property -dict { PACKAGE_PIN J17   IOSTANDARD LVCMOS33 } [get_ports { }]; #LVDS [TDI]   -> L23P_15_J17
#set_property -dict { PACKAGE_PIN H17   IOSTANDARD LVCMOS33 } [get_ports { }]; #LVDS [TCK]   -> L18P_15_H17
#set_property -dict { PACKAGE_PIN G18   IOSTANDARD LVCMOS33 } [get_ports { }]; #LVDS [TMS]   -> L22P_15_G18

## Resolver input and output signals
set_property -dict { PACKAGE_PIN U3    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[0] }];  #RES_PCI -> L8N_34_U3
set_property -dict { PACKAGE_PIN V2    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[1] }];  #RES_PCI -> L9N_34_V2
set_property -dict { PACKAGE_PIN U4    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[2] }];  #RES_PCI -> L8P_34_U4
set_property -dict { PACKAGE_PIN V4    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[3] }];  #RES_PCI -> L10N_34_V4
set_property -dict { PACKAGE_PIN V5    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[4] }];  #RES_PCI -> L10P_34_V5
set_property -dict { PACKAGE_PIN T6    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[5] }];  #RES_PCI -> L23N_34_T6
set_property -dict { PACKAGE_PIN T5    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[6] }];  #RES_PCI -> L12P_34_T5
set_property -dict { PACKAGE_PIN T4    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[7] }];  #RES_PCI -> L12N_34_T4
set_property -dict { PACKAGE_PIN T3    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[8] }];  #RES_PCI -> L11N_34_T3
set_property -dict { PACKAGE_PIN R3    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[9] }];  #RES_PCI -> L11P_34_R3
set_property -dict { PACKAGE_PIN R2    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[10] }]; #RES_PCI -> L15N_34_R2
set_property -dict { PACKAGE_PIN U7    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[11] }]; #RES_PCI -> L22P_34_U7
set_property -dict { PACKAGE_PIN V6    IOSTANDARD LVCMOS33 } [get_ports { p_top_ord_in[12] }]; #RES_PCI -> L20N_34_V6
set_property -dict { PACKAGE_PIN U6    IOSTANDARD LVCMOS33 } [get_ports { p_top_ordclk_in }];  #RES_PCI -> L22N_34_U6

set_property -dict { PACKAGE_PIN V7    IOSTANDARD LVCMOS33 } [get_ports { p_top_inhb_out }];   #RES_PCI <- L20P_34_V7
set_property -dict { PACKAGE_PIN U9    IOSTANDARD LVCMOS33 } [get_ports { p_top_va0_out  }];   #RES_PCI <- L21P_34_U9
set_property -dict { PACKAGE_PIN R6    IOSTANDARD LVCMOS33 } [get_ports { p_top_va1_out  }];   #RES_PCI <- L19P_34_R6

## HV ADC Interface (MISO-only SPI)
set_property -dict { PACKAGE_PIN B7     IOSTANDARD LVCMOS33 } [get_ports { p_top_hvADC_miso_in }];       #VDC_SPI -> L2P_35_B7
set_property -dict { PACKAGE_PIN C6     IOSTANDARD LVCMOS33 } [get_ports { p_top_hvADC_sck_out }];       #VDC_SPI <- L1P_35_C6
set_property -dict { PACKAGE_PIN B6     IOSTANDARD LVCMOS33 } [get_ports { p_top_hvADC_meas_cs_n_out }]; #VDC_SPI <- L2N_35_B6

## Currents ADC Interface
set_property -dict { PACKAGE_PIN P14    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[0] }];  #I_PCI_D0  -> L8N_14_P14
set_property -dict { PACKAGE_PIN M16    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[1] }];  #I_PCI_D1  -> L10P_14_M16
set_property -dict { PACKAGE_PIN N16    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[2] }];  #I_PCI_D2  -> L11N_14_N16
set_property -dict { PACKAGE_PIN R16    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[3] }];  #I_PCI_D3  -> L15P_14_R16
set_property -dict { PACKAGE_PIN L18    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[4] }];  #I_PCI_D4  -> L4P_14_L18
set_property -dict { PACKAGE_PIN M18    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[5] }];  #I_PCI_D5  -> L4N_14_M18
set_property -dict { PACKAGE_PIN M17    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[6] }];  #I_PCI_D6  -> L10N_14_M17
set_property -dict { PACKAGE_PIN N17    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[7] }];  #I_PCI_D7  -> L9P_14_N17
set_property -dict { PACKAGE_PIN P18    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[8] }];  #I_PCI_D8  -> L9N_14_P18
set_property -dict { PACKAGE_PIN P17    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[9] }];  #I_PCI_D9  -> L12P_14_P17
set_property -dict { PACKAGE_PIN R18    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[10] }]; #I_PCI_D10 -> L7P_14_R18
set_property -dict { PACKAGE_PIN R17    IOSTANDARD LVCMOS33 } [get_ports { p_top_currADC_data_bi[11] }]; #I_PCI_D11 -> L12N_14_R17

set_property -dict { PACKAGE_PIN T18    IOSTANDARD LVCMOS33 IOB FALSE} [get_ports { p_top_currADC_dataAv_in }];  #I_PCI_DAV  -> L7N_14_T18
set_property -dict { PACKAGE_PIN U18    IOSTANDARD LVCMOS33 IOB TRUE}  [get_ports { p_top_currADC_conv_n_out }]; #I_PCI_CLK  -> L17N_14_U18
set_property -dict { PACKAGE_PIN U17    IOSTANDARD LVCMOS33 IOB TRUE}  [get_ports { p_top_currADC_rd_n_out }];   #I_PCI_RD#  -> L17P_14_U17
set_property -dict { PACKAGE_PIN T16    IOSTANDARD LVCMOS33 IOB TRUE}  [get_ports { p_top_currADC_wr_n_out }];   #I_PCI_WR#  -> L15N_14_T16
set_property -dict { PACKAGE_PIN U16    IOSTANDARD LVCMOS33 IOB TRUE}  [get_ports { p_top_currADC_cs0_n_out }];  #I_PCI_CS0# -> L18P_14_U16
set_property -dict { PACKAGE_PIN V17    IOSTANDARD LVCMOS33 IOB TRUE}  [get_ports { p_top_currADC_cs1_out }];    #I_PCI_CS1  -> L18N_14_V17

## Configuration options
set_property BITSTREAM.GENERAL.COMPRESS       True  [current_design];    # Try to reduce the size of the bitstream
set_property BITSTREAM.CONFIG.CONFIGRATE      3     [current_design];    # Output on CCLK will be ~3 MHz when booting from FLASH; default value
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH    4     [current_design];    # Quad SPI will be used when booting from FLASH
set_property BITSTREAM.SEU.ESSENTIALBITS      Yes   [current_design];    # Generates essential bits report in the Implementation log (runme.log)

set_property CONFIG_MODE                      SPIx4 [current_design];
set_property CONFIG_VOLTAGE                   3.3   [current_design];
set_property CFGBVS                           VCCO  [current_design];