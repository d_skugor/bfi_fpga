##############################################################################################
##                                   Rimac Automobili d.o.o.
##                             Ljubljanska 7, 10431 Sveta Nedelja
##
##            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
##                                     All rights reserved
##############################################################################################
## file             control_board_timings.xdc
## author           Danijela Skugor Markovic (skugor.danijela@rimac-automobili.com)
## project name     BFI
## tarjet device    XA7A100T-1CSG324Q
## brief            Control board C sample constraint file for design clocks
##############################################################################################
## create date      08.11.2019
## Revision:
##      Revision 0.01 - File Created
##               0.02 - Added timing constraints
##               0.03 - Added timing constraints for the current measurement ADC
##               0.04 - Updated timing constraints for the Resolver clock
##############################################################################################

## System clock
create_clock -period 10.000 -name sys_clk_pin -waveform {0.000 5.000} -add [get_ports p_top_clk_in]

#Sample clock
create_generated_clock -name sampleClk -source [get_ports p_top_clk_in] -divide_by 100 [get_pins mdl_sampleClk/p_sampleClk_out_reg/Q]

#Resolver clock
#create_generated_clock -name resolverClk -source [get_ports p_top_clk_in] -divide_by 1250 [get_pins mdl_res_driver/p_res_driver_inhb_out]
create_clock -period 12500.000 -name resolverClk -waveform {110.000 12390.000} -add [get_ports p_top_inhb_out]

#Reset logic: false path and metastability resolution constraint on clock delay
set_false_path -from [get_ports p_top_reset_n_in]