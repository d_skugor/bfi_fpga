----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_adc_driver_watchdog.vhd
-- module name      Current ADC driver watchdog testbench
-- author           David Palvovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Current ADC driver watchdog testbench environment
----------------------------------------------------------------------------------------------
-- create date      04.06.2020.
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Updated watchdog component to be compatible with new watchdog
--                      fault signal name
----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity test_adc_driver_watchdog is
end entity;

architecture test of test_adc_driver_watchdog is

    component adc_driver_watchdog is
        generic (
            G_ADC_WD_MAX_ERR : natural := 3
        );
        port (
            -- main signals
            p_adc_wd_clk_in        : in  std_logic;
            p_adc_wd_rst_n_in      : in  std_logic;
            -- interface to ADC
            p_adc_wd_error_in      : in  std_logic;
            p_adc_wd_error_clr_out : out std_logic;
            -- fault signal
            p_adc_wd_flt_n_out     : out std_logic
        );
    end component;

    signal s_clk       : std_logic := '0';
    signal s_rst_n     : std_logic;
    signal s_error     : std_logic := '0';
    signal s_error_clr : std_logic;
    signal s_error_flt : std_logic;

    constant C_CLK_PER     : time := 10 ns;
    constant C_ADC_MAX_ERR : natural := 3;

begin

    dut : adc_driver_watchdog
        generic map (
            G_ADC_WD_MAX_ERR => C_ADC_MAX_ERR
        )
        port map (
            p_adc_wd_clk_in        => s_clk,
            p_adc_wd_rst_n_in      => s_rst_n,
            p_adc_wd_error_in      => s_error,
            p_adc_wd_error_clr_out => s_error_clr,
            p_adc_wd_flt_n_out     => s_error_flt
        );

    -- clock generator
    process is
    begin
        wait for C_CLK_PER/2;
        s_clk <= not s_clk;
    end process;

    -- reset
    s_rst_n <= '0', '1' after 2*C_CLK_PER;

    -- error signal process
    process is
    begin
        wait for 1 us;
        wait until rising_edge(s_clk);
        s_error <= '1';
        wait until falling_edge(s_error_clr);
        s_error <= '0';
    end process;

end architecture test;