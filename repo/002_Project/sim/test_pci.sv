//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             test_pci.vhd
// module name      PCI Test
// author           Arturo Montufar Arreola (arturo.arreola@rimac-automobili.com)
// project name     BFI
// tarjet device    XA7A100T-1CSG324Q
// brief            Parallel Communication Interface Block Simulation
//--------------------------------------------------------------------------------------------
// create date      08.07.2019
// Revision:
//      Revision 0.01 - File Created
//
//      Revision 0.02 by david.pavlovic
//                    - Removed DC current measurement signal and added
//                      signals for 24 bit speed calculation
//                    - Updated the stimulus generation for updated design
//
//      Revision 0.03 by david.pavlovic
//                    - Rewritten in SystemVerilog to use SV randomization capabilities
//
//      Revision 0.04 by danijela.skugor
//                    - Updated the stimulus generation due to refactored pci trasmission
//                       in design
//
//      Revision 0.05 by david.pavlovic
//                    - Added test to force PCI FSM into error state
//--------------------------------------------------------------------------------------------

import bfi_tb_pkg::crc;

module test_pci();

    timeunit 1ns/1ps;

    // parameters
    parameter            C_CLK_PER         = 10.0;
    const int            C_10_KHZ_PER      = 10000;
    const int            C_16_KHZ_PER      = 6250;
    const int            C_20_KHZ_PER      = 5000;
    const int            C_80_KHZ_PER      = 1250;
    const int            C_MCU_REQ_ON      = 1500;
    const int            C_REPS            = 16;
    const int            C_ADD_DELAY       = 1;
    const int            C_ADD_DELAY_TO    = 0; // delay timeout
    const int            C_INJECT_FLT      = 0; // inject fault to force FSM into error state
    parameter int        DW                = 120;
    parameter int        PW                = 8;
    parameter bit [PW:0] CRC_POLY          = 'b100011101; // CRC8 VDA CAN (x^8 + x^4 + x^3 + x^2 + 1)

    // types
    typedef enum {f_10_kHz, f_16_kHz, f_20_kHz} t_sw_freq;

    // signals
    logic        s_rst_n                = '0; // main reset / active low
    logic        s_clk                  = '0; // module clock
    logic        s_sampleClk            = '0; // system sample clock
    // MCU interface signals
    logic        s_dataReq_in           = '0; // request input from MCU
    logic [ 3:0] s_address_out          = '0; // address output to MCU
    logic [11:0] s_data_out             = '0; // data output to MCU
    logic        s_clk_out              = '0; // clock output to MCU
    // DAQ blocks interface signals
    logic [11:0] s_phCurrA_in           = '0;
    logic [11:0] s_phCurrB_in           = '0;
    logic [11:0] s_phCurrC_in           = '0;
    logic [11:0] s_dcVolt_in            = '0;
    logic [11:0] s_angle_in             = '0;
    logic [11:0] s_angle_in_d           = '0;
    logic [11:0] s_speed_msb_in         = '0;
    logic [11:0] s_speed_lsb_in         = '0;
    //pci top input signals
    logic        s_central_p            = '0;
    logic        s_start_pci_p          = '0;
    logic        s_rdy_p                = '0;
    logic        s_rdy_p_d              = '0;
    // PCI top output signals
    logic        s_pci_flt_n;


    // auxiliary signals
    t_sw_freq    s_sw_freq;
    logic [11:0] s_data_i[8] = '{default: '0};
    logic [11:0] s_data_o[8] = '{default: '0};
    logic [ 3:0] s_addr_o[8] = '{default: '0};
    static logic [ 3:0] s_trans_cnt = '0;

    // DUT instance
    pci_top dut
        (
            .p_pcitop_reset_n_in   (s_rst_n),
            .p_pcitop_clk_in       (s_clk),
            .p_pcitop_sampleClk_in (s_sampleClk),
            // MCU interface signals
            .p_pcitop_dataReq_in   (s_dataReq_in),
            .p_pcitop_address_out  (s_address_out),
            .p_pcitop_data_out     (s_data_out),
            .p_pcitop_clk_out      (s_clk_out),
            // DAQ blocks interface signals
            .p_pcitop_angle_ready_p_in (s_central_p),
            .p_pcitop_start_trans_p_in (s_start_pci_p),
            .p_pcitop_currents_ready_p_in (s_rdy_p_d),
            .p_pcitop_flt_n_out    (s_pci_flt_n),
            .p_pcitop_phCurrA_in   (s_phCurrA_in),
            .p_pcitop_phCurrB_in   (s_phCurrB_in),
            .p_pcitop_phCurrC_in   (s_phCurrC_in),
            .p_pcitop_dcVolt_in    (s_dcVolt_in),
            .p_pcitop_angle_in     (s_angle_in_d),
            .p_pcitop_speed_msb_in (s_speed_msb_in),
            .p_pcitop_speed_lsb_in (s_speed_lsb_in)
        );

    //-------------------------------------------------------------------------

    // clock generator
    always begin
        #(C_CLK_PER/2) s_clk = ~s_clk;
    end

    // generate signal delays
    always @(s_rdy_p) s_rdy_p_d <= #60ns s_rdy_p;
    always @(s_angle_in) s_angle_in_d <= #60ns s_angle_in;

    //-----------------------------------
    // Tasks
    //-----------------------------------

    // generates N cycle active-low reset signal synchronous with clock
    task reset_gen(input int N);
        s_rst_n <= 1'b0;
        repeat (N+1) @(posedge s_clk);
        s_rst_n <= 1'b1;
    endtask : reset_gen;

    // waits for N clock cycles
    task wait_clk(input int N);
        repeat (N)
            @(posedge s_clk);
    endtask : wait_clk

    task gen_sample_clk();
        forever begin
            wait_clk(50);
            s_sampleClk <= ~s_sampleClk;
        end
    endtask : gen_sample_clk

    // generates  DC voltage signals @ 1MHz
    task gen_dc_voltage();
        forever begin
            @( posedge s_sampleClk);
            if(!std::randomize(s_dcVolt_in))
                $fatal(1, "RAND ERROR (s_dcVolt_in)");
        end
    endtask : gen_dc_voltage

    // generate speed signals  @ 80 KHz
    task gen_speed();
        forever begin
            @( posedge s_sampleClk);
             if(!std::randomize(s_speed_msb_in))
                $fatal(1, "RAND ERROR (s_speed_msb_in)");
             if(!std::randomize(s_speed_lsb_in))
                $fatal(1, "RAND ERROR (s_speed_lsb_in)");
             wait_clk(C_80_KHZ_PER - C_CLK_PER);   // generated at 80 kHz
            end
    endtask : gen_speed

    function logic [11:0] format_and_calc_crc();
        logic [119:0] crc_data;
        logic [ 11:0] crc_value;
        crc_data = {4'h1, s_data_i[0], 4'h2, s_data_i[1], 4'h3, s_data_i[2], 4'h4, s_data_i[3],
                    4'h5, s_data_i[4], 4'h6, s_data_i[5], 4'h7, s_data_i[6], 4'h8, s_trans_cnt };
        crc_value = {s_trans_cnt, crc#(DW, PW, CRC_POLY)::calc_crc(crc_data)};
        return crc_value;
    endfunction : format_and_calc_crc;

    // waits for MCU request, then delays the generatino of phase currents
    // and angle up to 1us, after delay generates data ready signal
    task gen_currents_and_angle_and_data_ready();
        int delay;

        forever begin
            @( posedge s_sampleClk);
            if (!std::randomize(s_phCurrA_in))
                $fatal(1, "RAND ERROR (s_phCurrA_in)");
            if (!std::randomize(s_phCurrB_in))
                $fatal(1, "RAND ERROR (s_phCurrB_in)");
            if (!std::randomize(s_phCurrC_in))
                $fatal(1, "RAND ERROR (s_phCurrC_in)");
            if (!std::randomize(s_angle_in))
                $fatal(1, "RAND ERROR (s_angle_in)");
            if (C_ADD_DELAY == 1) begin
                if (C_ADD_DELAY_TO == 1) begin
                    if (!std::randomize(delay) with { delay dist {10:/90, 300:/10};})
                        $fatal(1, "RAND ERROR (delay)");
                end else begin
                    if (!std::randomize(delay) with { delay inside {[1:10]};})
                        $fatal(1, "RAND ERROR (delay)");
                end
                wait_clk(delay);
            end

          end
    endtask : gen_currents_and_angle_and_data_ready

    // sample the input data and store it in queue
    task sample_input();
         forever begin
                @(posedge s_dataReq_in);
                @(posedge s_clk);
                s_data_i[0] <=  s_dcVolt_in;
                s_data_i[1] <=  s_speed_msb_in;
                s_data_i[2] <=  s_speed_lsb_in;
                @(posedge s_central_p);
                s_data_i[3] <= s_angle_in_d;
                @(posedge s_rdy_p);
                s_data_i[4] <= s_phCurrA_in;
                s_data_i[5] <= s_phCurrB_in;
                s_data_i[6] <= s_phCurrC_in;
            @(posedge s_clk);
                s_data_i[7] <= format_and_calc_crc();
                s_trans_cnt++;
         end

    endtask: sample_input

    task gen_mcu_req();
        int delay;
        int t_off;
        if (!std::randomize(delay) with { delay inside {[1:99]}; })
            $fatal(1, "RAND ERROR (delay)");
        wait_clk(delay);
        if (!std::randomize(s_sw_freq))
            $fatal(1, "Randomization of s_sw_freq failed");
        s_dataReq_in <= ~s_dataReq_in;
        if (C_INJECT_FLT) begin
            wait_clk(500);
            s_dataReq_in <= ~s_dataReq_in;
            #1us;
            $finish;
        end else begin
            wait_clk(C_MCU_REQ_ON);
        end
        case (s_sw_freq)
            f_10_kHz : t_off = C_10_KHZ_PER - C_MCU_REQ_ON;
            f_16_kHz : t_off = C_16_KHZ_PER - C_MCU_REQ_ON;
            f_20_kHz : t_off = C_20_KHZ_PER - C_MCU_REQ_ON;
        endcase
        s_dataReq_in <= ~s_dataReq_in;
        wait_clk(t_off);
    endtask : gen_mcu_req

    // Simulate movAvg filter logic
    task sim_mov_avg_filter_buff();
          //generate angle ready, start transaction and current ready signals
         forever begin
            @(posedge s_clk);
            s_rdy_p = 0;
            s_central_p = 0;
            s_start_pci_p = 0;
            @(posedge s_dataReq_in);
            @(posedge s_sampleClk);
               wait_clk(102);
               s_central_p = 1;
               wait_clk(1);
               s_central_p = 0;
               wait_clk(199);
               s_start_pci_p = 1;
               wait_clk(1);
               s_start_pci_p = 0;
               wait_clk(399);
               s_rdy_p = 1;
        end
    endtask : sim_mov_avg_filter_buff

    function void check_data_and_addr();
        if (s_data_i != s_data_o)
            $fatal(1, "Input and output data are not equal");
        foreach (s_addr_o[i]) begin
            if (s_addr_o[i] != i+1)
                $fatal(1, "Output addresses are not correct");
        end
    endfunction : check_data_and_addr

    task pci_receiver();
        forever begin
            @(posedge s_dataReq_in);
                foreach(s_data_o[i]) begin
                    @(posedge s_clk_out);
                    s_data_o[i] <= s_data_out;
                    s_addr_o[i] <= s_address_out;
                end
            @(negedge s_dataReq_in);
            check_data_and_addr();
        end
    endtask : pci_receiver

    // main TB block
    initial begin
        reset_gen(3);
        wait_clk(5);
        fork
            gen_sample_clk();
            gen_dc_voltage();
            gen_speed();
            gen_currents_and_angle_and_data_ready();
            sim_mov_avg_filter_buff();
            sample_input();
            pci_receiver();
        join_none
        repeat (C_REPS) begin
            gen_mcu_req();
        end
        #10us;
        $finish;
    end

    // final block
    final begin
        $display("*** End of simulation! ***");
        $display("*** Simulation was successful! ***");
    end

endmodule