----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             bfi_simulation.vhd
-- module name      BFI Simulation
-- author           Arturo Montufar Arreola (arturo.arreola@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100TCSG324-1Q
-- brief            Top Level Module
----------------------------------------------------------------------------------------------
-- create date      03.07.2019
-- Revision:
--      Revision 0.01 by arturo.arreola
--                    - File Created
--
--      Revision 0.02 by arturo.arreola
--                    - New PCI setup
--                    - updated after changing variable names
--
--      Revision 0.03 by arturo.arreola
--                    - Added resolver interface simulation
--
--      Revision 0.04 by david.pavlovic
--                    - Added THS1206 simulation model
--                    - Removed previous THS1206 stimulus
--                    - changed MCU request frequency to 20 kHz
--
--      Revision 0.05 by david.pavlovic
--                    - Tabs changed to spaces
--                    - Fixed BFI top SPI signal names
--
--      Revision 0.06 by david.pavlovic
--                    - Changed timing for MCU request
--                    - Switched VA0 and VA1
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity bfi_simulation is
-- nothing here because it's a simulation
end;

architecture simulation of bfi_simulation is

    component bfi_top
        port (
            -- reset
            p_top_reset_n_in               : in    std_logic;

            -- CLK input
            p_top_clk_in                   : in    std_logic;   -- 100 MHz clock -> 10 ns clock ticks

            -- LED output
            p_top_led1_out                 : out   std_logic;
            p_top_led2_out                 : out   std_logic;
            p_top_led3_out                 : out   std_logic;

            -- Buttons
            p_top_sw1_in                   : in    std_logic;
            --p_top_sw2_in              : in std_logic;

            -- PCI interface
            p_top_req_in                   : in    std_logic;
            p_top_data_out                 : out   std_logic_vector(11 downto 0);
            p_top_address_out              : out   std_logic_vector(3 downto 0);
            p_top_clk_out                  : out   std_logic;

            -- Gate drivers monitor
            p_top_gdrv_flt_dcdc_in         : in    std_logic;
            p_top_gdrv_flt_vin_in          : in    std_logic;
            p_top_gdrv_flt_5v_in           : in    std_logic;
            p_top_gdrv_flt_hs_in           : in    std_logic;
            p_top_gdrv_flt_ls_in           : in    std_logic;
            p_top_gdrv_flt_gdrv_n_out      : out   std_logic;
            p_top_gdrv_flt_psup_n_out      : out   std_logic;
            p_top_gdrv_flt_vin_n_out       : out   std_logic;
            p_top_gdrv_flt_dcdc_n_out      : out   std_logic;
            p_top_gdrv_flt_5v_n_out        : out   std_logic;
            p_top_gdrv_flt_hs_n_out        : out   std_logic;
            p_top_gdrv_flt_ls_n_out        : out   std_logic;

            -- Resolver input and output signals
            p_top_ord_in                   : in    std_logic_vector (12 downto 0);
            p_top_ordclk_in                : in    std_logic;
            p_top_inhb_out                 : out   std_logic;
            p_top_va0_out                  : out   std_logic;
            p_top_va1_out                  : out   std_logic;

            -- PWM Monitor input signals
            p_top_aHs_in                   : in    std_logic;
            p_top_aLs_in                   : in    std_logic;
            p_top_bHs_in                   : in    std_logic;
            p_top_bLs_in                   : in    std_logic;
            p_top_cHs_in                   : in    std_logic;
            p_top_cLs_in                   : in    std_logic;

            --PWM Monitor output signals
            p_top_aHs_out                  : out   std_logic;
            p_top_aLs_out                  : out   std_logic;
            p_top_bHs_out                  : out   std_logic;
            p_top_bLs_out                  : out   std_logic;
            p_top_cHs_out                  : out   std_logic;
            p_top_cLs_out                  : out   std_logic;

            --PWM Monitor feedback input signals
            p_top_aHs_fb_in                : in    std_logic;
            p_top_aLs_fb_in                : in    std_logic;
            p_top_bHs_fb_in                : in    std_logic;
            p_top_bLs_fb_in                : in    std_logic;
            p_top_cHs_fb_in                : in    std_logic;
            p_top_cLs_fb_in                : in    std_logic;

            --Intra-inverter PWM synchronization
            p_top_iInvPWMSync_flt_in	   : in    std_logic;

            -- Diagnostics signals
            p_top_diag_flt_gen_n_out       : out   std_logic;
            p_top_diag_flt_fpga_n_out      : out   std_logic;

            -- SPI signals
            p_top_spi_sck_in               : in    std_logic;
            p_top_spi_ss_n_in              : in    std_logic;
            p_top_spi_mosi_in              : in    std_logic;
            p_top_spi_miso_out             : inout std_logic;

            -- HV ADC Interface(MISO-only SPI)
            p_top_hvADC_miso_in            : in    std_logic;
            p_top_hvADC_sck_out            : out   std_logic;
            p_top_hvADC_meas_cs_n_out      : out   std_logic;

            -- Currents ADC Interface
            p_top_currADC_conv_n_out       : out   std_logic;
            p_top_currADC_cs0_n_out        : out   std_logic;
            p_top_currADC_cs1_out          : out   std_logic;
            p_top_currADC_rd_n_out         : out   std_logic;
            p_top_currADC_wr_n_out         : out   std_logic;
            p_top_currADC_dataAv_in        : in    std_logic;
            p_top_currADC_data_bi          : inout std_logic_vector(11 downto 0);

            -- Intra-inverter error detection
            p_top_iInvErrDet_inv_pwmi_in   : in    std_logic;
            p_top_iInvErrDet_inv_pwmo_out  : out   std_logic;
            p_top_iInvErrDet_inv_synci_in  : in    std_logic;
            p_top_iInvErrDet_inv_synco_out : out   std_logic;
            p_top_iInvErrDet_mcu_pwmi_in   : in    std_logic;
            p_top_iInvErrDet_mcu_pwmo_out  : out   std_logic;
            p_top_iInvErrDet_mcu_synci_in  : in    std_logic;
            p_top_iInvErrDet_mcu_synco_out : out   std_logic
        );
    end component; -- bf_top

    -- simulation model of THS1206
    component ths1206_model is
        generic(induce_initialization_error : std_logic := '0'
               );
        port(
            -- ADC control lines
            p_adc_conv_n_in : in    std_logic; -- conversion trigger input
            p_adc_cs0_n_in  : in    std_logic; -- chip select 0
            p_adc_cs1_in    : in    std_logic; -- chip select 1
            p_adc_rd_n_in   : in    std_logic; -- read trigger
            p_adc_wr_n_in   : in    std_logic; -- write enable

            p_adc_dav_n_out : out   std_logic; -- data available flag

            -- ADC data
            p_adc_data_bi   : inout std_logic_vector(11 downto 0) -- Data bus (including D10/RA0 and D11/RA1)

        );
    end component ths1206_model;

    signal s_resetSim_n    : std_logic := '0';
    signal s_mainClkSim    : std_logic := '0';

    signal s_heartbeat  : std_logic := '0';
    signal s_debugLed   : std_logic := '0';
    signal s_adcerror   : std_logic := '0';

    signal s_adcReset   : std_logic := '1';

    signal s_mcuReqSim  : std_logic := '0';
    signal s_pciAddress : std_logic_vector(3 downto 0)  := (others => '0');
    signal s_pciData    : std_logic_vector(11 downto 0) := (others => '0');
    signal s_clkOut     : std_logic := '0';

    signal s_gdrv_flt_vec    : std_logic_vector(4 downto 0) := (others => '0');
    signal s_gdrv_flt_n      : std_logic := '0';
    signal s_gdrv_flt_psup_n : std_logic := '0';
    signal s_gdrv_flt_vin_n  : std_logic := '0';
    signal s_gdrv_flt_dcdc_n : std_logic := '0';
    signal s_gdrv_flt_5v_n   : std_logic := '0';
    signal s_gdrv_flt_hs_n   : std_logic := '0';
    signal s_gdrv_flt_ls_n   : std_logic := '0';

    signal s_ordSim     : std_logic_vector(12 downto 0) := (others => '0');
    signal s_ordClkSim  : std_logic := '0';
    signal s_inhb       : std_logic := '0';
    signal s_va0        : std_logic := '0';
    signal s_va1        : std_logic := '0';

    signal s_rotorAngleSim : std_logic_vector(12 downto 0) := (others => '0');
    signal s_rotorSpeedSim : std_logic_vector(12 downto 0) := "0000111101011";

    signal s_pwmIn  : std_logic_vector(5 downto 0) := (others => '0');
    signal s_pwmOut : std_logic_vector(5 downto 0) := (others => '0');
    signal s_pwmFb  : std_logic_vector(5 downto 0) := (others => '0');
    signal s_pwmFlt : std_logic := '0';
    signal s_iInvPWMSync : std_logic := '0';

    signal s_diag_flt_gen_n  : std_logic := '0';
    signal s_diag_flt_fpga_n : std_logic := '0';

    signal s_spi_sck  : std_logic := '0';
    signal s_spi_ss_n : std_logic := '1';
    signal s_spi_mosi : std_logic := '0';
    signal s_spi_miso : std_logic := '0';

    signal s_hvAdcMiso : std_logic := '0';
    signal s_hvAdcSck  : std_logic := '0';
    signal s_hvAdcCs   : std_logic := '0';

    signal s_adc_conv_n : std_logic := '0';
    signal s_adc_cs0_n  : std_logic := '0';
    signal s_adc_cs1    : std_logic := '0';
    signal s_adc_rd_n   : std_logic := '0';
    signal s_adc_wr_n   : std_logic := '0';
    signal s_adc_dav    : std_logic := '0';

    signal s_adc_data       : std_logic_vector(11 downto 0);

    signal s_inv_pwmi  : std_logic;
    signal s_inv_pwmo  : std_logic;
    signal s_inv_synci : std_logic;
    signal s_inv_synco : std_logic;
    signal s_mcu_pwmi  : std_logic;
    signal s_mcu_pwmo  : std_logic;
    signal s_mcu_synci : std_logic;
    signal s_mcu_synco : std_logic;


-- stimulation
begin
    dev_to_test : bfi_top
        port map(
            -- reset
            p_top_reset_n_in          => s_resetSim_n,

            -- CLK input
            p_top_clk_in              => s_mainClkSim,

            -- LED output
            p_top_led1_out            => s_heartbeat,
            p_top_led2_out            => s_debugLed,
            p_top_led3_out            => s_adcerror,

            -- Buttons
            p_top_sw1_in              => '0',
            --p_top_sw2_in              : in std_logic;

            -- PCI interface
            p_top_req_in              => s_mcuReqSim,
            p_top_address_out         => s_pciAddress,
            p_top_data_out            => s_pciData,
            p_top_clk_out             => s_clkOut,

            -- gate drivers monitor
            p_top_gdrv_flt_dcdc_in    => s_gdrv_flt_vec(0),
            p_top_gdrv_flt_vin_in     => s_gdrv_flt_vec(1),
            p_top_gdrv_flt_5v_in      => s_gdrv_flt_vec(2),
            p_top_gdrv_flt_hs_in      => s_gdrv_flt_vec(3),
            p_top_gdrv_flt_ls_in      => s_gdrv_flt_vec(4),
            p_top_gdrv_flt_gdrv_n_out => s_gdrv_flt_n,
            p_top_gdrv_flt_psup_n_out => s_gdrv_flt_psup_n,
            p_top_gdrv_flt_vin_n_out  => s_gdrv_flt_vin_n,
            p_top_gdrv_flt_dcdc_n_out => s_gdrv_flt_dcdc_n,
            p_top_gdrv_flt_5v_n_out   => s_gdrv_flt_5v_n,
            p_top_gdrv_flt_hs_n_out   => s_gdrv_flt_hs_n,
            p_top_gdrv_flt_ls_n_out   => s_gdrv_flt_ls_n,

            -- Resolver input and output signals
            p_top_ord_in              => s_ordSim,
            p_top_ordclk_in           => s_ordClkSim,
            p_top_inhb_out            => s_inhb,
            p_top_va0_out             => s_va0,
            p_top_va1_out             => s_va1,

            -- PWM Monitor input signals
            p_top_aHs_in              => s_pwmIn(0),
            p_top_aLs_in              => s_pwmIn(1),
            p_top_bHs_in              => s_pwmIn(2),
            p_top_bLs_in              => s_pwmIn(3),
            p_top_cHs_in              => s_pwmIn(4),
            p_top_cLs_in              => s_pwmIn(5),

            --PWM Monitor output signals
            p_top_aHs_out             => s_pwmOut(0),
            p_top_aLs_out             => s_pwmOut(1),
            p_top_bHs_out             => s_pwmOut(2),
            p_top_bLs_out             => s_pwmOut(3),
            p_top_cHs_out             => s_pwmOut(4),
            p_top_cLs_out             => s_pwmOut(5),

            --PWM Monitor feedback input signals
            p_top_aHs_fb_in           => s_pwmFb(0),
            p_top_aLs_fb_in           => s_pwmFb(1),
            p_top_bHs_fb_in           => s_pwmFb(2),
            p_top_bLs_fb_in           => s_pwmFb(3),
            p_top_cHs_fb_in           => s_pwmFb(4),
            p_top_cLs_fb_in           => s_pwmFb(5),

            p_top_iInvPWMSync_flt_in => s_iInvPWMSync,

            -- Diagnostics signals
            p_top_diag_flt_gen_n_out  => s_diag_flt_gen_n,
            p_top_diag_flt_fpga_n_out => s_diag_flt_fpga_n,

            -- SPI signals
            p_top_spi_sck_in          => s_spi_sck,
            p_top_spi_ss_n_in         => s_spi_ss_n,
            p_top_spi_mosi_in         => s_spi_mosi,
            p_top_spi_miso_out        => s_spi_miso,

            -- HV ADC Interface(MISO-only SPI)
            p_top_hvADC_miso_in       => s_hvAdcMiso,
            p_top_hvADC_sck_out       => s_hvAdcSck,
            p_top_hvADC_meas_cs_n_out => s_hvAdcCs,

            -- Currents ADC Interface
            p_top_currADC_conv_n_out  => s_adc_conv_n,
            p_top_currADC_cs0_n_out   => s_adc_cs0_n,
            p_top_currADC_cs1_out     => s_adc_cs1,
            p_top_currADC_rd_n_out    => s_adc_rd_n,
            p_top_currADC_wr_n_out    => s_adc_wr_n,
            p_top_currADC_dataAv_in   => s_adc_dav,
            p_top_currADC_data_bi     => s_adc_data,

            -- Intra inverter error detection
            p_top_iInvErrDet_inv_pwmi_in   => s_inv_pwmi,
            p_top_iInvErrDet_inv_pwmo_out  => s_inv_pwmo,
            p_top_iInvErrDet_inv_synci_in  => s_inv_synci,
            p_top_iInvErrDet_inv_synco_out => s_inv_synco,
            p_top_iInvErrDet_mcu_pwmi_in   => s_mcu_pwmi,
            p_top_iInvErrDet_mcu_pwmo_out  => s_mcu_pwmo,
            p_top_iInvErrDet_mcu_synci_in  => s_mcu_synci,
            p_top_iInvErrDet_mcu_synco_out => s_mcu_synco
        );

    ths1206_model_inst : component ths1206_model
        generic map(induce_initialization_error => '0')
        port map(
            p_adc_conv_n_in => s_adc_conv_n,
            p_adc_cs0_n_in  => s_adc_cs0_n,
            p_adc_cs1_in    => s_adc_cs1,
            p_adc_rd_n_in   => s_adc_rd_n,
            p_adc_wr_n_in   => s_adc_wr_n,
            p_adc_dav_n_out => s_adc_dav,
            p_adc_data_bi   => s_adc_data
        );

    rst_stimulus : process
    -- sets high the reset signal after 1 us
    begin
        s_resetSim_n <= '1';
        wait for 100 ns;
        s_resetSim_n <= '0';
        wait for 100 ns;
        s_resetSim_n <= '1';
        wait;
    end process rst_stimulus;

    clk_stimulus : process
    -- simulates clk stimulus
    begin
        wait for 5 ns; -- 100 MHz
        s_mainClkSim <= not s_mainClkSim;
    end process clk_stimulus;

    req_stimulus : process
    -- simulate MCU request signal at 20 kHz -> 10 us high, 40 us low
    begin
        wait for 10 us;
        loop
            s_mcuReqSim <= '1';
            wait for 17 us;
            s_mcuReqSim <= '0';
            wait for 35 us;
        end loop;
    end process req_stimulus;

    angle_stimulus : process
    begin
        wait for 700 ns;
        s_rotorAngleSim <= s_rotorAngleSim + '1';
    end process angle_stimulus;

    pga411_stimulus : process (s_inhb)
    begin
        if (rising_edge(s_inhb)) then

            if ((s_va0 = '0') and (s_va1 = '1')) then
                s_ordSim <= s_rotorAngleSim;
            elsif ((s_va0 = '1') and (s_va1 = '0')) then
                s_ordSim <= s_rotorSpeedSim;
            else
                s_ordSim <= (others => '0');
            end if;

            --wait for 25 ns;
            s_ordClkSim <= '1';

        elsif (falling_edge(s_inhb)) then
            --wait for 25 ns;
            s_ordClkSim <= '0';

        else
            s_ordSim <= (others => '0');
        end if;
    end process pga411_stimulus;

    --hvAdc_stimulus : process
    --begin
    --end process hvAdc_stimulus;

--    ths1206_stimulus : process
--    begin
--        wait for 5 us;
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        s_adc_dav <= '0';               --                  clear data available flag
--        wait for 1003030 ns;            -- 1.003.030 ns
--        s_adc_data <= X"09A";           --                  configuration register 0
--        wait for 60 ns;                 -- 1.003.090 ns
--        s_adc_data <= (others=>'Z');    --
--        wait for 40 ns;                 -- 1.003.130 ns
--        s_adc_data <= X"6A0";           --                  configuration register 1
--        wait for 60 ns;                 -- 1.003.190 ns
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 3850 ns;               -- 1.007.040 ns
--        s_adc_data <= X"800";           --                  test voltage at ch0
--        wait for 60 ns;                 -- 1.007.100 ns
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 40 ns;                 -- 1.007.140 ns
--        s_adc_data <= X"800";           --                  test voltage at ch1
--        wait for 60 ns;                 -- 1.007.200 ns
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 40 ns;                 -- 1.007.240 ns
--        s_adc_data <= X"800";           --                  test voltage at ch2
--        wait for 60 ns;                 -- 1.007.300 ns
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 40 ns;                 -- 1.007.340 ns
--        s_adc_data <= X"800";           --                  test voltage at ch3
--        wait for 60 ns;                 -- 1.007.400 ns
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 2500 ns;               -- 1.009.900 ns
--        s_adc_dav <= '1';               --                  set data availabel flag
--        wait for 1150 ns;               -- 1.011.050 ns
--        s_adc_data <= X"000";           --                  converted data in ch0
--        wait for 60 ns;                 -- 1.011.110 ns
--        s_adc_dav <= '0';               --                  clear data available flag
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 40 ns;                 -- 1.011.150 ns
--        s_adc_data <= X"000";           --                  converted data in ch1
--        wait for 60 ns;                 -- 1.011.210 ns
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 40 ns;                 -- 1.011.250 ns
--        s_adc_data <= X"000";           --                  converted data in ch2
--        wait for 60 ns;                 -- 1.011.310 ns
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 40 ns;                 -- 1.011.350 ns
--        s_adc_data <= X"000";           --                  converted data in ch3
--        wait for 60 ns;                 -- 1.011.410 ns
--        s_adc_data <= (others=>'Z');    --                  release data bus
--                                        -- 1.013.010 ns
--                                        --                  falling edge on conversion trigger pulse
--        wait for 2800 ns;               -- 1.014.210 ns
--        s_adc_dav <= '1';               --                  1.2 us later data becomes available
--        wait for 60 ns;                 -- 1.014.270 ns
--                                        --                  60-ns delay due to data available flag debouncing
--        s_adc_data <= X"012";           --                  converted data in ch0
--        wait for 60 ns;                 -- 1.014.330 ns
--        s_adc_dav <= '0';               --                  clear data available flag
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 40 ns;                 -- 1.014.370 ns
--        s_adc_data <= X"345";           --                  converted data in ch1
--        wait for 60 ns;                 -- 1.014.430 ns
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 40 ns;                 -- 1.014.470 ns
--        s_adc_data <= X"567";           --                  converted data in ch2
--        wait for 60 ns;                 -- 1.014.530 ns
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 40 ns;                 -- 1.014.570 ns
--        s_adc_data <= X"89A";           --                  converter data in ch3
--        wait for 60 ns;                 -- 1.014.630 ns
--        s_adc_data <= (others=>'Z');    --                  release data bus
--        wait for 1 ms;
--    end process ths1206_stimulus;

    --hvAdc_stimulus : process
    --begin
    --end process hvAdc_stimulus;

    -- Intra inverter error detection module stimulus

    process is
    begin
        loop
            wait for 1 us;
            s_spi_sck <= not s_spi_sck;
        end loop;
    end process;

    s_pwm_stimulus: process is
    begin
        s_inv_pwmi <= '0';
        s_mcu_pwmi <= '0';
        wait for 50 us;
        -- 10kHz
        for i in 0 to 4 loop
            s_inv_pwmi <= '1';
            s_mcu_pwmi <= '1';
            wait for 40 us;
            s_inv_pwmi <= '0';
            s_mcu_pwmi <= '0';
            wait for 60 us;
        end loop;
        wait for 100 us;
        -- 15kHz
        for i in 0 to 4 loop
            s_inv_pwmi <= '1';
            s_mcu_pwmi <= '1';
            wait for 40 us;
            s_inv_pwmi <= '0';
            s_mcu_pwmi <= '0';
            wait for 26.67 us;
        end loop;
        wait for 100 us;
        -- 20kHz
        for i in 0 to 4 loop
            s_inv_pwmi <= '1';
            s_mcu_pwmi <= '1';
            wait for 40 us;
            s_inv_pwmi <= '0';
            s_mcu_pwmi <= '0';
            wait for 10 us;
        end loop;
        wait for 100 us;
        -- 22kHz
        for i in 0 to 4 loop
            s_inv_pwmi <= '1';
            s_mcu_pwmi <= '1';
            wait for 40 us;
            s_inv_pwmi <= '0';
            s_mcu_pwmi <= '0';
            wait for 5.45 us;
        end loop;
        wait for 100 us;
        -- check if PWM sync is stuck on high level
        s_inv_pwmi <= '1';
        s_mcu_pwmi <= '1';
        wait for 100 us;
        s_inv_pwmi <= '0';
        s_mcu_pwmi <= '0';
        wait for 100 us;
        -- check if only 1 clock cycle pulse is generated
        wait until rising_edge(s_mainClkSim);
        s_inv_pwmi <= '1';
        s_mcu_pwmi <= '1';
        wait until rising_edge(s_mainClkSim);
        s_inv_pwmi <= '0';
        s_mcu_pwmi <= '0';
        wait;
    end process;

    pwm_synchronism_pulse : process
    begin
        wait until s_mcu_pwmi = '1';
        s_iInvPWMSync <= '1';
        wait for 500 ns;
        s_iInvPWMSync <= '0';
    end process;

    s_inv_sync_stimulus: process is
    begin
        s_inv_synci <= '1';
        wait for 200 us;
        s_inv_synci <= '0';
        wait for 50 us;
    end process;

    s_mcu_sync_stimulus: process is
    begin
        s_mcu_synci <= '1';
        wait for 250 us;
        s_mcu_synci <= '0';
        wait for 50 us;
    end process;

end simulation;
