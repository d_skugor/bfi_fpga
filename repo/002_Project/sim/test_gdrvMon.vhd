----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_gdrvMon.vhd
-- module name      Gate Driver Monitor Simulation
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100TCSG324-1L
-- brief            Top Level Module
----------------------------------------------------------------------------------------------
-- create date      10.10.2019
-- Revision:
--      Revision 0.01 - File Created
--
--      Revision 0.02 by david.pavlovic
--                    -- Testbench changed to be compatible with Gate Driver v4.0 monitor
--
--      Revision 0.03 by david.pavlovic
--                    -- Testbench changed to be compatible with updated version of Gate Driver
--                       v4.0 monitor
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity test_gdrvMon is
--  Port ( );
end test_gdrvMon;

architecture tb of test_gdrvMon is

    ---------------------------------------------------------------------------------
    --                                 components
    ---------------------------------------------------------------------------------

    component gate_drivers_monitor is
        port (
            p_gdrvMon_clk_in         : in  std_logic;
            p_gdrvMon_rst_n_in       : in  std_logic;
            p_gdrvMon_mcu_req_in     : in  std_logic;
            -- PWM input signals
            p_gdrvMon_pwm_in         : in  std_logic_vector(5 downto 0);
            -- Gate Driver fault signals
            p_gdrvMon_flt_n_in       : in  std_logic_vector(4 downto 0);
            p_gdrvMon_flt_n_out      : out std_logic_vector(6 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                 signals
    ---------------------------------------------------------------------------------
    signal s_clk       : std_logic := '0';
    signal s_rst_n     : std_logic := '1';
    signal s_mcu_req   : std_logic := '1';
    -- PWM signals
    signal s_pwm       : std_logic_vector(5 downto 0);
    -- Gate Drivers Monitor
    signal s_flt_n_in  : std_logic_vector(4 downto 0);
    signal s_flt_n_out : std_logic_vector(6 downto 0);

begin

    gdrv_monitor:  gate_drivers_monitor
        port map (
            p_gdrvMon_clk_in         => s_clk,
            p_gdrvMon_rst_n_in       => s_rst_n,
            p_gdrvMon_mcu_req_in     => s_mcu_req,
            -- PWM input signals
            p_gdrvMon_pwm_in         => s_pwm,
            -- Gate Driver fault signals
            p_gdrvMon_flt_n_in       => s_flt_n_in,
            p_gdrvMon_flt_n_out      => s_flt_n_out
        );

    s_rst_n <= '0', '1' after 110 ns ;

    clk_generate: process
    begin
        wait for 5 ns; -- 100 Mhz
        s_clk <= not s_clk;
    end process;

    mcu_req_gen : process is
    begin
        s_mcu_req <= '0';
        wait for 10us;
        wait until rising_edge(s_clk);
        s_mcu_req <= '1';
        wait for 10us;
        wait until rising_edge(s_clk);
    end process;

    s_flt_n_in <= "11111", "00100" after 12 us, "00000" after 20 us, "00110" after 30 us, "00001" after 40 us;

    process is
    begin
        s_pwm <= "000000";
        wait for 1 us;
        s_pwm <= "101010";
        loop
            wait for 1 us;
            s_pwm <= not s_pwm;
        end loop;
    end process;

end tb;
