----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_crc.vhd
-- module name      CRC Test
-- author           Arturo Montufar Arreola (arturo.arreola@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100T-1CSG324Q
-- brief            CRC Block Simulation
----------------------------------------------------------------------------------------------
-- create date      12.07.2019
-- Revision:
--      Revision 0.01 - File Created
----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity test_crc is
    -- nothing here because it's a simulation
end test_crc;

architecture test of test_crc is

component crc
    generic (
        crc_polynomial     : std_logic_vector(8 downto 0) := "100011101");
    port (
        -- main signals
        crc_reset_in       : in std_logic;
        crc_clk_in         : in std_logic;
        -- data input
        crc_req_in         : in std_logic;
        crc_data_in        : in std_logic_vector(127 downto 0);
        -- interface output
        crc_rdy_out        : out std_logic;
        crc_value_out      : out std_logic_vector(7 downto 0);
        crc_counter_out    : out std_logic_vector(3 downto 0));
end component;

signal reset : std_logic := '1';
signal clk   : std_logic := '0';
signal req   : std_logic := '0';
signal data  : std_logic_vector(127 downto 0) := "00010001000100010010001000100010001100110011001101000100010001000101010101010101011001100110011001110111011101111000010100000000";
signal ready : std_logic;
signal value : std_logic_vector(7 downto 0);
signal cntr  : std_logic_vector(3 downto 0);

begin

    dev_to_test : crc
        port map(
            crc_reset_in => reset,
            crc_clk_in => clk,
            crc_req_in => req,
            crc_data_in => data,
            crc_rdy_out => ready,
            crc_value_out => value,
            crc_counter_out => cntr);
            
    clk_stimulus : process
    begin
        wait for 5 ns;
        clk <= not clk;
    end process clk_stimulus;
    
    req_stimulus : process
    begin
        wait for 1 us;
        req <= '1';
        wait for 2 us;
        req <= '0';
        wait for 22 us;
    end process req_stimulus;

end test;
