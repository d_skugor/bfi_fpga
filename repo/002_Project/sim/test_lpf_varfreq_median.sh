xvhdl -nolog ../src/lpf/pwm_pack.vhd
xvhdl -nolog ../../001_Common/src/cdc_simple_synchro.vhd
xvhdl -nolog ../../001_Common/src/edge_detector_p.vhd
xvhdl -nolog ../src/lpf/lpf_pwm_fctrl.vhd
xvhdl -nolog ../src/lpf/medfilt_bblock.vhd
xvhdl -nolog ../src/lpf/medfilt_sampler.vhd
xvhdl -nolog ../src/lpf/medfilt_stam.vhd
xvhdl -2008 -nolog ../src/lpf/medfilt_sequencer.vhd
xvhdl -nolog ../src/lpf/medfilt_block.vhd
xvhdl -nolog ../src/lpf/lpf_varfreq_median.vhd
xvhdl -nolog test_lpf_varfreq_median.vhd$XILINX
# glbl library is required when the design uses UNISIM primitives
xvlog $XILINX_VIVADO/data/verilog/src/glbl.v
xelab -debug typical test_lpf_varfreq_median -s top_sim -nolog work.glbl
xsim top_sim -gui -nolog

# Cleanup to avoid getting intermediate and simulation output files into GIT
rm *.log
rm *.pb
rm -rf xsim.dir
rm *.jou
rm *.wdb