----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             spi_slave_sim.vhd
-- module name      AD7476 SPI Slave Simulation
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100TCSG324-1L
-- brief            Simulation file for simulating ad7476 behavioral
----------------------------------------------------------------------------------------------
-- create date      05.07.2019
-- Revision:
--      Revision 0.01 - File Created
--      Revision 0.02 - Fixed  ad7476 simulation file in  accordance with datasheet
----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity spi_slave_sim is
   port ( 
        p_spi_sck      : in std_logic;
        p_spi_miso     : out std_logic := 'Z'; 
        p_spi_cs       : in std_logic
   );
end spi_slave_sim;

architecture behavioral of spi_slave_sim is

    signal s_miso    : std_logic := 'Z';
    signal s_dataOut : std_logic_vector(15 downto 0) := ("0000000001111000");  -- raw value for cca 30V 

begin

    process(p_spi_sck, p_spi_cs)
        variable v_index : std_logic_vector(3 downto 0) := "1110"; -- c going low clocks the first leading zero, not sck 
    begin
        if (p_spi_cs = '0')  then
        
            if (falling_edge(p_spi_sck)) then
                if (v_index = "1111") then
                    s_miso <= 'Z';
                    v_index :=   v_index - 1;
                else
                    s_miso <= s_dataOut(conv_integer(unsigned(v_index)));
                    v_index := v_index - 1;
                end if;
             end if;
        else
             s_miso <= '0';
             v_index := "1110";  
        end if;
    end process;
    
    p_spi_miso <= s_miso after 40 ns; -- add  delay to simulate board delay  T4 from data sheet - data access time
                                      -- after sclk falling edge  - 40 ns max

  process(p_spi_sck, p_spi_cs)
  begin
    if (rising_edge(p_spi_sck)) then
        s_dataOut <= s_dataOut + '1';
    end if;
  end process;
      

end behavioral;
