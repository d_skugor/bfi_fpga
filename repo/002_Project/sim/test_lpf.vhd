library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity test_lpf is
	-- empty
end entity test_lpf;

architecture behavioral of test_lpf is
	-- Simulation parameters
	constant C_SAMPLE_TIME_IN_US          : real := 1.0;
	constant C_SAMPLING_CLOCK_SEMI_PERIOD : time := 500 nS;
	constant C_SYSTEM_CLOCK_SEMIPERIOD    : time := 5 nS;

	-- Test signals
	signal uStep    : std_logic_vector(11 downto 0) := (others => '0');
	signal uImpulse : std_logic_vector(11 downto 0) := (others => '0');
	signal filter_X : std_logic_vector(11 downto 0) := (others => '0');
	signal filter_Y : std_logic_vector(11 downto 0) := (others => '0');

	signal clk        : std_logic := '0'; -- System clock
	signal rst_n      : std_logic := '1'; -- System reset
	signal sample_clk : std_logic := '0'; -- Sampling clock
	signal sample_p   : std_logic := '0'; -- Sampling pulse

	-- DUT declaration	
	component lpf_block is
		port(clk_i         : in  std_logic; --! System clock input
		     rst_n_i       : in  std_logic; --! Synchronous reset input
		     sample_clk_i  : in  std_logic; --! Sample clock input
		     filter_data_i : in  std_logic_vector; --! Filter data input
		     filter_data_o : out std_logic_vector --! Filter data output		
		    );
	end component lpf_block;

	--for DUT : lpf_block use entity work.lpf_block(MAV);
	for DUT : lpf_block use entity work.lpf_block(IIR);

begin

	-- Reset
	reset_proc : process
	begin
		rst_n <= '0';
		wait for 4 * C_SYSTEM_CLOCK_SEMIPERIOD;
		wait until clk <= '0';
		rst_n <= '1';
		wait;
	end process;

	-- System clock generator
	system_clk : process
	begin
		clk <= '0';
		wait for C_SYSTEM_CLOCK_SEMIPERIOD;
		clk <= '1';
		wait for C_SYSTEM_CLOCK_SEMIPERIOD;
	end process;

	sampling_clk : process
	begin
		-- This clock has to be synchronous with the system clock
		wait until clk = '1';
		sample_clk <= '0';
		wait for C_SAMPLING_CLOCK_SEMI_PERIOD;
		wait until clk = '1';
		sample_clk <= '1';
		wait for C_SAMPLING_CLOCK_SEMI_PERIOD;
	end process;

	-- Excitation standard
	excitation : process
	begin
		wait until rst_n = '0';
		uStep    <= (others => '0');
		uImpulse <= (others => '0');
		wait until rst_n = '1';
		wait until sample_clk = '1';
		uStep    <= X"3FF";             -- Positive FS
		uImpulse <= X"7FF";             -- Positive FS
		wait until sample_clk = '1';
		uImpulse <= (others => '0');    -- Zero
		wait;
	end process;

	-- DECLARE THE TEST SIGNAL TO USE AS EXCITATION
	filter_X <= uStep;

	-- Filter block	
	DUT : component lpf_block
		port map(
			clk_i         => clk,
			rst_n_i       => rst_n,
			sample_clk_i  => sample_clk,
			filter_data_i => filter_X,
			filter_data_o => filter_Y
		);

end architecture;
