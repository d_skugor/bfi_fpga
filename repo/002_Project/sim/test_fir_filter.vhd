----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_fir_filter.vhd
-- module name      FIR filter testbench
-- author           David Palvovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            FIR filter testbench environment
----------------------------------------------------------------------------------------------
-- create date      28.05.2020.
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use STD.textio.all;
use ieee.std_logic_textio.all;

library std;
use std.env.all;

entity test_firFilter is
end entity test_firFilter;

architecture test of test_firFilter is

    component firFilter
        generic (
            G_FIRFILTER_DATA_W      : natural := 12;     -- Data width
            G_FIRFILTER_AVG_LEN     : natural := 7       -- Number of averaged samples (must be >= 3)
        );
        port (
            p_firFilter_reset_n_in       : in  std_logic;
            p_firFilter_clk_in           : in  std_logic;
            p_firFilter_sampleClk_in     : in  std_logic;
            p_firFilter_rawData_in       : in  std_logic_vector(11 downto 0);
            p_firFilter_filteredData_out : out std_logic_vector(11 downto 0)
        );
    end component;

    signal s_clk        : std_logic := '0';
    signal s_rst_n      : std_logic;
    signal s_sample_clk : std_logic;
    signal s_pwm_sync   : std_logic;
    signal s_pwm_freq   : std_logic_vector(2 downto 0);
    signal s_data_i     : std_logic_vector(11 downto 0);
    signal s_data_o     : std_logic_vector(11 downto 0);

    signal lfsr : std_logic_vector(31 downto 0);

    constant C_CLK_PER        : time    := 10 ns;
    constant C_SAMPLE_CLK_PER : time    := 1 us;
    constant C_PWM_SYNC_L     : integer := 6250;

    file file_i : text;
    file file_o : text;

begin

    dut : firFilter
        generic map (
            G_FIRFILTER_DATA_W      => 12,     -- Data width
            G_FIRFILTER_AVG_LEN     => 11       -- Number of averaged samples (must be >= 3)
        )
        port map (
            p_firFilter_reset_n_in       => s_rst_n,
            p_firFilter_clk_in           => s_clk,
            p_firFilter_sampleClk_in     => s_sample_clk,
            --p_firFilter_pwm_sync         => s_pwm_sync,
            --p_firFilter_pwm_freq         => s_pwm_freq,
            p_firFilter_rawData_in       => s_data_i,
            p_firFilter_filteredData_out => s_data_o
        );

    -- main clock process
    process is
    begin
        wait for C_CLK_PER/2;
        s_clk <= not s_clk;
    end process;

    -- reset
    s_rst_n <= '0', '1' after 500 ns;

    -- sample clock process
    process is
    begin
        s_sample_clk <= '0';
        wait until rising_edge(s_rst_n);
        wait for 0.7us;
        wait until rising_edge(s_clk);
        loop
            s_sample_clk <= not s_sample_clk;
            wait for C_SAMPLE_CLK_PER/2;
            wait until rising_edge(s_clk);
        end loop;
    end process;

    -- PWM sync stimulus
    process is
    begin
        s_pwm_sync <= '0';
        wait until rising_edge(s_rst_n);
        wait until rising_edge(s_clk);
        loop
            s_pwm_sync <= '1';
            wait until rising_edge(s_clk);
            s_pwm_sync <= '0';
            for i in 0 to C_PWM_SYNC_L - 2 loop
                wait until rising_edge(s_clk);
            end loop;
        end loop;
    end process;

    -- PWM freq stimulus
    process is
    begin
        if (C_PWM_SYNC_L = 12500) then
            s_pwm_freq <= "000";
        elsif (C_PWM_SYNC_L = 10000) then
            s_pwm_freq <= "001";
        elsif (C_PWM_SYNC_L = 8333) then
            s_pwm_freq <= "010";
        elsif (C_PWM_SYNC_L = 6250) then
            s_pwm_freq <= "011";
        elsif (C_PWM_SYNC_L = 5000) then
            s_pwm_freq <= "100";
        else
            s_pwm_freq <= "111";
        end if;
        wait;
    end process;

    process
        variable v_line_i : line;
        variable v_data_i : integer;
        variable v_line_o : line;
        variable v_data_o : integer;
    begin
        s_data_i <= (others => '0');
        wait until s_rst_n = '1';
        file_open(file_o, "i_o.txt", write_mode);
        for i in 0 to 2 loop
            file_open(file_i, "./../../../../../repo/002_Project/sim/ib_u.txt",  read_mode);
            while not endfile(file_i) loop
                wait until rising_edge(s_sample_clk);
                -- read sample
                readline(file_i, v_line_i);
                read(v_line_i, v_data_i);
                s_data_i <= std_logic_vector(to_unsigned(v_data_i, s_data_i'length));
                -- write sample
                v_data_o := to_integer(unsigned(s_data_o));
                write(v_line_o, v_data_o);
                writeline(file_o, v_line_o);
            end loop;
            file_close(file_i);
        end loop;
        file_close(file_o);
        finish(0);
    end process;

    -- random data process
    -- process (s_clk) is
    -- begin
        -- if rising_edge(s_clk) then
            -- if (s_rst_n = '0') then
                -- lfsr <= x"DEADDEAD";
            -- else
                -- lfsr(30 downto 0) <= lfsr(31 downto 1);
                -- lfsr(31) <= lfsr(31) xor lfsr(21) xor lfsr(1) xor lfsr(0);
            -- end if;
        -- end if;
    -- end process;

    -- input data is randomly generated number within range [0, 4095]
    -- s_data_i <= lfsr(31 downto 20);
    --s_data_i <= "000000000001";

    -- process is
    -- begin
        -- s_data_i <= (others => '0');
        -- wait until s_rst_n = '1';
        -- wait until rising_edge(s_clk);
        -- for i in 0 to 128 loop
            -- for j in 0 to 61 loop
                -- wait until rising_edge(s_sample_clk);
                -- s_data_i <= std_logic_vector(to_unsigned(i, s_data_i'length));
            -- end loop;
        -- end loop;
    -- end process;

end test;