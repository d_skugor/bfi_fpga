library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;
use work.pwm_pack.all;

entity test_medfilt_sequencer is
end entity test_medfilt_sequencer;

architecture behavioral of test_medfilt_sequencer is

	constant C_START_PULSE_CADENCE     : time := 10 uS;
	constant C_SAMPLING_CLOCK_PERIOD   : time := 50 nS;
	constant C_SYSTEM_CLOCK_SEMIPERIOD : time := 100 pS;

	signal clk        : std_logic := '0'; -- System clock
	signal rst_n      : std_logic := '1'; -- System reset
	signal sample_clk : std_logic := '0'; -- Sampling pulse
	signal start_p    : std_logic := '0'; -- Start pulse

	signal sample_p : std_logic := '0';
	signal rdy_p    : std_logic := '0';

	signal rdy_sampler_p : std_logic                     := '0';
	signal x, y1, y2, y3 : std_logic_vector(11 downto 0) := (others => '0');

	signal counter : integer := 0;

begin

	DUT : entity work.medfilt_sequencer
		port map(
			sample_p_i => sample_clk,
			clk_i      => clk,
			rst_n_i    => rst_n,
			rdy_p_o    => rdy_p,
			sample_p_o => sample_p,
			start_p_i  => start_p
		);

	DUT1 : entity work.medfilt_sampler
		generic map(
			BSIZE => 12
		)
		port map(
			clk_i      => clk,
			rst_n_i    => rst_n,
			rdy_p_o    => rdy_sampler_p,
			x_i        => x,
			y1_o       => y1,
			y2_o       => y2,
			y3_o       => y3,
			sample_p_i => sample_p,
			rdy_p_i    => rdy_p
		);

	-- Reset
	reset_proc : process
	begin
		rst_n <= '0';
		wait for 4 * C_SYSTEM_CLOCK_SEMIPERIOD;
		wait until clk <= '0';
		rst_n <= '1';
		wait;
	end process;

	-- System clock generator
	system_clk : process
	begin
		clk <= '0';
		wait for C_SYSTEM_CLOCK_SEMIPERIOD;
		clk <= '1';
		wait for C_SYSTEM_CLOCK_SEMIPERIOD;
	end process;

	sampling_clk : process
	begin
		-- This clock has to be synchronous with the system clock
		wait until clk = '1';
		sample_clk <= '0';
		wait for C_SAMPLING_CLOCK_PERIOD;
		wait until clk = '1';
		sample_clk <= '1';
		wait until clk = '1';
		sample_clk <= '0';
	end process;

	start : process
	begin
		wait for C_START_PULSE_CADENCE;
		wait until clk = '1';
		start_p <= '1';
		wait until clk = '1';
		start_p <= '0';
	end process;

	sample_generator : process
	begin
		wait until sample_clk = '1';
		counter <= counter + 1;
		x       <= std_logic_vector(to_unsigned(counter, 12));
	end process;

end architecture behavioral;
