----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             hvADC_test.vhd
-- module name      hvADC Interface test bench
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100TCSG324-1L
-- brief            Test bench for testing hvADC Interface
----------------------------------------------------------------------------------------------
-- create date      05.11.2019
-- Revision:
--      Revision 0.01 - File Created
----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity hvADC_test is
--  Port ( );
end hvADC_test;

architecture tb of hvADC_test is
    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component hvADC is
         port ( 
         -- system clock
         p_hvADC_clk_in          : in std_logic;
         -- reset
         p_hvADC_rst_n_in        : in std_logic;
         -- sample clock
--         p_hvADC_sampleClk_in    : in std_logic;
         -- SPI MISO signals
         p_hvADC_meas_miso       : in std_logic := 'Z';
         p_hvADC_meas_sck        : out std_logic;
         p_hvADC_meas_cs_n       : out std_logic;
         -- output data
         p_hvADC_data_out        : out std_logic_vector(11 downto 0)
      );
    end component;
    
    component spi_slave_sim is
       port ( 
            p_spi_sck      : in std_logic;
            p_spi_miso     : out std_logic := 'Z'; 
            p_spi_cs       : in std_logic
       );
    end component;

    signal s_clk         : std_logic := '0';
    signal s_rst_n       : std_logic := '1';
    signal s_sampleClk   : std_logic := '1';
    signal s_sampleClk_p : std_logic := '1';
    signal s_spi_miso    : std_logic;
    signal s_spi_sclk    : std_logic;
    signal s_spi_cs      : std_logic;
    signal s_spi_cs_d    : std_logic;
    signal s_spi_data    : std_logic_vector(11 downto 0);
    

begin

    clk_stimulus : process
    begin
        wait for 5 ns; -- 100 MHz
        s_clk <= not s_clk;
    end process clk_stimulus;
    
    s_rst_n <= '1', '0' after 3995 ns, '1' after 4195 ns;
    
    
--   sample_p_stimulus : process
--    begin
--        wait for 10 ns;
--        s_sampleClk_p <= '0';
--        wait for 490 ns; -- 1 MHz
--        s_sampleClk_p <= '1';
        
--    end process sample_p_stimulus;
    
    sample_stimulus : process
    begin
        wait for 500 ns; -- 1 MHz
        s_sampleClk <= not s_sampleClk;
        
    end process sample_stimulus;
     
         hvADc_meas_block: hvADC 
        port map(
            -- system clock
            p_hvADC_clk_in         => s_clk,
            -- reset
            p_hvADC_rst_n_in      => s_rst_n,
            -- sample clock
--            p_hvADC_sampleClk_in   => s_sampleClk,
            -- SPI MISO signals
            p_hvADC_meas_miso      => s_spi_miso,
            p_hvADC_meas_sck       => s_spi_sclk,
            p_hvADC_meas_cs_n       => s_spi_cs, 
            -- output data
            p_hvADC_data_out      => s_spi_data
            );


    -- delay cs
    s_spi_cs_d <= s_spi_cs after 11 ns; -- simulate t2 timing - cs to sclk setup time, min 11 ns 

spi_slave_tb: spi_slave_sim 
   port map( 
        p_spi_sck      => s_spi_sclk, 
        p_spi_miso     => s_spi_miso,
        p_spi_cs       => s_spi_cs_d
   );


end tb;
