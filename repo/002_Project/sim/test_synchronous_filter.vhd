library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

library WORK;
use WORK.ALL;

entity test_synchronous_filter is
end entity test_synchronous_filter;

architecture RTL of test_synchronous_filter is
	constant C_SAMPLING_CLOCK_PERIOD : time := 1 us; -- 1 MHz pulse train 
	constant C_SYSTEM_CLOCK_PERIOD   : time := 10 ns;

	signal clk       : std_logic                     := '0';
	signal rst_n     : std_logic                     := '0';
	signal adc_clk   : std_logic                     := '0';
	signal adc_rst   : std_logic                     := '0';
	signal adc_rdy_p : std_logic                     := '0';
	signal sample_p  : std_logic                     := '0'; -- ADC new sample pulse
	signal sync_p    : std_logic                     := '0'; -- PWM synchro pulse
	signal ch1_i     : std_logic_vector(11 downto 0) := (others => '0');
	signal ch2_i     : std_logic_vector(11 downto 0) := (others => '0');
	signal ch3_i     : std_logic_vector(11 downto 0) := (others => '0');
	signal ch1_o     : std_logic_vector(11 downto 0) := (others => '0');
	signal ch2_o     : std_logic_vector(11 downto 0) := (others => '0');
	signal ch3_o     : std_logic_vector(11 downto 0) := (others => '0');

	-- #### Setup here the input and output test files
	constant C_INPUT_FILE_NAME  : string := "Simulation8.dat";
	constant C_OUTPUT_FILE_NAME : string := "Simulation8_csb_output.dat";

	file fInputPtr  : TEXT open READ_MODE is C_INPUT_FILE_NAME;
	file fOutputPtr : TEXT open WRITE_MODE is C_OUTPUT_FILE_NAME;
	signal eof      : std_logic := '0';
begin

	DUT : entity work.synchronous_filter
		port map(
			clk_in            => clk,
			rst_n_in          => rst_n,
			sample_p_in       => sample_p,
			ch1_out           => ch1_o,
			ch2_out           => ch2_o,
			ch3_out           => ch3_o,
			adc_clk_out       => adc_clk,
			adc_rst_out       => adc_rst,
			ch1_in            => ch1_i,
			ch2_in            => ch2_i,
			ch3_in            => ch3_i,
			adc_data_rdy_p_in => adc_rdy_p,
			sync_p_a_in       => sync_p			
		);

	-- Stimulus read from simulated data file, preprocessed in matlab
	-- The stimulus format has to be as follows (each line in the input data file):
	-- Phase-A Phase-B Phase-C Sync
	stimuli : process
		variable fLine                              : line;
		variable ch1_sample, ch2_sample, ch3_sample : integer;
		variable data_pwm_sync                      : integer;
	begin
		wait until rst_n = '1';

		while (not ENDFILE(fInputPtr)) loop
			wait until adc_rdy_p = '1';
			READLINE(fInputPtr, fLine);
			READ(fLine, ch1_sample);
			READ(fLine, ch2_sample);
			READ(fLine, ch3_sample);
			READ(fLine, data_pwm_sync);

			ch1_i  <= std_logic_vector(to_signed(ch1_sample, 12));
			ch2_i  <= std_logic_vector(to_signed(ch2_sample, 12));
			ch3_i  <= std_logic_vector(to_signed(ch3_sample, 12));
			sync_p <= to_unsigned(data_pwm_sync, 1)(0);
		end loop;

		wait until sample_p = '1';
		eof <= '1';                     -- End of test
		wait;
	end process;

	-- Log output process. Write the filter output into a file
	-- The output format is (each line):
	--	Phase-A Phase-B Phase-C
	logoutput : process
		variable fLine                              : line;
		variable ch1_sample, ch2_sample, ch3_sample : integer;
	begin
		wait until sync_p = '1';
		ch1_sample := to_integer(signed(ch1_o));
		ch2_sample := to_integer(signed(ch2_o));
		ch3_sample := to_integer(signed(ch3_o));		
		WRITE(fLine, ch1_sample, Right, 12);		
		WRITE(fLine, ch2_sample, Right, 12);		
		WRITE(fLine, ch3_sample, Right, 12);
		WRITELINE(fOutputPtr, fLine);
		if (eof = '1') then
			wait;
		end if;
	end process;

	-- Reset
	reset_proc : process
	begin
		rst_n <= '0';
		wait for 10 * C_SYSTEM_CLOCK_PERIOD;
		wait until clk <= '0';
		rst_n <= '1';
		wait;
	end process;

	-- System clock generator
	system_clk : process
	begin
		clk <= '0';
		wait for C_SYSTEM_CLOCK_PERIOD / 2;
		clk <= '1';
		wait for C_SYSTEM_CLOCK_PERIOD / 2;
	end process;

	adc_rdy : process
		variable divider : integer := 100;
	begin
		-- The ADC produces a ready output pulse each 100 input clock cycles
		wait until adc_clk = '1';
		adc_rdy_p <= '0';
		divider   := divider - 1;
		if (divider = 0) then
			adc_rdy_p <= '1';
			wait until adc_clk = '1';
			divider   := 100;
			adc_rdy_p <= '0';
		end if;
	end process;

	sampling_clk : process
	begin
		wait for C_SAMPLING_CLOCK_PERIOD;
		wait until clk = '1';
		sample_p <= '1';
		wait until clk = '1';
		sample_p <= '0';
	end process;

end architecture RTL;
