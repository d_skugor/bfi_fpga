//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             test_pwm_monitor.vhd
// module name      test_pwm_monitor
// author           David Palvovic (david.pavlovic@rimac-automobili.com)
// project name     BFI
// target device    XA7A100T-1CSG324Q
// brief            PWM monitor module testbench environment
//--------------------------------------------------------------------------------------------
// create date      27.08.2020.
// Revision:
//      Revision 0.01 by david.pavlovic
//                    - File Created
//
//      Revision 0.02 by david.pavlovic
//                    - Updated testbench for fixed version of PWM monitor
//--------------------------------------------------------------------------------------------
// NOTE: This is a SystemVerilog
//--------------------------------------------------------------------------------------------

import bfi_tb_pkg::*;

module test_pwm_monitor;

    timeunit 1ns / 1ps;

    // parameters
    parameter G_TIMEOUT  = 500;
    parameter G_DEADTIME = 300;
    parameter G_DELTA    = 5;
    parameter C_CLK_PER  = 10.0;
    parameter C_DEADTIME = 3000.0; // ns

    logic       s_clk     = 1'b0;
    logic       s_rst_n   = 1'b0;
    logic       s_en      = 1'b1;
    logic       s_mcu_req = 1'b0;
    logic [2:0] s_flt_dt;
    logic [5:0] s_flt_roc;
    logic [5:0] s_flt_time;
    logic [5:0] s_flt_fb;
    bit         pwm_cycle_dbg = 1'b0;
    int         fb_delay;
    bit         s_cnt_en;
    int         s_cnt_dbg;
    int         s_cnt_dbg_int = 0;
    int         s_cnt_dbg_int_d = 0;
    int         s_cnt_dbg_int_dd = 0;
    int         s_cnt_dbg_int_ddd = 0;
    real        mcu_req_t_on = 10; // unit us
    real        mcu_req_t_off;

    // constants
    const int   C_ELE_FREQ     = 900;
    const int   C_PWM_FREQ     = 20000;
    const real  C_NO_OF_CYCLES = 10;

    // auxiliary signals
    typedef enum {f_10_kHz, f_16_kHz, f_20_kHz} t_sw_freq;
    t_sw_freq sw_freq = f_10_kHz;

    // PWM class
    pwm_c  #(C_DEADTIME) pwm;

    // PWM interface
    pwm_if #(C_DEADTIME) pwm_if_i();


    pwm_monitor_top
        #(
            .G_TIMEOUT                  (G_TIMEOUT),
            .G_DEADTIME                 (G_DEADTIME),
            .G_DELTA                    (G_DELTA)
        )
    dut
        (
            // Main signals
            .p_pwm_m_top_clk_in         (s_clk),
            .p_pwm_m_top_rst_n_in       (s_rst_n),
            .p_pwm_m_top_en_in          (s_en),
            .p_pwm_m_top_mcu_req_in     (s_mcu_req),
            // Input PWM signals
            .p_pwm_m_top_pwm_in         (pwm_if_i.pwm_i),
            // Input PWM feedback signals
            .p_pwm_m_top_pwm_fb_in      (pwm_if_i.pwm_fb),
            // PWM enable signal
            .p_pwm_m_top_pwm_en_in      (6'b111111),
            // Output PWM signals
            .p_pwm_m_top_pwm_out        (pwm_if_i.pwm_o),
            // Faults
            .p_pwm_m_top_flt_dt_n_out   (s_flt_dt),
            .p_pwm_m_top_flt_roc_n_out  (s_flt_roc),
            .p_pwm_m_top_flt_time_n_out (s_flt_time),
            .p_pwm_m_top_flt_fb_n_out   (s_flt_fb)
        );

    //-------------------------------------------------------------------------

    // clock & reset generator
    always begin
        #(C_CLK_PER/2) s_clk = ~s_clk;
    end

    initial reset_gen(3);

    always @(posedge s_clk)
        if (s_cnt_en) s_cnt_dbg_int++;

    always @(posedge s_clk) begin
        s_cnt_dbg_int_d   <= s_cnt_dbg_int;
        s_cnt_dbg_int_dd  <= s_cnt_dbg_int_d;
        s_cnt_dbg_int_ddd <= s_cnt_dbg_int_dd;
        s_cnt_dbg         <= s_cnt_dbg_int_ddd;
    end

    //-----------------------------------
    // Tasks
    //-----------------------------------

    // generates N cycle active-low reset signal synchronous with clock
    task reset_gen(input int N);
        s_rst_n <= 1'b0;
        repeat (N+1) @(posedge s_clk);
        s_rst_n <= 1'b1;
    endtask : reset_gen;

    // waits for N clock cycles
    task wait_clk(input int N);
        repeat (N) @(posedge s_clk);
    endtask : wait_clk

    // generates MCU request signal with random frequency (no noise)
    task gen_mcu_req();
        case (sw_freq)
            f_10_kHz : mcu_req_t_off = 100.0 - mcu_req_t_on;
            f_16_kHz : mcu_req_t_off = 62.5 - mcu_req_t_on;
            f_20_kHz : mcu_req_t_off = 50.0- mcu_req_t_on;
        endcase
        forever begin
            s_mcu_req <= 1'b1;
            #(mcu_req_t_on * 1us);
            s_mcu_req <= 1'b0;
            #(mcu_req_t_off * 1us);
        end
    endtask : gen_mcu_req

    task pwm_fb_test();
        repeat (C_NO_OF_CYCLES) begin
            pwm_cycle_dbg <= ~pwm_cycle_dbg;
            if (!std::randomize(fb_delay) with { fb_delay dist {[2000:4000]:/90, [5000:6000]:/10}; })
                 $fatal(1, "*** RANDOMIZATION ERROR (fb_delay) ***");
            $display("d = %0d", fb_delay);
            pwm_if_i.fb_delay = fb_delay;
            pwm.pwm_cycle();
            if ($countones(s_flt_fb) != 6) #(pwm.pwm_per) $finish;
        end
    endtask : pwm_fb_test

    task pwm_dt_test();
        pwm_cycle_dbg  <= ~pwm_cycle_dbg;
        @(posedge s_clk);
        pwm_if_i.pwm_i <= '0;
        repeat (100) @(posedge s_clk);
        pwm_if_i.pwm_i <= 6'b010101;
        repeat (100) @(posedge s_clk);
        pwm_if_i.pwm_i <= 6'b000000;
        s_cnt_en <= 1;
        repeat (295) @(posedge s_clk);
        pwm_if_i.pwm_i <= 6'b101010;
        repeat (100) @(posedge s_clk);
    endtask : pwm_dt_test

    task pwm_all_high_test();
        pwm_cycle_dbg  <= ~pwm_cycle_dbg;
        @(posedge s_clk);
        pwm_if_i.pwm_i <= '0;
        repeat (100) @(posedge s_clk);
        pwm_if_i.pwm_i <= '1;
        repeat (100) @(posedge s_clk);
    endtask : pwm_all_high_test

    // main TB block
    initial begin
        pwm = new(pwm_if_i);
        pwm.randomize() with { ele_freq == C_ELE_FREQ;
                               pwm_freq == C_PWM_FREQ; };
        case (C_PWM_FREQ)
            10000   : sw_freq = f_10_kHz;
            16000   : sw_freq = f_16_kHz;
            20000   : sw_freq = f_20_kHz;
            default : sw_freq = f_10_kHz;
        endcase
        @(posedge s_rst_n);
        wait_clk(100);
        fork
            gen_mcu_req();
        join_none
        #10us;
        pwm_fb_test();
        //pwm_dt_test();
        //pwm_all_high_test();
        #10us;
        $finish;
    end

    final begin
        $display("*** End of simulation! ***");
        $display("*** Simulation was successful! ***");
    end

endmodule : test_pwm_monitor