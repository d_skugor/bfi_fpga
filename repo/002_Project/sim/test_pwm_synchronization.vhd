----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_pwm_synchronization.vhd
-- module name      PWM synchronization module testbench
-- author           David Palvovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            PWM synchronization module testbench environment
----------------------------------------------------------------------------------------------
-- create date      04.06.2020.
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity test_pwm_synchronization is
end entity;

architecture test of test_pwm_synchronization is

    component pwm_synchronization
        port (
            p_pwmSync_clk_in   : in  std_logic;
            p_pwmSync_rst_n_in : in  std_logic;
            p_pwmSync_mcu_req  : in  std_logic;
            p_pwmSync_pwm_sync : out std_logic;
            p_pwmSync_pwm_freq : out std_logic_vector(2 downto 0)
        );
    end component;

    signal s_clk      : std_logic := '0';
    signal s_rst_n    : std_logic;
    signal s_mcu_req  : std_logic;
    signal s_pwm_sync : std_logic;
    signal s_pwm_freq : std_logic_vector(2 downto 0);

    constant C_CLK_PER        : time    := 10 ns;
    constant C_FREQ_8_PERIOD  : time    := 125 us;
    constant C_FREQ_10_PERIOD : time    := 100 us;
    constant C_FREQ_12_PERIOD : time    := 83.33 us;
    constant C_FREQ_16_PERIOD : time    := 62.5 us;
    constant C_FREQ_20_PERIOD : time    := 50 us;
    constant C_MCU_N_PERIOD   : integer := 10;

begin

    dut : pwm_synchronization
        port map (
            p_pwmSync_clk_in   => s_clk,
            p_pwmSync_rst_n_in => s_rst_n,
            p_pwmSync_mcu_req  => s_mcu_req,
            p_pwmSync_pwm_sync => s_pwm_sync,
            p_pwmSync_pwm_freq => s_pwm_freq
        );

    -- main clock process
    process is
    begin
        wait for C_CLK_PER/2;
        s_clk <= not s_clk;
    end process;

    -- reset
    s_rst_n <= '0', '1' after 500 ns;

    -- MCU request stimulus
    process is
        -- procedure for generating MCU requests
        procedure gen_mcu_req (constant period : in time;
                               constant N      : in integer) is
            variable mr_high : time := 10 us;
            variable mr_low  : time := period - mr_high;
        begin
            for i in 0 to N-1 loop
                s_mcu_req <= '1';
                wait for mr_high;
                s_mcu_req <= '0';
                wait for mr_low;
            end loop;
        end procedure;
    begin
        s_mcu_req <= '0';
        wait for 100 us;
        loop
            gen_mcu_req(C_FREQ_20_PERIOD, C_MCU_N_PERIOD);
            gen_mcu_req(C_FREQ_16_PERIOD, C_MCU_N_PERIOD);
            gen_mcu_req(C_FREQ_10_PERIOD, C_MCU_N_PERIOD);
            gen_mcu_req(C_FREQ_16_PERIOD, C_MCU_N_PERIOD);
            gen_mcu_req(C_FREQ_20_PERIOD, C_MCU_N_PERIOD);
            wait for 200 us;
        end loop;
    end process;

end architecture test;