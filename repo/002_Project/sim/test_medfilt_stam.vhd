library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;
use work.pwm_pack.all;

entity test_medfilt_stam is
end entity test_medfilt_stam;

architecture behavioral of test_medfilt_stam is
	constant C_SYSTEM_CLOCK_SEMIPERIOD : time := 100 pS;

	constant BSIZE : natural := 12;

	signal clk : std_logic := '0';      -- System clock
	signal yout : integer := 0; 
	signal rst_n : std_logic := '1';    -- System reset

	signal x1           : std_logic_vector(BSIZE - 1 downto 0) := (others => '0');
	signal x2           : std_logic_vector(BSIZE - 1 downto 0) := (others => '0');
	signal x3           : std_logic_vector(BSIZE - 1 downto 0) := (others => '0');
	signal y            : std_logic_vector(BSIZE - 1 downto 0) := (others => '0');
	signal rdy_i, rdy_o : std_logic                            := '0';

	procedure SET_X(val1      : in integer;
	                val2      : in integer;
	                val3      : in integer;
	                signal x1 : out std_logic_vector(BSIZE - 1 downto 0);
	                signal x2 : out std_logic_vector(BSIZE - 1 downto 0);
	                signal x3 : out std_logic_vector(BSIZE - 1 downto 0)
	               ) is
	begin
		x1 <= std_logic_vector(to_signed(val1, BSIZE));
		x2 <= std_logic_vector(to_signed(val2, BSIZE));
		x3 <= std_logic_vector(to_signed(val3, BSIZE));
	end procedure;

begin

	DUT : entity work.medfilt_stam
		generic map(
			BSIZE => BSIZE
		)
		port map(
			clk_i   => clk,
			rdy_i   => rdy_i,
			rdy_o   => rdy_o,
			rst_n_i => rst_n,
			x1_i    => x1,
			x2_i    => x2,
			x3_i    => x3,
			y_o     => y
		);

	-- Reset
	reset_proc : process
	begin
		rst_n <= '0';
		wait for 4 * C_SYSTEM_CLOCK_SEMIPERIOD;
		wait until clk <= '0';
		rst_n <= '1';
		wait;
	end process;

	-- System clock generator
	system_clk : process
	begin
		clk <= '0';
		wait for C_SYSTEM_CLOCK_SEMIPERIOD;
		clk <= '1';
		wait for C_SYSTEM_CLOCK_SEMIPERIOD;
	end process;

	yout <= to_integer(signed(y));

	stimuli : process
	begin
		wait until rst_n = '1';
		wait until clk = '1';           -- Pulse three times to account for pipeline delay
		wait until clk = '1';
		wait until clk = '1';
		rdy_i <= '1';
		SET_X(1, 3, 5, x1, x2, x3);
		wait until clk = '1';
		rdy_i <= '0';
		wait until rdy_o = '1';
		wait until clk = '1';
		assert yout = 3 report "Wrong value" severity failure;
		wait until clk = '1';
		rdy_i <= '1';
		SET_X(5, 1, 3, x1, x2, x3);
		wait until clk = '1';
		rdy_i <= '0';
		wait until rdy_o = '1';
		wait until clk = '1';
		assert yout = 3 report "Wrong value" severity failure;
		wait until clk = '1';
		rdy_i <= '1';
		SET_X(3, 1, 5, x1, x2, x3);
		wait until clk = '1';
		rdy_i <= '0';
		wait until rdy_o = '1';
		wait until clk = '1';
		assert yout = 3 report "Wrong value" severity failure;
		wait until clk = '1';
		rdy_i <= '1';
		SET_X(2, -8, 4, x1, x2, x3);
		wait until clk = '1';
		rdy_i <= '0';
		wait until rdy_o = '1';
		wait until clk = '1';
		assert yout = 2 report "Wrong value" severity failure;
		wait until clk = '1';
		rdy_i <= '1';
		SET_X(-4, -4, -4, x1, x2, x3);
		wait until clk = '1';
		rdy_i <= '0';
		wait until rdy_o = '1';
		wait until clk = '1';
		assert yout = -4 report "Wrong value" severity failure;
		wait until clk = '1';
		rdy_i <= '1';
		SET_X(-4, -3, -4, x1, x2, x3);
		wait until clk = '1';
		rdy_i <= '0';
		wait until rdy_o = '1';
		wait until clk = '1';
		assert yout = -4 report "Wrong value" severity failure;
		wait until clk = '1';
		rdy_i <= '1';
		SET_X(-4, -6, -10, x1, x2, x3);
		wait until clk = '1';
		rdy_i <= '0';
		wait until rdy_o = '1';
		wait until clk = '1';
		assert yout = -6 report "Wrong value" severity failure;
		
	end process;

end architecture behavioral;
