----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_speed_calculator.vhd
-- module name      Speed calculator testbench
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Parallel Communication Interface Block Simulation
----------------------------------------------------------------------------------------------
-- create date      08.07.2019
-- Revision:
--      Revision 0.01 - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Updated speed_calculator component with
--                      changed variable names
--
--      Revision 0.03 by david.pavlovic
--                    - Updated input angle data. Angles are read from the PLECS simulation file.
--                    - Ouptut speed data is saved in spee_out.txt file
--
--      Revision 0.04 by dario.drvenkar
--                    - New module for speed estimation.
--
--      Revision 0.05 by danijela.skugor
--                    - Update speed estimator module port names
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use STD.textio.all;
use ieee.std_logic_textio.all;
use std.env.finish;

entity test_speed_calculator is
    -- empty
end entity test_speed_calculator;

architecture behavioral of test_speed_calculator is

    -- Simulation parameters
    constant C_SAMPLE_TIME_IN_US          : real    := 1.0;
    constant C_SAMPLING_CLOCK_SEMI_PERIOD : time    := 500 ns;
    constant C_SYSTEM_CLOCK_SEMIPERIOD    : time    := 5 ns;
    constant C_SAMPLES                    : natural := 50000;
    constant C_ANGLE_VALID                : time    := 1 us;

    constant C_ROTATION_SPEED : real := 9000.0; -- Mechanical rotation in RPM
    constant C_TWO_PI         : real := 2 * MATH_PI;
    constant C_RESOLVER_Q     : real := 4096.0; -- Quantization steps

    -- Resolver simulation engine
    signal mechanical_angle                 : real                          := 0.0;
    signal mechanical_angle_delta_increment : real                          := 0.0;
    signal angle                            : std_logic_vector(11 downto 0) := (others => '0'); -- Coded angle output

    signal clk              : std_logic := '0'; -- System clock
    signal rst_n            : std_logic := '1'; -- System reset
    signal angle_val_p_in   : std_logic := '0';
    signal sample_clk       : std_logic := '0'; -- Sampling clock
    signal sample_p         : std_logic := '0'; -- Sampling pulse
    signal mcu_req          : std_logic := '0'; -- MCU request

    signal speed   : std_logic_vector(23 downto 0) := (others => '0'); -- Measured speed
    signal f_speed : real := 0.0; -- Measured speed in floating point

    -- debug
    signal speed_dut : signed(23 downto 0);

    file file_i : text;
    file file_o : text;

    -- DUT declaration

    component speed_estimator is
--        generic (
--            G_FILTER_CUTOFF_FREQ : string := "100_Hz" -- "100_Hz", "1000_Hz" or "2000_Hz"
--        );
        port(
            p_speed_est_clk_in          : in  std_logic;  --! System clock input
            p_speed_est_rst_n_in        : in  std_logic;  --! Synchronous reset
            p_speed_est_angle_val_p_in  : in  std_logic;
            p_speed_est_angle_in        : in  std_logic_vector(11 downto 0); --! Angle input from resolver
            p_speed_est_speed_out       : out std_logic_vector(23 downto 0)  --! Speed output
        ); 
    end component;
   
begin

    -- DUT
    dut : component speed_estimator
--        generic map (
--            G_FILTER_CUTOFF_FREQ => "100_Hz"
--        )
        port map(
            p_speed_est_clk_in          => clk,
            p_speed_est_rst_n_in        => rst_n,
            p_speed_est_angle_val_p_in  => angle_val_p_in,
            p_speed_est_angle_in        => angle,
            p_speed_est_speed_out       => speed
        );

    -- Reset
    reset_proc : process
    begin
        rst_n <= '0';
        wait for 4 * C_SYSTEM_CLOCK_SEMIPERIOD;
        wait until clk <= '0';
        rst_n <= '1';
        wait;
    end process;

    -- System clock generator
    system_clk : process
    begin
        clk <= '0';
        wait for C_SYSTEM_CLOCK_SEMIPERIOD;
        clk <= '1';
        wait for C_SYSTEM_CLOCK_SEMIPERIOD;
    end process;
    
    -- Resolver update signal
    angle_val : process
    begin
        angle_val_p_in <= '0';
        wait for C_ANGLE_VALID; 
        angle_val_p_in <= '1';
        wait for C_SYSTEM_CLOCK_SEMIPERIOD * 2;
    end process;

    sampling_clk : process
    begin
        -- This clock has to be synchronous with the system clock
        wait until clk = '1';
        sample_clk <= '0';
        wait for C_SAMPLING_CLOCK_SEMI_PERIOD;
        wait until clk = '1';
        sample_clk <= '1';
        wait for C_SAMPLING_CLOCK_SEMI_PERIOD;
    end process;

    -- This block generates an accessory sampling signal in pulse form, required
    -- to update the resolver emulation process
    sampling_pulse : process
    begin
        wait until sample_clk = '1';
        sample_p <= '1';
        wait until clk = '1';
        sample_p <= '0';
    end process;

    -- Resolver emulation
    -- resolver_proc : process(clk)
        -- variable int_angle : integer := 0;
    -- begin
        -- if Rising_edge(clk) then
            -- if (sample_p = '1') then
                -- mechanical_angle_delta_increment <= C_ROTATION_SPEED * C_SAMPLE_TIME_IN_US * C_RESOLVER_Q / 60.0 / 1000000;
                -- mechanical_angle                 <= mechanical_angle + mechanical_angle_delta_increment;
                -- if (mechanical_angle > 4095.0) then
                    -- mechanical_angle <= mechanical_angle - 4095.0;
                -- end if;
                -- int_angle := integer(mechanical_angle);
                -- angle <= std_logic_vector(to_unsigned(int_angle, 12));
            -- end if;
        -- end if;
    -- end process;

   -- speed_dut <= << signal dut.s_speed : signed(36 downto 0) >>;
   speed_dut <= (others => '0');

    process
        variable v_line_i : line;
        variable v_data_i : integer;
    begin
        angle <= (others => '0');
        wait until rst_n = '1';
        for i in 0 to 0 loop
            --file_open(file_i, "C:/Users/david.pavlovic/Documents/work/repos/bfi/bfi_fpga/repo/002_Project/sim/angle_plecs_ele_ipm_15000rpm_round.txt", read_mode);
--            file_open(file_i, "./../../../../../repo/002_Project/sim/angle_plecs_ele_spm_18000rpm_round.txt", read_mode);
--            file_open(file_i, "./../../../../../repo/002_Project/sim/angle_plecs_round_TEMAresolver_50rpm.txt", read_mode);
            file_open(file_i, "./../../../../../repo/002_Project/sim/angle_plecs_round_IPMresolver_neg15000rpm.txt", read_mode);
            --file_open(file_i, "C:/Users/david.pavlovic/Documents/work/repos/bfi/bfi_fpga/repo/002_Project/sim/angle_plecs_18000rpm_round.txt", read_mode);
            while not endfile(file_i) loop
            -- for j in 0 to 29999 loop
                wait until rising_edge(sample_p);
                -- read sample
                readline(file_i, v_line_i);
                read(v_line_i, v_data_i);
                v_data_i := v_data_i;
                angle <= std_logic_vector(to_unsigned(v_data_i, angle'length));
            end loop;
            file_close(file_i);
            report "File closed (" & integer'image(i) & "/1)";
        end loop;
        finish(0);
    end process;

    process
        variable v_line_o : line;
        variable v_data_o : integer;
    begin
        file_open(file_o, "speed_out.txt", write_mode);
        for i in 0 to C_SAMPLES-1 loop
            wait until rising_edge(sample_p);
            -- write sample
            v_data_o := to_integer(speed_dut);
            write(v_line_o, v_data_o);
            writeline(file_o, v_line_o);
        end loop;
        file_close(file_o);
        wait;
    end process;

    f_speed <= real(to_integer(signed(speed)))/(2**10);

end architecture behavioral;
