----------------------------------------------------------------------------------------------
--                                   ALTAM SYSTEMS SL
--
--          (c) Copyright 2018 - 2020, ALTAM SYSTEMS SL, Barcelona, Spain
--					
--                                 All rights reserved
--
-- 
-- COPYRIGHT NOTICE:  
-- All information contained herein is, and remains the property of ALTAM Systems S.L. 
-- and its suppliers, if any.  
-- The intellectual and technical concepts contained herein are proprietary to 
-- ALTAM Systems S.L. and its suppliers and may be covered by Spanish and Foreign Patents,
-- patents in process, and are protected by trade secret or copyright law.
-- Dissemination of this information, modification or reproduction of this material
-- is strictly forbidden unless prior written permission is obtained from ALTAM Systems S.L.
--
----------------------------------------------------------------------------------------------
--! @file       test_ths1206_model.vhd     
--! @brief      VHDL testbench for the ths1206 simulation model
--! @details	This is not a complete coverage test, but a simplified battery just to
--!				test the expected funcions of use in the BFI project.
--! @author     Jorge Sanchez (jsanchez@altamsys.com)    
--! @date	11.05.2020
----------------------------------------------------------------------------------------------
-- CHANGES:
--		11.05.2020 (JSQ) - Creation date
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity test_ths1206_model is
end entity test_ths1206_model;

architecture bhv of test_ths1206_model is

	component ths1206_model is
		port(
			-- ADC control lines
			p_adc_conv_n_in : in    std_logic; -- conversion trigger input 
			p_adc_cs0_n_in  : in    std_logic; -- chip select 0 
			p_adc_cs1_in    : in    std_logic; -- chip select 1 
			p_adc_rd_n_in   : in    std_logic; -- read trigger
			p_adc_wr_n_in   : in    std_logic; -- write enable

			p_adc_dav_n_out : out   std_logic; -- data available flag

			-- ADC data
			p_adc_data_bi   : inout std_logic_vector(11 downto 0) -- Data bus (including D10/RA0 and D11/RA1)

		);
	end component ths1206_model;

	signal conv_n   : std_logic                     := '1';
	signal p_conv_n : std_logic                     := '1';
	signal cs0_n    : std_logic                     := '1';
	signal cs1      : std_logic                     := '1';
	signal rd_n     : std_logic                     := '1';
	signal wr_n     : std_logic                     := '1';
	signal dav_n    : std_logic                     := '0';
	signal data_bi  : std_logic_vector(11 downto 0) := (others => 'Z');

	signal select_clk : std_logic := '0'; -- conv_n input selector
	signal cclk       : std_logic := '0';

	signal cr0_reg : std_logic_vector(9 downto 0) := (others => '0');
	signal cr1_reg : std_logic_vector(9 downto 0) := (others => '0');

	constant CR0_DEFAULT : std_logic_vector(9 downto 0) := "0001000000";
	constant CR1_DEFAULT : std_logic_vector(9 downto 0) := "0000110000";

	-- CR0 bits
	constant CR0_VREF   : natural := 0;
	constant CR0_MODE   : natural := 1;
	constant CR0_PD     : natural := 2;
	constant CR0_CHSEL0 : natural := 3;
	constant CR0_CHSEL1 : natural := 4;
	constant CR0_DIFF0  : natural := 5;
	constant CR0_DIFF1  : natural := 6;
	constant CR0_SCAN   : natural := 7;
	constant CR0_TEST0  : natural := 8;
	constant CR0_TEST1  : natural := 9;

	-- CR1 bits
	constant CR1_RESET  : natural := 0;
	constant CR1_FRST   : natural := 1;
	constant CR1_TRIG0  : natural := 2;
	constant CR1_TRIG1  : natural := 3;
	constant CR1_DATA_T : natural := 4;
	constant CR1_DATA_P : natural := 5;
	constant CR1_RW     : natural := 6;
	constant CR1_BIN2S  : natural := 7;
	constant CR1_OFFSET : natural := 8;
	constant CR1_RBACK  : natural := 9;

	constant CR0_SEL : std_logic_vector(1 downto 0) := "00";
	constant CR1_SEL : std_logic_vector(1 downto 0) := "01";

	constant ACCESS_TIME       : time := 25 ns;
	constant SAMPLING_CLK_TIME : time := 1 us;

	type t_testtype is (DEBUG, TESTMODE, SINGLE_CONV_ONE, SINGLE_CONV_FOUR, SINGLE_CONVERSION_AUTOSCAN, CONTINUOUS_FOUR);
	constant TEST_TYPE : t_testtype := SINGLE_CONVERSION_AUTOSCAN;

begin

	DUT : component ths1206_model
		port map(
			p_adc_conv_n_in => p_conv_n,
			p_adc_cs0_n_in  => cs0_n,
			p_adc_cs1_in    => cs1,
			p_adc_rd_n_in   => rd_n,
			p_adc_wr_n_in   => wr_n,
			p_adc_dav_n_out => dav_n,
			p_adc_data_bi   => data_bi
		);

	p_conv_n <= conv_n when (select_clk = '0') else cclk;

	process
	begin
		cclk <= '0';
		wait for SAMPLING_CLK_TIME;
		cclk <= '1';
		wait for SAMPLING_CLK_TIME;
	end process;

	process
	begin
		cr0_reg <= CR0_DEFAULT;
		cr1_reg <= CR1_DEFAULT;

		case TEST_TYPE is

			when DEBUG =>
				-- Reset
				wait for ACCESS_TIME;
				data_bi <= X"401";
				cs1     <= '1';
				cs0_n   <= '0';
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;
				data_bi <= X"400";
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;

				cr0_reg <= "00" & X"EC";
				wait for 1 ps;
				data_bi <= CR0_SEL & cr0_reg;
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;

				cr1_reg <= "11" & X"1C";
				wait for 1 ps;
				data_bi <= CR1_SEL & cr1_reg;
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;
				data_bi <= (others => 'Z');
				wait for ACCESS_TIME;

				-- Read sequentially the CR registers
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"0EC" report "Wrong debug return value for CR0" severity failure;
				rd_n <= '1';
				wait for ACCESS_TIME;
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"31C" report "Wrong debug return value for CR1" severity failure;
				rd_n <= '1';

				assert False report "End of test!" severity Note;
				wait;

			when TESTMODE =>
				-- Reset
				wait for ACCESS_TIME;
				data_bi                             <= X"401";
				cs1                                 <= '1';
				cs0_n                               <= '0';
				wr_n                                <= '0';
				wait for ACCESS_TIME;
				wr_n                                <= '1';
				wait for ACCESS_TIME;
				data_bi                             <= X"400";
				wr_n                                <= '0';
				wait for ACCESS_TIME;
				wr_n                                <= '1';
				wait for ACCESS_TIME;
				-- Enable test mode
				cr0_reg(CR0_TEST1 downto CR0_TEST0) <= "01";
				wait for 1 ps;
				data_bi                             <= CR0_SEL & cr0_reg;
				wr_n                                <= '0';
				wait for ACCESS_TIME;
				wr_n                                <= '1';
				wait for ACCESS_TIME;
				data_bi                             <= (others => 'Z');
				wait for ACCESS_TIME;
				rd_n                                <= '0';
				wait for ACCESS_TIME;
				assert data_bi = std_logic_vector(to_unsigned(4095, 12)) report "Wrong test value read" severity failure;
				rd_n                                <= '1';
				wait for ACCESS_TIME;

				cr0_reg(CR0_TEST1 downto CR0_TEST0) <= "10";
				wait for 1 ps;
				data_bi                             <= CR0_SEL & cr0_reg;
				wr_n                                <= '0';
				wait for ACCESS_TIME;
				wr_n                                <= '1';
				wait for ACCESS_TIME;
				data_bi                             <= (others => 'Z');
				wait for ACCESS_TIME;
				rd_n                                <= '0';
				wait for ACCESS_TIME;
				assert data_bi = std_logic_vector(to_unsigned(2047, 12)) report "Wrong test value read" severity failure;
				rd_n                                <= '1';
				wait for ACCESS_TIME;

				cr0_reg(CR0_TEST1 downto CR0_TEST0) <= "11";
				wait for 1 ps;
				data_bi                             <= CR0_SEL & cr0_reg;
				wr_n                                <= '0';
				wait for ACCESS_TIME;
				wr_n                                <= '1';
				wait for ACCESS_TIME;
				data_bi                             <= (others => 'Z');
				wait for ACCESS_TIME;
				rd_n                                <= '0';
				wait for ACCESS_TIME;
				assert data_bi = std_logic_vector(to_unsigned(0, 12)) report "Wrong test value read" severity failure;
				rd_n                                <= '1';

				assert False report "End of test!" severity Note;
				wait;

			when SINGLE_CONV_ONE =>
				-- Reset
				wait for ACCESS_TIME;
				data_bi <= X"401";
				cs1     <= '1';
				cs0_n   <= '0';
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;
				data_bi <= X"400";
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;

				cr0_reg(CR0_CHSEL1 downto CR0_CHSEL0) <= "00"; -- Single channel (1)
				cr0_reg(CR0_SCAN)                     <= '0'; -- No autoscan
				cr0_reg(CR0_MODE)                     <= '1'; -- single conversion mode
				wait for 1 ps;
				data_bi                               <= CR0_SEL & cr0_reg;
				wr_n                                  <= '0';
				wait for ACCESS_TIME;
				wr_n                                  <= '1';
				wait for ACCESS_TIME;
				data_bi                               <= (others => 'Z');

				conv_n <= '0';
				wait for 500 ns;
				conv_n <= '1';

				wait for ACCESS_TIME;
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"111" report "Wrong read value" severity failure;
				rd_n <= '1';

				assert False report "End of test!" severity Note;
				wait;

			when SINGLE_CONV_FOUR =>
				-- Reset
				wait for ACCESS_TIME;
				data_bi <= X"401";
				cs1     <= '1';
				cs0_n   <= '0';
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;
				data_bi <= X"400";
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;

				-- Four samples single conversion
				cr0_reg(CR0_CHSEL1 downto CR0_CHSEL0) <= "11";
				cr0_reg(CR0_SCAN)                     <= '0';
				cr0_reg(CR0_MODE)                     <= '1';
				wait for 1 ps;
				data_bi                               <= CR0_SEL & cr0_reg;
				wr_n                                  <= '0';
				wait for ACCESS_TIME;
				wr_n                                  <= '1';
				wait for ACCESS_TIME;
				cr1_reg(CR1_TRIG1 downto CR1_TRIG0)   <= "01";
				wait for 1 ps;
				data_bi                               <= CR1_SEL & cr1_reg;
				wr_n                                  <= '0';
				wait for ACCESS_TIME;
				wr_n                                  <= '1';
				wait for ACCESS_TIME;

				data_bi <= (others => 'Z');
				wait for 500 ns;
				-- Single conversion
				conv_n  <= '0';
				wait for 900 ns;
				conv_n  <= '1';
				--wait until dav_n = '0';
				wait for 100 ns;
				-- Read sample
				rd_n    <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"441" report "Error reading ADC: expected 0x441" severity failure;

				rd_n <= '1';
				wait for ACCESS_TIME;
				-- Read sample
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"442" report "Error reading ADC: expected 0x442" severity failure;
				rd_n <= '1';
				wait for ACCESS_TIME;
				-- Read sample
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"443" report "Error reading ADC: expected 0x443" severity failure;
				rd_n <= '1';
				wait for ACCESS_TIME;
				-- Read sample
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"444" report "Error reading ADC: expected 0x444" severity failure;
				rd_n <= '1';
				wait for ACCESS_TIME;

				assert False report "End of test!" severity Note;
				wait;

			when SINGLE_CONVERSION_AUTOSCAN =>
				-- Test the autoscan
				-- Reset
				wait for ACCESS_TIME;
				data_bi <= X"401";
				cs1     <= '1';
				cs0_n   <= '0';
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;
				data_bi <= X"400";
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;

				-- Four samples single conversion with autoscan
				cr0_reg(CR0_CHSEL1 downto CR0_CHSEL0) <= "11";
				cr0_reg(CR0_SCAN)                     <= '1';
				cr0_reg(CR0_MODE)                     <= '1';
				wait for 1 ps;
				data_bi                               <= CR0_SEL & cr0_reg;
				wr_n                                  <= '0';
				wait for ACCESS_TIME;
				wr_n                                  <= '1';
				wait for ACCESS_TIME;
				data_bi                               <= (others => 'Z');

				cr1_reg(CR1_TRIG1 downto CR1_TRIG0) <= "00";
				wait for 1 ps;
				data_bi                             <= CR1_SEL & cr1_reg;
				wr_n                                <= '0';
				wait for ACCESS_TIME;
				wr_n                                <= '1';
				wait for ACCESS_TIME;

				data_bi <= (others => 'Z');
				wait for 500 ns;
				-- Single conversion
				conv_n  <= '0';
				wait for 900 ns;
				conv_n  <= '1';
				--wait until dav_n = '0';
				wait for 100 ns;
				-- Read sample
				rd_n    <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"111" report "Error reading ADC: expected 0x111" severity failure;

				rd_n <= '1';
				wait for ACCESS_TIME;
				-- Read sample
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"221" report "Error reading ADC: expected 0x221" severity failure;
				rd_n <= '1';
				wait for ACCESS_TIME;
				-- Read sample
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"331" report "Error reading ADC: expected 0x331" severity failure;
				rd_n <= '1';
				wait for ACCESS_TIME;
				-- Read sample
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"441" report "Error reading ADC: expected 0x441" severity failure;
				rd_n <= '1';
				wait for ACCESS_TIME;

				assert False report "End of test!" severity Note;
				wait;

			when CONTINUOUS_FOUR =>
				-- Test the autoscan
				-- Reset
				wait for ACCESS_TIME;
				data_bi <= X"401";
				cs1     <= '1';
				cs0_n   <= '0';
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;
				data_bi <= X"400";
				wr_n    <= '0';
				wait for ACCESS_TIME;
				wr_n    <= '1';
				wait for ACCESS_TIME;

				-- Four samples single conversion with autoscan
				cr0_reg(CR0_CHSEL1 downto CR0_CHSEL0) <= "11";
				cr0_reg(CR0_SCAN)                     <= '1';
				cr0_reg(CR0_MODE)                     <= '0';
				wait for 1 ps;
				data_bi                               <= CR0_SEL & cr0_reg;
				wr_n                                  <= '0';
				wait for ACCESS_TIME;
				wr_n                                  <= '1';
				wait for ACCESS_TIME;
				data_bi                               <= (others => 'Z');

				cr1_reg(CR1_TRIG1 downto CR1_TRIG0) <= "00";
				wait for 1 ps;
				data_bi                             <= CR1_SEL & cr1_reg;
				wr_n                                <= '0';
				wait for ACCESS_TIME;
				wr_n                                <= '1';
				wait for ACCESS_TIME;

				data_bi <= (others => 'Z');
				wait for 100 ns;
				wait until cclk = '1';
				select_clk <= '1';

				wait until dav_n = '0';
				wait for 100 ns;
				-- Read sample
				rd_n   <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"111" report "Error reading ADC: expected 0x111" severity failure;

				rd_n <= '1';
				wait for ACCESS_TIME;
				-- Read sample
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"221" report "Error reading ADC: expected 0x221" severity failure;
				rd_n <= '1';
				wait for ACCESS_TIME;
				-- Read sample
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"331" report "Error reading ADC: expected 0x331" severity failure;
				rd_n <= '1';
				wait for ACCESS_TIME;
				-- Read sample
				rd_n <= '0';
				wait for ACCESS_TIME;
				assert data_bi = X"441" report "Error reading ADC: expected 0x441" severity failure;
				rd_n <= '1';
				wait for ACCESS_TIME;

				assert False report "End of test!" severity Note;
				wait;

		end case;
	end process;

end architecture;
