----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_res_driver.vhd
-- module name      res_driver_tb
-- author           Dario Drvenkar (dario.drvenkar@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100TCSG324-1Q
-- brief            Resolver Driver test module
----------------------------------------------------------------------------------------------
-- create date      12.02.2021
-- Revision:
--      Revision 0.01 by dario.drvenkar
--                    - File Created
--      Revision 0.02 by danijela.skugor
--                    -- Changed input to mcu_req input port of mov_avg_filter_top
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

entity test_movAVG_filter is
end entity test_movAVG_filter;

architecture RTL of test_movAVG_filter is

    constant C_SYSTEM_CLOCK_PERIOD   : time := 10 ns;  -- period of 100 MHz
	constant C_SAMPLING_CLOCK_PERIOD : time := 1 us;   -- period of 1 MHz 


	signal clk      : std_logic                     := '0';
	signal rst_n    : std_logic                     := '0';
	signal sample_p : std_logic                     := '0'; -- ADC new sample pulse
	signal sync_p   : std_logic                     := '0'; -- PWM synchro pulse
	signal rdy_p    : std_logic                     := '0'; -- Filter new sample ready
	signal ch1_i    : std_logic_vector(11 downto 0) := (others => '0');
	signal ch2_i    : std_logic_vector(11 downto 0) := (others => '0');
	signal ch3_i    : std_logic_vector(11 downto 0) := (others => '0');
	signal ch1_o    : std_logic_vector(11 downto 0) := (others => '0');
	signal ch2_o    : std_logic_vector(11 downto 0) := (others => '0');
	signal ch3_o    : std_logic_vector(11 downto 0) := (others => '0');
	
	signal s_mcu_req_sync : std_logic;

	-- #### Setup here the input and output test files
	constant C_INPUT_FILE_NAME  : string    := "./../../../../../repo/002_Project/sim/Simulation8.dat";
	constant C_OUTPUT_FILE_NAME : string    := "Simulation8_output.dat";

	file fInputPtr              : TEXT open READ_MODE is C_INPUT_FILE_NAME;
	file fOutputPtr             : TEXT open WRITE_MODE is C_OUTPUT_FILE_NAME;
	signal eof                  : std_logic := '0';
begin

    DUT : entity work. mov_avg_filter_top
        generic map(
            G_MOV_AVG_FILTER_TOP_DATA     => 12,     -- Data size);
            G_MOV_AVG_FILTER_TOP_AVG_LEN  => 13
        )
        port map(
            p_mov_avg_filter_top_rst_n_in    => rst_n,           -- System reset input
            p_mov_avg_filter_top_clk_in      => clk,             -- System clock input
            p_mov_avg_filter_top_sample_p_in => sample_p,        -- Sample pulse from ADC
            p_mov_avg_filter_top_mcu_req_in  => sync_p,          -- MCU request (start of PWM cycle)
            p_mov_avg_filter_top_rdy_p_out   => rdy_p,           -- Output ready pulse
            p_mov_avg_filter_top_phase_a_in      => ch1_i,           -- Stream input 1
            p_mov_avg_filter_top_phase_b_in      => ch2_i,           -- Stream input 2
            p_mov_avg_filter_top_phase_c_in      => ch3_i,           -- Stream input 3
            p_mov_avg_filter_top_phase_a_out     => ch1_o,           -- Sample output 1
            p_mov_avg_filter_top_phase_b_out     => ch2_o,           -- Sample output 2
            p_mov_avg_filter_top_phase_c_out     => ch3_o            -- Sample output 3
     );

	-- Stimulus read from simulated data file, preprocessed in matlab
	-- The stimulus format has to be as follows (each line in the input data file):
	-- Phase-A Phase-B Phase-C Sync
	stimuli : process
		variable fLine                              : line;
		variable ch1_sample, ch2_sample, ch3_sample : integer;
		variable data_pwm_sync                      : integer;
	begin
		wait until rst_n = '1';

		while (not ENDFILE(fInputPtr)) loop
			wait until sample_p = '1';
			READLINE(fInputPtr, fLine);
			READ(fLine, ch1_sample);
			READ(fLine, ch2_sample);
			READ(fLine, ch3_sample);
			READ(fLine, data_pwm_sync);

			ch1_i  <= std_logic_vector(to_unsigned(ch1_sample, 12));
			ch2_i  <= std_logic_vector(to_unsigned(ch2_sample, 12));
			ch3_i  <= std_logic_vector(to_unsigned(ch3_sample, 12));
--			sync_p <= to_unsigned(data_pwm_sync, 1)(0);
		end loop;

		wait until sample_p = '1';
		eof <= '1';                     -- End of test
		wait;
	end process;

	-- Log output process. Write the filter output into a file
	-- The output format is (each line):
	--	Phase-A Phase-B Phase-C
	logoutput : process
		variable fLine                              : line;
		variable ch1_sample, ch2_sample, ch3_sample : integer;
	begin
		wait until rdy_p = '1';
		ch1_sample := to_integer(signed(ch1_o));
		ch2_sample := to_integer(signed(ch2_o));
		ch3_sample := to_integer(signed(ch3_o));
		WRITE(fLine, ch1_sample);
		WRITE(fLine, ch2_sample);
		WRITE(fLine, ch3_sample);
		WRITELINE(fOutputPtr, fLine);
		if (eof = '1') then
			wait;
		end if;
	end process;

	-- Reset
	reset_proc : process
	begin
		rst_n <= '1';
		wait for 37 ns;
		rst_n <= '0';
        wait for 56 ns;
		rst_n <= '1';
		wait;
	end process;

	-- System clock generator
	system_clk : process
	begin
		clk <= '0';
		wait for C_SYSTEM_CLOCK_PERIOD / 2;
		clk <= '1';
		wait for C_SYSTEM_CLOCK_PERIOD / 2;
	end process;

	sampling_clk : process
	begin
		-- This clock has to be synchronous with the system clock
		wait until rising_edge(clk); 
		sample_p <= '1';
		wait until rising_edge(clk);
		-- period of 100 MHz
		sample_p <= '0';
		wait for C_SAMPLING_CLOCK_PERIOD - C_SYSTEM_CLOCK_PERIOD;  -- period of 1 MHz - period of 100 MHz
	end process;	
	
	MCU_req_p : process   
	begin
		-- This clock has to be synchronous with the system clock
		wait until rising_edge(clk); 
		sync_p <= '0';
		wait for 38.88 us;
		wait until rising_edge(clk); 
		sync_p <= '1';
		wait for 11.12 us;
		
	end process;
	
	 pedgeflt : entity work.edge_detector_p
        generic map(
            G_REG => 1
        )
        port map(
            p_edp_clk_in     => clk,
            p_edp_rst_n_in   => rst_n,
            p_edp_sig_in     => sync_p,
            p_edp_sig_re_out => s_mcu_req_sync,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
        );

end architecture RTL;
