# Bash script for current sampling block

# Common blocks
xvhdl -2008 -nolog ../../001_Common/src/pulse_rate_divider.vhd
xvhdl -2008 -nolog ../../001_Common/src/edge_detector_p.vhd
xvhdl -nolog ../../001_Common/src/cdc_simple_synchro.vhd
xvhdl -nolog ../../001_Common/src/pulse_synchronizer.vhd
xvhdl -nolog ../../001_Common/src/pll_clk_gen.vhd
xvhdl -nolog ../../001_Common/src/reset_generator.vhd
xvhdl -nolog ../src/lpf/pwm_pack.vhd

# LPF blocks
xvhdl -nolog ../src/lpf/lpf_pwm_fctrl.vhd
xvhdl -nolog ../src/lpf/medfilt_bblock.vhd
xvhdl -nolog ../src/lpf/medfilt_sampler.vhd
xvhdl -nolog ../src/lpf/medfilt_stam.vhd
xvhdl -2008 -nolog ../src/lpf/medfilt_sequencer.vhd
xvhdl -nolog ../src/lpf/medfilt_block.vhd
xvhdl -nolog ../src/lpf/lpf_varfreq_median.vhd
xvhdl -2008 -nolog ../src/lpf/frame_retimer.vhd
xvhdl -2008 -nolog ../src/lpf/synchronous_filter.vhd

xvhdl -nolog test_synchronous_filter.vhd

# glbl library is required when the design uses UNISIM primitives
xvlog $XILINX_VIVADO/data/verilog/src/glbl.v

xelab -debug typical test_synchronous_filter -s top_sim -nolog work.glbl
xsim -gui top_sim

# Cleanup
rm *.log
rm *.pb
rm -rf xsim.dir
rm *.jou
rm *.wdb
