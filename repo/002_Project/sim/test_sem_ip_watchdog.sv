//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             test_sem_ip_watchdog.vhd
// module name      test_sem_ip_watchdog
// author           david.pavlovic (david.pavlovic@rimac-automobili.com)
// project name     BFI
// target device    XA7A100T-CSG324Q1
// brief            SEM IP watchdog testbench module
//--------------------------------------------------------------------------------------------
// create date      26.02.2021
// Revision:
//      Revision 0.01 by david.pavlovic
//                    - File Created
//--------------------------------------------------------------------------------------------
// NOTE: This is a SystemVerilog file
//--------------------------------------------------------------------------------------------

module test_sem_ip_watchdog;

    timeunit 1ns/1ps;

    // parameters
    parameter C_CLK_PER = 10.0;

    // signals
    logic s_clk                   = '0;
    logic s_rst_n                 = '0;
    // SEM IP status signals
    logic s_status_heartbeat;
    logic s_status_initialization  = '0;
    logic s_status_observation     = '0;
    logic s_status_correction      = '0;
    logic s_status_classification  = '0;
    logic s_status_injection       = '0;
    logic s_status_essential       = '0;
    logic s_status_uncorrectable   = '0;
    // fault output
    logic s_icap_grant;
    // fault output
    logic [6:0] s_flt_n_out;

    // auxiliary signals
    logic s_status_heartbeat_p     = '0;
    logic s_status_heartbeat_en    = '0;

    // DUT instance
    sem_ip_watchdog dut
        (
            // main signals
            .p_sem_ip_wd_clk_in                (s_clk),
            .p_sem_ip_wd_rst_n_in              (s_rst_n),
            // SEM IP status signals
            .p_sem_ip_wd_status_heartbeat      (s_status_heartbeat),
            .p_sem_ip_wd_status_initialization (s_status_initialization),
            .p_sem_ip_wd_status_observation    (s_status_observation),
            .p_sem_ip_wd_status_correction     (s_status_correction),
            .p_sem_ip_wd_status_classification (s_status_classification),
            .p_sem_ip_wd_status_injection      (s_status_injection),
            .p_sem_ip_wd_status_essential      (s_status_essential),
            .p_sem_ip_wd_status_uncorrectable  (s_status_uncorrectable),
            // fault output
            .p_sem_ip_wd_icap_grant_out        (s_icap_grant),
            // fault output
            .p_sem_ip_wd_flt_n_out             (s_flt_n_out)
        );

    //-------------------------------------------------------------------------

    assign s_status_heartbeat = s_status_heartbeat_p && s_status_heartbeat_en;

    // clock generator
    always begin
        #(C_CLK_PER/2) s_clk = ~s_clk;
    end

    //-----------------------------------
    // Tasks
    //-----------------------------------

    // generates N cycle active-low reset signal synchronous with clock
    task reset_gen(input int N);
        s_rst_n <= 1'b0;
        repeat (N+1) @(posedge s_clk);
        s_rst_n <= 1'b1;
    endtask : reset_gen;

    // waits for N clock cycles
    task wait_clk(input int N);
        repeat (N)
            @(posedge s_clk);
    endtask : wait_clk

    // generates observation signal
    task gen_observation();
        int delay;
        if (!std::randomize(delay) with { delay inside {[5000:15000]}; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_observation <= 1'b1;
        if (!std::randomize(delay) with { delay inside {[50000:150000]}; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_observation <= 1'b0;
    endtask : gen_observation

    // generates heartbeat signal
    task gen_heartbeat();
        int delay;
        //if (!std::randomize(delay) with { delay inside {[100:160]}; })
        if (!std::randomize(delay) with { delay == 160; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        forever begin
            s_status_heartbeat_p <= 1'b1;
            wait_clk(1);
            s_status_heartbeat_p <= 1'b0;
            wait_clk(delay-1);
        end
    endtask : gen_heartbeat

    // generates heartbeat enable signal
    task gen_heartbeat_en();
        int delay;
        @(posedge s_status_observation);
        //if (!std::randomize(delay) with { delay dist {[200:400]:/95, 25000:/5}; })
        if (!std::randomize(delay) with { delay == 25000; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_heartbeat_en <= 1'b1;
    endtask : gen_heartbeat_en

    // generates observation and heartbeat stimulus
    task gen_observation_and_heartbeat();
        fork
            gen_observation();
            gen_heartbeat_en();
        join
    endtask : gen_observation_and_heartbeat

    // generates correction and uncorrectable
    task gen_correction_and_uncorrectable();
        wait_clk(1000);
        s_status_correction <= 1'b1;
        wait_clk(1000);
        s_status_correction <= 1'b0;
        wait_clk(1000);
        s_status_correction <= 1'b1;
        wait_clk(500);
        s_status_uncorrectable <= 1'b1;
        wait_clk(500);
        s_status_correction <= 1'b0;
    endtask : gen_correction_and_uncorrectable

    // generates fatal error - sets all state signals to HIGH
    task gen_fatal_error();
        wait_clk(1000);
        s_status_initialization <= 1'b1;
        s_status_observation    <= 1'b1;
        s_status_correction     <= 1'b1;
        s_status_classification <= 1'b1;
        s_status_injection      <= 1'b1;
        wait_clk(1000);
        s_status_initialization <= 1'b0;
        s_status_observation    <= 1'b1;
        s_status_correction     <= 1'b0;
        s_status_classification <= 1'b0;
        s_status_injection      <= 1'b0;
        wait_clk(1000);
        s_status_initialization <= 1'b1;
        s_status_observation    <= 1'b1;
        s_status_correction     <= 1'b1;
        s_status_classification <= 1'b1;
        s_status_injection      <= 1'b1;
    endtask : gen_fatal_error

    // generates boot error
    task gen_boot_error();
        int delay;
        //if (!std::randomize(delay) with { delay dist {[200:400]:/95, 25000:/5}; })
        if (!std::randomize(delay) with { delay == 15000000; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_initialization <= 1'b1;
    endtask : gen_boot_error

    // generates initialization timeout error
    task gen_initialization_timeout_error();
        int delay;
        //if (!std::randomize(delay) with { delay dist {[200:400]:/95, 25000:/5}; })
        if (!std::randomize(delay) with { delay inside {[1000:2000]}; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_initialization <= 1'b1;
        if (!std::randomize(delay) with { delay == 3500000; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_initialization <= 1'b0;
    endtask : gen_initialization_timeout_error

    // generates boot error
    task gen_initialization_no_observation_error();
        int delay;
        //if (!std::randomize(delay) with { delay dist {[200:400]:/95, 25000:/5}; })
        if (!std::randomize(delay) with { delay inside {[1000:2000]}; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_initialization <= 1'b1;
        if (!std::randomize(delay) with { delay == 3500; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_initialization <= 1'b0;
    endtask : gen_initialization_no_observation_error

    // generates boot error
    task gen_initialization_and_observation_transition();
        int delay;
        //if (!std::randomize(delay) with { delay dist {[200:400]:/95, 25000:/5}; })
        if (!std::randomize(delay) with { delay inside {[1000:2000]}; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_initialization <= 1'b1;
        if (!std::randomize(delay) with { delay == 3500; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_initialization <= 1'b0;
        s_status_observation    <= 1'b1;
        s_status_heartbeat_en   <= 1'b1;
    endtask : gen_initialization_and_observation_transition

    // generates correction error
    task gen_correction_state_error();
        int delay;
        //if (!std::randomize(delay) with { delay dist {[200:400]:/95, 25000:/5}; })
        if (!std::randomize(delay) with { delay inside {[1000:2000]}; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_correction <= 1'b1;
        if (!std::randomize(delay) with { delay == 1999; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_correction <= 1'b0;
        if (!std::randomize(delay) with { delay == 1999; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_correction <= 1'b1;
        if (!std::randomize(delay) with { delay == 11999; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_correction <= 1'b0;
    endtask : gen_correction_state_error

    // generates correction error
    task gen_classification_state_error();
        int delay;
        //if (!std::randomize(delay) with { delay dist {[200:400]:/95, 25000:/5}; })
        if (!std::randomize(delay) with { delay inside {[1000:2000]}; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_classification <= 1'b1;
        if (!std::randomize(delay) with { delay == 1999; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_classification <= 1'b0;
        if (!std::randomize(delay) with { delay == 1999; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_classification <= 1'b1;
        if (!std::randomize(delay) with { delay == 11999; })
            $fatal(1, "RANDOMIZATION ERROR (delay)");
        wait_clk(delay);
        s_status_classification <= 1'b0;
    endtask : gen_classification_state_error

    // main TB block
    initial begin
        reset_gen(3);
        fork
            gen_heartbeat();
        join_none
        //gen_observation_and_heartbeat();
        //gen_correction_and_uncorrectable();
        //gen_fatal_error();
        //gen_boot_error();
        //gen_initialization_timeout_error();
        //gen_initialization_no_observation_error();
        //gen_initialization_and_observation_transition();
        //gen_correction_state_error();
        //gen_classification_state_error();
        #100us;
        $finish;
    end

    final begin
        $display("*** End of simulation! ***");
        $display("*** Simulation was successful! ***");
    end

endmodule : test_sem_ip_watchdog

