----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_heartbeat.vhd
-- module name      heartbeat_tb
-- author           Danijela �kugor (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100TCSG324-1Q
-- brief            Heartbeat test module
----------------------------------------------------------------------------------------------
-- create date      29.06.2021
-- Revision:
--      Revision 0.01 by danijela.skugor
--                    - File Created
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;



entity test_heartbeat is
end ;

architecture behavioral of test_heartbeat is

    component heartbeat is
        generic (
            G_HB_CLOCK_FREQ   : integer := 100);        -- Input frequency of Clock in MHz
        port (
            p_hb_reset_n_in   : in  std_logic;
            p_hb_signal_out   : out std_logic;
            p_hb_clk_in       : in  std_logic);
    end component;
    
    constant C_HB_FREQ  : integer :=  100;
    constant C_HB_PERD  : time    :=  10 ns;
    
    signal s_hb_reset_n  :  std_logic;
    signal s_hb_signal   :  std_logic;
    signal s_hb_clk      :  std_logic;

begin
    
     uut: heartbeat
         generic map (
            G_HB_CLOCK_FREQ   => 100
            )
         port map(
            p_hb_reset_n_in   => s_hb_reset_n,
            p_hb_signal_out   => s_hb_signal,
            p_hb_clk_in       => s_hb_clk
         );   
    
    -- Reset
	reset_proc : process
	begin
		s_hb_reset_n <= '1';
		wait for 37 ns;
		s_hb_reset_n <= '0';
        wait for 56 ns;
		s_hb_reset_n <= '1';
		wait;
	end process;

	-- System clock generator
	system_clk : process
	begin
		s_hb_clk <= '0';
		wait for C_HB_PERD / 2;
		s_hb_clk <= '1';
		wait for C_HB_PERD / 2;
	end process;


end behavioral;
