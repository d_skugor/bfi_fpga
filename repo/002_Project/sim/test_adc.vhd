----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_adc.vhd
-- module name      test_adc - Behavioral
-- author           Marko Gulin (marko.gulin@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Test modul for the ADC driver
----------------------------------------------------------------------------------------------
-- create date      13.11.2019
-- Revision:
--      Revision 0.01 by marko.gulin
--                    - File Created
--        Revision 0.02 by J.Sanchez
--                        - Updated adc_driver code
--                        - Created a simulation model of the THS1206 tailored to the BFI
--                            needs.
----------------------------------------------------------------------------------------------
-- TODO: - modify model to inject errors
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Entity definition
entity test_adc is
    -- nothing here because it's a simulation
end;

-- Entity implementation
architecture Behavioral of test_adc is

    component ths1206_model is
        generic(induce_initialization_error : std_logic := '0'
               );
        port(
            -- ADC control lines
            p_adc_conv_n_in : in    std_logic; -- conversion trigger input
            p_adc_cs0_n_in  : in    std_logic; -- chip select 0
            p_adc_cs1_in    : in    std_logic; -- chip select 1
            p_adc_rd_n_in   : in    std_logic; -- read trigger
            p_adc_wr_n_in   : in    std_logic; -- write enable

            p_adc_dav_n_out : out   std_logic; -- data available flag

            -- ADC data
            p_adc_data_bi   : inout std_logic_vector(11 downto 0) -- Data bus (including D10/RA0 and D11/RA1)

        );
    end component ths1206_model;

    component adc_driver is
        generic(
            G_TRIGGER_LEVEL   : natural   := 0; -- Valid trigger levels range from 0 to 2 (see datasheet table 13, pp.28).
            G_CONV_MODE       : std_logic := '1'; -- Conversion mode: continuous = 0, on-demand = 1
            G_OUTPUT_FORMAT   : std_logic := '1'; -- 2's complement = 0, binary = 1
            G_FIFO_RESET      : std_logic := '1'; -- Enable automatic reset of the FIFO after each conversion.
            G_ADC_AUTO_OFFSET : std_logic := '0'; -- Automatic offset compensation
            G_ADC_REF_EXT     : std_logic := '0' -- Select the external ADC reference voltage
        );
        Port(
            -- Main signals
            clk_in           : in    std_logic; -- clock
            reset_n_in       : in    std_logic; -- reset

            -- ADC
            p_adc_conv_n_out : out   std_logic; -- conversion trigger (active low)
            p_adc_cs0_n_out  : out   std_logic; -- control signal (active low)
            p_adc_cs1_out    : out   std_logic; -- control signal (active high)
            p_adc_rd_n_out   : out   std_logic; -- read trigger (active low)
            p_adc_wr_n_out   : out   std_logic; -- write trigger (active low)
            p_adc_dav_in     : in    std_logic; -- data available flag
            p_adc_data_bi    : inout std_logic_vector(11 downto 0); -- ADC data bus

            -- Interface
            adc_error_out    : out   std_logic; -- Error condition flag
            clr_error_p_in   : in    std_logic; -- Clear error flag, and restart ADC
            init_fault_out   : out   std_logic; -- Initialization fault
            dataCh0_out      : out   std_logic_vector(11 downto 0);
            dataCh1_out      : out   std_logic_vector(11 downto 0);
            dataCh2_out      : out   std_logic_vector(11 downto 0);
            dataCh3_out      : out   std_logic_vector(11 downto 0);
            dataRdy_p_out    : out   std_logic -- Sample available
        );
    end component adc_driver;

    signal s_clk   : std_logic := '1';  -- starting value must be '1'
    signal reset_n : std_logic := '0';  -- Start applying reset

    signal s_adc_conv_n : std_logic;
    signal s_adc_cs0_n  : std_logic;
    signal s_adc_cs1    : std_logic;
    signal s_adc_rd_n   : std_logic;
    signal s_adc_wr_n   : std_logic;
    signal s_adc_dav    : std_logic;

    signal s_adc_data : std_logic_vector(11 downto 0);

    signal s_adc_dataCh0 : std_logic_vector(11 downto 0);
    signal s_adc_dataCh1 : std_logic_vector(11 downto 0);
    signal s_adc_dataCh2 : std_logic_vector(11 downto 0);
    signal s_adc_dataCh3 : std_logic_vector(11 downto 0);

    signal s_adc_dataRdy_p : std_logic;

    signal s_adc_error      : std_logic := '0';
    signal s_adc_clr_error  : std_logic := '0';
    signal s_adc_init_error : std_logic := '0';

begin

    clk_stimulus : process
    begin
        wait for 5 ns;
        s_clk <= not s_clk;
    end process clk_stimulus;

    reset : process
    begin
        reset_n <= '1' after 200 ns;
        wait;
    end process;

    DUT : component adc_driver
        generic map(
            G_TRIGGER_LEVEL   => 0,
            G_CONV_MODE       => '1',
            G_OUTPUT_FORMAT   => '1',
            G_FIFO_RESET      => '0',
            G_ADC_AUTO_OFFSET => '0',
            G_ADC_REF_EXT     => '0'
        )
        port map(
            clk_in           => S_CLK,
            reset_n_in       => reset_n,
            p_adc_conv_n_out => s_adc_conv_n,
            p_adc_cs0_n_out  => s_adc_cs0_n,
            p_adc_cs1_out    => s_adc_cs1,
            p_adc_rd_n_out   => s_adc_rd_n,
            p_adc_wr_n_out   => s_adc_wr_n,
            p_adc_dav_in     => s_adc_dav,
            p_adc_data_bi    => s_adc_data,
            adc_error_out    => s_adc_error,
            clr_error_p_in   => s_adc_clr_error,
            init_fault_out   => s_adc_init_error,
            dataCh0_out      => s_adc_dataCh0,
            dataCh1_out      => s_adc_dataCh1,
            dataCh2_out      => s_adc_dataCh2,
            dataCh3_out      => s_adc_dataCh3,
            dataRdy_p_out    => s_adc_dataRdy_p
        );

    ths1206_model_inst : component ths1206_model
        generic map(induce_initialization_error => '0')
        port map(
            p_adc_conv_n_in => s_adc_conv_n,
            p_adc_cs0_n_in  => s_adc_cs0_n,
            p_adc_cs1_in    => s_adc_cs1,
            p_adc_rd_n_in   => s_adc_rd_n,
            p_adc_wr_n_in   => s_adc_wr_n,
            p_adc_dav_n_out => s_adc_dav,
            p_adc_data_bi   => s_adc_data
        );

    data_stimulus : process
    begin
        wait;
    end process data_stimulus;

end Behavioral;
