----------------------------------------------------------------------------------------------
--                                   ALTAM SYSTEMS SL
--
--          (c) Copyright 2018 - 2020, ALTAM SYSTEMS SL, Barcelona, Spain
--					
--                                 All rights reserved
--
-- 
-- COPYRIGHT NOTICE:  
-- All information contained herein is, and remains the property of ALTAM Systems S.L. 
-- and its suppliers, if any.  
-- The intellectual and technical concepts contained herein are proprietary to 
-- ALTAM Systems S.L. and its suppliers and may be covered by Spanish and Foreign Patents,
-- patents in process, and are protected by trade secret or copyright law.
-- Dissemination of this information, modification or reproduction of this material
-- is strictly forbidden unless prior written permission is obtained from ALTAM Systems S.L.
--
----------------------------------------------------------------------------------------------
--! @file       ths1206_model.vhd     
--! @brief      VHDL simulation model, behavioral, not physical.
--! @details	
--! @author     Jorge Sanchez (jsanchez@altamsys.com)    
--! @date	11.05.2020
----------------------------------------------------------------------------------------------
-- CHANGES:
--		11.05.2020 (JSQ) - Creation date
--		02.06.2020 (JSQ) - Corrected an issue with the CRx reset
--						 - Corrected issue in the FIFO pointer calculation
--						- data_av supported only in level mode, not pulsed mode
--		12.11.2020 (JSQ) - Added generics to induce initialization errors
--						- FIFO reset internal mechanism is now edge-triggered with the CR1
--							contents. This is not well documented in the datasheet, but it has
--							been found experimentally.
--      12.05.2021 (DD) - changed counter for sampled values in ADC 
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ths1206_model is
	generic(induce_initialization_error : std_logic := '0'
	       );
	port(
		-- ADC control lines
		p_adc_conv_n_in : in    std_logic; -- conversion trigger input 
		p_adc_cs0_n_in  : in    std_logic; -- chip select 0 
		p_adc_cs1_in    : in    std_logic; -- chip select 1 
		p_adc_rd_n_in   : in    std_logic; -- read trigger
		p_adc_wr_n_in   : in    std_logic; -- write enable

		p_adc_dav_n_out : out   std_logic; -- data available flag

		-- ADC data
		p_adc_data_bi   : inout std_logic_vector(11 downto 0) -- Data bus (including D10/RA0 and D11/RA1)

	);

end entity ths1206_model;

architecture behavioral of ths1206_model is

	subtype t_adcreg is std_logic_vector(9 downto 0);
	subtype t_adcdata is std_logic_vector(11 downto 0);

	constant CONV_CLK_TIME : time    := 167 ns;
	constant FIFO_SIZE     : natural := 16;

	constant C_VREFP     : t_adcdata := std_logic_vector(to_unsigned(4095, 12));
	constant C_VMIDPOINT : t_adcdata := std_logic_vector(to_unsigned(2047, 12));
	constant C_VREFM     : t_adcdata := (others => '0');

	constant CR0_DEFAULT : t_adcreg := "00" & X"40";
	constant CR1_DEFAULT : t_adcreg := "00" & X"30";

	-- FIFO 
	type t_memory is array (0 to 15) of t_adcdata;
	signal r_fifo              : t_memory                           := (others => (others => '0'));
	signal fifo_rd_ptr         : integer range 0 to (FIFO_SIZE - 1) := 0;
	signal fifo_wr_ptr         : integer range 0 to (FIFO_SIZE - 1) := 0;
	signal fifo_wr             : std_logic                          := '0';
	signal fifo_rd             : std_logic                          := '0';
	signal fifo_full           : std_logic                          := '0';
	signal fifo_rdy            : std_logic                          := '1';
	signal fifo_data           : t_adcdata                          := (others => '0');
	signal fiforst, fiforstclr : std_logic                          := '0';

	-- Internal clock and global controls
	signal iclk : std_logic := '0';

	-- Control registers
	signal cr0_reg   : t_adcreg := (others => '0');
	signal cr1_reg   : t_adcreg := (others => '0');
	signal creg_data : t_adcreg := (others => '0');

	-- Channel selector and channel counters (for test data) and ADC sample registers
	signal channel_cnt   : integer range 0 to 3 := 0;
	signal channel_incr  : std_logic            := '0'; -- channel counter increment
	signal clr_channel   : std_logic            := '0';
	signal channel_limit : std_logic            := '0'; -- channel counter limit reached

	-- sample counters that provide 'variable' data to each ADC input
	signal cnt_1 : integer range 0 to 255 := 0;
	signal cnt_2 : integer range 0 to 255 := 0;
	signal cnt_3 : integer range 0 to 255 := 0;
	signal cnt_4 : integer range 0 to 255 := 0;

	-- ADC inputs ('analog' side)
	signal adc_ch1 : t_adcdata := (others => '0');
	signal adc_ch2 : t_adcdata := (others => '0');
	signal adc_ch3 : t_adcdata := (others => '0');
	signal adc_ch4 : t_adcdata := (others => '0');

	-- control bus 
	signal rd_enable      : std_logic := '0'; -- Internal R/W permits
	signal wr_enable      : std_logic := '0';
	signal rd_data        : t_adcdata := (others => '0');
	signal wr_data        : t_adcdata := (others => '0');
	signal data_available : std_logic := '0';

	-- ADC
	signal start_conv           : std_logic            := '0';
	signal conv_rdy             : std_logic            := '0';
	signal adc_data_in          : t_adcdata            := (others => '0'); -- 'analog' input
	signal adc_data_out         : t_adcdata            := (others => '0'); -- Digitized output
	signal adc_channel_selector : integer range 0 to 3 := 0; -- Current selected channel to the ADC input
	signal sample_hold          : std_logic            := '0';
	signal test_data            : t_adcdata            := (others => '0');

	-- control
	signal conv_state : integer   := 0;
	signal conv_clk   : std_logic := '0';
	signal debug_mode : std_logic := '0';
	signal test_mode  : std_logic := '0';
	signal debug_sel  : std_logic := '0';
	signal ireset     : std_logic := '0';

	signal conv_edge_filter : std_logic_vector(2 downto 0) := "000";
	signal wr_enable_filter : std_logic_vector(2 downto 0) := "000";

	-- CR0 register bits
	signal cr0_test  : std_logic_vector(1 downto 0) := (others => '0');
	signal cr0_scan  : std_logic                    := '0';
	signal cr0_diff  : std_logic_vector(1 downto 0) := "10";
	signal cr0_chsel : std_logic_vector(1 downto 0) := (others => '0');
	signal cr0_pd    : std_logic                    := '0';
	signal cr0_mode  : std_logic                    := '0';
	signal cr0_vref  : std_logic                    := '0';

	-- CR1 register bits
	signal fifo_trigger_level : integer range 0 to 15        := 0;
	signal cr1_data_t         : std_logic                    := '0';
	signal cr1_data_p         : std_logic                    := '0';
	signal wrd_enable         : std_logic                    := '0'; -- Select WR as R/nW
	signal cr1_reset          : std_logic                    := '0';
	signal cr1_rback          : std_logic                    := '0';
	signal cr1_trig           : std_logic_vector(1 downto 0) := "00";
	signal cr1_offset         : std_logic                    := '0';
	signal cr1_bin2s          : std_logic                    := '0';
	signal cr1_fst            : std_logic                    := '0';
	signal cr1_ovrf           : std_logic                    := '0';

begin
	-- MODEL NOTES:
	--
	-- Implementation
	-- Very simplistic model for the THS1206
	-- The model does NOT implement all the functions available in the ADC, but the fundamental
	--	ones: ADC, FIFO, trigger level, write/read control registers
	--
	-- Each ADC 'samples' data as:
	--	ADC CH1: 0x11n
	--	ADC CH2: 0x22n
	--	ADC CH3: 0x33n
	--	ADC	CH4: 0x44n
	--	with 'n' a counter advancing for each sample.
	--	

	process
	begin
		if (induce_initialization_error = '1') then
			iclk <= '0';
			wait;
		end if;
		iclk <= '0';
		wait for 1 ns;
		iclk <= '1';
		wait for 1 ns;
	end process;

	-- Conversion clock edge detector
	--	process
	--	begin
	--		wait until p_adc_conv_n_in = '0';
	--		wait until iclk <= '0';
	--		conv_clk <= '1';
	--		wait until iclk <= '0';
	--		conv_clk <= '0';
	--	end process;

	process(iclk)
	begin
		if Rising_edge(iclk) then
			conv_clk         <= '0';
			conv_edge_filter <= conv_edge_filter(1 downto 0) & p_adc_conv_n_in;
			if (conv_edge_filter = "110") then
				conv_clk <= '1';
			end if;
		end if;
	end process;

	-- Sample and hold 
	process
	begin
		wait until sample_hold = '1';
		-- A new sample is 'captured': samples follow an ascending sequence	
		cnt_1 <= cnt_1 + 1;
		cnt_2 <= cnt_2 + 1;
		cnt_3 <= cnt_3 + 1;
		cnt_4 <= cnt_4 + 1;
	end process;

	-- Assign the sampled data to each ADC register
	adc_ch1 <= X"1" & std_logic_vector(to_unsigned(cnt_1, 8));
	adc_ch2 <= X"2" & std_logic_vector(to_unsigned(cnt_2, 8));
	adc_ch3 <= X"3" & std_logic_vector(to_unsigned(cnt_3, 8));
	adc_ch4 <= X"4" & std_logic_vector(to_unsigned(cnt_4, 8));

	process(iclk, ireset)               -- Channel counter
	begin
		if (ireset = '1') then
			channel_cnt <= 0;
		elsif Rising_edge(iclk) then
			if (clr_channel = '1') then
				-- Priority to the clear operation
				channel_cnt <= 0;
			elsif (channel_incr = '1') then
				channel_cnt <= channel_cnt + 1;
				if (channel_limit = '1') then -- Autoclear when counter reaches the programmed number of channels
					channel_cnt <= 0;
				end if;
			end if;
		end if;
	end process;

	channel_limit <= '1' when (channel_cnt = to_integer(unsigned(cr0_chsel))) else '0';

	adc_channel_selector <= to_integer(unsigned(cr0_chsel)) when cr0_scan = '0' else channel_cnt;

	-- ADC process
	-- Here we 'convert' the samples, and store them in the FIFO for the selected number of channel(s)
	-- Note that up to four single ended channels can be used (differential mode channels are not implemented)
	-- This process behaves depending on the selected mode:
	--	in continous mode (mode = 0), the port p_adc_conv_n is a clock input, dictating the conversion timing. There will be
	--	a new conversion for each clock pulse, causing the sequential ADC conversion if scan is active.
	--	in single conversion mode, there is a single pulse and the internal clock will take over until all the channels have been
	--	acquired (if scan is active)

	-- ADC MUX
	adc_data_in <= adc_ch1 when adc_channel_selector = 0
	               else adc_ch2 when adc_channel_selector = 1
	               else adc_ch3 when adc_channel_selector = 2
	               else adc_ch4;

	process                             -- adc
	begin
		wait until start_conv = '1';
		conv_rdy     <= '0';
		wait for CONV_CLK_TIME;         -- internal clock. Wait until conversion is ready
		adc_data_out <= adc_data_in;
		conv_rdy     <= '1';
	end process;

	--		if (fifo_wr_ptr - fifo_rd_ptr = fifo_trigger_level) then

	process(iclk, ireset, fiforst)
	begin
		if (ireset = '1') then
			-- Reset controlled signals
			data_available <= '0';
			sample_hold    <= '0';
			conv_state     <= 0;
			clr_channel    <= '0';
			start_conv     <= '0';
			fifo_wr_ptr    <= 0;
			cr1_ovrf       <= '0';      -- FIFO overrun
			debug_mode     <= '0';
			test_mode      <= '0';

		elsif (fiforst = '1') then
			fifo_wr_ptr <= 0;
			cr1_ovrf    <= '0';         -- FIFO overrun

		elsif Rising_edge(iclk) then
			case conv_state is
				when 0 =>
					if (cr1_rback = '1') then -- debug mode
						conv_state <= 14;
					elsif (cr0_test > "00") then -- test mode
						conv_state <= 15;
					elsif (cr0_mode = '0') then -- Transition to the continous mode branch
						conv_state <= 10;
					elsif (conv_clk = '1') then -- Wait for the conversion pulse
						clr_channel    <= '0';
						sample_hold    <= '1';
						data_available <= '0';
						conv_state     <= 1;
					elsif (rd_enable = '1') then
						data_available <= '0';
					else
						conv_state <= 0;
					end if;

				when 1 =>
					-- pulse sample/hold block 
					sample_hold <= '0';
					if (p_adc_conv_n_in = '1' or cr0_mode = '0') then
						conv_state  <= 0; -- Wrong state, cancel
						clr_channel <= '1';
					else
						conv_state <= 2;
					end if;
				when 2 =>
					-- acquire data
					if (p_adc_conv_n_in = '1' or cr0_mode = '0') then
						conv_state  <= 0; -- Wrong state, cancel
						clr_channel <= '1';
					else
						start_conv <= '1';
						conv_state <= 3;
					end if;

				when 3 =>
					-- wait for conversion to take place
					if (p_adc_conv_n_in = '1' or cr0_mode = '0') then
						conv_state  <= 0; -- Wrong state, cancel
						clr_channel <= '1';
						start_conv  <= '0';
					else
						start_conv <= '0';
						if (conv_rdy = '0') then
							conv_state <= 3;
						else
							conv_state <= 4;
						end if;
					end if;

				when 4 =>
					-- store data in FIFO and increase write pointer
					if (p_adc_conv_n_in = '1' or cr0_mode = '0') then
						conv_state  <= 0; -- Wrong state, cancel
						clr_channel <= '1';
					else
						if (fifo_full = '0') then
							fifo_wr_ptr         <= fifo_wr_ptr + 1;
							if (fifo_wr_ptr = 15) then
								fifo_wr_ptr <= 0;
							end if;
							r_fifo(fifo_wr_ptr) <= adc_data_out;
						else
							cr1_ovrf <= '1'; -- FIFO is full. Overrun
						end if;
						conv_state <= 5;
					end if;

				when 5 =>
					-- check if autoscan is active, and prepare actions 
					if (p_adc_conv_n_in = '1' or cr0_mode = '0') then
						conv_state  <= 0; -- Wrong state, cancel
						clr_channel <= '1';
					else
						if ((to_unsigned(fifo_wr_ptr, 4) - to_unsigned(fifo_rd_ptr, 4)) = to_unsigned(fifo_trigger_level, 4)) then
							data_available <= '1';
							clr_channel    <= '1';
							conv_state     <= 0;
						else
							if (cr0_scan = '1') then
								channel_incr <= '1';
							else
								sample_hold <= '1'; -- If no autoscan, do a new sampling
							end if;
							conv_state <= 6;
						end if;
					end if;

				when 6 =>
					-- next channel
					if (p_adc_conv_n_in = '1' or cr0_mode = '0') then
						conv_state <= 0; -- Wrong state, cancel
					else
						sample_hold  <= '0';
						channel_incr <= '0';
						start_conv   <= '1';
						conv_state   <= 3;
					end if;

				when 10 =>
					channel_incr <= '0';
					clr_channel  <= '0';
					if (cr1_rback = '1') then -- debug mode
						conv_state <= 14;
					elsif (cr0_test > "00") then -- test mode
						conv_state <= 15;
					elsif (cr0_mode = '1') then
						conv_state <= 0;
					elsif (conv_clk = '1') then
						start_conv  <= '1';
						sample_hold <= '1';
						conv_state  <= 11;
					end if;

				when 11 =>
					channel_incr <= '0';
					start_conv   <= '0';
					sample_hold  <= '0';
					if (cr0_mode = '1') then
						conv_state <= 0;
					elsif (conv_rdy = '1') then
						if (fifo_full = '0') then
							fifo_wr_ptr         <= fifo_wr_ptr + 1;
							if (fifo_wr_ptr = 15) then
								fifo_wr_ptr <= 0;
							end if;
							r_fifo(fifo_wr_ptr) <= adc_data_out;
						else
							cr1_ovrf <= '1'; -- FIFO is full. Overrun
						end if;
						conv_state <= 12;
					end if;

				when 12 =>
					if (cr0_mode = '1') then
						conv_state <= 0;
					elsif ((to_unsigned(fifo_wr_ptr, 4) - to_unsigned(fifo_rd_ptr, 4)) = to_unsigned(fifo_trigger_level, 4)) then
						data_available <= '1';
						clr_channel    <= '1';
						conv_state     <= 10;
					else
						conv_state <= 13;
					end if;

				when 13 =>
					clr_channel <= '0';
					if (cr0_mode = '1') then
						conv_state <= 0;
					elsif (cr0_scan = '0') then
						-- No autoscan
						data_available <= '0';
						conv_state     <= 10;
					else
						-- Autoscan
						data_available <= '0';
						channel_incr   <= '1';
						start_conv     <= '1';
						conv_state     <= 11;
					end if;

				when 14 =>
					-- Debug mode
					debug_mode <= '1';
					conv_state <= 14;

				when 15 =>
					-- Test mode
					-- we mimick the ADC test mode, presenting the expected test value directly at the
					-- data bus. The data available flag is not used in this mode
					data_available <= '1';
					test_mode      <= '1';
					conv_state     <= 15;

				when others =>
					null;
			end case;
		end if;
	end process;

	--#### internal memory begin
	--********************** FIFO
	fifo_full <= '1' when ((fifo_wr_ptr - fifo_rd_ptr) = (FIFO_SIZE - 1)) else '0';

	-- Read fifo
	process(ireset, fiforst, rd_enable)
	begin
		if (ireset = '1') then
			fifo_rd_ptr <= 0;
		elsif (fiforst = '1') then
			fifo_rd_ptr <= 0;
			fiforstclr  <= '1';
		elsif Rising_edge(rd_enable) then
			fifo_rd_ptr <= fifo_rd_ptr + 1;
			if (fifo_rd_ptr = 15) then
				fifo_rd_ptr <= 0;
			end if;
			fifo_data   <= r_fifo(fifo_rd_ptr);
		end if;

		if (fiforst = '0' and fiforstclr = '1') then
			fiforstclr <= '0';
		end if;
	end process;

	-- Read data debug multiplexor
	rd_data <= fifo_data when (debug_mode = '0' and test_mode = '0')
	           else "00" & creg_data when (debug_mode = '1' and test_mode = '0')
	           else test_data when (debug_mode = '0' and test_mode = '1')
	           else (others => '0');

	-- Test data multiplexor
	test_data <= (others => '0') when (cr0_test = "00")
	             else C_VREFP when (cr0_test = "01")
	             else C_VMIDPOINT when (cr0_test = "10")
	             else C_VREFM when (cr0_test = "11")
	           ;

	           --**************** Control registers

	           -- crn registers
	           -- Store the register data when requested from the bus
	cr1_reg_cntr : process(iclk, ireset)
		variable cr_sel : std_logic_vector(1 downto 0) := (others => '0');
	begin
		if (ireset = '1') then
			cr1_reg <= CR1_DEFAULT;
			cr0_reg <= CR0_DEFAULT;

		elsif Rising_edge(iclk) then    --Falling_edge(wr_enable) then
			wr_enable_filter <= wr_enable_filter(1 downto 0) & wr_enable;
			if (wr_enable_filter = "110") then
				cr_sel    := wr_data(11 downto 10);
				cr1_reset <= '0';
				if (cr_sel = "01") then
					-- CR1 register selected
					cr1_reg <= wr_data(9 downto 0);
					cr1_fst <= wr_data(1);
				elsif (cr_sel = "00") then
					-- CR0 register selected
					cr0_reg <= wr_data(9 downto 0);
				end if;
			end if;
		end if;
	end process;

	process
	begin
		wait until cr1_fst = '1';		
		wait until iclk = '1';
		fiforst <= '1';		
		wait until fiforstclr = '1';
		wait until iclk = '1';
		fiforst <= '0';
	end process;

	-- hack-reset-controller
	process
	begin
		wait until wr_enable = '0';
		if (wr_data(11 downto 10) = "01" and wr_data(0) = '1') then
			ireset <= '1';
		else
			ireset <= '0';
		end if;
	end process;

	-- Debug read control register multiplexer
	process
	begin
		-- 'ping-pong' register selector after each read operation
		wait for 1 ps;
		if (debug_mode = '1') then
			wait until rd_enable = '1';
			debug_sel <= '1';
			wait until rd_enable = '1';
			debug_sel <= '0';
		end if;
	end process;

	creg_data <= cr1_reg(9 downto 2) & cr1_ovrf & '0' when (debug_sel = '0')
	             else cr0_reg when (debug_sel = '1')
	             else (others => '0');

	-- Extract individual bit fields to internal variables, sourced from the control registers
	-- CR0 register bits assignment
	cr0_vref  <= cr0_reg(0);
	cr0_mode  <= cr0_reg(1);
	cr0_pd    <= cr0_reg(2);
	cr0_chsel <= cr0_reg(4 downto 3);
	cr0_diff  <= cr0_reg(6 downto 5);
	cr0_scan  <= cr0_reg(7);
	cr0_test  <= cr0_reg(9 downto 8);

	-- CR1 register bits assignment
	cr1_data_t <= cr1_reg(4);
	cr1_data_p <= cr1_reg(5);
	cr1_reset  <= cr1_reg(0);
	wrd_enable <= cr1_reg(6);
	cr1_rback  <= cr1_reg(9);
	cr1_offset <= cr1_reg(8);
	cr1_bin2s  <= cr1_reg(7);

	cr1_trig           <= cr1_reg(3 downto 2);
	fifo_trigger_level <= 1 when cr1_trig = "00" and cr0_chsel = "00"
	                      else 4 when cr1_trig = "01" and cr0_chsel = "00"
	                      else 8 when cr1_trig = "10" and cr0_chsel = "00"
	                      else 14 when cr1_trig = "11" and cr0_chsel = "00"
	                      else 2 when cr1_trig = "00" and cr0_chsel = "01"
	                      else 4 when cr1_trig = "01" and cr0_chsel = "01"
	                      else 8 when cr1_trig = "10" and cr0_chsel = "01"
	                      else 12 when cr1_trig = "11" and cr0_chsel = "01"
	                      else 3 when cr1_trig = "00" and cr0_chsel = "10"
	                      else 6 when cr1_trig = "01" and cr0_chsel = "10"
	                      else 9 when cr1_trig = "10" and cr0_chsel = "10"
	                      else 12 when cr1_trig = "11" and cr0_chsel = "10"
	                      else 4 when cr1_trig = "00" and cr0_chsel = "11"
	                      else 8 when cr1_trig = "01" and cr0_chsel = "11"
	                      else 12 when cr1_trig = "10" and cr0_chsel = "11"
	                      else 0 when cr1_trig = "11" and cr0_chsel = "11" -- reserved
	                      else 0;       -- reserved

	--######## internal memory end
	p_adc_dav_n_out <= not data_available when cr1_data_p = '0' else data_available;

	rd_enable <= '1' when (p_adc_rd_n_in = '0' and p_adc_cs0_n_in = '0' and p_adc_cs1_in = '1' and wrd_enable = '0')
	             else '1' when (p_adc_wr_n_in = '1' and p_adc_cs0_n_in = '0' and p_adc_cs1_in = '1' and wrd_enable = '1')
	             else '0';
	wr_enable <= '1' when (p_adc_wr_n_in = '0' and p_adc_cs0_n_in = '0' and p_adc_cs1_in = '1' and wrd_enable = '0') else '0';

	p_adc_data_bi <= rd_data when (rd_enable = '1') else (others => 'Z') after 12 ns;
	wr_data       <= p_adc_data_bi;

end architecture behavioral;

