//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             test_uart_top.vhd
// module name      test_uart_top
// author           david.pavlovic (david.pavlovic@rimac-automobili.com)
// project name     BFI
// target device    XA7A100T-CSG324Q1
// brief            UART top testbench module
//--------------------------------------------------------------------------------------------
// create date      08.02.2021
// Revision:
//      Revision 0.01 by david.pavlovic
//                    - File Created
//--------------------------------------------------------------------------------------------

module test_uart;

    timeunit 1ns/1ps;

    // parameters
    parameter C_CLK_PER   = 10.0;
    parameter C_BAUD_RATE = 9600;

    // main signals
    logic       s_clk          = '0;
    logic       s_rst_n        = '0;
    // TX signals
    logic       s_tx;
    logic [7:0] s_data_tx      = '0;
    logic       s_data_tx_rdy  = '0;
    logic       s_tx_busy;
    // RX signals
    logic       s_rx           = '1;
    logic [7:0] s_data_rx;
    logic       s_data_rx_rdy;
    logic       s_rx_busy;

    // auxiliary signals
    logic [7:0] data_rx        = '0;;

    uart_top
        #(
            .G_BAUD_RATE                (C_BAUD_RATE)
        )
    dut
        (
            // main signals
            .p_uart_top_clk_in          (s_clk),
            .p_uart_top_rst_n_in        (s_rst_n),
            // TX signals
            .p_uart_top_tx_out          (s_tx),
            .p_uart_top_data_tx_in      (s_data_tx),
            .p_uart_top_data_tx_rdy_in  (s_data_tx_rdy),
            .p_uart_top_tx_busy_out     (s_tx_busy),
            // RX signals
            .p_uart_top_rx_in           (s_tx),
            .p_uart_top_data_rx_out     (s_data_rx),
            .p_uart_top_data_rx_rdy_out (s_data_rx_rdy),
            .p_uart_top_rx_busy_out     (s_rx_busy)
        );

    //-------------------------------------------------------------------------

    // clock generator
    always begin
        #(C_CLK_PER/2) s_clk = ~s_clk;
    end

    //-----------------------------------
    // Tasks
    //-----------------------------------

    // generates N cycle active-low reset signal synchronous with clock
    task reset_gen(input int N);
        s_rst_n <= 1'b0;
        repeat (N+1) @(posedge s_clk);
        s_rst_n <= 1'b1;
    endtask : reset_gen;

    // waits for N clock cycles
    task wait_clk(input int N);
        repeat (N)
            @(posedge s_clk);
    endtask : wait_clk

    task tx_trans();
        wait(s_tx_busy == 1'b0);
        @(posedge s_clk);
        if (!std::randomize(s_data_tx))
            $fatal(1, "s_data_tx randomization failed");
        s_data_tx_rdy <= 1'b1;
        @(posedge s_clk);
        s_data_tx_rdy <= 1'b0;
        @(negedge s_tx_busy);
    endtask : tx_trans

    task rx_trans();
        real bit_length;
        bit_length = 1.0/C_BAUD_RATE * 1e6;
        if (!std::randomize(data_rx))
            $fatal(1, "s_data_tx randomization failed");
        // start bit
        s_rx <= 1'b0;
        #(bit_length * 1us);
        // data bits
        foreach(data_rx[i]) begin
            s_rx <= data_rx[7-i];
            #(bit_length * 1us);
        end
        // stop bit
        s_rx <= 1'b1;
        #(bit_length * 1us);
    endtask : rx_trans

    initial begin
        reset_gen(3);
        @(posedge s_clk);
        tx_trans();
        #10us;
        $finish;
    end

endmodule : test_uart