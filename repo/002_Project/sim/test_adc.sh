# Bash script for ADC simulation testbench
xvhdl -2008 -nolog ../src/currentAdc/adc_driver.vhd
xvhdl -2008 -nolog ths1206_model.vhd
xvhdl -2008 -nolog ../../001_Common/src/pulse_rate_divider.vhd
xvhdl -2008 -nolog ../../001_Common/src/edge_detector.vhd
xvhdl -nolog test_adc.vhd
xelab -debug typical test_adc -s top_sim -nolog
xsim -gui top_sim  -nolog

# Cleanup
rm *.log
rm *.pb
rm -rf xsim.dir
rm *.jou
rm *.wdb
