----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_res_driver.vhd
-- module name      res_driver_tb
-- author           Dario Drvenkar (dario.drvenkar@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100TCSG324-1Q
-- brief            Resolver Driver test module
----------------------------------------------------------------------------------------------
-- create date      09.04.2021
-- Revision:
--      Revision 0.01 by dario.drvenkar
--                    - File Created
--		Revision 0.02 by danijela.skugor
--					  -- Added input port p_res_driver_sample_in do res_driver due to changes in source
--                    -- in version 4.0.0
--				      -- Added stimuli for p_res_driver_sample_in
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity res_driver_tb is
end;

architecture bench of res_driver_tb is

     component res_driver is
        generic (
            G_RES_DRIVER_CLK                 : integer;     -- input clock in MHz
            G_RES_DRIVER_RES_UPDATE_RATE     : integer      -- Resolver update rate in kHz     
        );
        port (
            -- main inputs
            p_res_driver_rst_n_in        :  in std_logic;   -- main reset line
            p_res_driver_clk_in          :  in std_logic;   -- main system clock
            p_res_driver_sample_in       :  in std_logic;     -- signal controlling ADC sampling
            -- resolver inputs
            p_res_driver_ord_in          :  in std_logic_vector(11 downto 0);   -- 12-bit parallel data in
--          p_res_driver_ordclk_in       :  in std_logic;                       -- sample ready clock in; not used (hardware deficiency)
            -- outputs
            p_res_driver_va0_out         : out std_logic;
            p_res_driver_va1_out         : out std_logic;
            p_res_driver_inhb_out        : out std_logic;
            p_res_driver_data_val_p_out  : out std_logic;
            p_res_driver_angle_out       : out std_logic_vector(11 downto 0); -- 12-bit angle value
            p_res_driver_velocity_out    : out std_logic_vector(11 downto 0)  -- 12-bit angle value
        );
    end component;

  signal p_res_driver_rst_n_in        : std_logic;
  signal p_res_driver_clk_in          : std_logic;
  signal p_res_driver_ord_in          : std_logic_vector(11 downto 0);
--  signal p_res_driver_ordclk_in: std_logic;
  signal p_res_driver_va0_out         : std_logic;
  signal p_res_driver_va1_out         : std_logic;
  signal p_res_driver_inhb_out        : std_logic;
  signal p_res_driver_angle_val_p_out : std_logic;
  signal p_res_driver_angle_out       : std_logic_vector(11 downto 0) ;
  signal p_res_driver_velocity_out    : std_logic_vector(11 downto 0) ;
  
  signal s_angle                      : unsigned(11 downto 0) := (others => '0');
  signal s_adc_sample                 : std_logic := '0'; 

  constant clock_period               : time := 10 ns;
  signal stop_the_clock               : boolean;

begin

  -- Insert values for generic parameters !!
  uut: res_driver generic map (
                                G_RES_DRIVER_CLK                => 100,
                                G_RES_DRIVER_RES_UPDATE_RATE    => 80
                            )
                     port map ( p_res_driver_rst_n_in        => p_res_driver_rst_n_in,
                                p_res_driver_clk_in          => p_res_driver_clk_in,
                                p_res_driver_sample_in       => s_adc_sample,
                                p_res_driver_ord_in          => p_res_driver_ord_in,
--                                p_res_driver_ordclk_in       => p_res_driver_ordclk_in,
                                p_res_driver_va0_out         => p_res_driver_va0_out,
                                p_res_driver_va1_out         => p_res_driver_va1_out,
                                p_res_driver_inhb_out        => p_res_driver_inhb_out,
                                p_res_driver_data_val_p_out  => p_res_driver_angle_val_p_out,
                                p_res_driver_angle_out       => p_res_driver_angle_out,
                                p_res_driver_velocity_out    => p_res_driver_velocity_out );

  stimulus: process
  begin
  
    -- Put initialisation code here
        p_res_driver_rst_n_in <= '1';
        wait for 100 ns;
        p_res_driver_rst_n_in <= '0';
        wait for 100 ns;
        p_res_driver_rst_n_in <= '1';
--        wait for 500 ms;
        wait for 10 sec;
    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;
  
--  inhb_process: process
--  begin
--    p_res_driver_ordclk_in <= '0';
--    wait until rising_edge(p_res_driver_inhb_out);
--    wait for 25 ns;
--    wait until rising_edge(p_res_driver_clk_in);
--    p_res_driver_ordclk_in <= '1';
--    wait for 45 ns;
--    wait until rising_edge(p_res_driver_clk_in);
--end process;

    angle_update: process
    begin
        wait until rising_edge(p_res_driver_clk_in);
        s_angle <= s_angle +1;
    end process;
    
    p_res_driver_ord_in <= std_logic_vector(s_angle);

  clocking: process
  begin
    while not stop_the_clock loop
      p_res_driver_clk_in <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;


  adc_sample : process
  begin
     wait until rising_edge(p_res_driver_clk_in);
        s_adc_sample <= '1';
        wait for 200 ns;
        s_adc_sample <= '0';
        wait for 800 ns;
  end process;

end;