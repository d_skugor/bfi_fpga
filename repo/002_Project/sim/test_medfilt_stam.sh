xvhdl -nolog ../src/lpf/pwm_pack.vhd
xvhdl -2008 -nolog ../src/lpf/medfilt_bblock.vhd
xvhdl -2008 -nolog ../src/lpf/medfilt_stam.vhd
xvhdl -nolog test_medfilt_stam.vhd
xelab -debug typical test_medfilt_stam -s top_sim -nolog
xsim top_sim -gui -nolog

# Cleanup
rm *.log
rm *.pb
rm -rf xsim.dir
rm *.jou
rm *.wdb
