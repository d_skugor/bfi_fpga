library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;
use work.pwm_pack.all;

entity test_lpf_pwm_fctrl is
end entity test_lpf_pwm_fctrl;

architecture behavioral of test_lpf_pwm_fctrl is

	constant C_SYNC_LOW_CADENCE  : time := 100 us;
	constant C_SYNC_MED_CADENCE  : time := 62.5 us;
	constant C_SYNC_HIGH_CADENCE : time := 50 us;

	constant C_SAMPLING_CLOCK_PERIOD   : time := 833 nS;
	constant C_SYSTEM_CLOCK_SEMIPERIOD : time := 5 nS;

	signal clk      : std_logic := '0'; -- System clock
	signal rst_n    : std_logic := '1'; -- System reset
	signal sample_p : std_logic := '0'; -- Sampling pulse

	signal sync_low_p  : std_logic := '0'; -- PWM sync LOW pulse
	signal sync_med_p  : std_logic := '0'; -- PWM sync MED pulse
	signal sync_high_p : std_logic := '0'; -- PWM sync HIGH pulse
	signal sync_p      : std_logic := '0'; -- PWM sync pulse

	signal pwm_sel  : integer    := 0;
	signal pwm_freq : t_pwm_freq := LOW;

begin

	DUT : entity work.lpf_pwm_fctrl
		port map(
			clk_i      => clk,
			rst_n_i    => rst_n,
			sample_p_i => sample_p,
			sync_p_i   => sync_p,
			pwm_freq_o => pwm_freq
		);

	stimuli : process
	begin
		wait for 400 us;
		wait until sample_p = '1';
		pwm_sel <= 1;
		wait for 400 us;
		wait until sample_p = '1';
		pwm_sel <= 2;
		wait for 400 us;
		wait until sample_p = '1';
		pwm_sel <= 0;
	end process;

	-- Reset
	reset_proc : process
	begin
		rst_n <= '0';
		wait for 4 * C_SYSTEM_CLOCK_SEMIPERIOD;
		wait until clk <= '0';
		rst_n <= '1';
		wait;
	end process;

	-- System clock generator
	system_clk : process
	begin
		clk <= '0';
		wait for C_SYSTEM_CLOCK_SEMIPERIOD;
		clk <= '1';
		wait for C_SYSTEM_CLOCK_SEMIPERIOD;
	end process;

	sampling_clk : process
	begin
		wait for C_SAMPLING_CLOCK_PERIOD;
		wait until clk = '1';
		sample_p <= '1';
		wait until clk = '1';
		sample_p <= '0';
	end process;

	sync_low : process
	begin
		wait for C_SYNC_LOW_CADENCE;
		wait until clk = '1';
		sync_low_p <= '1';
		wait until clk = '1';
		sync_low_p <= '0';
	end process;

	sync_med : process
	begin
		wait for C_SYNC_MED_CADENCE;
		wait until clk = '1';
		sync_med_p <= '1';
		wait until clk = '1';
		sync_med_p <= '0';
	end process;

	sync_high : process
	begin
		wait for C_SYNC_HIGH_CADENCE;
		wait until clk = '1';
		sync_high_p <= '1';
		wait until clk = '1';
		sync_high_p <= '0';
	end process;

	sync_p <= sync_low_p when pwm_sel = 0
	          else sync_med_p when pwm_sel = 1
	          else sync_high_p;

end architecture behavioral;
