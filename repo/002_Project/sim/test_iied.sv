//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             test_iied.sv
// module name      Intra-inverter error detection module testbench
// author           David Pavlović (david.pavlovic@rimac-automobili.com)
// project name     BFI
// target device    XA7A100TCSG324-1L
// brief            Testbench for testing Intra-inverter error detection
//--------------------------------------------------------------------------------------------
// create date      13.05.2020
// Revision:
//      Revision 0.01 by david.pavlovic
//                    - File Created
//
//      Revision 0.02 by david.pavlovic
//                    - Rewritten in SystemVerilog
//--------------------------------------------------------------------------------------------

module test_iied;

    timeunit 1ns/1ps;

    // parameters
    parameter G_NUM_VALID_PULSES = 3;
    parameter C_REPEATS          = 10;
    parameter C_CLK_PER          = 10.0;

    // types
    typedef enum {f_10_kHz, f_16_kHz, f_20_kHz} t_sw_freq;

    // signals
    logic s_clk = 1'b0;
    logic s_rst_n = 1'b0;
    logic s_inv_pwmi = 1'b0;
    logic s_inv_pwmo;
    logic s_inv_synci = 1'b0;
    logic s_inv_synco;
    logic s_mcu_pwmi;
    logic s_mcu_pwmo;
    logic s_mcu_synci = 1'b0;
    logic s_mcu_synco;


    // DUT instance
    intra_inv_err_detection
        #(
            .G_NUM_VALID_PULSES (G_NUM_VALID_PULSES)
        )
    dut
        (
            // Clock & Reset inputs
            .p_iInvErrDet_clk_in        (s_clk),
            .p_iInvErrDet_rst_n_in      (s_rst_n),
            // Signals from the IIED connector
            // PWM synchronization
            .p_iInvErrDet_inv_pwmi_in   (s_inv_pwmi), // PWM sync signal from the other side of inverter
            .p_iInvErrDet_inv_pwmo_out  (s_inv_pwmo), // PWM sync signal to the other side of inverter
            // Error signals (active low)
            .p_iInvErrDet_inv_synci_in  (s_inv_synci), // error sync signal from the other side of inverter
            .p_iInvErrDet_inv_synco_out (s_inv_synco), // error sync signal to the other side of inverter
            // Interface to the MCU
            // PWM synchronization
            .p_iInvErrDet_mcu_pwmi_in   (s_mcu_pwmi), // PWM sync signal from MCU
            .p_iInvErrDet_mcu_pwmo_out  (s_mcu_pwmo), // PWM sync signal from other side of the inverter
            // Error signals (active low)
            .p_iInvErrDet_mcu_synci_in  (s_mcu_synci), // error sync signal from MCU
            .p_iInvErrDet_mcu_synco_out (s_mcu_synco)  // error sync signal from other side of the inverter
        );

    //-------------------------------------------------------------------------

    // clock generator
    always begin
        #(C_CLK_PER/2) s_clk = ~s_clk;
    end

    //-----------------------------------
    // Tasks
    //-----------------------------------

    // generates N cycle active-low reset signal synchronous with clock
    task reset_gen(input int N);
        s_rst_n <= 1'b0;
        repeat (N+1) @(posedge s_clk);
        s_rst_n <= 1'b1;
    endtask : reset_gen;

    // waits for N clock cycles
    task wait_clk(input int N);
        repeat (N)
            @(posedge s_clk);
    endtask : wait_clk

    // generates random s_mcu_synci stimulus
    task mcu_synci_gen();
        int s_delay;
        forever begin
            if (!std::randomize(s_delay) with {s_delay inside {[300:450]};})
                $fatal(1, "Randomization of s_stuck failed");
            s_mcu_synci <= ~s_mcu_synci;
            #(s_delay*1us);
            if (!std::randomize(s_delay) with {s_delay inside {[25:50]};})
                $fatal(1, "Randomization of s_stuck failed");
            s_mcu_synci <= ~s_mcu_synci;
            #(s_delay*1us);
        end
    endtask : mcu_synci_gen

    // generates random s_inv_synci stimulus
    task inv_synci_gen();
        int s_delay;
        forever begin
            if (!std::randomize(s_delay) with {s_delay inside {[500:650]};})
                $fatal(1, "Randomization of s_stuck failed");
            s_inv_synci <= ~s_inv_synci;
            #(s_delay*1us);
            if (!std::randomize(s_delay) with {s_delay inside {[25:50]};})
                $fatal(1, "Randomization of s_stuck failed");
            s_inv_synci <= ~s_inv_synci;
            #(s_delay*1us);
        end
    endtask : inv_synci_gen

    // generates random s_inv_pwmi stimulus
    task inv_pwmi_gen();
        bit  s_stuck;
        int  s_cycles;
        int  s_delay_int;
        real s_delay;
        real s_period;
        t_sw_freq s_sw_freq;
        if (!std::randomize(s_stuck) with {s_stuck dist {0:/80, 1:/20};})
            $fatal(1, "Randomization of s_stuck failed");
        if (!s_stuck) begin
            if (!std::randomize(s_sw_freq))
                $fatal(1, "Randomization of s_sw_freq failed");
            if (!std::randomize(s_cycles) with {s_cycles inside {[2:10]};})
                $fatal(1, "Randomization of s_cycles failed");
            case (s_sw_freq)
                f_10_kHz : s_period = 100.0;
                f_16_kHz : s_period = 62.5;
                f_20_kHz : s_period = 50.0;
            endcase
            repeat (s_cycles) begin
                #(s_period*1us);
                s_inv_pwmi = ~s_inv_pwmi;
            end
        end else begin
            if (!std::randomize(s_delay_int) with {s_delay_int inside {[11000:15000]};})
                $fatal(1, "Randomization of s_sw_freq failed");
            s_delay = s_delay_int / 100.0;
            #(s_delay*1us);
        end
    endtask : inv_pwmi_gen

    assign s_mcu_pwmi = s_inv_pwmi;

    initial begin
        reset_gen(3);
        @(posedge s_clk);
        fork
            inv_synci_gen();
            mcu_synci_gen();
        join_none
        repeat (C_REPEATS) begin
            inv_pwmi_gen();
        end
        #100us;
        $finish;
    end

endmodule : test_iied