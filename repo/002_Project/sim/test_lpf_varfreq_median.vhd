library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

entity test_lpf_varfreq_median is
end entity test_lpf_varfreq_median;

architecture rtl of test_lpf_varfreq_median is

	constant C_SAMPLING_CLOCK_PERIOD : time := 80 ns; -- 1.2 MHz to 96 MHz is a factor 80
	constant C_SYSTEM_CLOCK_PERIOD   : time := 1 ns;

	signal clk      : std_logic                     := '0';
	signal rst_n    : std_logic                     := '0';
	signal sample_p : std_logic                     := '0'; -- ADC new sample pulse
	signal sync_p   : std_logic                     := '0'; -- PWM synchro pulse
	signal rdy_p    : std_logic                     := '0'; -- Filter new sample ready
	signal ch1_i    : std_logic_vector(11 downto 0) := (others => '0');
	signal ch2_i    : std_logic_vector(11 downto 0) := (others => '0');
	signal ch3_i    : std_logic_vector(11 downto 0) := (others => '0');
	signal ch1_o    : std_logic_vector(11 downto 0) := (others => '0');
	signal ch2_o    : std_logic_vector(11 downto 0) := (others => '0');
	signal ch3_o    : std_logic_vector(11 downto 0) := (others => '0');

	-- #### Setup here the input and output test files
	constant C_INPUT_FILE_NAME  : string    := "Simulation8.dat";
	constant C_OUTPUT_FILE_NAME : string    := "Simulation8_output.dat";

	file fInputPtr              : TEXT open READ_MODE is C_INPUT_FILE_NAME;
	file fOutputPtr             : TEXT open WRITE_MODE is C_OUTPUT_FILE_NAME;
	signal eof                  : std_logic := '0';
begin

	DUT : entity work.lpf_varfreq_median
		generic map(
			BSIZE => 12
		)
		port map(
			clk_i      => clk,
			rst_n_i    => rst_n,
			sample_p_i => sample_p,
			rdy_p_o    => rdy_p,
			sync_p_a_i => sync_p,
			ch1_i      => ch1_i,
			ch2_i      => ch2_i,
			ch3_i      => ch3_i,
			ch1_o      => ch1_o,
			ch2_o      => ch2_o,
			ch3_o      => ch3_o
		);

	-- Stimulus read from simulated data file, preprocessed in matlab
	-- The stimulus format has to be as follows (each line in the input data file):
	-- Phase-A Phase-B Phase-C Sync
	stimuli : process
		variable fLine                              : line;
		variable ch1_sample, ch2_sample, ch3_sample : integer;
		variable data_pwm_sync                      : integer;
	begin
		wait until rst_n = '1';

		while (not ENDFILE(fInputPtr)) loop
			wait until sample_p = '1';
			READLINE(fInputPtr, fLine);
			READ(fLine, ch1_sample);
			READ(fLine, ch2_sample);
			READ(fLine, ch3_sample);
			READ(fLine, data_pwm_sync);

			ch1_i  <= std_logic_vector(to_signed(ch1_sample, 12));
			ch2_i  <= std_logic_vector(to_signed(ch2_sample, 12));
			ch3_i  <= std_logic_vector(to_signed(ch3_sample, 12));
			sync_p <= to_unsigned(data_pwm_sync, 1)(0);
		end loop;

		wait until sample_p = '1';
		eof <= '1';                     -- End of test
		wait;
	end process;

	-- Log output process. Write the filter output into a file
	-- The output format is (each line):
	--	Phase-A Phase-B Phase-C
	logoutput : process
		variable fLine                              : line;
		variable ch1_sample, ch2_sample, ch3_sample : integer;
	begin
		wait until rdy_p = '1';
		ch1_sample := to_integer(signed(ch1_o));
		ch2_sample := to_integer(signed(ch2_o));
		ch3_sample := to_integer(signed(ch3_o));
		WRITE(fLine, ch1_sample);
		WRITE(fLine, ch2_sample);
		WRITE(fLine, ch3_sample);
		WRITELINE(fOutputPtr, fLine);
		if (eof = '1') then
			wait;
		end if;
	end process;

	-- Reset
	reset_proc : process
	begin
		rst_n <= '0';
		wait for 10 * C_SYSTEM_CLOCK_PERIOD;
		wait until clk <= '0';
		rst_n <= '1';
		wait;
	end process;

	-- System clock generator
	system_clk : process
	begin
		clk <= '0';
		wait for C_SYSTEM_CLOCK_PERIOD / 2;
		clk <= '1';
		wait for C_SYSTEM_CLOCK_PERIOD / 2;
	end process;

	sampling_clk : process
	begin
		-- This clock has to be synchronous with the system clock
		wait until clk = '1';
		sample_p <= '1';
		wait until clk = '1';
		sample_p <= '0';
		wait for C_SAMPLING_CLOCK_PERIOD - C_SYSTEM_CLOCK_PERIOD;
	end process;

end architecture rtl;
