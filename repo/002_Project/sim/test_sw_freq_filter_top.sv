//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             test_sw_freq_filter_top.vhd
// module name      test_sw_freq_filter_top
// author           David Palvovic (david.pavlovic@rimac-automobili.com)
// project name     BFI
// target device    XA7A100T-1CSG324Q
// brief            Variable switching frequency filter top module testbench file
//--------------------------------------------------------------------------------------------
// create date      29.09.2020.
// Revision:
//      Revision 0.01 by david.pavlovic
//                    - File Created
//--------------------------------------------------------------------------------------------
// NOTE: This is a SystemVerilog
//--------------------------------------------------------------------------------------------

module test_sw_freq_filter_top ();

    timeunit 1ns / 1ps;

    //import "DPI-C" function real sin(input real in);

    parameter G_DATA          = 12;
    parameter G_OFFSET        = 100;
    parameter C_CLK_PER       = 10;
    parameter C_NO_OF_PERIODS = 3;
    parameter C_MCU_REQ_REPS  = 5;
    parameter C_NOISE_MAX_ON  = 1; // us
    parameter C_NOISE_MIN_OFF = 20; // us
    parameter C_NOISE_MAX_OFF = 30; // us

    logic              s_clk        = 0;
    logic              s_rst_n      = 0;
    logic              s_sample_clk = 0;
    logic              s_mcu_req    = 0;
    logic [G_DATA-1:0] s_data_in    = 0;
    logic [G_DATA-1:0] s_data_out;

    // auxiliary signals
    logic noise = 1'b0;
    logic s_mcu_req_noise;
    int   fd;
    int   code;
    int   noise_delay_int;
    real  noise_delay;
    real  mcu_req_t_on = 10; // unit us
    real  mcu_req_t_off;

    typedef enum {f_10_kHz, f_16_kHz, f_20_kHz} t_sw_freq;
    t_sw_freq sw_freq;
    t_sw_freq sw_freq_curr;

    assign s_mcu_req_noise = s_mcu_req || noise;

    // DUT instance
    sw_freq_filter_top
        #(
            .G_DATA   (G_DATA),
            .G_OFFSET (G_OFFSET)
        )
    dut
        (
            .p_top_sff_clk_in        (s_clk),
            .p_top_sff_rst_n_in      (s_rst_n),
            .p_top_sff_sample_clk_in (s_sample_clk),
            .p_top_sff_mcu_req_in    (s_mcu_req_noise),
            .p_top_sff_data_in       (s_data_in),
            .p_top_sff_data_out      (s_data_out)
        );

    //-------------------------------------------------------------------------

    // clock generator
    always begin
        #(C_CLK_PER/2) s_clk = ~s_clk;
    end

    // sample clock generator
    always begin
        wait_clk(50);
        s_sample_clk <= ~s_sample_clk;
    end

    //-----------------------------------
    // Tasks
    //-----------------------------------

    // generates N cycle active-low reset signal synchronous with clock
    task reset_gen(input int N);
        s_rst_n <= 1'b0;
        repeat (N+1) @(posedge s_clk);
        s_rst_n <= 1'b1;
    endtask : reset_gen;

    // waits for N clock cycles
    task wait_clk(input int N);
        repeat (N)
            @(posedge s_clk);
    endtask : wait_clk

    // generates MCU request signal with random frequency (no noise)
    task mcu_req_rand();
        /* if (!std::randomize(sw_freq) with { sw_freq_curr == f_10_kHz -> sw_freq inside {f_10_kHz, f_16_kHz};
                                            sw_freq_curr == f_16_kHz -> sw_freq inside {f_10_kHz, f_16_kHz, f_20_kHz};
                                            sw_freq_curr == f_20_kHz -> sw_freq inside {f_16_kHz, f_20_kHz};
                                           }) */
        if (!std::randomize(sw_freq))
            $fatal(1, "RANDOMIZATION ERROR (sw_freq)");
            sw_freq_curr = sw_freq;
            case (sw_freq)
                f_10_kHz : mcu_req_t_off = 100.0 - mcu_req_t_on;
                f_16_kHz : mcu_req_t_off = 62.5 - mcu_req_t_on;
                f_20_kHz : mcu_req_t_off = 50.0- mcu_req_t_on;
            endcase
            repeat (C_MCU_REQ_REPS) begin
                s_mcu_req <= 1'b1;
                #(mcu_req_t_on * 1us);
                s_mcu_req <= 1'b0;
                #(mcu_req_t_off * 1us);
            end
    endtask : mcu_req_rand

    // generates MCU request signal (no noise)
    task mcu_req();
        case (sw_freq)
            f_10_kHz : mcu_req_t_off = 100.0 - mcu_req_t_on;
            f_16_kHz : mcu_req_t_off = 62.5 - mcu_req_t_on;
            f_20_kHz : mcu_req_t_off = 50.0- mcu_req_t_on;
        endcase
        repeat (C_MCU_REQ_REPS) begin
            s_mcu_req <= 1'b1;
            #(mcu_req_t_on * 1us);
            s_mcu_req <= 1'b0;
            #(mcu_req_t_off * 1us);
        end
    endtask : mcu_req

    task mcu_req_noise();
        forever begin
            if(!std::randomize(noise_delay_int) with { noise_delay_int inside {[1:C_NOISE_MAX_ON*100]}; })
                $fatal(1, "RANDOMIZATION ERROR (noise noise_delay_int)");
            noise_delay = noise_delay_int/100.0;
            #(noise_delay*1us);
            noise <= 1'b0;
            if(!std::randomize(noise_delay_int) with { noise_delay_int inside {[C_NOISE_MIN_OFF*100:C_NOISE_MAX_OFF*100]}; })
                $fatal(1, "RANDOMIZATION ERROR (noise noise_delay_int)");
            noise_delay = noise_delay_int/100.0;
            #(noise_delay*1us);
            noise <= 1'b1;
        end
    endtask : mcu_req_noise

    // MCU request generator
    initial begin
        @(posedge s_rst_n);
        wait_clk(1);
        fork
            mcu_req_noise();
        join_none
        sw_freq_curr = f_10_kHz;
        // starting frequency is 10 kHz
        /* if (!std::randomize(sw_freq) with { sw_freq == f_10_kHz; })
            $fatal(1, "RANDOMIZATION ERROR (sw_freq)");
        mcu_req(); */
        forever begin
            mcu_req_rand();
        end
    end

    initial begin
        reset_gen(3);
        repeat (C_NO_OF_PERIODS) begin
            fd = $fopen("./../../../../../repo/002_Project/sim/ib_u.txt", "r");
            if (!fd) $fatal(1, "Error while opening file!");
            while (!$feof(fd)) begin
                @(posedge s_sample_clk);
                code = $fscanf(fd, "%d", s_data_in);
            end
            $fclose(fd);
        end
        $finish;
    end;

    final begin
        $display("*** End of simulation! ***");
        $display("*** Simulation was successful! ***");
    end

endmodule : test_sw_freq_filter_top