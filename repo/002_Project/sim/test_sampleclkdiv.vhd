----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_sapleclkdiv.vhd
-- module name      Sample Clock divider - test bench
-- author           Pablo Velasco (pablo.velasco@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            100 times clock divider
-- note             used to generate 1MHz clock for sample of ADCs
----------------------------------------------------------------------------------------------
-- create date      12.09.2019
-- Revision:
--      Revision 0.01 - File Created
--
--      Revision 0.02 by danijela.skugor
--                    -- Changed port names of component sampleClkDiv due to existing changes
----------------------------------------------------------------------------------------------
-- TODO: nothing
--
----------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.ALL;
 
entity Tb_sample_clock_divider is
end Tb_sample_clock_divider;
  
architecture behavior of Tb_sample_clock_divider is
 
-- component declaration for the unit under test (uut)
 
    component sampleclkdiv
       port(
        p_sampleClk_reset_n_in : in std_logic;
        p_sampleClk_clk_in     : in std_logic;
        p_sampleClk_out        : out std_logic
       );
    end component;
 
--inputs
signal sig_clkdiv_clk : std_logic := '0';
signal sig_clkdiv_rst : std_logic := '0';
 
--outputs
signal sig_clkdiv_div : std_logic;
 
-- clock period definitions
constant clk_period : time := 10 ns;
 
begin
 
-- instantiate the unit under test (uut)
    uut: sampleclkdiv port map (
        p_sampleClk_clk_in 	   => sig_clkdiv_clk,
        p_sampleClk_reset_n_in => sig_clkdiv_rst,
        p_sampleClk_out 	   => sig_clkdiv_div
    );
 
-- clock process definitions
clk_process :process
begin
sig_clkdiv_clk <= '0';
wait for clk_period/2;
sig_clkdiv_clk <= '1';
wait for clk_period/2;
end process;
 
-- stimulus process
stim_proc: process
begin
wait for 100 ns;
sig_clkdiv_rst <= '1';
wait for 1000000 ns;
sig_clkdiv_rst <= '0';
wait;
end process;
 
end;
