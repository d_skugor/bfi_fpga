//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             test_diag.sv
// module name      FPGA diagnostics module testbench
// author           David Pavlovic (david.pavlovic@rimac-automobili.com)
// project name     BFI
// target device    XA7A100T-1CSG324Q
// brief            FPGA diagnostics module testbench environment
//--------------------------------------------------------------------------------------------
// create date      10.08.2020.
// Revision:
//      Revision 0.01 by david.pavlovic
//                    - File Created
//
//      Revision 0.02 by david.pavlovic
//                    - Updated fault generation and automatic check to be compliant with
//                      new implementation of diagnostics module
//
//      Revision 0.03 by david.pavlovic
//                    - Removed ports for PWM ROC, PWM Timeout and SEM IP faults
//                    - Removed port for enabling PWM signals
//
//--------------------------------------------------------------------------------------------
// NOTE: This is a SystemVerilog file
//--------------------------------------------------------------------------------------------

import bfi_tb_pkg::*;

module test_diag;

    timeunit 1ns/1ps;

    // parameters
    parameter C_CLK_PER              = 10.0;
    parameter C_SPI_CLK_FREQ         = 5.0;
    parameter C_SPI_GAP_TIME         = 1200; // ns
    parameter C_NO_OF_FAULTS         = 5;
    parameter C_NO_OF_BITSTREAM_MSGS = 3;
    parameter C_NO_OF_TRANS          = 1;
    parameter C_NO_OF_REPEATS        = 1024;
    parameter G_SPI_DATA_W           = 16;
    parameter G_BITSTREAM_VERSION    = 16'h3201;
    parameter G_BITSTREAM_TAG        = 8'h01;
    parameter G_DESIGN_HASH          = 28'hDEDBEEF;
    parameter G_DESIGN_HASH_MSB      = {16'h0, G_DESIGN_HASH[27:16]};
    parameter G_DESIGN_HASH_LSB      = G_DESIGN_HASH[15:0];
    parameter C_NO_OF_SLV            = 1;
    parameter C_ID_BVER              = 4'h0;
    parameter C_ID_BHASH_MSB         = 4'h1;
    parameter C_ID_BHASH_LSB         = 4'h2;
    parameter C_ID_DT                = 4'h5;
    parameter C_ID_FB                = 4'h6;
    parameter C_ID_GDRV              = 4'h7;
    parameter C_ID_PCI               = 4'h8;
    parameter C_ID_ADC               = 4'h9;
    parameter C_MEM_OFFSET           = C_ID_DT;
    parameter C_FLT_DT_MASK          = 8'b11111000;
    parameter C_FLT_FB_MASK          = 8'b11000000;
    parameter C_FLT_GDRV_MASK        = 8'b11100000;
    parameter C_FLT_PCI_MASK         = 8'b11111110;
    parameter C_FLT_ADC_MASK         = 8'b11111110;

    // signals
    logic           s_clk   = 1'b0;
    logic           s_rst_n = 1'b0;
    logic [7:0]     s_flt [C_NO_OF_FAULTS];
    logic [7:0]     s_flt_d [C_NO_OF_FAULTS];
    logic [7:0]     s_flt_mask [C_NO_OF_FAULTS] = {C_FLT_DT_MASK, C_FLT_FB_MASK, C_FLT_GDRV_MASK, C_FLT_PCI_MASK,  C_FLT_ADC_MASK};
    logic [5:0]     s_pwm_en;
    logic           s_flt_gen;
    logic           s_flt_fpga;
    logic [C_NO_OF_FAULTS-1:0] s_flt_en = '0;
    int             s_delay;
    logic           s_gdrv_rdy = 1'b0;
    int             s_n_flts;
    int             s_spi_trans_address;
    int             s_trans_done;
    logic [3:0]     s_id_array[C_NO_OF_FAULTS+C_NO_OF_BITSTREAM_MSGS] = '{C_ID_BVER, C_ID_BHASH_MSB, C_ID_BHASH_LSB,
                                                                          C_ID_DT, C_ID_FB, C_ID_GDRV, C_ID_PCI, C_ID_ADC};

    // events
    event e_trans_done;

    // queues
    logic [G_SPI_DATA_W-1:0] s_mosi_q[$];
    logic [G_SPI_DATA_W-1:0] s_miso_q[$];
    logic [G_SPI_DATA_W-1:0] s_flt_q[$];

    // class handles
    spi_master #(G_SPI_DATA_W, C_NO_OF_SLV) spi;

    // SPI interface
    spi_if #(C_NO_OF_SLV) spi_if_i();

    // DUT instance
    diag_top
        #(
            .G_SPI_DATA_W               (G_SPI_DATA_W),
            .G_BITSTREAM_VERSION        (G_BITSTREAM_VERSION),
            .G_BITSTREAM_TAG            (G_BITSTREAM_TAG),
            .G_DESIGN_HASH              (G_DESIGN_HASH)
        )
    dut_diag
        (
            // Main signals
            .p_diag_top_clk_in          (s_clk),
            .p_diag_top_rst_n_in        (s_rst_n),
            .p_diag_top_gdrv_rdy_in     (s_gdrv_rdy),
            // Fault signals
            .p_diag_top_flt_dt_n_in     (s_flt[0][2:0]),
            .p_diag_top_flt_fb_n_in     (s_flt[1][5:0]),
            .p_diag_top_flt_gdrv_n_in   (s_flt[2][4:0]),
            .p_diag_top_flt_pci_n_in    (s_flt[3][0]),
            .p_diag_top_flt_adc_n_in    (s_flt[4][0]),
            .p_diag_top_flt_gen_n_out   (s_flt_gen),
            .p_diag_top_flt_fpga_n_out  (s_flt_fpga),
            // SPI interface
            .p_diag_top_spi_sck_in      (spi_if_i.sck),
            .p_diag_top_spi_ss_n_in     (spi_if_i.ss_n),
            .p_diag_top_spi_mosi_in     (spi_if_i.mosi),
            .p_diag_top_spi_miso_out    (spi_if_i.miso)
        );

    //-------------------------------------------------------------------------

    // clock generator
    always begin
        #(C_CLK_PER/2) s_clk = ~s_clk;
    end

    always_ff @(posedge s_clk or negedge s_rst_n)
        if (!s_rst_n) s_flt_d <= '{default:'1};
        else s_flt_d <= s_flt;

    always_ff @(posedge s_clk or negedge s_rst_n)
        if (!s_rst_n) s_gdrv_rdy <= 1'b0;
        else if (s_flt[2][4:0] == 5'b11111) s_gdrv_rdy <= 1'b1;

    //-----------------------------------
    // Tasks
    //-----------------------------------

    // generates N cycle active-low reset signal synchronous with clock
    task reset_gen(input int N);
        s_rst_n <= 1'b1;
        @(posedge s_clk);
        s_rst_n <= 1'b0;
        repeat (N+1) @(posedge s_clk);
        s_rst_n <= 1'b1;
    endtask : reset_gen;

    // waits for N clock cycles
    task wait_clk(input int N);
        repeat (N)
            @(posedge s_clk);
    endtask : wait_clk

    //-----------------------------------
    // Functions
    //-----------------------------------

    function void randomize_flts();
        int cnt;
        if (!std::randomize(s_flt))
            $fatal(1, "RANDOMIZATION ERROR (s_flt)");
        if (!std::randomize(s_flt_en) with { s_flt_en != '1; } )
            $fatal(1, "RANDOMIZATION ERROR (s_flt_en)");
        foreach (s_flt[i])
            if (s_flt_en[i]) s_flt[i] <= '1;
            else s_flt[i] <= s_flt[i] | s_flt_mask[i];
    endfunction : randomize_flts

    task wait_random();
        if (!std::randomize(s_delay) with { s_delay inside {[30:50]}; } )
            $fatal(1, "RANDOMIZATION ERROR (s_delay)");
        wait_clk(s_delay);
    endtask : wait_random

    // checks if TB receives equal data word as sent from diag_top module
    task check_trans();
        bit [G_SPI_DATA_W-1:0]   mosi;
        bit [G_SPI_DATA_W-1:0]   miso;
        bit [G_SPI_DATA_W-1:0]   expected_miso;
        bit [G_SPI_DATA_W-4-1:0] header;
        bit [ 3:0]               address;
        bit [23:0]               crc_data;
        bit [ 7:0]               crc_val;
        mosi = s_mosi_q.pop_front();
        header = mosi[G_SPI_DATA_W-1:4];
        address = mosi[3:0];
        // ignore the first MISO data
        miso = s_miso_q.pop_front();
        miso = s_miso_q.pop_front();
        crc_data[23:8] = miso;
        // check data frame
        case (address)
            //--------------------------------
            C_ID_BVER : begin
                if (miso != G_BITSTREAM_VERSION) begin
                    $display("Received bitstream version = %4h", miso);
                    $display("Expected bitstream version = %4h", G_BITSTREAM_VERSION);
                    $fatal(1, "*** BITSTREAM VERSION FAULT ***");
                end
            end
            //--------------------------------
            C_ID_BHASH_MSB : begin
                if (miso != G_DESIGN_HASH_MSB) begin
                    $display("Received bitstream hash msb = %4h", miso);
                    $display("Expected bitstream hash msb = %4h", G_DESIGN_HASH_MSB);
                    $fatal(1, "*** BITSTREAM HASH MSB FAULT ***");
                end
            end
            //--------------------------------
            C_ID_BHASH_LSB : begin
                if (miso != G_DESIGN_HASH_LSB) begin
                    $display("Received bitstream hash lsb = %4h", miso);
                    $display("Expected bitstream hash lsb = %4h", G_DESIGN_HASH_LSB);
                    $fatal(1, "*** BITSTREAM HASH LSB FAULT ***");
                end
            end
            //--------------------------------
            C_ID_DT, C_ID_FB, C_ID_GDRV, C_ID_PCI, C_ID_ADC : begin
                expected_miso[15:12] = 4'h0;
                expected_miso[11:8] = address;
                expected_miso[7:0] = s_flt[address - C_MEM_OFFSET];
                if (miso != expected_miso) begin
                    $display("Received SPI frame = %4h", miso);
                    $display("Expected SPI frame = %4h", expected_miso);
                    $fatal(1, "*** DATA FRAME FAULT ***");
                end
            end
            //--------------------------------
            default : ;
            //--------------------------------
        endcase
        // check CRC frame
        miso = s_miso_q.pop_front();
        crc_data[7:0] = miso[15:8];
        crc_val = miso[7:0];
        if (!crc#(24, 8,'b100011101)::check_crc(crc_data, crc_val))
            $fatal(1, "*** Wrong CRC value! ***");
        case (address)
            //--------------------------------
            C_ID_BVER : begin
                if (miso[G_SPI_DATA_W-1:G_SPI_DATA_W-8] != G_BITSTREAM_TAG) begin
                    $display("Received bitstream tag = %4h", miso);
                    $display("Expected bitstream tag = %4h", G_BITSTREAM_TAG);
                    $fatal(1, "*** BITSTREAM TAG FAULT ***");
                end
            end
            //--------------------------------
            C_ID_DT, C_ID_FB, C_ID_GDRV, C_ID_PCI, C_ID_ADC : begin
                if (miso[G_SPI_DATA_W-1:G_SPI_DATA_W-8] != 8'h00) begin
                    $display("Received SPI frame = %4h", miso);
                    $display("Expected SPI frame = %4h", 8'h00);
                    $fatal(1, "*** CRC FRAME FAULT ***");
                end
            end
            //--------------------------------
            default : ;
            //--------------------------------
        endcase
    endtask : check_trans

    task spi_trans(input bit [3:0] addr);
        s_spi_trans_address = addr;
        spi.mosi = {12'hFFF, addr};
        s_mosi_q.push_back(spi.mosi);
        repeat (3) begin
            spi.drive();
            wait_random();
            s_miso_q.push_back(spi.miso);
        end
        check_trans();
    endtask

    task spi_read_all();
        foreach(s_id_array[i])
            spi_trans(s_id_array[i]);
        //spi_trans(4'h1);
    endtask

    task main_test();
        //s_gdrv_rdy = 1'b1;
        //spi_read_all();
        wait_random();
        repeat (C_NO_OF_REPEATS) begin
            randomize_flts();
            wait_random();
            spi_read_all();
            foreach(s_flt[i])
                s_flt[i] <= '1;
            reset_gen(5);
            wait_random();
        end
    endtask : main_test

    task gdrv_rdy_direct_test();
        s_flt[2] <= '0;
        for (int i = 0; i <= 3; i++) begin
            wait_clk(100);
            s_flt[2][i] <= 1'b1;
        end
        foreach (s_flt[2][i]) begin
            wait_clk(100);
            s_flt[2][i] <= 1'b0;
        end
        for (int i = 0; i <= 4; i++) begin
            wait_clk(100);
            s_flt[2][i] <= 1'b1;
        end
        //wait_clk(1);
        //s_flt[2][4] <= 1'b0;
        //wait_clk(1);
        //s_flt[2][4] <= 1'b1;
        wait_clk(100);
        foreach (s_flt[2][i]) begin
            wait_clk(100);
            s_flt[2][i] <= 1'b0;
        end
        for (int i = 0; i <= 4; i++) begin
            wait_clk(100);
            s_flt[2][i] <= 1'b1;
        end
    endtask : gdrv_rdy_direct_test

    // main TB sequence
    initial begin
        spi = new(spi_if_i);
        spi.set_freq(C_SPI_CLK_FREQ);
        spi.set_tcsc(spi.period/2);
        s_flt    <= '{default:'1};
        s_flt[2] <= '{default:'0};
        reset_gen(5);
        main_test();
        //gdrv_rdy_direct_test();
        #2us;
        $display("*** End of simulation! ***");
        $display("*** Simulation was successful! ***");
        $finish;
    end

endmodule : test_diag