xvhdl -nolog ../src/lpf/pwm_pack.vhd
xvhdl -2008 -nolog ../src/lpf/lpf_pwm_fctrl.vhd
xvhdl -nolog test_lpf_pwm_fctrl.vhd
xelab -debug typical test_lpf_pwm_fctrl -s top_sim -nolog
xsim top_sim -gui -nolog

# Cleanup
rm *.log
rm *.pb
rm -rf xsim.dir
rm *.jou
rm *.wdb