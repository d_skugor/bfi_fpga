xvhdl -nolog ../src/lpf/pwm_pack.vhd
xvhdl -nolog ../../001_Common/src/edge_detector_p.vhd
xvhdl -nolog ../src/lpf/medfilt_sampler.vhd
xvhdl -2008 -nolog ../src/lpf/medfilt_sequencer.vhd
xvhdl -nolog test_medfilt_sequencer.vhd
xelab -debug typical test_medfilt_sequencer -s top_sim -nolog 
xsim top_sim -gui -nolog

# Cleanup
rm *.log
rm *.pb
rm -rf xsim.dir
rm *.jou
rm *.wdb
