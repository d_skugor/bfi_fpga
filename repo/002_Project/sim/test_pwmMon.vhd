----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pwmMon_simulation.vhd
-- module name      PWM Monitor Simulation
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100TCSG324-1L
-- brief            Top Level Module
----------------------------------------------------------------------------------------------
-- create date      10.09.2019
-- Revision:
--      Revision 0.01 - File Created
----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity test_pwmMon is
--  Port ( );
end test_pwmMon;

architecture tb of test_pwmMon is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

component pwm_monitor_top is
    generic (
        G_NUM_OF_ERR_CYCLES : integer := 4;
        G_SYSTEM_FREQ       : integer := 100000000; -- 100 MHz
        G_PWM_MIN_FREQ      : integer := 1000;      -- 1 KHz
        G_DEAD_TIME_IN_NS   : integer := 100        -- 100 ns  
    );
    port(
        p_pwmMon_clk_in     : in  std_logic;  -- module clock                       
        p_pwmMon_rst_n_in   : in  std_logic;  -- main reset / active low
        -- PWM interface inputs
        p_pwmMon_aHs_in     : in std_logic;
        p_pwmMon_aLs_in     : in std_logic;
        p_pwmMon_bHs_in     : in std_logic;
        p_pwmMon_bLs_in     : in std_logic;
        p_pwmMon_cHs_in     : in std_logic;
        p_pwmMon_cLs_in     : in std_logic;
        --PWM enable
        p_pwmMon_pwmEn_in   : in std_logic;
         -- Gray counter error 
        p_pwmMon_gCErr_out  : out std_logic;  -- gray counter error - pulsed signal
         -- PWM error 
        p_pwmMon_fault_out  : out std_logic;  -- error flag
        -- PWM interface outputs
        p_pwmMon_aHs_out    : out std_logic;
        p_pwmMon_aLs_out    : out std_logic;
        p_pwmMon_bHs_out    : out std_logic;
        p_pwmMon_bLs_out    : out std_logic;
        p_pwmMon_cHs_out    : out std_logic;
        p_pwmMon_cLs_out    : out std_logic 
    );
end component;

    ---------------------------------------------------------------------------------
    --                                  constants
    ---------------------------------------------------------------------------------
    constant A_LINE_PERIOD : time :=  12500 ns; -- 40 kHz
    constant B_LINE_PERIOD : time :=  31250 ns; -- 16 kHZ
    constant C_LINE_PERIOD : time := 100000 ns; --  5 kHz
    constant SYSTEM_PERIOD : time := 5 ns ;      -- 100 MHz
    constant DEAD_TIME     : time := 100 ns;

    ---------------------------------------------------------------------------------
    --                                  signals
    ---------------------------------------------------------------------------------
    
    signal s_clk   : std_logic := '0';
    signal s_reset : std_logic := '1';
    
    signal s_pwmMon_aHs_in    : std_logic := '1';
    signal s_pwmMon_aLs_in    : std_logic := '0';  
    signal s_pwmMon_bHs_in    : std_logic := '1';  
    signal s_pwmMon_bLs_in    : std_logic := '0';  
    signal s_pwmMon_cHs_in    : std_logic := '1';  
    signal s_pwmMon_cLs_in    : std_logic := '0';  
                     
    signal s_pwmMon_pwmEn     : std_logic := '0';   
                     
    signal s_pwmMon_aHs_out   : std_logic := '0';  
    signal s_pwmMon_aLs_out   : std_logic := '0'; 
    signal s_pwmMon_bHs_out   : std_logic := '0';  
    signal s_pwmMon_bLs_out   : std_logic := '0'; 
    signal s_pwmMon_cHs_out   : std_logic := '0'; 
    signal s_pwmMon_cLs_out   : std_logic := '0'; 
    signal s_pwmMon_fault_out : std_logic := '0';
    signal s_grayCnt_err      : std_logic := '0';
    
begin

    
    s_reset <= '1', '0' after 350 us, '1' after 455 us ;
    s_pwmMon_pwmEn <= '1' after 50000 ns;
  

    clk_generate: process
    begin
        wait for 5 ns; -- 100 Mhz
        s_clk <= not s_clk;
    end process;
    
    
    --- GENERATE A LINES 
    -- generate frozen lines error on  A lines
    pwm_aL_generate: process
     variable v_strt_a : std_logic := '1';
    begin
         if v_strt_a = '1' then
            wait for A_LINE_PERIOD ;
            s_pwmMon_aLs_in <= not s_pwmMon_aLs_in;
            wait for A_LINE_PERIOD ;
            s_pwmMon_aLs_in <= not s_pwmMon_aLs_in;
            wait for A_LINE_PERIOD ;
            s_pwmMon_aLs_in <= not s_pwmMon_aLs_in;
            wait for A_LINE_PERIOD ;
            s_pwmMon_aLs_in <= not s_pwmMon_aLs_in;
            wait for A_LINE_PERIOD ;
            s_pwmMon_aLs_in <= not s_pwmMon_aLs_in;
            wait for A_LINE_PERIOD ;
            wait for  5*(2*A_LINE_PERIOD); -- froze line for 5 cycles
            v_strt_a := '0';
        else
            s_pwmMon_aLs_in <= not s_pwmMon_aLs_in;
            wait for A_LINE_PERIOD ;
        end if;
    end process;
    
    pwm_ah_generate: process
        variable v_strt_a : std_logic := '1';
    begin
        if v_strt_a = '1' then
            wait for A_LINE_PERIOD  - DEAD_TIME;
            s_pwmMon_aHs_in <= not s_pwmMon_aHs_in;
            wait for A_LINE_PERIOD + 2*DEAD_TIME;
            s_pwmMon_aHs_in <= not s_pwmMon_aHs_in;
             wait for A_LINE_PERIOD - 2*DEAD_TIME;
            s_pwmMon_aHs_in <= not s_pwmMon_aHs_in;
            wait for A_LINE_PERIOD + 2*DEAD_TIME;
            s_pwmMon_aHs_in <= not s_pwmMon_aHs_in;
            wait for A_LINE_PERIOD - 2*DEAD_TIME ;
            s_pwmMon_aHs_in <= not s_pwmMon_aHs_in;
            wait for A_LINE_PERIOD + 2*DEAD_TIME ;
            --sig_pwmMon_aHs_in <= not sig_pwmMon_aHs_in;
            wait for  5*(2*A_LINE_PERIOD) ;
            s_pwmMon_aHs_in <= not s_pwmMon_aHs_in;
            v_strt_a := '0';
        else
            wait for A_LINE_PERIOD - 2*DEAD_TIME;
            s_pwmMon_aHs_in <= not s_pwmMon_aHs_in;
            wait for A_LINE_PERIOD + 2*DEAD_TIME;
            s_pwmMon_aHs_in <= not s_pwmMon_aHs_in;
            wait for A_LINE_PERIOD - 2*DEAD_TIME;
            s_pwmMon_aHs_in <= not s_pwmMon_aHs_in;
            wait for A_LINE_PERIOD + 2*DEAD_TIME;
            s_pwmMon_aHs_in <= not s_pwmMon_aHs_in;
        end if;
               
    end process;
    
    
    --- GENERATE B LINES
    -- generate dead time error on B lines
    pwm_bL_generate: process
    begin
        wait for B_LINE_PERIOD ;
        s_pwmMon_bLs_in <= not s_pwmMon_bLs_in;
    end process;
    
     pwm_Bh_generate: process
        variable v_strt_b : std_logic := '1';
    begin
        if v_strt_b = '1' then
            wait for B_LINE_PERIOD  - DEAD_TIME;
            s_pwmMon_bHs_in <= not s_pwmMon_bHs_in;
            wait for B_LINE_PERIOD + 2*DEAD_TIME;
            s_pwmMon_bHs_in <= not s_pwmMon_bHs_in;
            wait for B_LINE_PERIOD - 2*DEAD_TIME ;
            s_pwmMon_bHs_in <= not s_pwmMon_bHs_in;
            wait for B_LINE_PERIOD + 2*DEAD_TIME + 200 ns;
            s_pwmMon_bHs_in <= not s_pwmMon_bHs_in;
            wait for B_LINE_PERIOD - 2*DEAD_TIME;
            s_pwmMon_bHs_in <= not s_pwmMon_bHs_in;
            wait for B_LINE_PERIOD + 2*DEAD_TIME - 200 ns;
            s_pwmMon_bHs_in <= not s_pwmMon_bHs_in;
            v_strt_b := '0';
        else
            wait for B_LINE_PERIOD - 2*DEAD_TIME;
            s_pwmMon_bHs_in <= not s_pwmMon_bHs_in;
            wait for B_LINE_PERIOD + 2*DEAD_TIME;
            s_pwmMon_bHs_in <= not s_pwmMon_bHs_in;
        end if;
    end process;  
    
            

    --- GENERATE C LINES      
    -- generate shoot trough on C lines  
    pwm_cL_generate: process
    begin
        wait for C_LINE_PERIOD ;
        s_pwmMon_cLs_in <= not s_pwmMon_cLs_in;
    end process;
    
    pwm_cH_generate: process
      variable v_strt_c : std_logic := '1';
    begin
        if v_strt_c = '1' then
            wait for C_LINE_PERIOD  - DEAD_TIME;
            s_pwmMon_cHs_in <= not s_pwmMon_cHs_in;
            wait for C_LINE_PERIOD + 2*DEAD_TIME;
            s_pwmMon_cHs_in <= not s_pwmMon_cHs_in;
            wait for C_LINE_PERIOD - 2*DEAD_TIME;
            s_pwmMon_cHs_in <= '1';
            wait for C_LINE_PERIOD + 2*DEAD_TIME;
            v_strt_c := '0';
        else
            wait for C_LINE_PERIOD - 2*DEAD_TIME;
            s_pwmMon_cHs_in <= not s_pwmMon_cHs_in;
            wait for C_LINE_PERIOD + 2*DEAD_TIME;
            s_pwmMon_cHs_in <= not s_pwmMon_cHs_in;
        end if;
    end process;  
    

    pwm_mon_top_block : pwm_monitor_top
        generic map(
            G_NUM_OF_ERR_CYCLES => 4,
            G_SYSTEM_FREQ       => 100000000, -- 100 MHz
            G_PWM_MIN_FREQ      => 1000,      -- 1 KHz
            G_DEAD_TIME_IN_NS   => 100        -- 100 ns  
        )
        port map(
            p_pwmMon_clk_in    => s_clk,                   
            p_pwmMon_rst_n_in  => s_reset,
            -- PWM interface inputs
            p_pwmMon_aHs_in    => s_pwmMon_aHs_in,
            p_pwmMon_aLs_in    => s_pwmMon_aLs_in,
            p_pwmMon_bHs_in    => s_pwmMon_bHs_in,
            p_pwmMon_bLs_in    => s_pwmMon_bLs_in,
            p_pwmMon_cHs_in    => s_pwmMon_cHs_in,
            p_pwmMon_cLs_in    => s_pwmMon_cLs_in,
            --PWM enable
            p_pwmMon_pwmEn_in  => s_pwmMon_pwmEn,
            p_pwmMon_gCErr_out => s_grayCnt_err, 
            p_pwmMon_fault_out => s_pwmMon_fault_out, 
            -- PWM interface sig_pwmMon_fault_out
            p_pwmMon_aHs_out   => s_pwmMon_aHs_out,
            p_pwmMon_aLs_out   => s_pwmMon_aLs_out,
            p_pwmMon_bHs_out   => s_pwmMon_bHs_out,
            p_pwmMon_bLs_out   => s_pwmMon_bLs_out,
            p_pwmMon_cHs_out   => s_pwmMon_cHs_out,
            p_pwmMon_cLs_out   => s_pwmMon_cLs_out  
        );

end tb;
