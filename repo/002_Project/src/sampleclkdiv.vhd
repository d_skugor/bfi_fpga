----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             sapleclkdiv.vhd
-- module name      Clock divider - Behavioral
-- author           Pablo Velasco (pablo.velasco@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            clock divider for sample clk
-- note             used to generate 1MHz clock for sampling of ADCs
----------------------------------------------------------------------------------------------
-- create date      12.09.2019
-- Revision:
--      Revision 0.01 by pablo.velasco
--                    - File Created
--      Revision 0.02 by arturo.arreola
--                    - Modified to comply with coding standard
--                    - Modified indentation
--                    - Modified libraries
--      Revision 0.03 by danijela.skugor
--                    - Substitute binary counter with gray code counter
--
----------------------------------------------------------------------------------------------
-- TODO: - change counters to define a duty cycle D and not HI and LO times
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity sampleclkdiv is
    generic (
        -- number of main clk cycles on high-polarity. Count starts at 0. A limit of 49
        -- = "00110001" will result in a 100 times division which will output a 1 MHz clock
        G_COUNT_LIM_HI       : std_logic_vector(7 downto 0):= "00110001";
        -- number of main clk cycles on low polarity. Care must be taken to keep sum of
        -- low and high proportional
        G_COUNT_LIM_LO       : std_logic_vector(7 downto 0):= "00110001");
    port (
        -- main inputs
        p_sampleClk_reset_n_in : in  std_logic; -- main reset line
        p_sampleClk_clk_in     : in  std_logic; -- main system clock (100 MHz)
        --outputs
        p_sampleClk_out        : out std_logic  -- output of divided clock
    );
end entity sampleclkdiv;

architecture rtl of sampleclkdiv is


    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component gray_counter is
        generic(
            G_GRAYCNT_WIDTH             : integer := 4;
            G_GRAYCNT_CORRECTION_ON     : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port(
            p_grayCnt_clk_in    : in std_logic;
            p_grayCnt_rst_n_in  : in std_logic;  -- sync reset
            p_grayCnt_en_in     : in std_logic;  -- enable
            p_grayCnt_err_p_out : out std_logic; -- pulsed error
            p_grayCnt_res_out   : out std_logic_vector(G_GRAYCNT_WIDTH -1 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                  functions
    ---------------------------------------------------------------------------------
    function bin_to_gray (bin : std_logic_vector) return std_logic_vector is
        variable v_gray : std_logic_vector(bin'range);
    begin
        v_gray(bin'left) := bin(bin'left);
        for i in bin'left-1 downto 0 loop
            v_gray(i) := bin(i+1) xor bin(i);
        end loop;
        return v_gray;
    end function bin_to_gray;

    ---------------------------------------------------------------------------------
    --                                  constants
    ---------------------------------------------------------------------------------

    constant C_COUNT_LIM_LEN  : integer := 8;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_count     : std_logic_vector(C_COUNT_LIM_LEN-1 downto 0):=(others => '0');
    signal s_clkval    : std_logic := '0';
    signal s_count_en  : std_logic;
    signal s_cnt_rst_n : std_logic;

begin

    -- clock divider process
    process(p_sampleClk_clk_in, p_sampleClk_reset_n_in)
        begin
            -- reset
            if (p_sampleClk_reset_n_in = '0') then
                s_count_en      <= '0';
                s_cnt_rst_n     <= '0';
                s_clkval        <= '0';
                p_sampleClk_out <= '0';
                -- clock increment and output toggle
            elsif(rising_edge (p_sampleClk_clk_in)) then
                s_cnt_rst_n  <= '1';
                s_count_en   <= '1';
                if (s_clkval = '0') then
                    if (s_count = bin_to_gray(G_COUNT_LIM_LO-1)) then
                        s_clkval    <= '1';
                        s_cnt_rst_n <= '0';
                    end if;
                else
                    if (s_count = bin_to_gray(G_COUNT_LIM_HI-1)) then
                        s_clkval    <= '0';
                        s_cnt_rst_n <= '0';
                    end if;
                end if;
            p_sampleClk_out <= s_clkval;
        end if;
    end process;

	-- Gray code  counter
    gray_cnt : gray_counter
        generic map(
            G_GRAYCNT_WIDTH            => C_COUNT_LIM_LEN,
            G_GRAYCNT_CORRECTION_ON    => true
        )
        port map(
            p_grayCnt_clk_in    => p_sampleClk_clk_in,
            p_grayCnt_rst_n_in  => s_cnt_rst_n,
            p_grayCnt_en_in     => s_count_en,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_count
        );
        
end architecture rtl;

