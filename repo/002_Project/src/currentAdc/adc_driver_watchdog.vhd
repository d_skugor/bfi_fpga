----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             adc_driver_watchdog.vhd
-- module name      ADC driver watchdog
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Watchdog module for ADC driver for TI THS1206 ADC chip
----------------------------------------------------------------------------------------------
-- create date      17.07.2020.
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Changed watchdog fault signal to be active-low
--
--      Revision 0.03 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.04 by danijela.skugor
--                    - Substitute binary counter with gray code counter
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real."ceil";
use ieee.math_real."log2";

entity adc_driver_watchdog is
    generic (
        G_ADC_WD_MAX_ERR       : natural := 3
    );
    port (
        -- main signals
        p_adc_wd_clk_in        : in  std_logic;
        p_adc_wd_rst_n_in      : in  std_logic;
        -- interface to ADC
        p_adc_wd_error_in      : in  std_logic;
        p_adc_wd_error_clr_out : out std_logic;
        -- fault signal
        p_adc_wd_flt_n_out     : out std_logic
    );
end entity adc_driver_watchdog;

architecture rtl of adc_driver_watchdog is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component gray_counter is
        generic(
            G_GRAYCNT_WIDTH             : integer := 4;
            G_GRAYCNT_CORRECTION_ON     : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port(
            p_grayCnt_clk_in    : in std_logic;
            p_grayCnt_rst_n_in  : in std_logic;  -- sync reset
            p_grayCnt_en_in     : in std_logic;  -- enable
            p_grayCnt_err_p_out : out std_logic; -- pulsed error
            p_grayCnt_res_out   : out std_logic_vector(G_GRAYCNT_WIDTH -1 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------

    function bin_to_gray (bin : std_logic_vector) return std_logic_vector is
        variable v_gray : std_logic_vector(bin'range);
    begin
        v_gray(bin'left) := bin(bin'left);
        for i in bin'left-1 downto 0 loop
            v_gray(i) := bin(i+1) xor bin(i);
        end loop;
        return v_gray;
    end function bin_to_gray;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------
    
    constant C_CNT_WAIT_LEN : integer := 7;
    constant C_WAIT_PERIOD  : std_logic_vector(C_CNT_WAIT_LEN-1 downto 0) := bin_to_gray(std_logic_vector(to_unsigned(99, C_CNT_WAIT_LEN))); -- 1 us wait period
    constant C_CNT_W        : integer := integer(ceil(log2(real(G_ADC_WD_MAX_ERR))));

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    type t_state_wd is (
        t_state_wd_wait_err,
        t_state_wd_reset_adc,
        t_state_wd_wait_period,
        t_state_wd_error
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk        : std_logic;
    signal s_rst_n      : std_logic;
    signal s_error      : std_logic;
    signal s_error_en   : std_logic;
    signal s_error_d    : std_logic;
    signal s_error_re   : std_logic;
    signal s_error_re_d : std_logic;
    signal s_error_clr  : std_logic;
    signal s_error_flt  : std_logic;
    signal s_state      : t_state_wd;
    signal s_cnt        : std_logic_vector(C_CNT_WAIT_LEN-1 downto 0);
    signal s_cnt_en     : std_logic;
    signal s_cnt_err    : std_logic_vector(C_CNT_W downto 0);

begin

    -- input signal assignments
    s_clk   <= p_adc_wd_clk_in;
    s_rst_n <= p_adc_wd_rst_n_in;
    s_error <= p_adc_wd_error_in;

    -- output signal assignments
    p_adc_wd_error_clr_out <= s_error_clr;
    p_adc_wd_flt_n_out     <= s_error_flt;

    -- s_error rising edge detector
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_error_d  <= '0';
            s_error_re <= '0';
        elsif rising_edge(s_clk) then
            s_error_d  <= s_error;
            if (s_error = '1' and s_error_d = '0') then
                s_error_re   <= '1';
            else
                s_error_re <= '0';
            end if;
            s_error_re_d <= s_error_re;
        end if;
    end process;

    -- Gray code error counter
    gray_cnt : gray_counter
        generic map(
            G_GRAYCNT_WIDTH            => C_CNT_W+1,
            G_GRAYCNT_CORRECTION_ON    => true
        )
        port map(
            p_grayCnt_clk_in    => s_clk,
            p_grayCnt_rst_n_in  => s_rst_n,
            p_grayCnt_en_in     => s_error_en,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_cnt_err
        );

    s_error_en <= s_error_re and s_error_flt;

    -- Gray code wait counter
    gray_cnt_wait : gray_counter
        generic map(
            G_GRAYCNT_WIDTH            => C_CNT_WAIT_LEN,
            G_GRAYCNT_CORRECTION_ON    => true
        )
        port map(
            p_grayCnt_clk_in    => s_clk,
            p_grayCnt_rst_n_in  => s_error_clr,
            p_grayCnt_en_in     => s_cnt_en,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_cnt
        );

    -- FSM
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_cnt_en    <= '0';
            s_error_clr <= '0';
            s_error_flt <= '1';
            s_state     <= t_state_wd_wait_err;
        elsif rising_edge(s_clk) then
            s_error_clr <= '0';
            s_error_flt <= '1';
            case s_state is
                ------------------------------------------------
                when t_state_wd_wait_err =>
                    if (s_error_re_d = '1') then
                        if (s_cnt_err > bin_to_gray(std_logic_vector(to_unsigned(integer(G_ADC_WD_MAX_ERR), C_CNT_W+1)))) then
                            s_state     <= t_state_wd_error;
                            s_error_flt <= '0';
                        else
                            s_state     <= t_state_wd_wait_period;
                            s_cnt_en    <= '0';
                            s_error_clr <= '0';
                        end if;
                    end if;
                ------------------------------------------------
                when t_state_wd_wait_period =>
                    if (s_cnt = C_WAIT_PERIOD) then
                        s_error_clr <= '1';
                        s_state     <= t_state_wd_reset_adc;
                    else
                        s_cnt_en    <= '1';
                        s_error_clr <= '1';
                    end if;
                ------------------------------------------------
                when t_state_wd_reset_adc =>
                    s_state <= t_state_wd_wait_err;
                ------------------------------------------------
                when others =>
                    -- t_state_wd_error state
                    s_error_flt <= '0';
                ------------------------------------------------
            end case;
        end if;
    end process;

end architecture rtl;