
----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             adc_driver.vhd
-- module name      adc_driver - Behavioral
-- author           Marko Gulin (marko.gulin@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Driver for TI THS1206 ADC chip
----------------------------------------------------------------------------------------------
-- create date      29.10.2019
-- Revision:
--      Revision 0.01 by marko.gulin
--                    - File Created
--
--      Revision 0.02 by jsanchez
--                  - Code rewritten completely
--                  - Parametrizable trigger level for ADC FIFO, to increase the througput when
--                      using decimation filters.
--                  - Renaming ports to identify easily which ones go to the ADC. Removing the
--                      p_adc_ prefix
--                  - Error condition output port. This will detect when the ADC is not operating
--                      properly, because: initialization test was unsuccessful, or when there is
--                      no data valid pulses from the ADC.
--                  - THIS VERSION DOES NOT HAVE SPECIFIC SEU MITIGATION MECHANISMS
--
--      Revision 0.03 by jsanchez
--                  - Rewritten ISM to fix a sampling interval jitter, by separating the CONVSTn
--                      signal generation process from the FIFO read process.
--                  - Added a timeout error indication when no data is received from the ADC for more
--                      than three sampling intervals. The timeout error will be signaled in the error
--                      condition output port.
--                  - Corrected the continuous sampling mode, where the speed was /4 due to a bug in
--                      the configuration of a clock divider.
--
--      Revision 0.04 by jsanchez
--                  - Corrected errors in comments
--                  - Removed the restriction to use a 100MHz clock, in order to sample at different rates.
--                  - Added a selectable FIFO_RESET after each conversion, in order to guarantee the channel
--                      sequencing from frame to frame in case of a synchronization error.
--                  - Configuration registers do not use an intermediate register now. Generic-initialized constants
--                      are used instead.
--                  - Conv_mode generic type changed to std_logic. Only two values can be selected, and the generic can
--                      be used to initialize a constant, removing intermediate logic.
--
--      Revision 0.05 by jsanchez
--                  - Updated generics
--                  - G_ADC_TRIGGER_LEVEL generic was not having any effect. Corrected.
--                  - Test data during the initialization phase, was not using the correct register (s_test_data). Corrected.
--
--      Revision 0.06 by jsanchez
--                  - Modified bidirectional port control logic, to avoid inconsistend packing of FFs in IOBs.
--                  - Modified ADC initialization sequence, introducing wait-states to avoid initialization problems. It has been shown during
--                      lab tests that the internal FIFO requires more than 833 nS to reset, and so three sample intervals have been inserted to
--                      guarantee the operation after the initialization phase.
--                      The error routine now has been improved, and there is a self-recovery in case of an initialization failure. This will work only
--                      if there was an initialization fault due to a SEU event during the initialization phase, but will not work if there was a long
--                      transient in the power.
--                      If the initialization fails, now a flag will indicate the problem. This has been done to distinguish between errors that happened
--                      before the system was running (i.e. when the motor is not rotating yet), from errors that happened during operation and which can be
--                      potentially harmful. The external watchdog will take care of the latter, indicated by the adc_error_flag.
--                  - Reorganized FSM states to have a more logical order.
--
--      Revision 0.06 by dario.drvenkar
--                  - Added xpm_cdc_single macro for signal synchronization
--                  - Edge detector substituted with edge_detector_p component
--
--      Revision 0.07 by david.pavlovic
--                  - Code cleanup
--
----------------------------------------------------------------------------------------------
--       - TBC: Add FIFO reset after every read to ensure synchronism => explain why we won't add FIFO reset
--       - Account for functional safety
--
-- NOTE: This is a VHDL2008 file
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

-- Entity definition
entity adc_driver is
    generic (
        G_ADC_TRIGGER_LEVEL  : natural   := 0;                      -- Valid trigger levels range from 0 to 2 (see datasheet table 13, pp.28).
        G_ADC_CONV_MODE      : std_logic := '1';                    -- Conversion mode: continuous = 0, on-demand = 1
        G_ADC_OUTPUT_FORMAT  : std_logic := '1';                    -- 2's complement = 0, binary = 1
        G_ADC_FIFO_RESET     : std_logic := '1';                    -- Enable automatic reset of the FIFO after each conversion.
        G_ADC_AUTO_OFFSET    : std_logic := '0';                    -- Automatic offset compensation
        G_ADC_REF_EXT        : std_logic := '0'                     -- Select the external ADC reference voltage
    );
    port (
        -- Main signals
        p_adc_clk_in         : in    std_logic;                     -- clock
        p_adc_rst_n_in       : in    std_logic;                     -- reset (active-low)
        -- ADC
        p_adc_conv_n_out     : out   std_logic;                     -- conversion trigger (active low)
        p_adc_cs0_n_out      : out   std_logic;                     -- control signal (active low)
        p_adc_cs1_out        : out   std_logic;                     -- control signal (active high)
        p_adc_rd_n_out       : out   std_logic;                     -- read trigger (active low)
        p_adc_wr_n_out       : out   std_logic;                     -- write trigger (active low)
        p_adc_dav_in         : in    std_logic;                     -- data available flag
        p_adc_data_bi        : inout std_logic_vector(11 downto 0); -- ADC data bus
        -- Interface
        p_adc_error_out      : out   std_logic;                     -- Error condition flag
        p_adc_clr_error_p_in : in    std_logic;                     -- Clear error flag, and restart ADC
        p_adc_init_fault_out : out   std_logic;                     -- Initialization fault
        p_adc_data_ch0_out   : out   std_logic_vector(11 downto 0); -- ADC data channel 0
        p_adc_data_ch1_out   : out   std_logic_vector(11 downto 0); -- ADC data channel 1
        p_adc_data_ch2_out   : out   std_logic_vector(11 downto 0); -- ADC data channel 2
        p_adc_data_ch3_out   : out   std_logic_vector(11 downto 0); -- ADC data channel 3
        p_adc_data_rdy_p_out : out   std_logic                      -- Sample available
    );
end entity adc_driver;

architecture rtl of adc_driver is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component PulseRateDivider is
        port (
            p_pulse_rate_divider_clk_in         : in  std_logic;                    -- Input clock
            p_pulse_rate_divider_rst_n_in       : in  std_logic;                    -- Reset input. Synchronous reset, atcive high!
            p_pulse_rate_divider_rate_load_p_in : in  std_logic;                    -- Load new pulse rate input
            p_pulse_rate_divider_rate_in        : in  std_logic_vector(4 downto 0); -- Pulse rate selector input
            p_pulse_rate_divider_enable_in      : in  std_logic;                    -- Enable input
            p_pulse_rate_divider_trig_in        : in  std_logic;                    -- Pulse train input
            p_pulse_rate_divider_sync_out       : out std_logic;                    -- Sychro ready output. Pulsed when the new selected rate is active.
            p_pulse_rate_divider_pulse_out      : out std_logic                     -- Pulse train output
        );
    end component PulseRateDivider;

    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    -- Allowed deviation from mid-point (2.5V->2048) in test mode
    constant C_MID_POINT         : natural := 2048;
    constant C_DELTA_TEST        : natural := 128; -- 3;

    -- Rate divider
    -- Note: for clock based dividers (not triggered), to obtain a pulse train with a division factor N just from the clock (trigger_i = 1), use a division factor N - 1
    constant C_RATE_DIV_FACTOR_1 : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(19, 5)); -- Manual conversion pulse train CLK/20 pulse train (5 MHz with CLK = 100 MHz)
    constant C_RATE_DIV_FACTOR_2 : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(11, 5)); -- p_cont_pulsetrain CLK/12 divider (approx. 8.33 MHz with CLK = 100 MHz)

    -- CR0 and CR1 for test mode and operation mode
    constant CR0_TESTMODE        : std_logic_vector(11 downto 0) := X"202";
    constant CR1_TESTMODE        : std_logic_vector(11 downto 0) := X"430";
    constant CR0_OPERATION       : std_logic_vector(11 downto 0) := X"09" & "10" & G_ADC_CONV_MODE & G_ADC_REF_EXT; -- SCAN active, 4 single ended channels
    -- Selectable conversion mode
    -- Selectable external reference
    constant CR1_OPERATION       : std_logic_vector(11 downto 0) := "010" & G_ADC_AUTO_OFFSET & G_ADC_OUTPUT_FORMAT & "000" & std_logic_vector(to_unsigned(G_ADC_TRIGGER_LEVEL, 2)) & "00"; -- DATA_AV static and active low
    constant CR1_OPERATION_FR    : std_logic_vector(11 downto 0) := "010" & G_ADC_AUTO_OFFSET & G_ADC_OUTPUT_FORMAT & "000" & std_logic_vector(to_unsigned(G_ADC_TRIGGER_LEVEL, 2)) & "10"; -- DATA_AV static and active low, force FIFO reset
    -- Trigger level = 0
    -- Selectable automatic offset compensation
    -- Selectable output format
    -- Fifo-reset active upon selection

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    -- Main control STM
    type t_adcStateMachine is (
        ADCSTATE_START,
        ADCSTATE_RESET,
        ADCSTATE_S00,
        ADCSTATE_S10,
        ADCSTATE_S20,
        ADCSTATE_S30,
        ADCSTATE_S40,
        ADCSTATE_S50,
        ADCSTATE_S51,
        ADCSTATE_S52,
        ADCSTATE_S53,
        ADCSTATE_S54,
        ADCSTATE_S55,
        ADCSTATE_S56,
        ADCSTATE_S57,
        ADCSTATE_S58,
        ADCSTATE_S60,
        ADCSTATE_S61,
        ADCSTATE_S62,
        ADCSTATE_S63,
        ADCSTATE_S64,
        ADCSTATE_S65,
        ADCSTATE_S70,
        ADCSTATE_S71,
        ADCSTATE_S72,
        ADCSTATE_S73,
        ADCSTATE_ERROR
    );

    -- Bus control STM
    type t_buscontrol is (
        BUSCTRL_0,
        BUSCTRL_1,
        BUSCTRL_2,
        BUSCTRL_3,
        BUSCTRL_4,
        BUSCTRL_5
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    -- main signals
    signal s_clk                   : std_logic;
    signal s_rst_n                 : std_logic;

    -- Internal sample pulse trains
    signal s_divBy20_ptrain        : std_logic;                    -- CLK/20 pulse train (on-demand conversion)
    signal s_cont_ptrain           : std_logic;                    -- CLK/12 pulse train (continuous conversion)

    -- State variables
    signal s_state_curr            : t_adcStateMachine;
    signal s_state_next            : t_adcStateMachine;

    -- State variables
    signal s_buscontrol_state_curr : t_buscontrol;
    signal s_buscontrol_state_next : t_buscontrol;

    -- Bus control signals
    signal s_buscontrol_wr         : std_logic;
    signal s_buscontrol_rd         : std_logic;
    signal s_buscontrol_busy       : std_logic;
    signal s_buscontrol_rdy        : std_logic;
    signal s_buscontrol_drd        : std_logic_vector(11 downto 0);
    signal s_buscontrol_dwr        : std_logic_vector(11 downto 0);
    signal s_buscontrol_dwr_reg    : std_logic_vector(11 downto 0);
    signal s_adc_doe               : std_logic;

    -- Measurements
    signal s_dataCh0               : std_logic_vector(11 downto 0);
    signal s_dataCh1               : std_logic_vector(11 downto 0);
    signal s_dataCh2               : std_logic_vector(11 downto 0);
    signal s_dataCh3               : std_logic_vector(11 downto 0);
    signal s_dataRdy_p             : std_logic;
    signal s_test_data             : std_logic_vector(11 downto 0);

    -- Internal control signals
    signal s_data_av_p             : std_logic;                    -- Captured data valid pulse
    signal s_init_rdy              : std_logic;                    -- Initialization ready flag
    signal s_test_rdy              : std_logic;                    -- ADC test passed
    signal s_trigger_conv          : std_logic;                    -- In conv_mode = 2 (on-demand), this is the conversion trigger signal
    signal s_convst                : std_logic;                    -- CONVST internal signal
    signal s_delay_vector          : std_logic_vector(4 downto 0); -- Delay vector for convst pulse generation
    signal s_convst_tmo            : std_logic_vector(4 downto 0); -- Timeout counter for no DATA_AV activity after CONVST pulse
    signal s_tmo_error_p           : std_logic;                    -- Timeout pulse
    signal s_init_flt              : std_logic_vector(4 downto 0); -- Initialization fault retrial counter
    signal s_init_fault            : std_logic;                    -- Initialization fault internal flag
    signal s_data_av_syn           : std_logic;                    -- DATA_AV synchronizer signals

    ---------------------------------------------------------------------------------
    --                                  attributes
    ---------------------------------------------------------------------------------

    attribute ASYNC_REG                     : string;
    attribute ASYNC_REG of s_buscontrol_drd : signal is "TRUE";
    attribute ASYNC_REG of s_adc_doe        : signal is "TRUE";

begin

    -- input signal assignments
    s_clk   <= p_adc_clk_in;
    s_rst_n <= p_adc_rst_n_in;

    p_adc_data_ch0_out   <= s_dataCh0;
    p_adc_data_ch1_out   <= s_dataCh1;
    p_adc_data_ch2_out   <= s_dataCh2;
    p_adc_data_ch3_out   <= s_dataCh3;
    p_adc_data_rdy_p_out <= s_dataRdy_p;

    -- *******************************************************************************************************************************************************************************
    -- Pulse train generators
    -- *******************************************************************************************************************************************************************************
    pdivider_stage_1 : component PulseRateDivider
        port map (
            p_pulse_rate_divider_clk_in         => s_clk,
            p_pulse_rate_divider_rst_n_in       => s_rst_n,
            p_pulse_rate_divider_rate_load_p_in => '0',
            p_pulse_rate_divider_rate_in        => C_RATE_DIV_FACTOR_1,
            p_pulse_rate_divider_enable_in      => '1',
            p_pulse_rate_divider_trig_in        => '1',
            p_pulse_rate_divider_sync_out       => open,
            p_pulse_rate_divider_pulse_out      => s_divBy20_ptrain
        );

    pdivider_ptrain_1 : component PulseRateDivider
        port map (
            p_pulse_rate_divider_clk_in         => s_clk,
            p_pulse_rate_divider_rst_n_in       => s_rst_n,
            p_pulse_rate_divider_rate_load_p_in => '0',
            p_pulse_rate_divider_rate_in        => C_RATE_DIV_FACTOR_2,
            p_pulse_rate_divider_enable_in      => '1',
            p_pulse_rate_divider_trig_in        => '1',
            p_pulse_rate_divider_sync_out       => open,
            p_pulse_rate_divider_pulse_out      => s_cont_ptrain
        );

    -- Fault register
    process (s_clk, s_rst_n)
    begin
        if (s_rst_n = '0') then
            p_adc_init_fault_out <= '0';
        elsif rising_edge(s_clk) then
            p_adc_init_fault_out <= s_init_fault;
        end if;
    end process;

    -- *******************************************************************************************************************************************************************************
    -- DATA_AV resynchro and edge detection block
    -- The DATA_AV signal is expected to be active low
    -- *******************************************************************************************************************************************************************************

    xpm_cdc_single_inst : xpm_cdc_single
    generic map (
        DEST_SYNC_FF   => 2,               -- DECIMAL; range: 2-10
        INIT_SYNC_FF   => 1,               -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 1,               -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG  => 0                -- DECIMAL; 0=do not register input, 1=register input
    )
    port map (
        dest_out       => s_data_av_syn,   -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
        dest_clk       => s_clk,           -- 1-bit input: Clock signal for the destination clock domain.
        src_clk        => '0',             -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in         => not p_adc_dav_in -- 1-bit input: Input signal to be synchronized to dest_clk domain.
    );

        -- s_mcu_req rising edge detector
    pedgeflt : edge_detector_p
        generic map (
            G_REG => 1
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_data_av_syn,
            p_edp_sig_re_out => s_data_av_p,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
    );

    -- *******************************************************************************************************************************************************************************
    -- Conversion pulse train generator
    -- This process is in charge of the signal CONVSTn. During the initialization the signal is controlled by the main STM, when the ADC is being tested and configured.
    -- After the initialization phase, this process takes control, and produces the pulse trains accordingly with the selected operation mode: single conversion / continuous conv.
    -- *******************************************************************************************************************************************************************************
    process (s_clk, s_rst_n)
    begin
        if (s_rst_n = '0') then
            s_convst       <= '0';
            s_tmo_error_p  <= '0';
            s_delay_vector <= "00001";
            s_convst_tmo   <= (others => '0');
        elsif rising_edge(s_clk) then
            -- Conversion control port
            if (s_init_rdy = '0') then
                -- Receive pulses from trigger directly
                p_adc_conv_n_out <= not s_trigger_conv;
            else
                p_adc_conv_n_out <= not s_convst;

                if (s_data_av_p = '1') then
                    -- Reset timeout counter
                    s_convst_tmo <= (others => '0');
                end if;

                -- After initialization, source clock upon selected operation mode (via generic)
                if (G_ADC_CONV_MODE = '0') then
                    if (s_cont_ptrain = '1') then
                        -- Continuous conversion mode requires of a forwarded clock of 4xSampleRate to the converter
                        s_convst <= not s_convst;

                        if (s_convst = '1') then -- An entire sampling interval is contained between two rising edges of s_convst
                            s_convst_tmo <= s_convst_tmo(3 downto 0) & '1';
                        end if;

                        -- Timeout check
                        if (s_convst_tmo = "11111") then
                            -- Timeout condition
                            s_convst_tmo <= (others => '0');
                            --s_tmo_error_p <= '1'; --TODO: check initialization in continuous mode, for DATA_AV behaviour
                        end if;
                    end if;
                elsif (s_divBy20_ptrain = '1') then
                    -- Single conversion mode. Trigger conversion for T1, then repeat every Ts
                    -- T1 is 900 nS, Ts is CLK/20/5 (for CLK = 100 MHz -> 1 MHz)
                    if (s_delay_vector = "11111") then
                        s_convst       <= '0';
                        s_delay_vector <= "00001";
                        s_convst_tmo   <= s_convst_tmo(3 downto 0) & '1';

                        -- Timeout check
                        if (s_convst_tmo = "11111") then
                            -- Timeout condition
                            s_convst_tmo  <= (others => '0');
                            s_tmo_error_p <= '1';
                        end if;
                    else
                        s_convst       <= '1';
                        s_delay_vector <= s_delay_vector(3 downto 0) & '1';
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- *******************************************************************************************************************************************************************************
    -- #### BUS CONTROL STM
    -- This STM controls the ADC ports, and its in charge of synchronizing the signals to avoid bus conflicts. The signals are registered, so they must be mapped to the IOB FF's
    --  Make sure this is the case in the constraints file, declaring the ADC ports as IOB mapped.
    -- NOTE: do not change the style of this STM. All the ports have to be registered, to avoid timing closure problems in the external interface
    -- *******************************************************************************************************************************************************************************

    p_adc_data_bi <= (others => 'Z') when s_adc_doe = '0' else s_buscontrol_dwr_reg;

    process (s_clk, s_rst_n)
    begin
        if (s_rst_n = '0') then
            s_buscontrol_state_curr <= BUSCTRL_0;
            s_adc_doe               <= '0';
            s_buscontrol_drd        <= (others => '0');
            s_buscontrol_dwr_reg    <= (others => '0');
            p_adc_cs0_n_out         <= '1';
            p_adc_cs1_out           <= '0';
            p_adc_rd_n_out          <= '1';
            p_adc_wr_n_out          <= '1';
        elsif rising_edge(s_clk) then

            s_buscontrol_state_curr <= s_buscontrol_state_next;

            case s_buscontrol_state_curr is
                when BUSCTRL_0 =>
                    if (s_buscontrol_rd = '1') then
                        -- Read operation
                        p_adc_cs0_n_out <= '0';
                        p_adc_cs1_out   <= '1';
                        p_adc_rd_n_out  <= '0';
                        p_adc_wr_n_out  <= '1';
                    elsif (s_buscontrol_wr = '1') then
                        -- Write operation
                        p_adc_cs0_n_out      <= '0';
                        p_adc_cs1_out        <= '1';
                        p_adc_rd_n_out       <= '1';
                        p_adc_wr_n_out       <= '0';
                        s_adc_doe            <= '1'; --p_adc_data_bi   <= s_buscontrol_dwr;
                        s_buscontrol_dwr_reg <= s_buscontrol_dwr;
                    else
                        -- No operation
                        p_adc_cs0_n_out <= '1';
                        p_adc_cs1_out   <= '0';
                        p_adc_rd_n_out  <= '1';
                        p_adc_wr_n_out  <= '1';
                    end if;

                when BUSCTRL_1 =>
                    -- Read op. Wait for data.
                    p_adc_cs0_n_out <= '0';
                    p_adc_cs1_out   <= '1';
                    p_adc_rd_n_out  <= '0';
                when BUSCTRL_2 =>
                    -- Write op
                    p_adc_cs0_n_out <= '0';
                    p_adc_cs1_out   <= '1';
                    p_adc_rd_n_out  <= '1';
                    p_adc_wr_n_out  <= '0';
                when BUSCTRL_3 =>
                    -- Capture RD data
                    p_adc_cs0_n_out  <= '1';
                    p_adc_cs1_out    <= '0';
                    p_adc_rd_n_out   <= '0';
                    p_adc_wr_n_out   <= '1';
                    s_buscontrol_drd <= p_adc_data_bi; -- Data bus should be already stable. Transfer data into current clock domain.
                when BUSCTRL_4 =>
                    -- Write op
                    p_adc_cs0_n_out <= '1';
                    p_adc_cs1_out   <= '1';
                    p_adc_rd_n_out  <= '1';
                    p_adc_wr_n_out  <= '0';

                when BUSCTRL_5 =>
                    -- Release bus
                    p_adc_cs0_n_out <= '1';
                    p_adc_cs1_out   <= '0';
                    p_adc_rd_n_out  <= '1';
                    p_adc_wr_n_out  <= '1';
                    s_adc_doe       <= '0'; --p_adc_data_bi   <= (others => 'Z');

            end case;
        end if;
    end process;

    process (all)
    begin
        s_buscontrol_rdy  <= '0';
        s_buscontrol_busy <= '0';
        case s_buscontrol_state_curr is
            when BUSCTRL_0 =>
                -- Acquire bus
                if (s_buscontrol_rd = '1') then
                    -- Read operation
                    s_buscontrol_busy       <= '1';
                    s_buscontrol_state_next <= BUSCTRL_1;
                elsif (s_buscontrol_wr = '1') then
                    -- Write operation
                    s_buscontrol_busy       <= '1';
                    s_buscontrol_state_next <= BUSCTRL_2;
                else
                    -- No operation
                    s_buscontrol_busy       <= '0';
                    s_buscontrol_state_next <= BUSCTRL_0;
                end if;

            when BUSCTRL_1 =>
                -- Read op
                s_buscontrol_busy       <= '1';
                s_buscontrol_state_next <= BUSCTRL_3;

            when BUSCTRL_2 =>
                -- Write op
                s_buscontrol_busy       <= '1';
                s_buscontrol_state_next <= BUSCTRL_4;

            when BUSCTRL_3 =>
                -- Read op, capture data
                s_buscontrol_busy       <= '1';
                s_buscontrol_state_next <= BUSCTRL_5;

            when BUSCTRL_4 =>
                -- Write op, hold data
                s_buscontrol_busy       <= '1';
                s_buscontrol_state_next <= BUSCTRL_5;

            when BUSCTRL_5 =>
                -- Release bus
                s_buscontrol_rdy <= '1';
                if (s_buscontrol_rd = '0' and s_buscontrol_wr = '0') then
                    s_buscontrol_state_next <= BUSCTRL_0;
                else
                    s_buscontrol_state_next <= BUSCTRL_4;
                end if;
        end case;
    end process;

    -- ### ADC DRIVER ### --

    -- *******************************************************************************************************************************************************************************
    -- Capturing multiplexer
    -- *******************************************************************************************************************************************************************************

    sync_multiplex : process (s_clk, s_rst_n)
    begin
        if (s_rst_n = '0') then
            s_dataCh0 <= (others => '0');
            s_dataCh1 <= (others => '0');
            s_dataCh2 <= (others => '0');
            s_dataCh3 <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_buscontrol_rdy = '1') then
                -- Capture read data from ADC, when data is available
                -- Use the main FSM state to decide which channel receives the data from the multiplex
                case s_state_curr is
                    when ADCSTATE_S62 =>
                        s_dataCh0 <= s_buscontrol_drd;
                    when ADCSTATE_S63 =>
                        s_dataCh1 <= s_buscontrol_drd;
                    when ADCSTATE_S64 =>
                        s_dataCh2 <= s_buscontrol_drd;
                    when ADCSTATE_S65 =>
                        s_dataCh3 <= s_buscontrol_drd;
                    when ADCSTATE_ERROR =>
                        null;
                    when others =>
                        null;
                end case;
            end if;
        end if;
    end process;

    -- *******************************************************************************************************************************************************************************
    -- Main control STM, synchronous process
    -- *******************************************************************************************************************************************************************************
    seq_adc : process (s_clk, s_rst_n)
    begin
        if (s_rst_n = '0') then

            s_state_curr <= ADCSTATE_RESET;
            s_init_flt   <= (others => '0');
            s_test_rdy   <= '0';
            s_init_rdy   <= '0';
            s_dataRdy_p  <= '0';

            p_adc_error_out <= '0';

        elsif rising_edge(s_clk) then

            -- Pulsed signals
            s_buscontrol_rd <= '0';
            s_buscontrol_wr <= '0';
            s_dataRdy_p     <= '0';

            if (s_buscontrol_busy = '0') then
                s_state_curr <= s_state_next;

                p_adc_error_out <= '0';

                case s_state_curr is
                    when ADCSTATE_RESET =>
                        s_test_rdy <= '0';
                        s_init_rdy <= '0';
                        s_init_flt <= (others => '0');

                    when ADCSTATE_START =>
                        s_buscontrol_wr <= '1';
                    when ADCSTATE_S00 =>
                        s_buscontrol_wr <= '1';
                    when ADCSTATE_S10 =>
                        s_buscontrol_wr <= '1';
                    when ADCSTATE_S20 =>
                        s_buscontrol_wr <= '1';

                    when ADCSTATE_S53 =>
                        s_buscontrol_rd <= '1';
                    when ADCSTATE_S54 =>
                        s_test_data <= s_buscontrol_drd;
                    when ADCSTATE_S55 =>
                        s_test_rdy <= '1';

                    when ADCSTATE_S60 =>
                        s_init_rdy <= '1';

                    -- Acq. routine. Read FIFO samples
                    when ADCSTATE_S61 =>
                        s_buscontrol_rd <= '1';
                    when ADCSTATE_S62 =>
                        s_buscontrol_rd <= '1';
                    when ADCSTATE_S63 =>
                        s_buscontrol_rd <= '1';
                    when ADCSTATE_S64 =>
                        s_buscontrol_rd <= '1';

                    -- FIFO RESET
                    when ADCSTATE_S70 =>
                        if (G_ADC_FIFO_RESET = '1') then
                            s_buscontrol_wr <= '1';
                        end if;

                    when ADCSTATE_S71 =>
                        s_buscontrol_wr <= '1';

                    -- ACQ end
                    when ADCSTATE_S73 =>
                        s_dataRdy_p <= '1';
                    when ADCSTATE_ERROR =>
                        s_init_rdy <= '0'; -- Force init ready = 0
                        -- Update initialziation fault if required
                        if (s_init_rdy = '0') then
                            s_init_flt <= s_init_flt(3 downto 0) & "1";
                        else
                            p_adc_error_out <= '1'; -- Raise error flag
                        end if;
                    when others =>
                        null;
                end case;
            end if;
        end if;
    end process;

    comb_adc : process (all)
    begin
        -- Default logic
        s_trigger_conv   <= '0';
        s_buscontrol_dwr <= (others => '0');
        s_state_next     <= s_state_curr;
        s_init_fault     <= '0';

        -- Select function from current state
        case s_state_curr is
            when ADCSTATE_RESET =>
                s_state_next <= ADCSTATE_START;

            -- *******************************************************************************************************************************************************************************
            -- Configuration
            -- *******************************************************************************************************************************************************************************
            when ADCSTATE_START =>
                s_state_next <= ADCSTATE_S00;
            when ADCSTATE_S00 =>
                s_buscontrol_dwr <= X"401"; -- Reset ADC core
                s_state_next     <= ADCSTATE_S10;
            when ADCSTATE_S10 =>
                s_buscontrol_dwr <= X"400"; -- remove reset
                s_state_next     <= ADCSTATE_S20;
            when ADCSTATE_S20 =>
                if (s_test_rdy = '0') then
                    s_buscontrol_dwr <= CR0_TESTMODE; -- write testmode CR0
                else
                    s_buscontrol_dwr <= CR0_OPERATION; -- write operational CR0
                end if;
                s_state_next <= ADCSTATE_S30;

            -- Select next operation
            when ADCSTATE_S30 =>
                if (s_test_rdy = '0') then
                    s_buscontrol_dwr <= CR1_TESTMODE; -- write testmode CR1
                    -- Go into test routine
                    s_state_next     <= ADCSTATE_S40;
                else
                    s_buscontrol_dwr <= CR1_OPERATION_FR; -- write operational CR1
                    s_state_next     <= ADCSTATE_S56; -- Go to wait routine
                end if;

            -- *******************************************************************************************************************************************************************************
            -- ADC test routine
            -- *******************************************************************************************************************************************************************************
            when ADCSTATE_S40 =>
                -- Acquire test value: launch conversion and wait
                s_trigger_conv <= '1';
                if (s_divBy20_ptrain = '1') then
                    s_state_next <= ADCSTATE_S50;
                else
                    s_state_next <= ADCSTATE_S40;
                end if;
            when ADCSTATE_S50 =>
                s_trigger_conv <= '1';
                if (s_divBy20_ptrain = '1') then
                    s_state_next <= ADCSTATE_S51;
                else
                    s_state_next <= ADCSTATE_S50;
                end if;

            when ADCSTATE_S51 =>
                if (s_divBy20_ptrain = '1') then
                    s_state_next <= ADCSTATE_S52;
                else
                    s_state_next <= ADCSTATE_S51;
                end if;

            when ADCSTATE_S52 =>
                if (s_divBy20_ptrain = '1') then
                    s_state_next <= ADCSTATE_S53;
                else
                    s_state_next <= ADCSTATE_S52;
                end if;

            when ADCSTATE_S53 =>
                s_state_next <= ADCSTATE_S54;

            when ADCSTATE_S54 =>
                -- Read test value
                s_state_next <= ADCSTATE_S55;

            when ADCSTATE_S55 =>
                -- Check if the returned value is correct
                if (((to_integer(unsigned(s_test_data)) > (C_MID_POINT - C_DELTA_TEST)) and (to_integer(unsigned(s_test_data)) < (C_MID_POINT + C_DELTA_TEST)))) then
                    -- Restart configuration to get into operation mode, and go to the acquisition routine
                    s_state_next <= ADCSTATE_START;
                else
                    -- Error condition
                    s_state_next <= ADCSTATE_ERROR;
                end if;

            -- *******************************************************************************************************************************************************************************
            -- Wait routine. Wait two to three consecutive sampling periods before starting operation
            -- *******************************************************************************************************************************************************************************
            when ADCSTATE_S56 =>
                if (s_divBy20_ptrain = '1') then
                    s_state_next <= ADCSTATE_S57;
                else
                    s_state_next <= ADCSTATE_S56;
                end if;

            when ADCSTATE_S57 =>
                if (s_divBy20_ptrain = '1') then
                    s_state_next <= ADCSTATE_S58;
                else
                    s_state_next <= ADCSTATE_S57;
                end if;

            when ADCSTATE_S58 =>
                if (s_divBy20_ptrain = '1') then
                    s_state_next <= ADCSTATE_S60;
                else
                    s_state_next <= ADCSTATE_S58;
                end if;

            -- *******************************************************************************************************************************************************************************
            -- Acquisition routine
            -- Initialization phase is done. Conversion + acquisition routine.
            -- The acquisition routine does empty the FIFO each time there is new data (DATA_AV = 0).
            -- An external process will be generating the sampling pulse, accordingly to the selected operational mode (single conversion / continous mode)
            -- *******************************************************************************************************************************************************************************
            when ADCSTATE_S60 =>
                if (s_data_av_p = '1') then
                    s_state_next <= ADCSTATE_S61;
                elsif (s_tmo_error_p = '1') then
                    -- Error condition when no activity is detected in the ADC
                    s_state_next <= ADCSTATE_ERROR;
                else
                    s_state_next <= ADCSTATE_S60;
                end if;
            when ADCSTATE_S61 =>
                s_state_next <= ADCSTATE_S62;
            when ADCSTATE_S62 =>
                -- Store CH0 sample
                s_state_next <= ADCSTATE_S63;
            when ADCSTATE_S63 =>
                -- Store CH1 sample
                s_state_next <= ADCSTATE_S64;
            when ADCSTATE_S64 =>
                -- Store CH2 sample
                s_state_next <= ADCSTATE_S65;
            when ADCSTATE_S65 =>
                -- Store CH3 sample
                s_state_next <= ADCSTATE_S70;

            when ADCSTATE_S70 =>
                -- Select next state
                if (G_ADC_FIFO_RESET = '1') then
                    s_state_next <= ADCSTATE_S71; -- Go to FIFO RST routine
                else
                    s_state_next <= ADCSTATE_S73;
                end if;

            -- Reset FIFO after sample read, section
            when ADCSTATE_S71 =>
                -- reset fifo if enabled
                s_buscontrol_dwr <= CR1_OPERATION_FR;
                s_state_next     <= ADCSTATE_S72;
            when ADCSTATE_S72 =>
                -- remove FIFO reset
                s_buscontrol_dwr <= CR1_OPERATION;
                s_state_next     <= ADCSTATE_S73;

            when ADCSTATE_S73 =>
                s_state_next <= ADCSTATE_S60;

            -- *******************************************************************************************************************************************************************************
            --  ERROR State
            -- *******************************************************************************************************************************************************************************
            -- When an error is detected during operation, we land here
            -- The error state is kept until an external actor clears the error. The driver automatically will recover the previous operation state.
            when ADCSTATE_ERROR =>
                if (s_init_rdy = '0') then
                    if (s_init_flt = "11111") then
                        -- Too many initialization trials. Signal fault and hard stop.
                        s_init_fault <= '1';
                        s_state_next <= ADCSTATE_ERROR;
                    else
                        s_state_next <= ADCSTATE_START; -- If the error ocurred during the initialization routine, re-start again
                    end if;
                else
                    if (p_adc_clr_error_p_in = '1') then
                        s_state_next <= ADCSTATE_START; -- Go to the START routine, to reset core and re-start acquisition
                    end if;
                    s_state_next <= ADCSTATE_ERROR;
                end if;

            when others =>
                -- Whe end here if a SEU, or a power transient, occurs.
                -- Avoid a deadlock condition, and move straight into the ERROR state
                s_state_next <= ADCSTATE_ERROR;
        end case;
    end process;

end architecture rtl;
