----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             adc_driver_top.vhd
-- module name      adc_driver - structural
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            ADC driver top module
----------------------------------------------------------------------------------------------
-- create date      15.09.2020.
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by jsanchez
--                    - Updated adc_driver entity.
--                    - Adding a generic to control the automatic ADC FIFO_RESET behavior
--
--      Revision 0.03 by david.pavlovic
--                    - Code cleanup
------------------------------------------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             adc_driver_top.vhd
-- module name      adc_driver - structural
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            ADC driver top module
----------------------------------------------------------------------------------------------
-- create date      15.09.2020.
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by jsanchez
--                    - Updated adc_driver entity.
--                    - Adding a generic to control the automatic ADC FIFO_RESET behavior
--
--      Revision 0.03 by david.pavlovic
--                    - Code cleanup
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity adc_driver_top is
    generic (
        G_ADC_DRIVER_TOP_TRIGGER_LEVEL : natural   := 0;                      -- Valid trigger levels range from 0 to 2 (see datasheet table 13, pp.28).
        G_ADC_DRIVER_TOP_CONV_MODE     : std_logic := '1';                    -- Conversion mode: continuous = 0, on-demand = 1
        G_ADC_DRIVER_TOP_OUTPUT_FORMAT : std_logic := '1';                    -- 2's complement = 0, binary = 1
        G_ADC_DRIVER_TOP_FIFO_RESET    : std_logic := '1';                    -- Enable automatic reset of the FIFO after each conversion.
        G_ADC_DRIVER_TOP_AUTO_OFFSET   : std_logic := '0';                    -- Automatic offset compensation
        G_ADC_DRIVER_TOP_REF_EXT       : std_logic := '0';                    -- Select the external ADC reference voltage
        G_ADC_DRIVER_TOP_WD_MAX_ERR    : natural   := 3                       -- Number of allowed initialization errors
    );
    port (
        -- Main signals
        p_adc_top_clk_in               : in    std_logic;                     -- main clock (expected 100 MHz)
        p_adc_top_rst_n_in             : in    std_logic;                     -- async reset
        -- ADC signals
        p_adc_top_conv_n_out           : out   std_logic;                     -- conversion trigger (active low)
        p_adc_top_cs0_n_out            : out   std_logic;                     -- control signal (active low)
        p_adc_top_cs1_out              : out   std_logic;                     -- control signal (active high)
        p_adc_top_rd_n_out             : out   std_logic;                     -- read trigger (active low)
        p_adc_top_wr_n_out             : out   std_logic;                     -- write trigger (active low)
        p_adc_top_dav_in               : in    std_logic;                     -- data available flag
        p_adc_top_data_bi              : inout std_logic_vector(11 downto 0); -- ADC data bus
        -- Interface
        p_top_adc_dataCh0_out          : out   std_logic_vector(11 downto 0); -- ADC data channel 0
        p_top_adc_dataCh1_out          : out   std_logic_vector(11 downto 0); -- ADC data channel 1
        p_top_adc_dataCh2_out          : out   std_logic_vector(11 downto 0); -- ADC data channel 2
        p_top_adc_dataCh3_out          : out   std_logic_vector(11 downto 0); -- ADC data channel 3
        p_top_adc_dataRdy_out          : out   std_logic;                     -- Sample available
        -- Fault
        p_top_adc_wd_flt_n_out         : out   std_logic                      -- ADC watchdog fault
    );
end entity;

architecture structural of adc_driver_top is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component adc_driver is
        generic (
            G_ADC_TRIGGER_LEVEL  : natural   := 0;                      -- Valid trigger levels range from 0 to 2 (see datasheet table 13, pp.28).
            G_ADC_CONV_MODE      : std_logic := '1';                    -- Conversion mode: continuous = 0, on-demand = 1
            G_ADC_OUTPUT_FORMAT  : std_logic := '1';                    -- 2's complement = 0, binary = 1
            G_ADC_FIFO_RESET     : std_logic := '1';                    -- Enable automatic reset of the FIFO after each conversion.
            G_ADC_AUTO_OFFSET    : std_logic := '0';                    -- Automatic offset compensation
            G_ADC_REF_EXT        : std_logic := '0'                     -- Select the external ADC reference voltage
        );
        port (
            -- Main signals
            p_adc_clk_in         : in    std_logic;                     -- clock
            p_adc_rst_n_in       : in    std_logic;                     -- reset (active-low)
            -- ADC
            p_adc_conv_n_out     : out   std_logic;                     -- conversion trigger (active low)
            p_adc_cs0_n_out      : out   std_logic;                     -- control signal (active low)
            p_adc_cs1_out        : out   std_logic;                     -- control signal (active high)
            p_adc_rd_n_out       : out   std_logic;                     -- read trigger (active low)
            p_adc_wr_n_out       : out   std_logic;                     -- write trigger (active low)
            p_adc_dav_in         : in    std_logic;                     -- data available flag
            p_adc_data_bi        : inout std_logic_vector(11 downto 0); -- ADC data bus
            -- Interface
            p_adc_error_out      : out   std_logic;                     -- Error condition flag
            p_adc_clr_error_p_in : in    std_logic;                     -- Clear error flag, and restart ADC
            p_adc_init_fault_out : out   std_logic;                     -- Initialization fault
            p_adc_data_ch0_out   : out   std_logic_vector(11 downto 0); -- ADC data channel 0
            p_adc_data_ch1_out   : out   std_logic_vector(11 downto 0); -- ADC data channel 1
            p_adc_data_ch2_out   : out   std_logic_vector(11 downto 0); -- ADC data channel 2
            p_adc_data_ch3_out   : out   std_logic_vector(11 downto 0); -- ADC data channel 3
            p_adc_data_rdy_p_out : out   std_logic                      -- Sample available
        );
    end component;

    component adc_driver_watchdog is
        generic (
            G_ADC_WD_MAX_ERR       : natural := 3
        );
        port (
            -- main signals
            p_adc_wd_clk_in        : in  std_logic;
            p_adc_wd_rst_n_in      : in  std_logic;
            -- interface to ADC
            p_adc_wd_error_in      : in  std_logic;
            p_adc_wd_error_clr_out : out std_logic;
            -- fault signal
            p_adc_wd_flt_n_out     : out std_logic
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_adc_init_flt : std_logic;
    signal s_adc_flt      : std_logic;
    signal s_adc_flt_clr  : std_logic;

begin

    mdl_adc_driver : adc_driver
        generic map (
            G_ADC_TRIGGER_LEVEL  => G_ADC_DRIVER_TOP_TRIGGER_LEVEL, -- Valid trigger levels range from 0 to 2 (see
            G_ADC_CONV_MODE      => G_ADC_DRIVER_TOP_CONV_MODE,     -- Conversion mode: continuous = 0, on-demand
            G_ADC_OUTPUT_FORMAT  => G_ADC_DRIVER_TOP_OUTPUT_FORMAT, -- 2's complement = 0, binary = 1
            G_ADC_FIFO_RESET     => G_ADC_DRIVER_TOP_AUTO_OFFSET,   -- Enable automatic reset of the FIFO after ea
            G_ADC_AUTO_OFFSET    => G_ADC_DRIVER_TOP_REF_EXT,       -- Automatic offset compensation
            G_ADC_REF_EXT        => G_ADC_DRIVER_TOP_FIFO_RESET     -- Select the external ADC reference voltage
        )
        port map (
            -- Main signals
            p_adc_clk_in         => p_adc_top_clk_in,               -- main clock (expected 100 MHz)
            p_adc_rst_n_in       => p_adc_top_rst_n_in,             -- async reset
            -- ADC
            p_adc_conv_n_out     => p_adc_top_conv_n_out,           -- conversion trigger (active low)
            p_adc_cs0_n_out      => p_adc_top_cs0_n_out,            -- control signal (active low)
            p_adc_cs1_out        => p_adc_top_cs1_out,              -- control signal (active high)
            p_adc_rd_n_out       => p_adc_top_rd_n_out,             -- read trigger (active low)
            p_adc_wr_n_out       => p_adc_top_wr_n_out,             -- write trigger (active low)
            p_adc_dav_in         => p_adc_top_dav_in,               -- data available flag
            p_adc_data_bi        => p_adc_top_data_bi,              -- ADC data bus
            -- Interface
            p_adc_error_out      => s_adc_flt,                      -- Error condition flag
            p_adc_clr_error_p_in => s_adc_flt_clr,                  -- Clear error flag, and restart ADC
            p_adc_init_fault_out => open,                           -- Initialization fault
            p_adc_data_ch0_out   => p_top_adc_dataCh0_out,          -- ADC data channel 0
            p_adc_data_ch1_out   => p_top_adc_dataCh1_out,          -- ADC data channel 1
            p_adc_data_ch2_out   => p_top_adc_dataCh2_out,          -- ADC data channel 2
            p_adc_data_ch3_out   => p_top_adc_dataCh3_out,          -- ADC data channel 3
            p_adc_data_rdy_p_out => p_top_adc_dataRdy_out           -- Sample available
        );

    mdl_adc_driver_wd : adc_driver_watchdog
        generic map (
            G_ADC_WD_MAX_ERR       => G_ADC_DRIVER_TOP_WD_MAX_ERR
        )
        port map (
            -- main signals
            p_adc_wd_clk_in        => p_adc_top_clk_in,
            p_adc_wd_rst_n_in      => p_adc_top_rst_n_in,
            -- interface to ADC
            p_adc_wd_error_in      => s_adc_flt,
            p_adc_wd_error_clr_out => s_adc_flt_clr,
            -- fault signal
            p_adc_wd_flt_n_out     => p_top_adc_wd_flt_n_out
        );

end architecture structural;