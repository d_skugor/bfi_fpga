----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             intra_inv_err_detection.vhd
-- module name      Intra-inverter error detection
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Module detects any error on the other side of inverter and informs
--                  MCU about this error;
--                  Module sends error signal to the other side of inverter if any error
--                  occurs on this side of inverter
----------------------------------------------------------------------------------------------
-- create date      13.05.2020
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Added PWM alivness detector
--
--      Revision 0.03 by david.pavlovic
--                    - Changed timeout period from 65us to 105us
--
--      Revision 0.04 by david.pavlovic
--                    - Improved FSM logic for avoiding false triggering caused by HW issues
--
--      Revision 0.05 by david.pavlovic
--                    - Fixed wrong assertion of s_out_en signal
--                    - Removed redundant signals
--
--      Revision 0.06 by danijela.skugor
--                    - Changes in accordance with changed gray_counter component:
--                          -- reset changed from active-high to active-low
--                          -- update of port and generic names of gray_counter
--
--      Revision 0.07 by david.pavlovic
--                    - Code cleanup
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

entity intra_inv_err_detection is
    generic (
        G_NUM_VALID_PULSES         : natural := 3   -- must be 0 - 15
    );
    port (
        -- Clock & Reset inputs
        p_iInvErrDet_clk_in        : in  std_logic;
        p_iInvErrDet_rst_n_in      : in  std_logic;

        -- Signals from the IIED connector
          -- PWM synchronization
        p_iInvErrDet_inv_pwmi_in   : in  std_logic; -- PWM sync signal from the other side of inverter
        p_iInvErrDet_inv_pwmo_out  : out std_logic; -- PWM sync signal to the other side of inverter
          -- Error signals (active low)
        p_iInvErrDet_inv_synci_in  : in  std_logic; -- error sync signal from the other side of inverter
        p_iInvErrDet_inv_synco_out : out std_logic; -- error sync signal to the other side of inverter

        -- Interface to the MCU
          -- PWM synchronization
        p_iInvErrDet_mcu_pwmi_in   : in  std_logic; -- PWM sync signal from MCU
        p_iInvErrDet_mcu_pwmo_out  : out std_logic; -- PWM sync signal from other side of the inverter
          -- Error signals (active low)
        p_iInvErrDet_mcu_synci_in  : in  std_logic; -- error sync signal from MCU
        p_iInvErrDet_mcu_synco_out : out std_logic  -- error sync signal from other side of the inverter
    );
end entity intra_inv_err_detection;

architecture rtl of intra_inv_err_detection is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component gray_counter is
        generic (
            G_GRAYCNT_WIDTH         : integer := 4;
            G_GRAYCNT_CORRECTION_ON : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port (
            p_grayCnt_clk_in        : in std_logic;
            p_grayCnt_rst_n_in      : in std_logic;  -- sync reset
            p_grayCnt_en_in         : in std_logic;  -- enable
            p_grayCnt_err_p_out     : out std_logic; -- pulsed error
            p_grayCnt_res_out       : out std_logic_vector(G_GRAYCNT_WIDTH-1 downto 0)
        );
    end component;

    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------

    function bin_to_gray (bin : std_logic_vector) return std_logic_vector is
        variable v_gray : std_logic_vector(bin'range);
    begin
        v_gray(bin'left) := bin(bin'left);
        for i in bin'left-1 downto 0 loop
            v_gray(i) := bin(i+1) xor bin(i);
        end loop;
        return v_gray;
    end function bin_to_gray;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    -- timeout and pulse counters width
    constant C_CNT_W             : natural := 32;
    constant C_CNT_PULSE_W       : natural := 12;
    constant C_CNT_PULSE_VALID_W : natural := 4;
    -- timeout value when s_out_en is LOW (5s)
    constant C_TIMEOUT_SYNC      : std_logic_vector(C_CNT_W-1 downto 0) := bin_to_gray(std_logic_vector(to_unsigned(49999999, C_CNT_W)));
    -- timeout value when s_out_en is HIGH (105us)
    constant C_TIMEOUT_TOGGLE    : std_logic_vector(C_CNT_W-1 downto 0) := bin_to_gray(std_logic_vector(to_unsigned(10499, C_CNT_W)));
    -- pulse must be HIGH for at least 30us
    constant C_PULSE_VALID       : std_logic_vector(C_CNT_PULSE_W-1 downto 0) := bin_to_gray(std_logic_vector(to_unsigned(2999, C_CNT_PULSE_W)));
    -- because generic is of natural type
    constant C_NUM_VALID_PULSES  : std_logic_vector(C_CNT_PULSE_VALID_W-1 downto 0) := bin_to_gray(std_logic_vector(to_unsigned(G_NUM_VALID_PULSES, C_CNT_PULSE_VALID_W)));

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    type t_state is (
        t_state_idle,
        t_state_wait_start_cond,
        t_state_wait_falling_edge,
        t_state_wait_rising_edge,
        t_state_wait_recovery_cond,
        t_state_dead
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk                   : std_logic;
    signal s_rst_n                 : std_logic;
    signal s_state                 : t_state;
    signal s_dead_n                : std_logic;
    signal s_cdc_vec_src           : std_logic_vector(3 downto 0);
    signal s_cdc_vec_dest          : std_logic_vector(3 downto 0);
    -- timeout counter
    signal s_cnt                   : std_logic_vector(C_CNT_W-1 downto 0);
    signal s_cnt_en                : std_logic;
    signal s_cnt_clr               : std_logic;
    signal s_cnt_rst_n             : std_logic;
    -- pulse length counter
    signal s_cnt_pulse             : std_logic_vector(C_CNT_PULSE_W-1 downto 0);
    signal s_cnt_pulse_en          : std_logic;
    signal s_cnt_pulse_clr         : std_logic;
    signal s_cnt_pulse_rst_n       : std_logic;
    -- valid pulse counter
    signal s_cnt_pulse_valid       : std_logic_vector(C_CNT_PULSE_VALID_W-1 downto 0);
    signal s_cnt_pulse_valid_en    : std_logic;
    signal s_cnt_pulse_valid_clr   : std_logic;
    signal s_cnt_pulse_valid_rst_n : std_logic;
    -- pulses
    signal s_pulse_valid           : std_logic;
    -- output enable
    signal s_out_en                : std_logic;
    -- IIED signals
    signal s_inv_pwmi              : std_logic;
    signal s_inv_pwmi_re           : std_logic;
    signal s_inv_pwmi_fe           : std_logic;
    signal s_inv_synci             : std_logic;
    signal s_mcu_pwmi              : std_logic;
    signal s_mcu_synci             : std_logic;

begin

    -- input signal assignments
    s_clk            <= p_iInvErrDet_clk_in;
    s_rst_n          <= p_iInvErrDet_rst_n_in;
    s_cdc_vec_src(0) <= p_iInvErrDet_inv_pwmi_in;
    s_cdc_vec_src(1) <= p_iInvErrDet_inv_synci_in;
    s_cdc_vec_src(2) <= p_iInvErrDet_mcu_pwmi_in;
    s_cdc_vec_src(3) <= p_iInvErrDet_mcu_synci_in;

    -- output signal assignemt
    -- bypassing PWM signal
    p_iInvErrDet_inv_pwmo_out  <= s_mcu_pwmi;
    -- bypassing error signal
    p_iInvErrDet_inv_synco_out <= s_mcu_synci;

    -- xpm_cdc_array_single: Single-bit Array Synchronizer
    -- Xilinx Parameterized Macro, version 2018.3
    xpm_cdc_array_single_inst : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF   => 2,              -- DECIMAL; range: 2-10
        INIT_SYNC_FF   => 1,              -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 1,              -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG  => 0,              -- DECIMAL; 0=do not register input, 1=register input
        WIDTH          => 4               -- DECIMAL; range: 1-1024
    )
    port map (
        dest_out       => s_cdc_vec_dest, -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
        dest_clk       => s_clk,          -- 1-bit input: Clock signal for the destination clock domain.
        src_clk        => '0',            -- 1-bit input: optional; required when SRC_INPUT_REG = 1
        src_in         => s_cdc_vec_src   -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
                                          -- domain. It is assumed that each bit of the array is unrelated to the others.
                                          -- This is reflected in the constraints applied to this macro. To transfer a binary
                                          -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro instead.
    );

    s_inv_pwmi  <= s_cdc_vec_dest(0);
    s_inv_synci <= s_cdc_vec_dest(1);
    s_mcu_pwmi  <= s_cdc_vec_dest(2);
    s_mcu_synci <= s_cdc_vec_dest(3);

    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            p_iInvErrDet_mcu_pwmo_out  <= '1';
            p_iInvErrDet_mcu_synco_out <= '1';
        elsif rising_edge(s_clk) then
            if (s_out_en = '1') then
                p_iInvErrDet_mcu_pwmo_out  <= s_dead_n;
                p_iInvErrDet_mcu_synco_out <= s_inv_synci;
            else
                p_iInvErrDet_mcu_pwmo_out  <= '1';
                p_iInvErrDet_mcu_synco_out <= '1';
            end if;
        end if;
    end process;

    edge_det : edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_inv_pwmi,
            p_edp_sig_re_out => s_inv_pwmi_re,
            p_edp_sig_fe_out => s_inv_pwmi_fe,
            p_edp_sig_rf_out => open
        );

    -- Gray timeout counter
    gray_cnt : gray_counter
        generic map (
           G_GRAYCNT_WIDTH         => C_CNT_W,
           G_GRAYCNT_CORRECTION_ON => true
        )
        port map (
            p_grayCnt_clk_in       => s_clk,
            p_grayCnt_rst_n_in     => s_cnt_rst_n,
            p_grayCnt_en_in        => s_cnt_en,
            p_grayCnt_err_p_out    => open,
            p_grayCnt_res_out      => s_cnt
        );

    -- Gray pulse length counter
    gray_cnt_pulse : gray_counter
        generic map (
           G_GRAYCNT_WIDTH         => C_CNT_PULSE_W,
           G_GRAYCNT_CORRECTION_ON => true
        )
        port map (
            p_grayCnt_clk_in       => s_clk,
            p_grayCnt_rst_n_in     => s_cnt_pulse_rst_n,
            p_grayCnt_en_in        => s_cnt_pulse_en,
            p_grayCnt_err_p_out    => open,
            p_grayCnt_res_out      => s_cnt_pulse
        );

    -- Gray valid pulse counter
    gray_cnt_pulse_vld : gray_counter
        generic map(
           G_GRAYCNT_WIDTH         => C_CNT_PULSE_VALID_W,
           G_GRAYCNT_CORRECTION_ON => true
        )
        port map(
            p_grayCnt_clk_in       => s_clk,
            p_grayCnt_rst_n_in     => s_cnt_pulse_valid_rst_n,
            p_grayCnt_en_in        => s_cnt_pulse_valid_en,
            p_grayCnt_err_p_out    => open,
            p_grayCnt_res_out      => s_cnt_pulse_valid
        );

    -- counter's resets
    s_cnt_rst_n             <= s_rst_n and (not s_cnt_clr);
    s_cnt_pulse_rst_n       <= s_rst_n and (not s_cnt_pulse_clr);
    s_cnt_pulse_valid_rst_n <= s_rst_n and (not s_cnt_pulse_valid_clr);

    s_pulse_valid <= '1' when s_cnt_pulse = C_PULSE_VALID else '0';

    -- State machine
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_state               <= t_state_idle;
            s_cnt_clr             <= '0';
            s_cnt_en              <= '0';
            s_cnt_pulse_clr       <= '0';
            s_cnt_pulse_en        <= '0';
            s_cnt_pulse_valid_en  <= '0';
            s_cnt_pulse_valid_clr <= '0';
            s_dead_n              <= '1';
            s_out_en              <= '0';
        elsif rising_edge(s_clk) then
             -- default values
            s_cnt_clr             <= '0';
            s_cnt_en              <= '1';
            s_cnt_pulse_clr       <= '0';
            s_cnt_pulse_valid_clr <= '0';
            s_cnt_pulse_valid_en  <= '0';
            s_dead_n              <= '1';
            case s_state is
                ------------------------------------------------
                when t_state_idle =>
                    if (s_cnt = C_TIMEOUT_SYNC) then
                        s_state   <= t_state_dead;
                        s_dead_n  <= '0';
                        s_cnt_clr <= '1';
                        s_cnt_en  <= '0';
                        s_out_en  <= '1';
                        s_cnt_pulse_valid_clr <= '1';
                    elsif (s_inv_pwmi_re = '1') then
                        s_state        <= t_state_wait_start_cond;
                        s_cnt_pulse_en <= '1';
                    end if;
                ------------------------------------------------
                when t_state_wait_start_cond =>
                    if (s_cnt = C_TIMEOUT_SYNC) then
                        s_state   <= t_state_dead;
                        s_dead_n  <= '0';
                        s_cnt_clr <= '1';
                        s_cnt_en  <= '0';
                        s_out_en  <= '1';
                        s_cnt_pulse_valid_clr <= '1';
                    elsif (s_cnt_pulse_valid = C_NUM_VALID_PULSES) then
                        s_out_en  <= '1';
                        -- check current state of s_inv_pwmi signal
                        if (s_inv_pwmi = '1') then
                            s_state   <= t_state_wait_falling_edge;
                            s_cnt_clr <= '1';
                        else
                            s_state   <= t_state_wait_rising_edge;
                            s_cnt_clr <= '1';
                        end if;
                    elsif (s_pulse_valid = '1') then
                        s_cnt_pulse_clr      <= '1';
                        s_cnt_pulse_en       <= '0';
                        s_cnt_pulse_valid_en <= '1';
                    elsif (s_inv_pwmi_re = '1') then
                        s_cnt_pulse_en <= '1';
                    end if;
                ------------------------------------------------
                when t_state_wait_falling_edge =>
                    if (s_cnt = C_TIMEOUT_TOGGLE) then
                        s_state   <= t_state_dead;
                        s_dead_n  <= '0';
                        s_cnt_clr <= '1';
                        s_cnt_en  <= '0';
                        s_cnt_pulse_valid_clr <= '1';
                    elsif (s_inv_pwmi_fe = '1') then
                        s_state   <= t_state_wait_rising_edge;
                        s_cnt_clr <= '1';
                    end if;
                ------------------------------------------------
                when t_state_wait_rising_edge =>
                    if (s_cnt = C_TIMEOUT_TOGGLE) then
                        s_state   <= t_state_dead;
                        s_dead_n  <= '0';
                        s_cnt_clr <= '1';
                        s_cnt_en  <= '0';
                        s_cnt_pulse_valid_clr <= '1';
                    elsif (s_inv_pwmi_re = '1') then
                        s_state   <= t_state_wait_falling_edge;
                        s_cnt_clr <= '1';
                    end if;
                ------------------------------------------------
                when t_state_wait_recovery_cond =>
                    s_dead_n <= '0';
                    if (s_cnt = C_TIMEOUT_SYNC) then
                        s_state   <= t_state_dead;
                        s_dead_n  <= '0';
                        s_cnt_clr <= '1';
                        s_cnt_en  <= '0';
                        s_cnt_pulse_valid_clr <= '1';
                    elsif (s_cnt_pulse_valid = C_NUM_VALID_PULSES) then
                        s_dead_n <= '1';
                        -- check current state of s_inv_pwmi signal
                        if (s_inv_pwmi = '1') then
                            s_state   <= t_state_wait_falling_edge;
                            s_cnt_clr <= '1';
                        else
                            s_state   <= t_state_wait_rising_edge;
                            s_cnt_clr <= '1';
                        end if;
                    elsif (s_pulse_valid = '1') then
                        s_cnt_pulse_clr      <= '1';
                        s_cnt_pulse_en       <= '0';
                        s_cnt_pulse_valid_en <= '1';
                    elsif (s_inv_pwmi_re = '1') then
                        s_cnt_pulse_en <= '1';
                    end if;
                ------------------------------------------------
                when others =>
                    -- t_state_dead state
                    s_cnt_en       <= '0';
                    s_cnt_pulse_en <= '0';
                    s_dead_n       <= '0';
                    if (s_inv_pwmi_re = '1') then
                        s_state        <= t_state_wait_recovery_cond;
                        s_cnt_en       <= '1';
                        s_cnt_pulse_en <= '1';
                        s_dead_n       <= '0';
                    end if;
                ------------------------------------------------
            end case;
        end if;
    end process;

end architecture rtl;
