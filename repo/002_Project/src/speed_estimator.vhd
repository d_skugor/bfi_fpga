----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             speed_estimator.vhd
-- module name      Speed estimator block
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Top Level Module
----------------------------------------------------------------------------------------------
-- create date      19.11.2019
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
--
--      Revision 0.02 by jsq
--                      Solved an scaling problem in the delta_phi calculation for high speeds.
--                      Added more comments and a doc. header
--
--      Revision 0.03 by jsq
--                      Increased output size to 22 bits
--                      Speed output is scaled as fx12.10, FS = -2048 to 2047.999023
--                                                          Units are radians/s (for a 1MHz sampling clock)
--
--      Revision 0.04 by david.pavlovic
--                      Changed variable names to be compliant with coding format rules
--                      Increased output size to 24 bits
--                      Speed output is scaled as fx16.8, FS = -32768 to 32767.99609375
--                                                          Units are RPM (for a 1MHz sampling clock)
--
--      Revision 0.05 by david.pavlovic
--                      Removed internal sampling clock division so filters are working at Ts = 1us
--                      Added another LPF in cascade
--                      Changed filter's cutoff frequency to 1 kHz (wc = 2*pi*1e-3)
--
--      Revision 0.06 by david.pavlovic
--                      Added pipeline registers after wide multipliers
--
--      Revision 0.07 by dario.drvenkar
--                      Synchronous reset changed to asynchronous
--
--      Revision 0.08 by david.pavlovic
--                      Added generic for selecting the filter cutoff frequency
--                      Added function to initialize filter's coefficients
--
--      Revision 0.09 by dario.drvenkar
--                      Two IIR filter with cutoff frequency of 100 Hz
--                      Block is made to work with 80kHz sample rate from resolver
--                      Output speed format is Q17.7
--
--      Revision 0.10 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.11 by danijela.skugor
--                    - Substitute downsample binary counter with gray code counter
--                    - Update port names
--
--      Revision 0.12 by david.pavlovic
--                    - Add the enable signal that is high when there are two samples in the buffers
--
----------------------------------------------------------------------------------------------
--    Description
--
--    The Speed Estimator Module calculates the electrical speed from angle values measured by the resolver.
--    Encoded angle input is in 12/bit format Q12.0 (12 signed integer bits, 0 fractional bits, 2C format).
--    The module expects a new angle sample every 1us, but the module downsamples the angle by a factor of 13. This is equivalent to the sample rate of 76.9 kSps.
--    Calculated electrical rotational speed in RPM is provided at the output as a Q17.7 number
--    (17 signed integer bits and 7 fractional bits, 2C format). The fractional part is in the LSB's and can
--    be truncated. Full-scale value is 65535.99219 rad/sec.
--    This module operates with an enable signal with a period of 13 us (76.9 kHz). The speed output variable is synchronous with the system clock.
--    A cascade of 2 LPFs are set at the output to smooth the calculated speed, which can have some numerical noise given the aperiodic behavior of
--    the calculated angle difference, given the asynchronicity between the rotation angle and the internal timebase.
--    This LPF is a single-pole filter with a cutoff frequency of 100 Hz.
----------------------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity speed_estimator is
    port (
        p_speed_est_clk_in         : in  std_logic;                     -- System clock input
        p_speed_est_rst_n_in       : in  std_logic;                     -- Synchronous reset
        p_speed_est_angle_val_p_in : in  std_logic;                     -- New angle sample pulse
        p_speed_est_angle_in       : in  std_logic_vector(11 downto 0); -- Angle input from resolver
        p_speed_est_speed_out      : out std_logic_vector(23 downto 0)  -- Speed output
    );
end entity speed_estimator;

architecture rtl of speed_estimator is
    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component gray_counter is
        generic (
            G_GRAYCNT_WIDTH         : integer := 4;
            G_GRAYCNT_CORRECTION_ON : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port (
            p_grayCnt_clk_in        : in std_logic;
            p_grayCnt_rst_n_in      : in std_logic;     -- sync reset
            p_grayCnt_en_in         : in std_logic;     -- enable
            p_grayCnt_err_p_out     : out std_logic;    -- pulsed error
            p_grayCnt_res_out       : out std_logic_vector(G_GRAYCNT_WIDTH-1 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------

    function bin_to_gray (bin : std_logic_vector) return std_logic_vector is
        variable v_gray : std_logic_vector(bin'range);
    begin
        v_gray(bin'left) := bin(bin'left);
        for i in bin'left-1 downto 0 loop
            v_gray(i) := bin(i+1) xor bin(i);
        end loop;
        return v_gray;
    end function bin_to_gray;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_DOWNSAMPLE_FACTOR     : natural := 13;
    constant C_DOWNSAMPLE_FACTOR_CNT : std_logic_vector(3 downto 0) := bin_to_gray(std_logic_vector(to_unsigned((C_DOWNSAMPLE_FACTOR - 1), 4)));  -- 13-1
    constant C_CNT_W                 : natural := 18;
    constant C_ANGLE_W               : natural := 12;
    constant C_CONV_FACTOR_QI        : natural := 12;
    constant C_CONV_FACTOR_QF        : natural := 13;
    constant C_CONV_FACTOR_W         : natural := C_CONV_FACTOR_QI + C_CONV_FACTOR_QF;
    constant C_SPEED_QI              : natural := C_ANGLE_W + C_CONV_FACTOR_QI;
    constant C_SPEED_QF              : natural := C_CONV_FACTOR_QF;
    constant C_SPEED_W               : natural := C_SPEED_QI + C_SPEED_QF;
    constant C_COEFF_QI              : natural := 1;
    constant C_COEFF_QF              : natural := 17;
    constant C_COEFF_W               : natural := C_COEFF_QI + C_COEFF_QF;
    constant C_FILTER_QI             : natural := C_SPEED_QI + C_COEFF_QI;
    constant C_FILTER_QF             : natural := C_SPEED_QF + C_COEFF_QF;
    constant C_FILTER_W              : natural := C_FILTER_QI + C_FILTER_QF;
    constant C_FILTER_R_QI           : natural := C_FILTER_QF + C_SPEED_QI - 1;
    constant C_FILTER_R_QF           : natural := C_FILTER_QF - C_SPEED_QF;
    constant C_SPEED_OUT_QI          : natural := 17;
    constant C_SPEED_OUT_QF          : natural := 7;
    constant C_SPEED_OUT_TRUNC_QI    : natural := C_SPEED_QF + C_SPEED_OUT_QI - 1;
    constant C_SPEED_OUT_TRUNC_QF    : natural := C_SPEED_QF - C_SPEED_OUT_QF;
    constant C_CONV_FACTOR           : signed(C_CONV_FACTOR_W - 1 downto 0) := to_signed(9230769, C_CONV_FACTOR_W); -- Q12.13; 80 ksps

    -- NOTE: Select here the coefficients to apply for the desired filter bandwidth
    -- Coefficients formula:
    --   A0 = cos(wc) - 1 + sqrt(cos(wc)^2 - 4*cos(wc) + 3)
    --   B0 = 1 - A0
    -- Coefficients for a wc = 2*pi*1e-3 discrete frequency (1 kHz Hz at Tsample = 1uS)
    constant C_LPF_A0 : signed(C_COEFF_W - 1 downto 0) := to_signed(1066,   C_COEFF_W); --Q1.17
    constant C_LPF_B0 : signed(C_COEFF_W - 1 downto 0) := to_signed(130006, C_COEFF_W); --Q1.17

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    -- internal signals
    signal s_clk              : std_logic;
    signal s_rst_n            : std_logic;
    signal s_angle_val_p      : std_logic;
    signal s_angle            : signed(C_ANGLE_W - 1 downto 0);  -- Input angle
    signal s_phi              : signed(C_ANGLE_W - 1 downto 0);  -- Current angle sample
    signal s_phi_d            : signed(C_ANGLE_W - 1 downto 0);  -- Previous angle sample
    signal s_delta_phi        : signed(C_ANGLE_W - 1 downto 0);  -- Angle difference
    signal s_speed            : signed(C_SPEED_W - 1 downto 0);  -- Q24.13
    signal s_sample_p         : std_logic;                       -- pulse for selecting samples
    signal s_sample_p_d       : std_logic;                       -- 1cc delayed pulse for selecting samples
    signal s_downsample_cnt   : std_logic_vector(3 downto 0);    -- downsample counter
    signal s_downsample_en    : std_logic;                       -- downsample counter enable
    signal s_downsample_rst_n : std_logic;                       -- downsample counter reset
    signal s_shift_reg        : std_logic_vector(1 downto 0);    -- shift register for enable signal
    signal s_enable           : std_logic;                       -- enable signal

    -- Exponentially weighted moving average IIR LPF type
    signal s_x0_filter        : signed(C_SPEED_W  - 1 downto 0); -- Q24.13
    signal s_y0_filter        : signed(C_FILTER_W - 1 downto 0); -- Low pass filter internal variables
    signal s_y0_filter_r      : signed(C_SPEED_W  - 1 downto 0); -- Low pass filter internal variables (rounded)
    signal s_x1_filter        : signed(C_SPEED_W  - 1 downto 0); -- Q24.13
    signal s_y1_filter        : signed(C_FILTER_W - 1 downto 0); -- Low pass filter internal variables
    signal s_y1_filter_r      : signed(C_SPEED_W  - 1 downto 0); -- Low pass filter internal variables (rounded)
    -- Intermediate results
    signal s_y0_mult_0        : signed(C_FILTER_W - 1 downto 0); -- Q25.30
    signal s_y0_mult_1        : signed(C_FILTER_W - 1 downto 0); -- Q25.30
    signal s_y1_mult_0        : signed(C_FILTER_W - 1 downto 0); -- Q25.30
    signal s_y1_mult_1        : signed(C_FILTER_W - 1 downto 0); -- Q25.30

    ---------------------------------------------------------------------------------
    --                                  attributes
    ---------------------------------------------------------------------------------

    attribute USE_DSP : string;
    attribute USE_DSP of s_y0_mult_0 : signal is "YES";
    attribute USE_DSP of s_y0_mult_1 : signal is "YES";
    attribute USE_DSP of s_y1_mult_0 : signal is "YES";
    attribute USE_DSP of s_y1_mult_1 : signal is "YES";

begin

    -- input signal assignments
    s_clk         <= p_speed_est_clk_in;
    s_rst_n       <= p_speed_est_rst_n_in;
    s_angle_val_p <= p_speed_est_angle_val_p_in;
    s_angle       <= signed(p_speed_est_angle_in);

    -- Concurrent signal assignment for the output vector
    p_speed_est_speed_out <= std_logic_vector(s_y1_filter_r(C_SPEED_OUT_TRUNC_QI downto C_SPEED_OUT_TRUNC_QF)); -- Speed output from scaler

    -- downsample counter
    miso_cnt_block : gray_counter
        generic map(
           G_GRAYCNT_WIDTH         => 4,
           G_GRAYCNT_CORRECTION_ON => true
        )
        port map(
            p_grayCnt_clk_in    => s_clk,
            p_grayCnt_rst_n_in  => s_downsample_rst_n,
            p_grayCnt_en_in     => s_downsample_en,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_downsample_cnt
        );

    -- clock divider process
    process (s_rst_n, s_clk)
    begin
        -- reset
        if (s_rst_n = '0') then
            s_downsample_en <= '0';
            s_downsample_rst_n <= '0';
            s_sample_p <= '0';
        -- clock increment and output toggle
        elsif rising_edge (s_clk) then
            if (s_angle_val_p = '1') then
                s_downsample_en <= '1';
                if (s_downsample_cnt = C_DOWNSAMPLE_FACTOR_CNT) then
                    s_downsample_rst_n <= '0';
                    s_sample_p <= '1';
                else
                    s_downsample_rst_n <= '1';
                    s_sample_p <= '0';
                end if;
            else
                s_downsample_en <= '0';
                s_sample_p <= '0';
            end if;
        end if;
    end process;

    -- shift register signal process
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_shift_reg <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_sample_p = '1' and s_shift_reg(1) = '0') then
                s_shift_reg(0) <= '1';
                s_shift_reg(1) <= s_shift_reg(0);
            end if;
        end if;
    end process;

    s_enable <= s_shift_reg(1);

    -- 1cc delay on s_sample_p signal
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_sample_p_d <= '0';
        elsif rising_edge(s_clk) then
            s_sample_p_d <= s_sample_p;
        end if;
    end process;

    -- Encoder output 1st order differentiator, backwards
    differentiator : process(s_clk, s_rst_n)
    begin
        if (s_rst_n = '0') then
            s_delta_phi <= (others => '0');
            s_phi_d     <= (others => '0');
            s_phi       <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_sample_p = '1') then
                -- Update state
                s_phi   <= s_angle;
                s_phi_d <= s_phi;
            elsif (s_sample_p_d = '1' and s_enable = '1') then
                -- Simply subtracting unsigned quantities, we have a safe boundary crossing in the
                -- difference
                s_delta_phi <= signed(unsigned(s_phi) - unsigned(s_phi_d));
            end if;
        end if;
    end process differentiator;

    -- General scaling block
    scaler : process(s_clk, s_rst_n)
    begin
        if (s_rst_n = '0') then
            s_speed <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_sample_p_d = '1' and s_enable = '1') then
                -- Angle difference to electrical speed in RPM
                s_speed <= s_delta_phi * C_CONV_FACTOR; -- fx25.25
            end if;
        end if;
    end process scaler;

    -----------------------------------------------

    s_x0_filter <= s_speed;

    -- Low pass output filter #0
    lpf_0 : process(s_rst_n, s_clk)
    begin
        if (s_rst_n = '0') then
            s_y0_filter    <= (others => '0');
            s_y0_filter_r  <= (others => '0');
            s_y0_mult_0    <= (others => '0');
            s_y0_mult_1    <= (others => '0');
        elsif rising_edge(s_clk) then
                s_y0_mult_0    <= s_x0_filter * C_LPF_A0;
                s_y0_mult_1    <= s_y0_filter_r * C_LPF_B0;
                s_y0_filter    <= s_y0_mult_0 + s_y0_mult_1;
            if (s_sample_p_d = '1' and s_enable = '1') then
                s_y0_filter_r <= s_y0_filter(C_FILTER_R_QI downto C_FILTER_R_QF);
            end if;
        end if;
    end process lpf_0;

    s_x1_filter <= s_y0_filter_r;

    -- Low pass output filter #1
    lpf_1 : process(s_rst_n, s_clk)
    begin
        if (s_rst_n = '0') then
            s_y1_filter    <= (others => '0');
            s_y1_filter_r  <= (others => '0');
            s_y1_mult_0    <= (others => '0');
            s_y1_mult_1    <= (others => '0');
        elsif rising_edge(s_clk) then
            s_y1_mult_0    <= s_x1_filter * C_LPF_A0;
            s_y1_mult_1    <= s_y1_filter_r * C_LPF_B0;
            s_y1_filter    <= s_y1_mult_0 + s_y0_mult_1;
            if (s_sample_p_d = '1' and s_enable = '1') then
                s_y1_filter_r <= s_y1_filter(C_FILTER_R_QI downto C_FILTER_R_QF);
            end if;
        end if;
    end process lpf_1;

end architecture rtl;