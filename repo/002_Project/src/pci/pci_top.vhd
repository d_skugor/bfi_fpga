----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pci_top.vhd
-- module name      PCI_TOP - Structural
-- author           Arturo Montufar sArreola (arturo.arreola@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100T-1CSG324Q
-- brief            Parallel Communication Interface Top Module
----------------------------------------------------------------------------------------------
-- create date      20.10.2019
-- Revision:
--      Revision 0.01 - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Removed PCI buffer because main FSM will sample the input data
--
--      Revision 0.03 by david.pavlovic
--                    - PCI buffer is returned into PCI top module
--
--      Revision 0.04 by david.pavlovic
--                    - Added new signals for PCI buffer, FSM and CRC module
--
--      Revision 0.05 by dario.drvenkar
--                    - Updated addresses constants and CRC vector
--
--      Revision 0.06 by david.pavlovic
--                    - Code cleanup
----------------------------------------------------------------------------------------------
-- TODO: use gray counter
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity pci_top is
    port (
        -- main signals
        p_pcitop_reset_n_in          : in  std_logic;                     -- main reset / active low
        p_pcitop_clk_in              : in  std_logic;                     -- module clock
        -- MCU interface signals
        p_pcitop_dataReq_in          : in  std_logic;                     -- request input from MCU
        p_pcitop_address_out         : out std_logic_vector(3 downto 0);  -- address output to MCU
        p_pcitop_data_out            : out std_logic_vector(11 downto 0); -- data output to MCU
        p_pcitop_clk_out             : out std_logic;                     -- clock output to MCU
        -- DAQ blocks interface signals
        p_pcitop_angle_ready_p_in    : in  std_logic;
        p_pcitop_start_trans_p_in    : in  std_logic;
        p_pcitop_currents_ready_p_in : in  std_logic;
        p_pcitop_flt_n_out           : out std_logic;
        p_pcitop_phCurrA_in          : in  std_logic_vector(11 downto 0);
        p_pcitop_phCurrB_in          : in  std_logic_vector(11 downto 0);
        p_pcitop_phCurrC_in          : in  std_logic_vector(11 downto 0);
        p_pcitop_dcVolt_in           : in  std_logic_vector(11 downto 0);
        p_pcitop_angle_in            : in  std_logic_vector(11 downto 0);
        p_pcitop_speed_msb_in        : in  std_logic_vector(11 downto 0);
        p_pcitop_speed_lsb_in        : in  std_logic_vector(11 downto 0)
    );
end entity pci_top;

architecture structural of pci_top is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component pci_buffer is
        port (
            -- main signals
            p_pciBuffer_reset_n_in          : in  std_logic; -- main reset / active low
            p_pciBuffer_clk_in              : in  std_logic; -- module clock
            -- MCU interface signals
            p_pciBuffer_dataReq_in          : in  std_logic; -- request input from MCU
            -- DAQ blocks interface signals
            p_pciBuffer_angle_ready_p_in    : in  std_logic;
            p_pciBuffer_currents_ready_p_in : in  std_logic;
            p_pciBuffer_phaseCurrentA_in    : in  std_logic_vector(11 downto 0);
            p_pciBuffer_phaseCurrentB_in    : in  std_logic_vector(11 downto 0);
            p_pciBuffer_phaseCurrentC_in    : in  std_logic_vector(11 downto 0);
            p_pciBuffer_dcVoltage_in        : in  std_logic_vector(11 downto 0);
            p_pciBuffer_angle_in            : in  std_logic_vector(11 downto 0);
            p_pciBuffer_speed_msb_in        : in  std_logic_vector(11 downto 0);
            p_pciBuffer_speed_lsb_in        : in  std_logic_vector(11 downto 0);
            -- PCI msg block interface signals
            p_pciBuffer_calc_crc_out        : out std_logic;
            p_pciBuffer_phaseCurrentA_out   : out std_logic_vector(11 downto 0);
            p_pciBuffer_phaseCurrentB_out   : out std_logic_vector(11 downto 0);
            p_pciBuffer_phaseCurrentC_out   : out std_logic_vector(11 downto 0);
            p_pciBuffer_dcVoltage_out       : out std_logic_vector(11 downto 0);
            p_pciBuffer_angle_out           : out std_logic_vector(11 downto 0);
            p_pciBuffer_speed_msb_out       : out std_logic_vector(11 downto 0);
            p_pciBuffer_speed_lsb_out       : out std_logic_vector(11 downto 0)
        );
    end component;

    component pci_fsm is
        generic (
            G_PCI_CYCLES_DELAY     : integer := 50                      -- clock cycles delay that define output clock frequency. TODO: define time values
        );
        port (
            -- main signals
            p_pci_reset_n_in       : in  std_logic;                     -- main reset / active low
            p_pci_clk_in           : in  std_logic;                     -- module clock
            -- MCU interface signals
            p_pci_dataReq_in       : in  std_logic;                     -- request input from MCU
            p_pci_address_out      : out std_logic_vector(3 downto 0);  -- address output to MCU
            p_pci_data_out         : out std_logic_vector(11 downto 0); -- data output to MCU
            p_pci_clk_out          : out std_logic;                     -- clock output to MCU
            -- DAQ blocks interface signals
            p_pci_start_trans_p_in : in  std_logic;
            p_pci_phaseCurrentA_in : in  std_logic_vector(11 downto 0); -- data input from phase currents filter
            p_pci_phaseCurrentB_in : in  std_logic_vector(11 downto 0); -- data input from phase currents filter
            p_pci_phaseCurrentC_in : in  std_logic_vector(11 downto 0); -- data input from phase currents filter
            p_pci_dcVoltage_in     : in  std_logic_vector(11 downto 0); -- data input from dc voltage filter
            p_pci_angle_in         : in  std_logic_vector(11 downto 0); -- data input from resolver interface
            p_pci_speed_msb_in     : in  std_logic_vector(11 downto 0); -- data input from speed estimator (MSB)
            p_pci_speed_lsb_in     : in  std_logic_vector(11 downto 0); -- data input from speed estimator (LSB)
            p_pci_flt_n_out        : out std_logic;                     -- PCI FSM in error state
            -- CRC block interface signals
            p_pci_crcCounter_out   : out std_logic_vector(3 downto 0);
            p_pci_crcData_in       : in  std_logic_vector(7 downto 0);
            p_pci_crcReady_in      : in  std_logic
        );
    end component; -- pci

    component crc is
        generic (
            G_CRC_POLYNOMIAL : std_logic_vector(8 downto 0) := "100011101"
        );
        port (
            -- main signals
            p_crc_reset_n_in : in std_logic;
            p_crc_clk_in     : in std_logic;
            -- interface input
            p_crc_req_in     : in std_logic;
            p_crc_data_in    : in std_logic_vector(127 downto 0);
            -- interface output
            p_crc_rdy_out    : out std_logic;
            p_crc_value_out  : out std_logic_vector(7 downto 0)
        );
    end component; -- crc

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_PCI_DC_VOLTAGE_ADDRESS      : std_logic_vector(3 downto 0) := "0001";
    constant C_PCI_SPEED_MSB_ADDRESS       : std_logic_vector(3 downto 0) := "0010";
    constant C_PCI_SPEED_LSB_ADDRESS       : std_logic_vector(3 downto 0) := "0011";
    constant C_PCI_ANGLE_ADDRESS           : std_logic_vector(3 downto 0) := "0100";
    constant C_PCI_PHASE_CURRENT_A_ADDRESS : std_logic_vector(3 downto 0) := "0101";
    constant C_PCI_PHASE_CURRENT_B_ADDRESS : std_logic_vector(3 downto 0) := "0110";
    constant C_PCI_PHASE_CURRENT_C_ADDRESS : std_logic_vector(3 downto 0) := "0111";
    constant C_PCI_COUNTER_AND_CRC_ADDRESS : std_logic_vector(3 downto 0) := "1000";

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    -- main signals --
    signal s_reset_n          : std_logic; -- reset signal input
    signal s_mainClk          : std_logic; -- main clk signal input

    -- Request signal --
    signal s_mcuDataReq       : std_logic;
    signal s_start_trans_p    : std_logic;
    signal s_angle_ready_p    : std_logic;
    signal s_currents_ready_p : std_logic;
    signal s_pci_flt_n        : std_logic;

    -- Data transmission signal
    signal s_mcuAddress       : std_logic_vector(3 downto 0);
    signal s_mcuData          : std_logic_vector(11 downto 0);
    signal s_mcuClk           : std_logic;

    -- DAQ signals --
    signal s_phCurrAIn        : std_logic_vector(11 downto 0);
    signal s_phCurrBIn        : std_logic_vector(11 downto 0);
    signal s_phCurrCIn        : std_logic_vector(11 downto 0);
    signal s_dcVoltIn         : std_logic_vector(11 downto 0);
    signal s_angleIn          : std_logic_vector(11 downto 0);
    signal s_speedMsbIn       : std_logic_vector(11 downto 0);
    signal s_speedLsbIn       : std_logic_vector(11 downto 0);

    -- Buffer signals
    signal s_calc_crc         : std_logic;
    signal s_phCurrABuffer    : std_logic_vector(11 downto 0);
    signal s_phCurrBBuffer    : std_logic_vector(11 downto 0);
    signal s_phCurrCBuffer    : std_logic_vector(11 downto 0);
    signal s_dcVoltBuffer     : std_logic_vector(11 downto 0);
    signal s_angleBuffer      : std_logic_vector(11 downto 0);
    signal s_speedMsbBuffer   : std_logic_vector(11 downto 0);
    signal s_speedLsbBuffer   : std_logic_vector(11 downto 0);

    -- Counter signal
    signal s_counter          : std_logic_vector(3 downto 0);

    -- CRC signals
    signal s_crcReady         : std_logic;
    signal s_crcValue         : std_logic_vector(7 downto 0);

begin

    -- main signals
    s_reset_n            <= p_pcitop_reset_n_in;
    s_mainClk            <= p_pcitop_clk_in;
    -- MCU interface signals
    s_mcuDataReq         <= p_pcitop_dataReq_in;
    p_pcitop_address_out <= s_mcuAddress;
    p_pcitop_data_out    <= s_mcuData;
    p_pcitop_clk_out     <= s_mcuClk;
    -- DAQ blocks interface signals
    s_angle_ready_p      <= p_pcitop_angle_ready_p_in;
    s_currents_ready_p   <= p_pcitop_currents_ready_p_in;
    s_start_trans_p      <= p_pcitop_start_trans_p_in;
    p_pcitop_flt_n_out   <= s_pci_flt_n;
    s_phCurrAIn          <= p_pcitop_phCurrA_in;
    s_phCurrBIn          <= p_pcitop_phCurrB_in;
    s_phCurrCIn          <= p_pcitop_phCurrC_in;
    s_dcVoltIn           <= p_pcitop_dcVolt_in;
    s_angleIn            <= p_pcitop_angle_in;
    s_speedMsbIn         <= p_pcitop_speed_msb_in;
    s_speedLsbIn         <= p_pcitop_speed_lsb_in;

    mdl_pci_buffer : pci_buffer
        port map (
            -- main signals
            p_pciBuffer_reset_n_in          => s_reset_n,    -- main reset / active low
            p_pciBuffer_clk_in              => s_mainClk,    -- module clock
            -- MCU interface signals
            p_pciBuffer_dataReq_in          => s_mcuDataReq, -- request input from MCU
            -- DAQ blocks interface signals
            p_pciBuffer_angle_ready_p_in    => s_angle_ready_p,
            p_pciBuffer_currents_ready_p_in => s_currents_ready_p,
            p_pciBuffer_phaseCurrentA_in    => s_phCurrAIn,
            p_pciBuffer_phaseCurrentB_in    => s_phCurrBIn,
            p_pciBuffer_phaseCurrentC_in    => s_phCurrCIn,
            p_pciBuffer_dcVoltage_in        => s_dcVoltIn,
            p_pciBuffer_angle_in            => s_angleIn,
            p_pciBuffer_speed_msb_in        => s_speedMsbIn,
            p_pciBuffer_speed_lsb_in        => s_speedLsbIn,
            -- PCI msg block interface signals
            p_pciBuffer_calc_crc_out        => s_calc_crc,
            p_pciBuffer_phaseCurrentA_out   => s_phCurrABuffer,
            p_pciBuffer_phaseCurrentB_out   => s_phCurrBBuffer,
            p_pciBuffer_phaseCurrentC_out   => s_phCurrCBuffer,
            p_pciBuffer_dcVoltage_out       => s_dcVoltBuffer,
            p_pciBuffer_angle_out           => s_angleBuffer,
            p_pciBuffer_speed_msb_out       => s_speedMsbBuffer,
            p_pciBuffer_speed_lsb_out       => s_speedLsbBuffer
        );

    mdl_pci_fsm : pci_fsm
        generic map (
            G_PCI_CYCLES_DELAY     => 100
        )
        port map (
            -- main signals
            p_pci_reset_n_in       => s_reset_n,
            p_pci_clk_in           => s_mainClk,
            -- MCU interface signals
            p_pci_dataReq_in       => s_mcuDataReq,
            p_pci_address_out      => s_mcuAddress,
            p_pci_data_out         => s_mcuData,
            p_pci_clk_out          => s_mcuClk,
            -- DAQ blocks interface signals
            p_pci_start_trans_p_in => s_start_trans_p,
            p_pci_phaseCurrentA_in => s_phCurrABuffer,
            p_pci_phaseCurrentB_in => s_phCurrBBuffer,
            p_pci_phaseCurrentC_in => s_phCurrCBuffer,
            p_pci_dcVoltage_in     => s_dcVoltBuffer,
            p_pci_angle_in         => s_angleBuffer,
            p_pci_speed_msb_in     => s_speedMsbBuffer,
            p_pci_speed_lsb_in     => s_speedLsbBuffer,
            p_pci_flt_n_out        => s_pci_flt_n,
            --CRC block interface signals
            p_pci_crcCounter_out   => s_counter,
            p_pci_crcReady_in      => s_crcReady,
            p_pci_crcData_in       => s_crcValue
        );

    mdl_crc : crc
        port map (
             -- main signals
            p_crc_reset_n_in              => s_reset_n,
            p_crc_clk_in                  => s_mainClk,
            -- interface input
            p_crc_req_in                  => s_calc_crc,
            p_crc_data_in(127 downto 124) => C_PCI_DC_VOLTAGE_ADDRESS,      -- DC voltage address
            p_crc_data_in(123 downto 112) => s_dcVoltBuffer,                -- DC voltage data

            p_crc_data_in(111 downto 108) => C_PCI_SPEED_MSB_ADDRESS,       -- speed MSB address
            p_crc_data_in(107 downto 96)  => s_speedMsbBuffer,              -- speed MSB data

            p_crc_data_in(95 downto 92)   => C_PCI_SPEED_LSB_ADDRESS,       -- speed LSB address
            p_crc_data_in(91 downto 80)   => s_speedLsbBuffer,              -- speed LSB data

            p_crc_data_in(79 downto 76)   => C_PCI_ANGLE_ADDRESS,           -- angle address
            p_crc_data_in(75 downto 64)   => s_angleBuffer,                 -- angle data

            p_crc_data_in(63 downto 60)   => C_PCI_PHASE_CURRENT_A_ADDRESS, -- phase A current address
            p_crc_data_in(59 downto 48)   => s_phCurrABuffer,               -- phase A current data

            p_crc_data_in(47 downto 44)   => C_PCI_PHASE_CURRENT_B_ADDRESS, -- phase B current address
            p_crc_data_in(43 downto 32)   => s_phCurrBBuffer,               -- phase B current data

            p_crc_data_in(31 downto 28)   => C_PCI_PHASE_CURRENT_C_ADDRESS, -- phase C current address
            p_crc_data_in(27 downto 16)   => s_phCurrCBuffer,               -- phase C current data

            P_crc_data_in(15 downto 12)   => C_PCI_COUNTER_AND_CRC_ADDRESS, -- counter and CRC address
            P_crc_data_in(11 downto 8)    => s_counter,                     -- counter and CRC value
            P_crc_data_in(7 downto 0)     => (others => '0'),               -- CRC padding

            -- interface output
            P_crc_rdy_out                 => s_crcReady,
            P_crc_value_out               => s_crcValue
        );

end architecture structural;