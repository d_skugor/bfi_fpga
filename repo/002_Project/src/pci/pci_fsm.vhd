----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pci.vhd
-- module name      PCI - rtl
-- author           Arturo Montufar Arreola (arturo.arreola@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Parallel Communication Interface Block
----------------------------------------------------------------------------------------------
-- create date      10.06.2019
-- Revision:
--      Revision 0.01 by arturo.arreola
--                    - File Created
--
--      Revision 0.02 by arturo.arreola
--                    - outputs CLK and address instead of reading it
--                    - interfacing with CLK and data blocks
--                    - based on a state machine
--
--      Revision 0.03 by arturo.arreola
--                    - created PCI top module
--                    - updated ports and signals names
--                    - added MCU req and DAQ sample clk synchronization
--
--      Revision 0.04 by david.pavlovic
--                    - Removed DC current measurement signal and added
--                      signals for 24 bit speed calculation
--
--      Revision 0.05 by david.pavlovic
--                    - Added synchronous data request signal from MCU
--                    - Changed signal names
--
--      Revision 0.06 by dario.drvenkar
--                    - Removed timeout timer
--
--      Revision 0.07 by david.pavlovic
--                    - Added port for PCI transaction start
--
--      Revision 0.08 by dario.drvenkar
--                    - Major redesign in the FSM
--
--      Revision 0.09 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.10 by danijela.skugor
--                    - Substitute binary counter with gray code counter
--
----------------------------------------------------------------------------------------------
-- TODO:
--       - Check MCU request rising edge instead of signal state
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity pci_fsm is
    generic (
        G_PCI_CYCLES_DELAY     : integer := 100                     -- clock cycles delay that define output clock frequency. TODO: define time values
    );
    port (
        -- main signals
        p_pci_reset_n_in       : in  std_logic;                     -- main reset / active low
        p_pci_clk_in           : in  std_logic;                     -- module clock
        -- MCU interface signals
        p_pci_dataReq_in       : in  std_logic;                     -- request input from MCU
        p_pci_address_out      : out std_logic_vector(3 downto 0);  -- address output to MCU
        p_pci_data_out         : out std_logic_vector(11 downto 0); -- data output to MCU
        p_pci_clk_out          : out std_logic;                     -- clock output to MCU
        -- DAQ blocks interface signals
        p_pci_start_trans_p_in : in  std_logic;
        p_pci_phaseCurrentA_in : in  std_logic_vector(11 downto 0); -- data input from phase currents filter
        p_pci_phaseCurrentB_in : in  std_logic_vector(11 downto 0); -- data input from phase currents filter
        p_pci_phaseCurrentC_in : in  std_logic_vector(11 downto 0); -- data input from phase currents filter
        p_pci_dcVoltage_in     : in  std_logic_vector(11 downto 0); -- data input from dc voltage filter
        p_pci_angle_in         : in  std_logic_vector(11 downto 0); -- data input from resolver interface
        p_pci_speed_msb_in     : in  std_logic_vector(11 downto 0); -- data input from speed estimator (MSB)
        p_pci_speed_lsb_in     : in  std_logic_vector(11 downto 0); -- data input from speed estimator (LSB)
        p_pci_flt_n_out        : out std_logic;                     -- PCI FSM in error state
        -- CRC block interface signals
        p_pci_crcCounter_out   : out std_logic_vector(3 downto 0);
        p_pci_crcData_in       : in  std_logic_vector(7 downto 0);
        p_pci_crcReady_in      : in  std_logic
    );

end pci_fsm;

architecture rtl of pci_fsm is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component gray_counter is
       generic(
           G_GRAYCNT_WIDTH             : integer := 4;
           G_GRAYCNT_CORRECTION_ON     : boolean := false  -- can gray counter fix itself if an error occurs
       );
       port(
           p_grayCnt_clk_in    : in std_logic;
           p_grayCnt_rst_n_in  : in std_logic;  -- sync reset
           p_grayCnt_en_in     : in std_logic;  -- enable
           p_grayCnt_err_p_out : out std_logic; -- pulsed error
           p_grayCnt_res_out   : out std_logic_vector(G_GRAYCNT_WIDTH -1 downto 0)
       );
    end component;

    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------

    --Convert binary to gray code
    function bin_to_gray (bin : std_logic_vector) return std_logic_vector is
        variable v_gray : std_logic_vector(bin'range);
    begin
        v_gray(bin'left) := bin(bin'left);
        for i in bin'left-1 downto 0 loop
            v_gray(i) := bin(i+1) xor bin(i);
        end loop;
        return v_gray;
    end function bin_to_gray;

    -- Convert gray code to binary
    function gray_to_bin( gray : std_logic_vector ) return std_logic_vector is
        variable v_bin : std_logic_vector(gray'range);
    begin
        v_bin(v_bin'high) := gray(gray'high);
        for i in gray'high-1 downto 0 loop
            v_bin(i) := gray(i) xor v_bin(i+1);
        end loop;
        return v_bin;
    end function;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_PCI_CYCLES_DELAY_HALF       : natural := G_PCI_CYCLES_DELAY / 2 - 1;
    constant C_PCI_DC_VOLTAGE_ADDRESS      : std_logic_vector(3 downto 0) := "0001";
    constant C_PCI_SPEED_MSB_ADDRESS       : std_logic_vector(3 downto 0) := "0010";
    constant C_PCI_SPEED_LSB_ADDRESS       : std_logic_vector(3 downto 0) := "0011";
    constant C_PCI_ANGLE_ADDRESS           : std_logic_vector(3 downto 0) := "0100";
    constant C_PCI_PHASE_CURRENT_A_ADDRESS : std_logic_vector(3 downto 0) := "0101";
    constant C_PCI_PHASE_CURRENT_B_ADDRESS : std_logic_vector(3 downto 0) := "0110";
    constant C_PCI_PHASE_CURRENT_C_ADDRESS : std_logic_vector(3 downto 0) := "0111";
    constant C_PCI_COUNTER_AND_CRC_ADDRESS : std_logic_vector(3 downto 0) := "1000";

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    -- state machine type definition
    type t_pciStateMachine is (
        t_pciStateMachine_idle,
        t_pciStateMachine_send_dcVoltage,
        t_pciStateMachine_send_speed_msb,
        t_pciStateMachine_send_speed_lsb,
        t_pciStateMachine_send_angle,
        t_pciStateMachine_send_phaseCurrentA,
        t_pciStateMachine_send_phaseCurrentB,
        t_pciStateMachine_send_phaseCurrentC,
        t_pciStateMachine_send_crc,
        t_pciStateMachine_error
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk             : std_logic;
    signal s_rst_n           : std_logic;
    signal s_mcu_req         : std_logic;
    signal s_start_trans_p   : std_logic;
    signal s_pciState        : t_pciStateMachine;             -- state machine variable
    signal s_dataOutput      : std_logic_vector(11 downto 0); -- data signal to output from PCI to MCU
    signal s_dataAddress     : std_logic_vector(3 downto 0);  -- address signal to output from PCI to MCU
    signal s_addressCounter  : std_logic_vector(3 downto 0);  -- counter that modifies data address
    signal s_outputClk       : std_logic;                     -- output sample clock for MCU
    signal s_cyclesCnt_en    : std_logic;
    signal s_cyclesCnt_rst_n : std_logic;
    signal s_cyclesCnt       : std_logic_vector(11 downto 0); -- MCU sample clock signal counter
    signal s_pci_flt_n       : std_logic;                     -- PCI FSM in the error state

    -- input data
    signal s_phaseACurrent  : std_logic_vector(11 downto 0);
    signal s_phaseBCurrent  : std_logic_vector(11 downto 0);
    signal s_phaseCCurrent  : std_logic_vector(11 downto 0);
    signal s_dcVoltage      : std_logic_vector(11 downto 0);
    signal s_angle          : std_logic_vector(11 downto 0);
    signal s_speed_msb      : std_logic_vector(11 downto 0);
    signal s_speed_lsb      : std_logic_vector(11 downto 0);

    -- CRC and output counter
    signal s_crc_rdy        : std_logic;
    signal s_messageCounter : std_logic_vector(3 downto 0);  -- output signal to the CRC module
    signal s_messageCrc     : std_logic_vector(7 downto 0);  -- input value of the CRC

begin

    -- input signal assignments
    s_clk           <= p_pci_clk_in;
    s_rst_n         <= p_pci_reset_n_in;
    s_mcu_req       <= p_pci_dataReq_in;
    s_start_trans_p <= p_pci_start_trans_p_in;
    s_messageCrc    <= p_pci_crcData_in;
    s_crc_rdy       <= p_pci_crcReady_in;

    s_phaseACurrent <= p_pci_phaseCurrentA_in;
    s_phaseBCurrent <= p_pci_phaseCurrentB_in;
    s_phaseCCurrent <= p_pci_phaseCurrentC_in;
    s_dcVoltage     <= p_pci_dcVoltage_in;
    s_angle         <= p_pci_angle_in;
    s_speed_msb     <= p_pci_speed_msb_in;
    s_speed_lsb     <= p_pci_speed_lsb_in;

    -- output signal assignments
    p_pci_data_out       <= s_dataOutput;
    p_pci_address_out    <= s_dataAddress;
    p_pci_clk_out        <= s_outputClk;
    p_pci_crcCounter_out <= s_messageCounter;
    p_pci_flt_n_out      <= s_pci_flt_n;

    gray_cnt : gray_counter
        generic map(
            G_GRAYCNT_WIDTH            => 12,
            G_GRAYCNT_CORRECTION_ON    => true
        )
        port map(
            p_grayCnt_clk_in    => s_clk,
            p_grayCnt_rst_n_in  => s_cyclesCnt_rst_n,
            p_grayCnt_en_in     => s_cyclesCnt_en,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_cyclesCnt
        );

    process (s_clk, s_rst_n)
    begin
        if (s_rst_n = '0') then
            s_pciState        <= t_pciStateMachine_idle;
            s_dataOutput      <= (others => '0');
            s_dataAddress     <= (others => '0');
            s_addressCounter  <= (others => '0');
            s_outputClk       <= '0';
            s_pci_flt_n       <= '1';
            s_cyclesCnt_rst_n <= '0';
            s_cyclesCnt_en    <= '0';
            s_messageCounter  <= (others => '0');
        elsif rising_edge(s_clk) then
            case s_pciState is
                -----------------------------------------------------------------------
                when t_pciStateMachine_idle =>
                    s_cyclesCnt_rst_n <= '1';
                    s_dataAddress <= (others => '0');
                    -- if data request signal from MCU is received
                    if (s_mcu_req = '1' and s_start_trans_p = '1') then
                        -- change state machine
                        s_pciState <= t_pciStateMachine_send_dcVoltage;
                    end if;
                -----------------------------------------------------------------------
                when t_pciStateMachine_send_dcVoltage =>
                    -- if data request signal from MCU is still set
                    if (s_mcu_req = '1') then
                        s_cyclesCnt_en <= '1';  -- enable counting
                        s_dataOutput  <= s_dcVoltage;
                        s_dataAddress <= C_PCI_DC_VOLTAGE_ADDRESS;
                        if (gray_to_bin(s_cyclesCnt) >= 0 and gray_to_bin(s_cyclesCnt) < C_PCI_CYCLES_DELAY_HALF) then
                            s_outputClk <= '0';
                        elsif (gray_to_bin(s_cyclesCnt) >= C_PCI_CYCLES_DELAY_HALF and gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 2) then
                            s_outputClk <= '1';
                        elsif (gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 1) then
                             s_cyclesCnt_rst_n <= '0';
                        else
                            s_cyclesCnt_rst_n <= '1';
                            s_outputClk <= '0';
                            s_pciState <= t_pciStateMachine_send_speed_msb;
                        end if;
                    else -- (s_mcu_req = '0')
                        s_pci_flt_n <= '0';
                        s_pciState  <= t_pciStateMachine_error;
                    end if;
                -----------------------------------------------------------------------
                when t_pciStateMachine_send_speed_msb =>
                    -- if data request signal from MCU is still set
                    if (s_mcu_req = '1') then
                        s_dataOutput  <= s_speed_msb;
                        s_dataAddress <= C_PCI_SPEED_MSB_ADDRESS;
                         s_cyclesCnt_en <= '1';  -- enable counting
                          s_cyclesCnt_rst_n <= '1';
                        if (gray_to_bin(s_cyclesCnt) >= 0 and gray_to_bin(s_cyclesCnt) < C_PCI_CYCLES_DELAY_HALF ) then
                            s_outputClk <= '0';
                        elsif (gray_to_bin(s_cyclesCnt) >= C_PCI_CYCLES_DELAY_HALF and gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 2) then
                            s_outputClk <= '1';
                        elsif (gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 1) then
                             s_cyclesCnt_rst_n <= '0';
                        else
                            s_cyclesCnt_rst_n <= '1';
                            s_outputClk <= '0';
                            s_pciState <= t_pciStateMachine_send_speed_lsb;
                        end if;
                    else -- (s_mcu_req = '0')
                        s_pci_flt_n <= '0';
                        s_pciState  <= t_pciStateMachine_error;
                end if;
                -----------------------------------------------------------------------
                when t_pciStateMachine_send_speed_lsb =>
                    -- if data request signal from MCU is still set
                    if (s_mcu_req = '1') then
                        s_cyclesCnt_en <= '1';  -- enable counting
                        s_dataOutput  <= s_speed_lsb;
                        s_dataAddress <= C_PCI_SPEED_LSB_ADDRESS;
                        if (gray_to_bin(s_cyclesCnt) >= 0 and gray_to_bin(s_cyclesCnt) < C_PCI_CYCLES_DELAY_HALF) then
                            s_outputClk <= '0';
                        elsif (gray_to_bin(s_cyclesCnt) >= C_PCI_CYCLES_DELAY_HALF and gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 2) then
                            s_outputClk <= '1';
                        elsif (gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 1) then
                            s_cyclesCnt_rst_n <= '0';
                        else
                            s_cyclesCnt_rst_n <= '1';
                            s_outputClk <= '0';
                            s_pciState <= t_pciStateMachine_send_angle;
                        end if;
                    else -- (s_mcu_req = '0')
                        s_pci_flt_n <= '0';
                        s_pciState  <= t_pciStateMachine_error;
                end if;
                -----------------------------------------------------------------------
                when t_pciStateMachine_send_angle =>
                    -- if data request signal from MCU is still set
                    if (s_mcu_req = '1') then
                        s_cyclesCnt_en <= '1';  -- enable counting
                        s_dataOutput  <= s_angle;
                        s_dataAddress <= C_PCI_ANGLE_ADDRESS;
                        if (gray_to_bin(s_cyclesCnt) >= 0 and gray_to_bin(s_cyclesCnt) < C_PCI_CYCLES_DELAY_HALF) then
                            s_outputClk <= '0';
                        elsif (gray_to_bin(s_cyclesCnt) >= C_PCI_CYCLES_DELAY_HALF and gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 2) then
                            s_outputClk <= '1';
                        elsif (gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 1) then
                            s_cyclesCnt_rst_n <= '0';
                        else
                            s_cyclesCnt_rst_n <= '1';
                            s_outputClk <= '0';
                            s_pciState <= t_pciStateMachine_send_phaseCurrentA;
                        end if;
                    else -- (s_mcu_req = '0')
                        s_pci_flt_n <= '0';
                        s_pciState  <= t_pciStateMachine_error;
                end if;
                -----------------------------------------------------------------------
                when t_pciStateMachine_send_phaseCurrentA =>
                    -- if data request signal from MCU is still set
                    if (s_mcu_req = '1') then
                        s_cyclesCnt_en <= '1';  -- enable counting
                        s_dataOutput  <= s_phaseACurrent;
                        s_dataAddress <= C_PCI_PHASE_CURRENT_A_ADDRESS;
                        if (gray_to_bin(s_cyclesCnt) >= 0 and gray_to_bin(s_cyclesCnt) < C_PCI_CYCLES_DELAY_HALF) then
                            s_outputClk <= '0';
                        elsif (gray_to_bin(s_cyclesCnt) >= C_PCI_CYCLES_DELAY_HALF and gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 2) then
                            s_outputClk <= '1';
                        elsif (gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 1) then
                            s_cyclesCnt_rst_n <= '0';
                        else
                            s_cyclesCnt_rst_n <= '1';
                            s_outputClk <= '0';
                            s_pciState <= t_pciStateMachine_send_phaseCurrentB;
                        end if;
                    else -- (s_mcu_req = '0')
                        s_pci_flt_n <= '0';
                        s_pciState  <= t_pciStateMachine_error;
                end if;
                -----------------------------------------------------------------------
                when t_pciStateMachine_send_phaseCurrentB =>
                    -- if data request signal from MCU is still set
                    if (s_mcu_req = '1') then
                        s_cyclesCnt_en <= '1';  -- enable counting
                        s_dataOutput  <= s_phaseBCurrent;
                        s_dataAddress <= C_PCI_PHASE_CURRENT_B_ADDRESS;
                        if (gray_to_bin(s_cyclesCnt) >= 0 and gray_to_bin(s_cyclesCnt) < C_PCI_CYCLES_DELAY_HALF) then
                            s_outputClk <= '0';
                        elsif (gray_to_bin(s_cyclesCnt) >= C_PCI_CYCLES_DELAY_HALF and gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 2) then
                            s_outputClk <= '1';
                        elsif (gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 1) then
                            s_cyclesCnt_rst_n <= '0';
                        else
                            s_cyclesCnt_rst_n <= '1';
                            s_outputClk <= '0';
                            s_pciState <= t_pciStateMachine_send_phaseCurrentC;
                        end if;
                    else -- (s_mcu_req = '0')
                        s_pci_flt_n <= '0';
                        s_pciState  <= t_pciStateMachine_error;
                end if;
                -----------------------------------------------------------------------
                when t_pciStateMachine_send_phaseCurrentC =>
                    -- if data request signal from MCU is still set
                    if (s_mcu_req = '1') then
                        s_cyclesCnt_en <= '1';  -- enable counting
                        s_dataOutput  <= s_phaseCCurrent;
                        s_dataAddress <= C_PCI_PHASE_CURRENT_C_ADDRESS;
                        if (gray_to_bin(s_cyclesCnt) >= 0 and gray_to_bin(s_cyclesCnt) < C_PCI_CYCLES_DELAY_HALF) then
                            s_outputClk <= '0';
                        elsif (gray_to_bin(s_cyclesCnt) >= C_PCI_CYCLES_DELAY_HALF and gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 2) then
                            s_outputClk <= '1';
                        elsif (gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 1) then
                            s_cyclesCnt_rst_n <= '0';
                        else
                            s_cyclesCnt_rst_n <= '1';
                            s_outputClk <= '0';
                            s_pciState <= t_pciStateMachine_send_crc;
                        end if;
                    else -- (s_mcu_req = '0')
                        s_pci_flt_n <= '0';
                        s_pciState  <= t_pciStateMachine_error;
                end if;
                -----------------------------------------------------------------------
                when t_pciStateMachine_send_crc =>
                    -- if data request signal from MCU is still set
                    if (s_mcu_req = '1' and s_crc_rdy = '1') then
                        s_cyclesCnt_en <= '1';  -- enable counting
                        s_dataOutput  <= s_messageCounter & s_messageCrc;
                        s_dataAddress <= C_PCI_COUNTER_AND_CRC_ADDRESS;
                        if (gray_to_bin(s_cyclesCnt) >= 0 and gray_to_bin(s_cyclesCnt) < C_PCI_CYCLES_DELAY_HALF) then
                            s_outputClk <= '0';
                        elsif (gray_to_bin(s_cyclesCnt) >= C_PCI_CYCLES_DELAY_HALF and gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 2) then
                            s_outputClk <= '1';
                        elsif (gray_to_bin(s_cyclesCnt) < G_PCI_CYCLES_DELAY - 1) then
                            s_cyclesCnt_rst_n <= '0';
                        else
                            s_cyclesCnt_rst_n <= '1';
                            s_cyclesCnt_en <= '0';  -- disable counting
                            s_outputClk <= '0';
                            s_messageCounter <= s_messageCounter + 1;
                            s_pciState <= t_pciStateMachine_idle;
                        end if;
                    else -- (s_mcu_req = '0')
                        s_pci_flt_n <= '0';
                        s_pciState  <= t_pciStateMachine_error;
                end if;
                -----------------------------------------------------------------------
                when t_pciStateMachine_error =>
                    s_pci_flt_n <= '0';
                    -- error state can only be cleared with a reset
                -----------------------------------------------------------------------
                when others =>
                    s_pciState <= t_pciStateMachine_error;
            end case;
        end if;
    end process;

end architecture rtl;
