----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pci_buffer.vhd
-- module name      PCI Buffer - rtl
-- author           Arturo Montufar Arreola (arturo.arreola@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Parallel Communication Interface DAQ Modules Data Buffer
----------------------------------------------------------------------------------------------
-- create date      22.10.2019
-- Revision:
--      Revision 0.01 by arturo.arreola
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Removed DC current measurement signal and added
--                      signals for 24 bit speed calculation
--                    - File Created
--
--      Revision 0.03 by david.pavlovic
--                    - The buffer code is simplified
--                    - Buffer is updated when both MCU request and data ready signal
--                      from median filter are set to HIGH
--
--      Revision 0.04 by david.pavlovic
--                    - Added separate process for buffering DC voltage and speed estimation
--                          - trigger => s_mcu_req_p
--                    - Added separate process for buffering angle
--                          - trigger => s_angle_rdy_p
--                    - Added separate process for phase currents
--                          - trigger => s_currents_rdy_p
--
--      Revision 0.05 by david.pavlovic
--                    - Code cleanup
--
----------------------------------------------------------------------------------------------
-- TODO: - nothing
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity pci_buffer is
    port (
        -- main signals
        p_pciBuffer_reset_n_in          : in  std_logic; -- main reset / active low
        p_pciBuffer_clk_in              : in  std_logic; -- module clock
        -- MCU interface signals
        p_pciBuffer_dataReq_in          : in  std_logic; -- request input from MCU
        -- DAQ blocks interface signals
        p_pciBuffer_angle_ready_p_in    : in  std_logic;
        p_pciBuffer_currents_ready_p_in : in  std_logic;
        p_pciBuffer_phaseCurrentA_in    : in  std_logic_vector(11 downto 0);
        p_pciBuffer_phaseCurrentB_in    : in  std_logic_vector(11 downto 0);
        p_pciBuffer_phaseCurrentC_in    : in  std_logic_vector(11 downto 0);
        p_pciBuffer_dcVoltage_in        : in  std_logic_vector(11 downto 0);
        p_pciBuffer_angle_in            : in  std_logic_vector(11 downto 0);
        p_pciBuffer_speed_msb_in        : in  std_logic_vector(11 downto 0);
        p_pciBuffer_speed_lsb_in        : in  std_logic_vector(11 downto 0);
        -- PCI msg block interface signals
        p_pciBuffer_calc_crc_out        : out std_logic;
        p_pciBuffer_phaseCurrentA_out   : out std_logic_vector(11 downto 0);
        p_pciBuffer_phaseCurrentB_out   : out std_logic_vector(11 downto 0);
        p_pciBuffer_phaseCurrentC_out   : out std_logic_vector(11 downto 0);
        p_pciBuffer_dcVoltage_out       : out std_logic_vector(11 downto 0);
        p_pciBuffer_angle_out           : out std_logic_vector(11 downto 0);
        p_pciBuffer_speed_msb_out       : out std_logic_vector(11 downto 0);
        p_pciBuffer_speed_lsb_out       : out std_logic_vector(11 downto 0)
    );
end entity pci_buffer;

architecture rtl of pci_buffer is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk            : std_logic;
    signal s_rst_n          : std_logic;
    signal s_mcu_req        : std_logic;
    signal s_mcu_req_p      : std_logic;
    signal s_data_rdy       : std_logic;
    signal s_angle_rdy_p    : std_logic;
    signal s_currents_rdy_p : std_logic;
    signal s_calc_crc       : std_logic;

begin

    -- input signals assignments
    s_clk            <= p_pciBuffer_clk_in;
    s_rst_n          <= p_pciBuffer_reset_n_in;
    s_mcu_req        <= p_pciBuffer_dataReq_in;
    s_angle_rdy_p    <= p_pciBuffer_angle_ready_p_in;
    s_currents_rdy_p <= p_pciBuffer_currents_ready_p_in;

    -- output signal assignments
    p_pciBuffer_calc_crc_out <= s_calc_crc;

    -- s_mcu_req rising edge detector
    mcu_req_ed : edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_mcu_req,
            p_edp_sig_re_out => s_mcu_req_p,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
        );

    -- DC voltage and speed buffer
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            p_pciBuffer_dcVoltage_out <= (others => '0');
            p_pciBuffer_speed_msb_out <= (others => '0');
            p_pciBuffer_speed_lsb_out <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_mcu_req_p = '1') then
                p_pciBuffer_dcVoltage_out <= p_pciBuffer_dcVoltage_in;
                p_pciBuffer_speed_msb_out <= p_pciBuffer_speed_msb_in;
                p_pciBuffer_speed_lsb_out <= p_pciBuffer_speed_lsb_in;
            end if;
        end if;
    end process;

    -- angle buffer
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            p_pciBuffer_angle_out <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_mcu_req = '1' and s_angle_rdy_p = '1') then
                p_pciBuffer_angle_out <= p_pciBuffer_angle_in;
            end if;
        end if;
    end process;

    -- phase currents buffer
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_calc_crc <= '0';
            p_pciBuffer_phaseCurrentA_out <= (others => '0');
            p_pciBuffer_phaseCurrentB_out <= (others => '0');
            p_pciBuffer_phaseCurrentC_out <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_mcu_req = '0') then
                s_calc_crc <= '0';
            end if;
            if (s_mcu_req = '1' and s_currents_rdy_p = '1') then
                s_calc_crc <= '1';
                p_pciBuffer_phaseCurrentA_out <= p_pciBuffer_phaseCurrentA_in;
                p_pciBuffer_phaseCurrentB_out <= p_pciBuffer_phaseCurrentB_in;
                p_pciBuffer_phaseCurrentC_out <= p_pciBuffer_phaseCurrentC_in;
            end if;
        end if;
    end process;

end rtl;
