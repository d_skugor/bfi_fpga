----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             crc.vhd
-- module name      CRC - Behavioral
-- author           Arturo Montufar Arreola (arturo.arreola@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Cyclick Redundancy Check value calculation block
----------------------------------------------------------------------------------------------
-- create date      12.06.2019
-- Revision:
--      Revision 0.01 - File Created - Support for CRC8 VDA CAN (x^8 + x^4 + x^3 + x^2 + 1)
--
--      Revision 0.02 - Changed variable names according to standard
--
--      Revision 0.03 by david.pavlovic
--                    - Code cleanup
----------------------------------------------------------------------------------------------
-- TODO: error handler
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity crc is
    generic (
        G_CRC_POLYNOMIAL : std_logic_vector(8 downto 0) := "100011101";
        G_CRC_INPUT_SIZE : integer := 127
    );
    port (
        -- main signals input
        p_crc_reset_n_in : in std_logic;
        p_crc_clk_in     : in std_logic;
        -- interface input
        p_crc_req_in     : in std_logic;
        p_crc_data_in    : in std_logic_vector(G_CRC_INPUT_SIZE downto 0);
        -- interface output
        p_crc_rdy_out    : out std_logic;
        p_crc_value_out  : out std_logic_vector(7 downto 0)
    );
end entity crc;

architecture behavioral of crc is

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_CRC_OUTPUT_SIZE : integer := 8;

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    -- state machine type definition
    type t_crcStateMachine is (
        t_crcStateMachine_idle,
        t_crcStateMachine_pciRequestReceived,
        t_crcStateMachine_calculatingCrc,
        t_crcStateMachine_crcCalculated,
        t_crcStateMachine_sendingCrc,
        t_crcStateMachine_crcSent,
        t_crcStateMachine_error
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_state      : t_crcStateMachine;                                                     -- state variable - controls state machine
    signal s_crcBuffer  : std_logic_vector(G_CRC_INPUT_SIZE downto 0) := (others => '0');        -- holds algorithm data during calculation
    signal s_crcValue   : std_logic_vector(C_CRC_OUTPUT_SIZE - 1 downto 0)   := (others => '0'); -- final CRC value
    signal s_crcPointer : integer range 0 to G_CRC_INPUT_SIZE := G_CRC_INPUT_SIZE;

begin

    process (p_crc_reset_n_in, p_crc_clk_in)
    begin
        if (p_crc_reset_n_in = '0') then
            p_crc_rdy_out <= '0';
            s_state       <= t_crcStateMachine_idle;
            -- TODO: set all output signals off

        elsif rising_edge(p_crc_clk_in) then
            case s_state is
                when t_crcStateMachine_idle =>
                    if (p_crc_req_in = '1') then
                        -- initialize operation
                        s_state <= t_crcStateMachine_pciRequestReceived;
                    end if;

                when t_crcStateMachine_pciRequestReceived =>
                    if (p_crc_req_in = '1') then
                        -- store input data to calculate crc
                        s_crcBuffer <= p_crc_data_in;
                        -- change state machine
                        s_state <= t_crcStateMachine_calculatingCrc;
                    else
                        s_state <= t_crcStateMachine_error;
                    end if;

                when t_crcStateMachine_calculatingCrc =>
                    if (p_crc_req_in = '1') then
                        if (s_crcBuffer(s_crcPointer) = '1') then
                            -- current 8 bits XOR CRC polinomial
                            s_crcBuffer(s_crcPointer - 0) <=
                                    s_crcBuffer(s_crcPointer - 0) xor G_CRC_POLYNOMIAL(8 - 0);
                            s_crcBuffer(s_crcPointer - 1) <=
                                    s_crcBuffer(s_crcPointer - 1) xor G_CRC_POLYNOMIAL(8 - 1);
                            s_crcBuffer(s_crcPointer - 2) <=
                                    s_crcBuffer(s_crcPointer - 2) xor G_CRC_POLYNOMIAL(8 - 2);
                            s_crcBuffer(s_crcPointer - 3) <=
                                    s_crcBuffer(s_crcPointer - 3) xor G_CRC_POLYNOMIAL(8 - 3);
                            s_crcBuffer(s_crcPointer - 4) <=
                                    s_crcBuffer(s_crcPointer - 4) xor G_CRC_POLYNOMIAL(8 - 4);
                            s_crcBuffer(s_crcPointer - 5) <=
                                    s_crcBuffer(s_crcPointer - 5) xor G_CRC_POLYNOMIAL(8 - 5);
                            s_crcBuffer(s_crcPointer - 6) <=
                                    s_crcBuffer(s_crcPointer - 6) xor G_CRC_POLYNOMIAL(8 - 6);
                            s_crcBuffer(s_crcPointer - 7) <=
                                    s_crcBuffer(s_crcPointer - 7) xor G_CRC_POLYNOMIAL(8 - 7);
                            s_crcBuffer(s_crcPointer - 8) <=
                                    s_crcBuffer(s_crcPointer - 8) xor G_CRC_POLYNOMIAL(8 - 8);
                        end if;

                        -- increase pointer
                        s_crcPointer <= s_crcPointer - 1;

                        -- check if data should be read
                        if (s_crcPointer = C_CRC_OUTPUT_SIZE) then
                            -- clear pointer
                            s_crcPointer <= G_CRC_INPUT_SIZE;
                            -- change state machine
                            s_state <= t_crcStateMachine_crcCalculated;

                        elsif (s_crcPointer < C_CRC_OUTPUT_SIZE) then
                            s_state <= t_crcStateMachine_error;

                        end if;

                    else
                        s_state <= t_crcStateMachine_error;

                    end if;

                when t_crcStateMachine_crcCalculated =>
                    if (p_crc_req_in = '1') then
                        -- store CRC value into signal to output
                        s_crcValue(0) <= s_crcBuffer(0);
                        s_crcValue(1) <= s_crcBuffer(1);
                        s_crcValue(2) <= s_crcBuffer(2);
                        s_crcValue(3) <= s_crcBuffer(3);
                        s_crcValue(4) <= s_crcBuffer(4);
                        s_crcValue(5) <= s_crcBuffer(5);
                        s_crcValue(6) <= s_crcBuffer(6);
                        s_crcValue(7) <= s_crcBuffer(7);
                        -- change state machine
                        s_state <= t_crcStateMachine_sendingCrc;

                    else
                        s_state <= t_crcStateMachine_error;

                    end if;

                when t_crcStateMachine_sendingCrc =>
                    if (p_crc_req_in = '1') then
                        -- output ready signal in the next cycle
                        p_crc_rdy_out <= '1';
                        -- change state machine
                        s_state <= t_crcStateMachine_crcSent;

                    else
                        s_state <= t_crcStateMachine_error;

                    end if;

                when t_crcStateMachine_crcSent =>
                    if (p_crc_req_in = '0') then
                        -- set ready signal low
                        p_crc_rdy_out <= '0';
                        -- clear signals
                        s_crcValue <= (others => '0');
                        s_crcBuffer <= (others => '0');
                        -- reset state machine
                        s_state <= t_crcStateMachine_idle;
                    end if;

                when others =>
                    s_state <= t_crcStateMachine_error;
            end case;
        end if;
    end process;

    -- signal-to-port mapping
    p_crc_value_out <= s_crcValue;

end behavioral;