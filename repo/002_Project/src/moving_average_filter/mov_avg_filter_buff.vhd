----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2021, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             movAvg_filter_buff.vhd
-- module name      Moving average filter buffer
-- author           Dario Drvenkar (dario.drvenkar@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Buffer for controlling output from Moving Average Filters
--
----------------------------------------------------------------------------------------------
-- create date      05.02.2021
-- Revision:
--      Revision 0.01 by dario.drvenkar
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Added signals for PCI buffer and FSM
--                    - Pulses for: central sample, start of PCI transaction and end of MA
--                      filter buffer
--
--      Revision 0.03 by david.pavlovic
--                    - Code cleanup
--
---     Revision 0.03 by danijela.skugor
--                    - Substitute binary counter with gray code counter
--
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real."ceil";
use ieee.math_real."log2";

entity mov_avg_filter_buff is
    generic (
        G_MOV_AVG_FILTER_BUFF_DATA            : natural := 12;                                                  -- Data size
        G_MOV_AVG_FILTER_BUFF_AVG_LEN         : natural := 7                                                    -- Filter length
    );
    port (
        p_mov_avg_filter_buff_rst_n_in        : in  std_logic;                                                  -- System reset input
        p_mov_avg_filter_buff_clk_in          : in  std_logic;                                                  -- System clock input
        p_mov_avg_filter_buff_sample_p_in     : in  std_logic;                                                  -- Sample pulse from ADC
        p_mov_avg_filter_buff_mcu_req_in      : in  std_logic;                                                  -- MCU request (start of PWM cycle)
        p_mov_avg_filter_buff_phase_a_in      : in  std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);  -- Stream input 1
        p_mov_avg_filter_buff_phase_b_in      : in  std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);  -- Stream input 2
        p_mov_avg_filter_buff_phase_c_in      : in  std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);  -- Stream input 3
        p_mov_avg_filter_buff_central_p_out   : out std_logic;                                                  -- Central sample detected pulse
        p_mov_avg_filter_buff_pci_start_p_out : out std_logic;                                                  -- Start PCI transaction pulse
        p_mov_avg_filter_buff_rdy_p_out       : out std_logic;                                                  -- Output ready pulse
        p_mov_avg_filter_buff_phase_a_out     : out std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);  -- Sample output 1
        p_mov_avg_filter_buff_phase_b_out     : out std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);  -- Sample output 2
        p_mov_avg_filter_buff_phase_c_out     : out std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0)   -- Sample output 3
     );
end entity mov_avg_filter_buff;

architecture rtl of mov_avg_filter_buff is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    component gray_counter is
        generic(
            G_GRAYCNT_WIDTH             : integer := 4;
            G_GRAYCNT_CORRECTION_ON     : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port(
            p_grayCnt_clk_in    : in std_logic;
            p_grayCnt_rst_n_in  : in std_logic;  -- sync reset
            p_grayCnt_en_in     : in std_logic;  -- enable
            p_grayCnt_err_p_out : out std_logic; -- pulsed error
            p_grayCnt_res_out   : out std_logic_vector(G_GRAYCNT_WIDTH -1 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------
    function bin_to_gray (bin : std_logic_vector) return std_logic_vector is
        variable v_gray : std_logic_vector(bin'range);
    begin
        v_gray(bin'left) := bin(bin'left);
        for i in bin'left-1 downto 0 loop
            v_gray(i) := bin(i+1) xor bin(i);
        end loop;
        return v_gray;
    end function bin_to_gray;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_CNT_WIDTH           : integer := integer(ceil(log2(real(G_MOV_AVG_FILTER_BUFF_AVG_LEN)))) + 1 ;
    constant C_ADC_CENTRAL_DELAY_I : integer := 2;
    constant C_ADC_CENTRAL_DELAY   : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(C_ADC_CENTRAL_DELAY_I, C_CNT_WIDTH)); -- number of sampels to detect central sample (ADC takes 1.8us to deliver data to FPGA)
    constant C_AVG_LEN_HALF_I      : integer := G_MOV_AVG_FILTER_BUFF_AVG_LEN / 2;
    constant C_SAMPLE_COUNTER_I    : integer := C_AVG_LEN_HALF_I + C_ADC_CENTRAL_DELAY_I;
    constant C_SAMPLE_COUNTER      : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(C_SAMPLE_COUNTER_I, C_CNT_WIDTH));  -- number of samples to average after MCU request
    constant C_PCI_START_TRANS_I   : integer := C_SAMPLE_COUNTER_I - 4;
    constant C_PCI_START_TRANS     : std_logic_vector(4 downto 0) := std_logic_vector(to_unsigned(C_PCI_START_TRANS_I, C_CNT_WIDTH)); -- number of sampels after which PCI transaction can start
    constant C_F_DELAY             : natural := 4;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk                 : std_logic;
    signal s_rst_n               : std_logic;
    signal s_sample_p            : std_logic;
    signal s_mcu_req             : std_logic;
    signal s_rdy_out             : std_logic;

    signal s_mcu_req_p           : std_logic;        --
    signal s_rdy_p               : std_logic ; -- Internal ready pulse
    signal s_rdy_p_d             : std_logic_vector(C_F_DELAY + 1 downto 0);
    signal s_buff_update         : std_logic;
    signal s_sync_detected       : std_logic ; -- Start pulse received flag
    signal s_central_detected    : std_logic ; -- Central sample pulse flag
    signal s_central_p           : std_logic ; -- Central sample pulse
    signal s_pci_start_detected  : std_logic ; -- Central sample pulse flag
    signal s_pci_start_p         : std_logic ; -- Central sample pulse
    signal s_buff_ctrl_cnt       : std_logic_vector(C_CNT_WIDTH-1 downto 0);
    signal s_buff_ctrl_en        : std_logic;
    signal s_buff_ctrl_rst_n     : std_logic;

    signal s_phase_a_in          : std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);
    signal s_phase_b_in          : std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);
    signal s_phase_c_in          : std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);
    signal s_phase_a_out         : std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);
    signal s_phase_b_out         : std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);
    signal s_phase_c_out         : std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);

begin

        -- input signals assignments
    s_clk        <= p_mov_avg_filter_buff_clk_in;
    s_rst_n      <= p_mov_avg_filter_buff_rst_n_in;
    s_sample_p   <= p_mov_avg_filter_buff_sample_p_in;
    s_mcu_req    <= p_mov_avg_filter_buff_mcu_req_in;
    s_phase_a_in <= p_mov_avg_filter_buff_phase_a_in;
    s_phase_b_in <= p_mov_avg_filter_buff_phase_b_in;
    s_phase_c_in <= p_mov_avg_filter_buff_phase_c_in;

        -- output signals assignments
    p_mov_avg_filter_buff_central_p_out   <= s_central_p;
    p_mov_avg_filter_buff_pci_start_p_out <= s_pci_start_p;
    p_mov_avg_filter_buff_rdy_p_out       <= s_rdy_out;
    p_mov_avg_filter_buff_phase_a_out     <= s_phase_a_out;
    p_mov_avg_filter_buff_phase_b_out     <= s_phase_b_out;
    p_mov_avg_filter_buff_phase_c_out     <= s_phase_c_out;

    -- s_mcu_req rising edge detector
    mcu_req_ed : edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_mcu_req,
            p_edp_sig_re_out => s_mcu_req_p,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
        );

    -- s_mcu_req rising edge detector
    central_sample_ed : edge_detector_p
        generic map(
            G_REG            => 0
        )
        port map(
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_central_detected,
            p_edp_sig_re_out => s_central_p,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
    );

    -- s_mcu_req rising edge detector
    pci_start_ed : edge_detector_p
        generic map(
            G_REG            => 0
        )
        port map(
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_pci_start_detected,
            p_edp_sig_re_out => s_pci_start_p,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
    );

    -- Gray code buffer control counter
    gray_cnt : gray_counter
        generic map(
            G_GRAYCNT_WIDTH            => C_CNT_WIDTH,
            G_GRAYCNT_CORRECTION_ON    => true
        )
        port map(
            p_grayCnt_clk_in    => s_clk,
            p_grayCnt_rst_n_in  => s_buff_ctrl_rst_n,
            p_grayCnt_en_in     => s_sample_p,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_buff_ctrl_cnt
        );

    -- counter for synchronous MA samples
    buff_control : process (s_rst_n, s_clk)
    begin
        if (s_rst_n = '0') then
            s_rdy_p              <= '0';
            s_sync_detected      <= '0';
            s_central_detected   <= '0';
            s_pci_start_detected <= '0';
            s_buff_ctrl_rst_n    <= '0';
            s_buff_ctrl_en       <= '0';
        elsif rising_edge(s_clk) then
            s_rdy_p              <= '0';
            s_pci_start_detected <= '0';
            s_buff_ctrl_en       <= '0';
            s_central_detected   <= '0';
            if (s_mcu_req_p = '1') then
                s_sync_detected      <= '1';
                s_buff_ctrl_rst_n    <= '1';
            elsif (s_sync_detected = '1' and s_sample_p = '1') then
                s_buff_ctrl_en       <= '1';
            elsif (s_buff_ctrl_cnt = bin_to_gray(C_ADC_CENTRAL_DELAY)) then
                s_central_detected   <= '1';
            elsif (s_buff_ctrl_cnt = bin_to_gray(C_PCI_START_TRANS)) then
                s_pci_start_detected <= '1';
            elsif (s_buff_ctrl_cnt = bin_to_gray(C_SAMPLE_COUNTER)) then
                s_rdy_p              <= '1';
                s_sync_detected      <= '0';
                s_pci_start_detected <= '0';
                s_central_detected   <= '0';
                s_buff_ctrl_rst_n    <= '0';
                s_buff_ctrl_en       <= '0';
            end if;
        end if;
    end process;

    -- multiplier delay compensation
    timing_delay : process (s_rst_n, s_clk)
    begin
        if (s_rst_n = '0') then
            s_rdy_p_d <= (others => '0');
        elsif rising_edge(s_clk) then
            for i in 0 to s_rdy_p_d'length-2 loop
                s_rdy_p_d(i+1) <= s_rdy_p_d(i);
            end loop;
            s_rdy_p_d(0) <= s_rdy_p;
       end if;
    end process;

    s_buff_update <= s_rdy_p_d(s_rdy_p_d'high - 1);
    s_rdy_out     <= s_rdy_p_d(s_rdy_p_d'high);

    output_buffer : process (s_rst_n, s_clk)
    begin
        if (s_rst_n = '0') then
            s_phase_a_out   <= (others => '0');
            s_phase_b_out   <= (others => '0');
            s_phase_c_out   <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_buff_update = '1') then
                s_phase_a_out <= s_phase_a_in;
                s_phase_b_out <= s_phase_b_in;
                s_phase_c_out <= s_phase_c_in;
            end if;
        end if;
    end process;

end architecture rtl;
