----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2021, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             mov_avg_filter_top.vhd
-- module name      Moving average filter top
-- author           Dario Drvenkar (dario.drvenkar@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Moving average filter with an odd number of samples
--                  Middle sample is aligned with the MCU request
--                  Number of filter samples must be >= 9 (because of PCI transaction)
--
----------------------------------------------------------------------------------------------
-- create date      05.02.2021
-- Revision:
--      Revision 0.01 by dario.drvenkar
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Added signals for PCI buffer and PCI FSM
--
--      Revision 0.03 by david.pavlovic
--                    - Code cleanup
--
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real."ceil";
use ieee.math_real."log2";

entity mov_avg_filter_top is
    generic (
        G_MOV_AVG_FILTER_TOP_DATA            : natural := 12;                                                 -- Data size
        G_MOV_AVG_FILTER_TOP_AVG_LEN         : natural := 7                                                   -- Filter length
    );
    port (
        p_mov_avg_filter_top_rst_n_in        : in  std_logic;                                                 -- System reset input
        p_mov_avg_filter_top_clk_in          : in  std_logic;                                                 -- System clock input
        p_mov_avg_filter_top_sample_p_in     : in  std_logic;                                                 -- Sample pulse from ADC
        p_mov_avg_filter_top_mcu_req_in      : in  std_logic;                                                 -- MCU request (start of PWM cycle)
        p_mov_avg_filter_top_central_p_out   : out std_logic;                                                 -- Central sample detected pulse
        p_mov_avg_filter_top_start_pci_p_out : out std_logic;                                                 -- Start PCI transaction pulse
        p_mov_avg_filter_top_rdy_p_out       : out std_logic;                                                 -- Output ready pulse
        p_mov_avg_filter_top_phase_a_in      : in  std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Stream input 1
        p_mov_avg_filter_top_phase_b_in      : in  std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Stream input 2
        p_mov_avg_filter_top_phase_c_in      : in  std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Stream input 3
        p_mov_avg_filter_top_phase_a_out     : out std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Sample output 1
        p_mov_avg_filter_top_phase_b_out     : out std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Sample output 2
        p_mov_avg_filter_top_phase_c_out     : out std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0)   -- Sample output 3
    );
end entity mov_avg_filter_top;

architecture structural of mov_avg_filter_top is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component mov_avg_filter_buff is
        generic (
            G_MOV_AVG_FILTER_BUFF_DATA            : natural := 12;                                                  -- Data size
            G_MOV_AVG_FILTER_BUFF_AVG_LEN         : natural := 7                                                    -- Filter length
        );
        port (
            p_mov_avg_filter_buff_rst_n_in        : in  std_logic;                                                  -- System reset input
            p_mov_avg_filter_buff_clk_in          : in  std_logic;                                                  -- System clock input
            p_mov_avg_filter_buff_sample_p_in     : in  std_logic;                                                  -- Sample pulse from ADC
            p_mov_avg_filter_buff_mcu_req_in      : in  std_logic;                                                  -- MCU request (start of PWM cycle)
            p_mov_avg_filter_buff_phase_a_in      : in  std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);  -- Stream input 1
            p_mov_avg_filter_buff_phase_b_in      : in  std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);  -- Stream input 2
            p_mov_avg_filter_buff_phase_c_in      : in  std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);  -- Stream input 3
            p_mov_avg_filter_buff_central_p_out   : out std_logic;                                                  -- Central sample detected pulse
            p_mov_avg_filter_buff_pci_start_p_out : out std_logic;                                                  -- start PCI transaction pulse
            p_mov_avg_filter_buff_rdy_p_out       : out std_logic;                                                  -- Output ready pulse
            p_mov_avg_filter_buff_phase_a_out     : out std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);  -- Sample output 1
            p_mov_avg_filter_buff_phase_b_out     : out std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0);  -- Sample output 2
            p_mov_avg_filter_buff_phase_c_out     : out std_logic_vector(G_MOV_AVG_FILTER_BUFF_DATA - 1 downto 0)   -- Sample output 3
        );
    end component;

    component firFilter is
        generic (
            G_FIRFILTER_DATA_W           : natural := 12; -- Data width
            G_FIRFILTER_AVG_LEN          : natural := 7   -- Number of averaged samples (must be >= 3)
        );
        port (
            p_firFilter_reset_n_in       : in  std_logic;
            p_firFilter_clk_in           : in  std_logic;
            p_firFilter_sampleClk_in     : in  std_logic;
            p_firFilter_rawData_in       : in  std_logic_vector(11 downto 0);
            p_firFilter_filteredData_out : out std_logic_vector(11 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_AVG_LEN : integer := G_MOV_AVG_FILTER_TOP_AVG_LEN;

        ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk                : std_logic;
    signal s_rst_n              : std_logic;
    signal s_sample_p           : std_logic;
    signal s_mcu_req            : std_logic;
    signal s_rdy_out            : std_logic;
    signal s_central_p          : std_logic; -- Central sample detected pulse
    signal s_start_pci_trans    : std_logic;

    signal s_current_a_filt_out : std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);
    signal s_current_b_filt_out : std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);
    signal s_current_c_filt_out : std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);

    signal s_current_a_buff_out : std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);
    signal s_current_b_buff_out : std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);
    signal s_current_c_buff_out : std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);

    signal s_phase_a_in         : std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Stream input 1
    signal s_phase_b_in         : std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Stream input 2
    signal s_phase_c_in         : std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Stream input 3

begin

        -- input signals assignments
    s_clk        <= p_mov_avg_filter_top_clk_in;
    s_rst_n      <= p_mov_avg_filter_top_rst_n_in;
    s_sample_p   <= p_mov_avg_filter_top_sample_p_in;
    s_mcu_req    <= p_mov_avg_filter_top_mcu_req_in;
    s_phase_a_in <= p_mov_avg_filter_top_phase_a_in;
    s_phase_b_in <= p_mov_avg_filter_top_phase_b_in;
    s_phase_c_in <= p_mov_avg_filter_top_phase_c_in;

        -- output signals assignments
    p_mov_avg_filter_top_rdy_p_out       <= s_rdy_out;
    p_mov_avg_filter_top_phase_a_out     <= s_current_a_buff_out;
    p_mov_avg_filter_top_phase_b_out     <= s_current_b_buff_out;
    p_mov_avg_filter_top_phase_c_out     <= s_current_c_buff_out;
    p_mov_avg_filter_top_central_p_out   <= s_central_p;
    p_mov_avg_filter_top_start_pci_p_out <= s_start_pci_trans;

    buff_control : mov_avg_filter_buff
        generic map (
            G_MOV_AVG_FILTER_BUFF_DATA            => G_MOV_AVG_FILTER_TOP_DATA,
            G_MOV_AVG_FILTER_BUFF_AVG_LEN         => C_AVG_LEN
        )
        port map (
            p_mov_avg_filter_buff_rst_n_in        => s_rst_n,
            p_mov_avg_filter_buff_clk_in          => s_clk,
            p_mov_avg_filter_buff_sample_p_in     => s_sample_p,
            p_mov_avg_filter_buff_mcu_req_in      => s_mcu_req,
            p_mov_avg_filter_buff_phase_a_in      => s_current_a_filt_out,
            p_mov_avg_filter_buff_phase_b_in      => s_current_b_filt_out,
            p_mov_avg_filter_buff_phase_c_in      => s_current_c_filt_out,
            p_mov_avg_filter_buff_central_p_out   => s_central_p,
            p_mov_avg_filter_buff_pci_start_p_out => s_start_pci_trans,
            p_mov_avg_filter_buff_rdy_p_out       => s_rdy_out,
            p_mov_avg_filter_buff_phase_a_out     => s_current_a_buff_out,
            p_mov_avg_filter_buff_phase_b_out     => s_current_b_buff_out,
            p_mov_avg_filter_buff_phase_c_out     => s_current_c_buff_out
        );

    current_a : firFilter
        generic map (
            G_FIRFILTER_DATA_W           => G_MOV_AVG_FILTER_TOP_DATA,
            G_FIRFILTER_AVG_LEN          => C_AVG_LEN
        )
        port map (
            p_firFilter_reset_n_in       => s_rst_n,
            p_firFilter_clk_in           => s_clk,
            p_firFilter_sampleClk_in     => s_sample_p,
            p_firFilter_rawData_in       => s_phase_a_in,
            p_firFilter_filteredData_out => s_current_a_filt_out
        );

   current_b : firFilter
        generic map (
            G_FIRFILTER_DATA_W           => G_MOV_AVG_FILTER_TOP_DATA,
            G_FIRFILTER_AVG_LEN          => C_AVG_LEN
        )
        port map (
            p_firFilter_reset_n_in       => s_rst_n,
            p_firFilter_clk_in           => s_clk,
            p_firFilter_sampleClk_in     => s_sample_p,
            p_firFilter_rawData_in       => s_phase_b_in,
            p_firFilter_filteredData_out => s_current_b_filt_out
        );

    current_c : firFilter
        generic map (
            G_FIRFILTER_DATA_W           => G_MOV_AVG_FILTER_TOP_DATA,
            G_FIRFILTER_AVG_LEN          => C_AVG_LEN
        )
        port map (
            p_firFilter_reset_n_in       => s_rst_n,
            p_firFilter_clk_in           => s_clk,
            p_firFilter_sampleClk_in     => s_sample_p,
            p_firFilter_rawData_in       => s_phase_c_in,
            p_firFilter_filteredData_out => s_current_c_filt_out
        );

end architecture structural;
