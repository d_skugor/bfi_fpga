----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             sw_freq_detector.vhd
-- module name      sw_freq_detector
-- author           David Pavlović (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Detects currently active switching frequency
----------------------------------------------------------------------------------------------
-- create date      29.09.2020.
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Added XPM synchronizer
--                    - File Created
--
--      Revision 0.03 by david.pavlovic
--                    - Added generic for setting time offset in frequency detection
--                    - Added more robust logic for detecting valid MCU request pulse
--
--      Revision 0.04 by david.pavlovic
--                    - Added locked signal which indicates that module detected
--                      starting frequency
--
--      Revision 0.05 by dario.drvenkar
--                    - removed xpm_cdc_single macro
--                    - (synchronization of the MCU req signal is done on the top)
--
--      Revision 0.06 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.07 by danijela.skugor
--                    - Substitute binary counter with gray code counter
--
----------------------------------------------------------------------------------------------
-- TODO: - nothing
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

entity sw_freq_detector is
    generic (
        G_OFFSET          : natural := 100
    );
    port (
        p_sfd_clk_in      : in  std_logic;
        p_sfd_rst_n_in    : in  std_logic;
        p_sfd_mcu_req_in  : in  std_logic;
        p_sfd_data_10_kHz : in  std_logic_vector(11 downto 0);
        p_sfd_data_16_kHz : in  std_logic_vector(11 downto 0);
        p_sfd_data_20_kHz : in  std_logic_vector(11 downto 0);
        p_sfd_data_out    : out std_logic_vector(11 downto 0)
    );
end entity;

architecture rtl of sw_freq_detector is


    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component gray_counter is
        generic(
            G_GRAYCNT_WIDTH             : integer := 4;
            G_GRAYCNT_CORRECTION_ON     : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port(
            p_grayCnt_clk_in    : in std_logic;
            p_grayCnt_rst_n_in  : in std_logic;  -- sync reset
            p_grayCnt_en_in     : in std_logic;  -- enable
            p_grayCnt_err_p_out : out std_logic; -- pulsed error
            p_grayCnt_res_out   : out std_logic_vector(G_GRAYCNT_WIDTH -1 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------

    -- Convert binary code to gray code
    function bin_to_gray (bin : std_logic_vector) return std_logic_vector is
        variable v_gray : std_logic_vector(bin'range);
    begin
        v_gray(bin'left) := bin(bin'left);
        for i in bin'left-1 downto 0 loop
            v_gray(i) := bin(i+1) xor bin(i);
        end loop;
        return v_gray;
    end function bin_to_gray;

    -- Convert gray code to binary
    function gray_to_bin( gray : std_logic_vector ) return std_logic_vector is
        variable v_bin : std_logic_vector(gray'range);
    begin
        v_bin(v_bin'high) := gray(gray'high);
        for i in gray'high-1 downto 0 loop
            v_bin(i) := gray(i) xor v_bin(i+1);
        end loop;
        return v_bin;
    end function;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_10_KHZ_PER  : integer := 10000;
    constant C_16_KHZ_PER  : integer :=  6250;
    constant C_20_KHZ_PER  : integer :=  5000;

    constant C_10_KHZ_MIN  : std_logic_vector(13 downto 0) := std_logic_vector(to_unsigned((C_10_KHZ_PER - integer(G_OFFSET)), 14));
    constant C_10_KHZ_MAX  : std_logic_vector(13 downto 0) := std_logic_vector(to_unsigned((C_10_KHZ_PER + integer(G_OFFSET)), 14));
    constant C_16_KHZ_MIN  : std_logic_vector(13 downto 0) := std_logic_vector(to_unsigned((C_16_KHZ_PER - integer(G_OFFSET)), 14));
    constant C_16_KHZ_MAX  : std_logic_vector(13 downto 0) := std_logic_vector(to_unsigned((C_16_KHZ_PER + integer(G_OFFSET)), 14));
    constant C_20_KHZ_MIN  : std_logic_vector(13 downto 0) := std_logic_vector(to_unsigned((C_20_KHZ_PER - integer(G_OFFSET)), 14));
    constant C_20_KHZ_MAX  : std_logic_vector(13 downto 0) := std_logic_vector(to_unsigned((C_20_KHZ_PER + integer(G_OFFSET)), 14));
    constant C_PULSE_VALID : std_logic_vector( 13 downto 0) := std_logic_vector(to_unsigned(  949 , 14));
    constant C_VALID_TEMP  : std_logic_vector( 13 downto 0) := bin_to_gray (C_PULSE_VALID);

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    type t_sw_freq is (
        f_10_kHz,
        f_16_kHz,
        f_20_kHz
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk             : std_logic;
    signal s_rst_n           : std_logic;
    signal s_mcu_req         : std_logic;
    signal s_locked          : std_logic;
    signal s_cnt             : std_logic_vector(13 downto 0);
    signal s_cnt_reg         : std_logic_vector(13 downto 0);
    signal s_cnt_pulse       : std_logic_vector(13 downto 0);
    signal s_cnt_pulse_en    : std_logic;
    signal s_cnt_pulse_clr_n : std_logic;
    signal s_cnt_clr_n       : std_logic;
    signal s_pulse_valid     : std_logic;
    signal s_pulse_valid_n   : std_logic;
    signal s_pulse_valid_d   : std_logic;
    signal s_sw_freq         : t_sw_freq;
    signal s_data_10_kHz     : std_logic_vector(11 downto 0);
    signal s_data_16_kHz     : std_logic_vector(11 downto 0);
    signal s_data_20_kHz     : std_logic_vector(11 downto 0);
    signal s_data_out        : std_logic_vector(11 downto 0);
    
begin

    -- input signal assignments
    s_clk         <= p_sfd_clk_in;
    s_rst_n       <= p_sfd_rst_n_in;
    s_data_10_kHz <= p_sfd_data_10_kHz;
    s_data_16_kHz <= p_sfd_data_16_kHz;
    s_data_20_kHz <= p_sfd_data_20_kHz;
    s_mcu_req     <= p_sfd_mcu_req_in;

    -- output signal assignment
    p_sfd_data_out <= s_data_out;


    -- Gray code pulse counter
    gray_cnt : gray_counter
        generic map(
            G_GRAYCNT_WIDTH            => 14,
            G_GRAYCNT_CORRECTION_ON    => true
        )
        port map(
            p_grayCnt_clk_in    => s_clk,
            p_grayCnt_rst_n_in  => s_cnt_pulse_clr_n,
            p_grayCnt_en_in     => s_cnt_pulse_en,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_cnt_pulse
        );

    s_cnt_pulse_clr_n <= s_rst_n and s_mcu_req ;
    s_cnt_pulse_en <= s_mcu_req when gray_to_bin(s_cnt_pulse) < C_PULSE_VALID else '0';

    s_pulse_valid <= '1' when s_cnt_pulse = bin_to_gray(C_PULSE_VALID) else '0';

    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_pulse_valid_d <= '0';
        elsif rising_edge(s_clk) then
            s_pulse_valid_d <= s_pulse_valid;
        end if;
    end process;


    -- Gray code valid pulse counter
    gray_cnt_valid_p : gray_counter
        generic map(
            G_GRAYCNT_WIDTH            => 14,
            G_GRAYCNT_CORRECTION_ON    => true
        )
        port map(
            p_grayCnt_clk_in    => s_clk,
            p_grayCnt_rst_n_in  => s_cnt_clr_n,
            p_grayCnt_en_in     => s_pulse_valid_n,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_cnt
        );

    s_cnt_clr_n     <=  s_rst_n and not(s_pulse_valid);
    s_pulse_valid_n <= not s_pulse_valid;

    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_cnt_reg <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_pulse_valid = '1') then
                s_cnt_reg <= gray_to_bin(s_cnt);
            end if;
        end if;
    end process;

    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_sw_freq <= f_10_kHz;
            s_locked  <= '0';
        elsif rising_edge(s_clk) then
            if (s_pulse_valid_d = '1') then
                if (s_locked = '0') then
                    if (s_cnt_reg >= (C_10_KHZ_MIN) and s_cnt_reg <= (C_10_KHZ_MAX)) then
                        s_sw_freq <= f_10_kHz;
                        s_locked  <= '1';
                    elsif (s_cnt_reg >= (C_16_KHZ_MIN) and s_cnt_reg <= (C_16_KHZ_MAX)) then
                        s_sw_freq <= f_16_kHz;
                        s_locked  <= '1';
                    elsif (s_cnt_reg >= (C_20_KHZ_MIN) and s_cnt_reg <= (C_20_KHZ_MAX)) then
                        s_sw_freq <= f_20_kHz;
                        s_locked  <= '1';
                    end if;
                else
                    if (s_sw_freq = f_10_kHz) then -- 10 kHz
                        if (s_cnt_reg >= (C_16_KHZ_MIN) and s_cnt_reg <= (C_16_KHZ_MAX)) then
                            s_sw_freq <= f_16_kHz;
                        end if;
                    elsif (s_sw_freq = f_16_kHz) then -- 16 kHz
                        if (s_cnt_reg >= (C_10_KHZ_MIN) and s_cnt_reg <= (C_10_KHZ_MAX)) then
                            s_sw_freq <= f_10_kHz;
                        elsif (s_cnt_reg >= (C_20_KHZ_MIN) and s_cnt_reg <= (C_20_KHZ_MAX)) then
                            s_sw_freq <= f_20_kHz;
                        end if;
                    elsif (s_sw_freq = f_20_kHz) then -- 20 kHz
                        if (s_cnt_reg >= (C_16_KHZ_MIN) and s_cnt_reg <= (C_16_KHZ_MAX)) then
                            s_sw_freq <= f_16_kHz;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_data_out <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_locked = '0') then
                s_data_out <= s_data_10_kHz;
            else
                if (s_sw_freq = f_10_kHz) then
                    s_data_out <= s_data_10_kHz;
                elsif (s_sw_freq = f_16_kHz) then
                    s_data_out <= s_data_16_kHz;
                elsif (s_sw_freq = f_20_kHz) then
                    s_data_out <= s_data_20_kHz;
                end if;
            end if;
        end if;
    end process;

end architecture rtl;