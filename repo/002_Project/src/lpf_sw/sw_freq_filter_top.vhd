----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             sw_freq_filter_top.vhd
-- module name      sw_freq_filter_top
-- author           David Pavlović (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Variable switching frequency moving average filter top file
----------------------------------------------------------------------------------------------
-- create date      29.09.2020.
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by dario.drvenkar
--                    - update of firFilter component
--
--      Revision 0.03 by david.pavlovic
--                    - Code cleanup
----------------------------------------------------------------------------------------------
-- TODO: - nothing
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity sw_freq_filter_top is

    generic (
        G_DATA                  : natural := 12;
        G_OFFSET                : natural := 100
    );
    port (
        p_top_sff_clk_in        : in  std_logic;
        p_top_sff_rst_n_in      : in  std_logic;
        p_top_sff_sample_clk_in : in  std_logic;
        p_top_sff_mcu_req_in    : in  std_logic;
        p_top_sff_data_in       : in  std_logic_vector(G_DATA-1 downto 0);
        p_top_sff_data_out      : out std_logic_vector(G_DATA-1 downto 0)
    );

end entity;

architecture structural of sw_freq_filter_top is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component sw_freq_detector is
        generic (
            G_OFFSET          : natural := 100
        );
        port (
            p_sfd_clk_in      : in  std_logic;
            p_sfd_rst_n_in    : in  std_logic;
            p_sfd_mcu_req_in  : in  std_logic;
            p_sfd_data_10_kHz : in  std_logic_vector(11 downto 0);
            p_sfd_data_16_kHz : in  std_logic_vector(11 downto 0);
            p_sfd_data_20_kHz : in  std_logic_vector(11 downto 0);
            p_sfd_data_out    : out std_logic_vector(11 downto 0)
        );
    end component;

    component firFilter
        generic (
            G_FIRFILTER_DATA_W           : natural := 12;     -- Data width
            G_FIRFILTER_AVG_LEN          : natural := 7       -- Number of averaged samples (must be >= 3)
        );
        port (
            p_firFilter_reset_n_in       : in  std_logic;
            p_firFilter_clk_in           : in  std_logic;
            p_firFilter_sampleClk_in     : in  std_logic;
            p_firFilter_rawData_in       : in  std_logic_vector(11 downto 0);
            p_firFilter_filteredData_out : out std_logic_vector(11 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk         : std_logic;
    signal s_rst_n       : std_logic;
    signal s_mcu_req     : std_logic;
    signal s_sample_clk  : std_logic;
    signal s_data_in     : std_logic_vector(G_DATA-1 downto 0);
    signal s_data_10_kHz : std_logic_vector(G_DATA-1 downto 0);
    signal s_data_16_kHz : std_logic_vector(G_DATA-1 downto 0);
    signal s_data_20_kHz : std_logic_vector(G_DATA-1 downto 0);
    signal s_data_out    : std_logic_vector(G_DATA-1 downto 0);

begin

    -- input signals assignments
    s_clk        <= p_top_sff_clk_in;
    s_rst_n      <= p_top_sff_rst_n_in;
    s_mcu_req    <= p_top_sff_mcu_req_in;
    s_sample_clk <= p_top_sff_sample_clk_in;
    s_data_in    <= p_top_sff_data_in;

    -- input signals assignments
    p_top_sff_data_out <= s_data_out;

    mdl_sw_freq_det : sw_freq_detector
        generic map (
            G_OFFSET          => G_OFFSET
        )
        port map (
            p_sfd_clk_in      => s_clk,
            p_sfd_rst_n_in    => s_rst_n,
            p_sfd_mcu_req_in  => s_mcu_req,
            p_sfd_data_10_kHz => s_data_10_kHz,
            p_sfd_data_16_kHz => s_data_16_kHz,
            p_sfd_data_20_kHz => s_data_20_kHz,
            p_sfd_data_out    => s_data_out
        );

    mdl_ma_filter_10_kHz : firFilter
        generic map (
            G_FIRFILTER_DATA_W           => 12,
            G_FIRFILTER_AVG_LEN          => 100
        )
        port map (
            p_firFilter_reset_n_in       => s_rst_n,
            p_firFilter_clk_in           => s_clk,
            p_firFilter_sampleClk_in     => s_sample_clk,
            p_firFilter_rawData_in       => s_data_in,
            p_firFilter_filteredData_out => s_data_10_kHz
        );

    mdl_ma_filter_16_kHz : firFilter
        generic map (
            G_FIRFILTER_DATA_W           => 12,
            G_FIRFILTER_AVG_LEN          => 62
        )
        port map (
            p_firFilter_reset_n_in       => s_rst_n,
            p_firFilter_clk_in           => s_clk,
            p_firFilter_sampleClk_in     => s_sample_clk,
            p_firFilter_rawData_in       => s_data_in,
            p_firFilter_filteredData_out => s_data_16_kHz
        );

    mdl_ma_filter_20_kHz : firFilter
        generic map (
            G_FIRFILTER_DATA_W           => 12,
            G_FIRFILTER_AVG_LEN          => 50
        )
        port map (
            p_firFilter_reset_n_in       => s_rst_n,
            p_firFilter_clk_in           => s_clk,
            p_firFilter_sampleClk_in     => s_sample_clk,
            p_firFilter_rawData_in       => s_data_in,
            p_firFilter_filteredData_out => s_data_20_kHz
        );

end architecture structural;