----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             gate_drivers_monitor.vhd
-- module name      gate_drivers_monitor - rtl
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Faults are stored in 5-bit vector as follows:
--                      p_gdrvMon_flt_n_in(4) <= main DCDC converter not operating      (Vin)
--                      p_gdrvMon_flt_n_in(3) <= auxiliary DCDC converter not operating (DCDC)
--                      p_gdrvMon_flt_n_in(2) <= failure in 5V buck converter           (5V)
--                      p_gdrvMon_flt_n_in(1) <= failure detected in high-side gate drivers
--                      p_gdrvMon_flt_n_in(0) <= failure detected in low-side gate drivers
----------------------------------------------------------------------------------------------
-- create date     10.10.2019
-- Revision:
--      Revision 0.01 - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Changed monitor to check Gate Driver v4.0 fault signals
--
--      Revision 0.03 by david.pavlovic
--                    - Added power supply fault signals as outputs
--
--      Revision 0.04 by david.pavlovic
--                    - Added XPM CDC macro for synchronization
--                    - Added logic for start of GDRV monitoring
--
--      Revision 0.05 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.06 by david.pavlovic
--                    - Added pulse filters on GDRV fault signals
--                      - minimum stable state length is 500 ns except for DCDC fault
--                      - where minimum stable length is 10 us
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gate_drivers_monitor is
    port (
        p_gdrvMon_clk_in        : in  std_logic;
        p_gdrvMon_rst_n_in      : in  std_logic;
        p_gdrvMon_sample_clk_in : in  std_logic;
        -- Gate Driver fault signals
        p_gdrvMon_flt_n_in      : in  std_logic_vector(4 downto 0);
        p_gdrvMon_gdrv_rdy_out  : out std_logic;
        p_gdrvMon_flt_n_out     : out std_logic_vector(6 downto 0)
    );
end entity gate_drivers_monitor;

architecture rtl of gate_drivers_monitor is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component edge_detector_p
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    component pulse_filter is
        generic (
            G_PULSE_FILTER_LENGTH   : natural := 10
        );
        port (
            -- main signals
            p_pulse_filter_clk_in   : in  std_logic;
            p_pulse_filter_rst_n_in : in  std_logic;
            p_pulse_filter_en_in    : in  std_logic;
            -- input signal
            p_pulse_filter_sig_in   : in  std_logic;
            p_pulse_filter_sig_out  : out std_logic
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_VIN_FLT_FILTER_LENGTH  : natural := 50;
    constant C_DCDC_FLT_FILTER_LENGTH : natural := 10;
    constant C_5V_FLT_FILTER_LENGTH   : natural := 50;
    constant C_HS_FLT_FILTER_LENGTH   : natural := 50;
    constant C_LS_FLT_FILTER_LENGTH   : natural := 50;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk           : std_logic;
    signal s_rst_n         : std_logic;
    signal s_sample_clk    : std_logic;
    signal s_sample_clk_re : std_logic;
    signal s_ready         : std_logic;
    signal s_ready_d       : std_logic;
    signal s_flt_n         : std_logic_vector(6 downto 0);
    signal s_flt_vin_n     : std_logic;
    signal s_flt_dcdc_n    : std_logic;
    signal s_flt_5v_n      : std_logic;
    signal s_flt_hs_n      : std_logic;
    signal s_flt_ls_n      : std_logic;

begin

    -- input signal assignments
    s_clk        <= p_gdrvMon_clk_in;
    s_rst_n      <= p_gdrvMon_rst_n_in;
    s_sample_clk <= p_gdrvMon_sample_clk_in;

    -- output signal assignments
    p_gdrvMon_flt_n_out    <= s_flt_n;
    p_gdrvMon_gdrv_rdy_out <= s_ready_d;

    -- rising edge detector on s_sample_clk
    mdl_ed_pwm : edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_sample_clk,
            p_edp_sig_re_out => s_sample_clk_re,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
    );

    -- pulse filter on GDRV faults
    mdl_pulse_filter_vin_n : pulse_filter
        generic map (
            G_PULSE_FILTER_LENGTH   => C_VIN_FLT_FILTER_LENGTH
        )
        port map (
            -- main signals
            p_pulse_filter_clk_in   => s_clk,
            p_pulse_filter_rst_n_in => s_rst_n,
            p_pulse_filter_en_in    => '1',
            -- input signal
            p_pulse_filter_sig_in   => p_gdrvMon_flt_n_in(4),
            p_pulse_filter_sig_out  => s_flt_vin_n
        );

    mdl_pulse_filter_dcdc_n : pulse_filter
        generic map (
            G_PULSE_FILTER_LENGTH   => C_DCDC_FLT_FILTER_LENGTH
        )
        port map (
            -- main signals
            p_pulse_filter_clk_in   => s_clk,
            p_pulse_filter_rst_n_in => s_rst_n,
            p_pulse_filter_en_in    => s_sample_clk_re,
            -- input signal
            p_pulse_filter_sig_in   => p_gdrvMon_flt_n_in(3),
            p_pulse_filter_sig_out  => s_flt_dcdc_n
        );

    mdl_pulse_filter_5v_n : pulse_filter
        generic map (
            G_PULSE_FILTER_LENGTH   => C_5V_FLT_FILTER_LENGTH
        )
        port map (
            -- main signals
            p_pulse_filter_clk_in   => s_clk,
            p_pulse_filter_rst_n_in => s_rst_n,
            p_pulse_filter_en_in    => '1',
            -- input signal
            p_pulse_filter_sig_in   => p_gdrvMon_flt_n_in(2),
            p_pulse_filter_sig_out  => s_flt_5v_n
        );

    mdl_pulse_filter_hs_n : pulse_filter
        generic map (
            G_PULSE_FILTER_LENGTH   => C_HS_FLT_FILTER_LENGTH
        )
        port map (
            -- main signals
            p_pulse_filter_clk_in   => s_clk,
            p_pulse_filter_rst_n_in => s_rst_n,
            p_pulse_filter_en_in    => '1',
            -- input signal
            p_pulse_filter_sig_in   => p_gdrvMon_flt_n_in(1),
            p_pulse_filter_sig_out  => s_flt_hs_n
        );

    mdl_pulse_filter_ls_n : pulse_filter
        generic map (
            G_PULSE_FILTER_LENGTH   => C_LS_FLT_FILTER_LENGTH
        )
        port map (
            -- main signals
            p_pulse_filter_clk_in   => s_clk,
            p_pulse_filter_rst_n_in => s_rst_n,
            p_pulse_filter_en_in    => '1',
            -- input signal
            p_pulse_filter_sig_in   => p_gdrvMon_flt_n_in(0),
            p_pulse_filter_sig_out  => s_flt_ls_n
        );

    -- generates ready signal that starts the monitoring
    process (s_clk, s_rst_n) begin
        if (s_rst_n = '0') then
            s_ready   <= '0';
            s_ready_d <= '0';
        elsif rising_edge(s_clk) then
            if (s_flt_vin_n = '1' and s_flt_dcdc_n = '1' and s_flt_5v_n = '1' and s_flt_hs_n = '1' and s_flt_ls_n = '1') then
                s_ready <= '1';
            end if;
            s_ready_d <= s_ready;
        end if;
    end process;

    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_flt_n <= (others => '1');
        elsif rising_edge(s_clk) then
            s_flt_n(6) <= s_flt_vin_n and s_flt_dcdc_n and s_flt_5v_n and s_flt_hs_n and s_flt_ls_n;
            s_flt_n(5) <= s_flt_vin_n and s_flt_dcdc_n and s_flt_5v_n;
            s_flt_n(4) <= s_flt_vin_n;
            s_flt_n(3) <= s_flt_dcdc_n;
            s_flt_n(2) <= s_flt_5v_n;
            s_flt_n(1) <= s_flt_hs_n;
            s_flt_n(0) <= s_flt_ls_n;
        end if;
    end process;

end architecture rtl;
