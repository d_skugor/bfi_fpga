----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             uart_tx.vhd
-- module name      uart_tx
-- author           david.pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            UART TX module
----------------------------------------------------------------------------------------------
-- create date      08.02.2021
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart_tx is

    generic (
        G_BAUD_RATE                : integer := 9600
    );
    port (
        -- main signals
        p_uart_tx_clk_in           : in  std_logic;
        p_uart_tx_rst_n_in         : in  std_logic;
        -- TX signals
        p_uart_tx_tx_out           : out std_logic;
        p_uart_tx_data_tx_in       : in  std_logic_vector(7 downto 0);
        p_uart_tx_data_tx_rdy_in   : in  std_logic;
        p_uart_tx_busy_out         : out std_logic
    );

end entity uart_tx;

architecture rtl of uart_tx is

    -- constants
    constant C_CNT_W      : integer := 14;
    constant C_CLK_FREQ   : integer := 100_000_000;
    constant C_BIT_LENGTH : unsigned(13 downto 0) := to_unsigned(integer(real(C_CLK_FREQ)/real(G_BAUD_RATE))-1, C_CNT_W);

    -- types
    type t_state is (
        t_state_idle,
        t_state_start,
        t_state_data,
        t_state_stop
    );

    -- internal signals
    signal s_clk          : std_logic;
    signal s_rst_n        : std_logic;
    signal s_tx           : std_logic;
    signal s_data_tx_in   : std_logic_vector(7 downto 0);
    signal s_data_tx      : std_logic_vector(7 downto 0);
    signal s_data_tx_rdy  : std_logic;
    signal s_tx_busy      : std_logic;
    signal s_state        : t_state;
    signal s_cnt          : unsigned(C_CNT_W-1 downto 0);
    signal s_cnt_bit      : unsigned(2 downto 0);

begin

    -- input signal assignments
    s_clk         <= p_uart_tx_clk_in;
    s_rst_n       <= p_uart_tx_rst_n_in;
    s_data_tx_in  <= p_uart_tx_data_tx_in;
    s_data_tx_rdy <= p_uart_tx_data_tx_rdy_in;

    -- output signal assignments
    p_uart_tx_tx_out   <= s_tx;
    p_uart_tx_busy_out <= s_tx_busy;

    -- TX FSM
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_state      <= t_state_idle;
            s_tx         <= '1';
            s_data_tx    <= (others => '0');
            s_tx_busy    <= '0';
            s_cnt        <= (others => '0');
            s_cnt_bit    <= (others => '0');
        elsif rising_edge(s_clk) then
            s_tx_busy <= '1';
            case (s_state) is
                -------------------------------------------------------------
                when t_state_idle =>
                    s_tx      <= '1';
                    s_tx_busy <= '0';
                    if (s_data_tx_rdy = '1') then
                        s_state   <= t_state_start;
                        s_tx      <= '0';
                        s_data_tx <= s_data_tx_in;
                        s_tx_busy <= '1';
                    end if;
                -------------------------------------------------------------
                when t_state_start =>
                    s_tx <= '0';
                    if (s_cnt = C_BIT_LENGTH) then
                        s_state <= t_state_data;
                        s_tx    <= s_data_tx(to_integer(s_cnt_bit));
                        s_cnt   <= (others => '0');
                    else
                        s_cnt <= s_cnt + 1;
                    end if;
                -------------------------------------------------------------
                when t_state_data =>
                    s_tx <= s_data_tx(to_integer(s_cnt_bit));
                    if (s_cnt = C_BIT_LENGTH and s_cnt_bit = 7) then
                        s_state   <= t_state_stop;
                        s_tx      <= '1';
                        s_cnt     <= (others => '0');
                        s_cnt_bit <= (others => '0');
                    elsif (s_cnt = C_BIT_LENGTH) then
                        s_cnt     <= (others => '0');
                        s_cnt_bit <= s_cnt_bit + 1;
                    else
                        s_cnt     <= s_cnt + 1;
                    end if;
                -------------------------------------------------------------
                when t_state_stop =>
                    if (s_cnt = C_BIT_LENGTH) then
                        s_state   <= t_state_idle;
                        s_tx_busy <= '0';
                        s_cnt     <= (others => '0');
                        s_cnt_bit <= (others => '0');
                    else
                        s_cnt     <= s_cnt + 1;
                    end if;
                -------------------------------------------------------------
                when others => null;
                -------------------------------------------------------------
            end case;
        end if;
    end process;

end architecture rtl;