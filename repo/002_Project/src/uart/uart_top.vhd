----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             uart_top.vhd
-- module name      uarT_top
-- author           david.pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            UART Top Level Module
----------------------------------------------------------------------------------------------
-- create date      08.02.2021
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity uart_top is

    generic (
        G_BAUD_RATE                 : integer := 9600
    );
    port (
        -- main signals
        p_uart_top_clk_in           : in  std_logic;
        p_uart_top_rst_n_in         : in  std_logic;
        -- TX signals
        p_uart_top_tx_out           : out std_logic;
        p_uart_top_data_tx_in       : in  std_logic_vector(7 downto 0);
        p_uart_top_data_tx_rdy_in   : in  std_logic;
        p_uart_top_tx_busy_out      : out std_logic;
        -- RX signals
        p_uart_top_rx_in            : in  std_logic;
        p_uart_top_data_rx_out      : out std_logic_vector(7 downto 0);
        p_uart_top_data_rx_rdy_out  : out std_logic;
        p_uart_top_rx_busy_out      : out std_logic
    );

end entity uart_top;

architecture rtl of uart_top is

    -- components
    component uart_tx is
        generic (
            G_BAUD_RATE                : integer := 9600
        );
        port (
            -- main signals
            p_uart_tx_clk_in           : in  std_logic;
            p_uart_tx_rst_n_in         : in  std_logic;
            -- TX signals
            p_uart_tx_tx_out           : out std_logic;
            p_uart_tx_data_tx_in       : in  std_logic_vector(7 downto 0);
            p_uart_tx_data_tx_rdy_in   : in  std_logic;
            p_uart_tx_busy_out         : out std_logic
        );
    end component;

    component uart_rx is
        generic (
            G_BAUD_RATE                : integer := 9600
        );
        port (
            -- main signals
            p_uart_rx_clk_in           : in  std_logic;
            p_uart_rx_rst_n_in         : in  std_logic;
            -- RX signals
            p_uart_top_rx_in           : in  std_logic;
            p_uart_top_data_rx_out     : out std_logic_vector(7 downto 0);
            p_uart_top_data_rx_rdy_out : out std_logic;
            p_uart_top_rx_busy_out     : out std_logic
        );
    end component;

    -- internal signals
    signal s_clk         : std_logic;
    signal s_rst_n       : std_logic;
    signal s_tx          : std_logic;
    signal s_data_tx     : std_logic_vector(7 downto 0);
    signal s_data_tx_rdy : std_logic;
    signal s_tx_busy     : std_logic;
    signal s_rx          : std_logic;
    signal s_data_rx     : std_logic_vector(7 downto 0);
    signal s_data_rx_rdy : std_logic;
    signal s_rx_busy     : std_logic;

begin

    -- input signal assignments
    s_clk         <= p_uart_top_clk_in;
    s_rst_n       <= p_uart_top_rst_n_in;
    s_data_tx     <= p_uart_top_data_tx_in;
    s_data_tx_rdy <= p_uart_top_data_tx_rdy_in;
    s_rx          <= p_uart_top_rx_in;

    -- output signal assignments
    p_uart_top_tx_out          <= s_tx;
    p_uart_top_tx_busy_out     <= s_tx_busy;
    p_uart_top_data_rx_out     <= s_data_rx;
    p_uart_top_data_rx_rdy_out <= s_data_rx_rdy;
    p_uart_top_rx_busy_out     <= s_rx_busy;

    -- UART TX instance
    mdl_uart_tx : uart_tx
        generic map (
            G_BAUD_RATE              => G_BAUD_RATE
        )
        port map (
            -- main signals
            p_uart_tx_clk_in         => s_clk,
            p_uart_tx_rst_n_in       => s_rst_n,
            -- TX signals
            p_uart_tx_tx_out         => s_tx,
            p_uart_tx_data_tx_in     => s_data_tx,
            p_uart_tx_data_tx_rdy_in => s_data_tx_rdy,
            p_uart_tx_busy_out       => s_tx_busy
        );

    mdl_uart_rx : uart_rx
        generic map (
            G_BAUD_RATE                => G_BAUD_RATE
        )
        port map (
            -- main signals
            p_uart_rx_clk_in           => s_clk,
            p_uart_rx_rst_n_in         => s_rst_n,
            -- RX signals
            p_uart_top_rx_in           => s_rx,
            p_uart_top_data_rx_out     => s_data_rx,
            p_uart_top_data_rx_rdy_out => s_data_rx_rdy,
            p_uart_top_rx_busy_out     => s_rx_busy
        );

end architecture rtl;