----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             uart_rx.vhd
-- module name      uart_rx
-- author           david.pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            UART RX module
----------------------------------------------------------------------------------------------
-- create date      09.02.2021
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

entity uart_rx is

    generic (
        G_BAUD_RATE                : integer := 9600
    );
    port (
        -- main signals
        p_uart_rx_clk_in           : in  std_logic;
        p_uart_rx_rst_n_in         : in  std_logic;
        -- RX signals
        p_uart_top_rx_in           : in  std_logic;
        p_uart_top_data_rx_out     : out std_logic_vector(7 downto 0);
        p_uart_top_data_rx_rdy_out : out std_logic;
        p_uart_top_rx_busy_out     : out std_logic
    );

end entity uart_rx;

architecture rtl of uart_rx is

    -- constants
    constant C_CNT_W           : integer := 14;
    constant C_CLK_FREQ        : integer := 100_000_000;
    constant C_BIT_LENGTH      : unsigned(13 downto 0) := to_unsigned(integer(real(C_CLK_FREQ)/real(G_BAUD_RATE))-1, C_CNT_W);
    constant C_BIT_LENGTH_HALF : unsigned(13 downto 0) := to_unsigned(integer(real(C_CLK_FREQ)/real(G_BAUD_RATE))/2-1, C_CNT_W);

    -- types
    type t_state is (
        t_state_idle,
        t_state_start,
        t_state_data,
        t_state_stop
    );

    -- internal signals
    signal s_clk          : std_logic;
    signal s_rst_n        : std_logic;
    signal s_rx           : std_logic;
    signal s_data_rx      : std_logic_vector(7 downto 0);
    signal s_data_rx_reg  : std_logic_vector(7 downto 0);
    signal s_data_rx_rdy  : std_logic;
    signal s_rx_busy      : std_logic;
    signal s_state        : t_state;
    signal s_cnt          : unsigned(C_CNT_W-1 downto 0);
    signal s_cnt_bit      : unsigned(2 downto 0);

begin

    -- input signal assignments
    s_clk   <= p_uart_rx_clk_in;
    s_rst_n <= p_uart_rx_rst_n_in;

    -- input signal assignments
    p_uart_top_data_rx_out     <= s_data_rx;
    p_uart_top_data_rx_rdy_out <= s_data_rx_rdy;
    p_uart_top_rx_busy_out     <= s_rx_busy;

    -- RX input signal synchronizer
    xpm_cdc_single_inst : xpm_cdc_single
    generic map (
       DEST_SYNC_FF   => 2,              -- DECIMAL; range: 2-10
       INIT_SYNC_FF   => 0,              -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
       SIM_ASSERT_CHK => 1,              -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
       SRC_INPUT_REG  => 0               -- DECIMAL; 0=do not register input, 1=register input
    )
    port map (
       dest_out       => s_rx,            -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
       dest_clk       => s_clk,           -- 1-bit input: Clock signal for the destination clock domain.
       src_clk        => '0',             -- 1-bit input: optional; required when SRC_INPUT_REG = 1
       src_in         => p_uart_top_rx_in -- 1-bit input: Input signal to be synchronized to dest_clk domain.
    );

    -- RX FSM
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_state       <= t_state_idle;
            s_data_rx     <= (others => '0');
            s_data_rx_reg <= (others => '0');
            s_data_rx_rdy <= '0';
            s_rx_busy     <= '0';
            s_cnt         <= (others => '0');
            s_cnt_bit     <= (others => '0');
        elsif rising_edge(s_clk) then
            -- default value
            s_data_rx_rdy <= '0';
            s_rx_busy     <= '1';
            case (s_state) is
                -------------------------------------------------------------
                when t_state_idle =>
                    s_rx_busy <= '0';
                    if (s_rx = '0') then
                        s_state   <= t_state_start;
                        s_rx_busy <= '1';
                    end if;
                -------------------------------------------------------------
                when t_state_start =>
                    if (s_cnt = C_BIT_LENGTH_HALF) then
                        s_cnt <= (others => '0');
                        if (s_rx = '0') then
                            s_state   <= t_state_data;
                        else -- start bit is not detected
                            s_state   <= t_state_idle;
                            s_rx_busy <= '0';
                        end if;
                    else
                        s_cnt <= s_cnt + 1;
                    end if;
                -------------------------------------------------------------
                when t_state_data =>
                    if (s_cnt = C_BIT_LENGTH and s_cnt_bit = 7) then
                        s_state   <= t_state_stop;
                        s_data_rx_reg(to_integer(s_cnt_bit)) <= s_rx;
                        s_cnt     <= (others => '0');
                        s_cnt_bit <= (others => '0');
                    elsif (s_cnt = C_BIT_LENGTH) then
                        s_data_rx_reg(to_integer(s_cnt_bit)) <= s_rx;
                        s_cnt     <= (others => '0');
                        s_cnt_bit <= s_cnt_bit + 1;
                    else
                        s_cnt     <= s_cnt + 1;
                    end if;
                -------------------------------------------------------------
                when t_state_stop =>
                    if (s_cnt = C_BIT_LENGTH) then
                        s_cnt <= (others => '0');
                        if (s_rx = '1') then
                            s_state       <= t_state_idle;
                            s_data_rx     <= s_data_rx_reg;
                            s_data_rx_rdy <= '1';
                            s_rx_busy     <= '0';
                        else -- stop bit is not detected
                            s_state       <= t_state_idle;
                            s_rx_busy     <= '0';
                        end if;
                    else
                        s_cnt <= s_cnt + 1;
                    end if;
                -------------------------------------------------------------
                when others => null;
                -------------------------------------------------------------
            end case;
        end if;
    end process;

end architecture rtl;