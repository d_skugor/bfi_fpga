----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             hvADC.vhd
-- module name      hvADC - Behavioral
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Interface for AD7476 high voltage measurement chip.
--                  It drives generated sclk of 20 Mhz and cs# to chip, and reads data from miso
--                  input.
--                  16 bits of data needs to be received, and after each conversion,
--                  it's required to have quiet time for minimum 50 ns.
----------------------------------------------------------------------------------------------
-- create date     01.11.2019
-- Revision:
--      Revision 0.01 by danijela.skugor
--                    - File Created -
--
--      Revision 0.02 by danijela.skugro
--                    -- Fixed measuring half of the present voltage
--
--      Revision 0.03 by dario.drvenkar
--                    -- Output data is synchronized with xpm_cdc_gray macro
--
--      Revision 0.04 by danijela.skugor
--                    - Changes in accordance with changed gray_counter component:
--                          -- reset changed from active-high to active-low
--                          -- update of port and generic names of gray_counter
--
--      Revision 0.05 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.6 by danijela.skugor
--                    - Removed sample clock signal and setting data on it
--                    - all process are now set on system clock
--                    - locked signal from pll synced to system clock
--
--      Revision 0.7 by dario.drvenkar
--                    - Output data is synchronized using flag from FSM

----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library unisim;
use unisim.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

entity hvADC is
    port (
        -- system clock
        p_hvADC_clk_in       : in  std_logic;
        -- reset
        p_hvADC_rst_n_in     : in  std_logic;
        -- SPI MISO signals
        p_hvADC_meas_miso    : in  std_logic;
        p_hvADC_meas_sck     : out std_logic;
        p_hvADC_meas_cs_n    : out std_logic;
        -- output data
        p_hvADC_data_out     : out std_logic_vector(11 downto 0)
    );
end entity hvADC;

architecture rtl of hvADC is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component gray_counter is
        generic (
            G_GRAYCNT_WIDTH         : integer := 4;
            G_GRAYCNT_CORRECTION_ON : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port (
            p_grayCnt_clk_in        : in std_logic;
            p_grayCnt_rst_n_in      : in std_logic;     -- sync reset
            p_grayCnt_en_in         : in std_logic;     -- enable
            p_grayCnt_err_p_out     : out std_logic;    -- pulsed error
            p_grayCnt_res_out       : out std_logic_vector(G_GRAYCNT_WIDTH-1 downto 0)
        );
    end component;
    
    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    -- state machine type definition
    type t_spiStateMachine is (
        t_spiStateMachine_normal,
        t_spiStateMachine_dummyOp,
        t_spiStateMachine_quiet_1st_cycle,
        t_spiStateMachine_quiet_2nd_cycle,
        t_spiStateMachine_powerDown
     );

    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------

    function gray_to_bin ( gray : std_logic_vector ) return std_logic_vector is
        variable v_temp : std_logic_vector(gray'range);
    begin
        v_temp(gray'left) := gray(gray'left);
        for i in gray'left - 1 downto 0 loop
           v_temp(i) := gray(i) xor v_temp(i+1);
        end loop;
        return v_temp;
    end gray_to_bin;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_spiState         : t_spiStateMachine;

    signal s_data_parallel    : std_logic_vector(11 downto 0);
    signal s_data_out         : std_logic_vector(11 downto 0);
    signal s_data_sampled     : std_logic_vector(11 downto 0);

    signal s_spi_cs_n         : std_logic;

    signal s_data_cnt         : std_logic_vector(3 downto 0); -- 16 bits of data output fits in 4 bits when count
    signal s_data_cnt_en      : std_logic;
    signal s_data_cnt_clr_n   : std_logic;
    signal s_cnt_err          : std_logic;

    signal s_serial_clk       : std_logic;
    signal s_pllmclk_fb       : std_logic;                    -- MCLK PLL feedback
    signal s_pllmclk_locked_i : std_logic;
    signal s_locked_sync      : std_logic;
    signal s_start_meas       : std_logic;

    signal s_data_stable      : std_logic;
    
    -- reset active high
    signal s_rst              : std_logic;

begin

    s_rst    <= not p_hvADC_rst_n_in;

    PLLE2_BASE_inst : PLLE2_BASE
    generic map (
        BANDWIDTH          => "OPTIMIZED", -- OPTIMIZED, HIGH, LOW
        CLKFBOUT_MULT      => 8,           -- Multiply value for all CLKOUT, (2-64)
        CLKFBOUT_PHASE     => 0.0,         -- Phase offset in degrees of CLKFB, (-360.000-360.000).
        CLKIN1_PERIOD      => 10.0,        -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
        -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
        CLKOUT0_DIVIDE     => 40,
        CLKOUT1_DIVIDE     => 9,
        CLKOUT2_DIVIDE     => 9,
        CLKOUT3_DIVIDE     => 9,
        CLKOUT4_DIVIDE     => 9,
        CLKOUT5_DIVIDE     => 9,
        -- CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
        CLKOUT0_DUTY_CYCLE => 0.5,
        CLKOUT1_DUTY_CYCLE => 0.5,
        CLKOUT2_DUTY_CYCLE => 0.5,
        CLKOUT3_DUTY_CYCLE => 0.5,
        CLKOUT4_DUTY_CYCLE => 0.5,
        CLKOUT5_DUTY_CYCLE => 0.5,
        -- CLKOUT0_PHASE - CLKOUT5_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
        CLKOUT0_PHASE      => 0.0,
        CLKOUT1_PHASE      => 0.0,
        CLKOUT2_PHASE      => 0.0,
        CLKOUT3_PHASE      => 0.0,
        CLKOUT4_PHASE      => 0.0,
        CLKOUT5_PHASE      => 0.0,
        DIVCLK_DIVIDE      => 1,           -- Master division value, (1-56)
        REF_JITTER1        => 0.0,         -- Reference input jitter in UI, (0.000-0.999).
        STARTUP_WAIT       => "FALSE"      -- Delay DONE until PLL Locks, ("TRUE"/"FALSE")
    )
    port map (
        -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
        CLKOUT0 => s_serial_clk,           -- 1-bit output: CLKOUT0
        CLKOUT1 => open,                   -- 1-bit output: CLKOUT1
        CLKOUT2 => open,                   -- 1-bit output: CLKOUT2
        CLKOUT3 => open,                   -- 1-bit output: CLKOUT3
        CLKOUT4 => open,                   -- 1-bit output: CLKOUT4
        CLKOUT5 => open,                   -- 1-bit output: CLKOUT5
        -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
        CLKFBOUT => s_pllmclk_fb,          -- 1-bit output: Feedback clock
        LOCKED   => s_pllmclk_locked_i,    -- 1-bit output: LOCK
        CLKIN1   => p_hvADC_clk_in,        -- 1-bit input: Input clock
        -- Control Ports: 1-bit (each) input: PLL control ports
        PWRDWN   => '0',                   -- 1-bit input: Power-down
        RST      =>  s_rst,              -- 1-bit input: Reset -- TODO: ADD SLR 16
        -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
        CLKFBIN  => s_pllmclk_fb           -- 1-bit input: Feedback clock
    );
 
    --synchronize locked signal
  xpm_cdc_lockedSig_inst : xpm_cdc_single
   generic map (
      DEST_SYNC_FF   => 2,   -- DECIMAL; range: 2-10
      INIT_SYNC_FF   => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
      SIM_ASSERT_CHK => 0,   -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      SRC_INPUT_REG  => 0    -- DECIMAL; 0=do not register input, 1=register input
   )
   port map (
      dest_out => s_locked_sync,       -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
      dest_clk => p_hvADC_clk_in,      -- 1-bit input: Clock signal for the destination clock domain.
      src_clk  => '0',                 -- 1-bit input: optional; required when SRC_INPUT_REG = 1
      src_in   => s_pllmclk_locked_i   -- 1-bit input: Input signal to be synchronized to dest_clk domain.
   );
    
    -- start spi conversion after sclk is ready
    process(p_hvADC_clk_in, p_hvADC_rst_n_in)
    begin
        if (p_hvADC_rst_n_in = '0') then
            s_start_meas <= '0';
        elsif rising_edge(p_hvADC_clk_in) then
            if (s_locked_sync = '1') then
                s_start_meas <= '1';
            end if;
        end if;
    end process;
   

    -- count 16 bits of data
    miso_cnt_block : gray_counter
        generic map(
           G_GRAYCNT_WIDTH         => 4,
           G_GRAYCNT_CORRECTION_ON => true
        )
        port map(
            p_grayCnt_clk_in    => s_serial_clk,
            p_grayCnt_rst_n_in  => s_data_cnt_clr_n,
            p_grayCnt_en_in     => s_data_cnt_en,
            p_grayCnt_err_p_out => s_cnt_err,
            p_grayCnt_res_out   => s_data_cnt
        );

    -- state machine for SPI MISO-only data transmission
    process (s_serial_clk, p_hvADC_rst_n_in)
    begin
        if (p_hvADC_rst_n_in = '0') then
            s_spiState      <= t_spiStateMachine_powerDown; -- go in powerDown
            s_spi_cs_n      <= '1';
            s_data_cnt_en   <= '0';
            s_data_cnt_clr_n  <= '0';
            s_data_parallel <= (others => '0');
            s_data_out      <= (others => '0');
            s_data_stable   <= '0';
        else
            if rising_edge(s_serial_clk) then
               s_data_stable <= '0';
               case s_spiState is

                when t_spiStateMachine_powerDown =>
                    if (s_start_meas = '1') then                -- if sclk from pll is ready
                        s_spi_cs_n <= '0';                      -- start data transition
                        s_data_cnt_en <= '1';                   -- enable counter for conting data
                        s_spiState <= t_spiStateMachine_dummyOp;
                    else                                        -- if sclk is not ready
                        s_spi_cs_n <= '1';                      -- don't start data transition
                        s_data_cnt_en <= '0';
                    end if;

                -- After power down, one dummy conversion needs to be perform in order to get real results
                when t_spiStateMachine_dummyOp =>
                    if (gray_to_bin(s_data_cnt) = "1111") then  -- if 16 data bits are received
                        s_spi_cs_n <= '1';                      -- stop data transition
                        s_data_cnt_clr_n <= '0';                  -- clear counter
                        s_data_cnt_en <= '0';
                        s_spiState <= t_spiStateMachine_quiet_1st_cycle;
                    else                                        -- if 16 data bits are not received
                        s_spi_cs_n <= '0';                      -- hold cs active
                        s_data_cnt_en <= '1';
                        s_data_cnt_clr_n <= '1';
                    end if;

                --After every data conversion, hold cs in high state for 2 clock cycle ~ 2*55 ns
                when t_spiStateMachine_quiet_1st_cycle =>
                    s_data_out      <= s_data_parallel;              -- sample received data
                    s_spi_cs_n      <= '1';                          -- hold chip selet inactive
                    s_spiState      <= t_spiStateMachine_quiet_2nd_cycle;

                -- After 2 clock cycle where chip select is inactive, set it in active state
                when t_spiStateMachine_quiet_2nd_cycle =>
                    s_spi_cs_n <= '0';                                                     -- start data transition
                    s_data_parallel <=  s_data_parallel(10 downto 0 ) & p_hvADC_meas_miso; --  catch first zero clocked by cs
                    s_spiState <= t_spiStateMachine_normal;
                    s_data_cnt_clr_n <= '1';
                    s_data_cnt_en <= '1';
                    s_data_stable <= '1';

                --Start Normaln state - constant reading of data
                when t_spiStateMachine_normal =>
                    s_data_parallel <=  s_data_parallel(10 downto 0 ) & p_hvADC_meas_miso; -- shift received data
                    if (gray_to_bin(s_data_cnt) = "1111") then   -- if 16 data bits are received
                       s_spi_cs_n <= '1';                        -- stop data transition
                       s_data_cnt_clr_n <= '0';
                       s_data_cnt_en <= '0';
                       s_spiState <= t_spiStateMachine_quiet_1st_cycle;
                    else
                       s_spi_cs_n <= '0';
                       s_data_cnt_en <= '1';
                       s_data_cnt_clr_n <= '1';
                    end if;
                end case;
             end if;
        end if;
    end process;


    -- CDC from 20MHz to 100MHz
    -- CDC output register
    process(p_hvADC_clk_in, p_hvADC_rst_n_in)
    begin
        if (p_hvADC_rst_n_in = '0') then
            s_data_sampled <= (others => '0');
        elsif rising_edge(p_hvADC_clk_in) then
            if (s_data_stable = '1') then
                s_data_sampled <= s_data_out;
            else
                s_data_sampled <= s_data_sampled;
            end if;
        end if;
    end process;
    
    -- drive outputs to ad7476
    p_hvADC_meas_sck   <= s_serial_clk;
    p_hvADC_meas_cs_n  <= s_spi_cs_n;

    -- drive ad7476 outputs to mcu
    p_hvADC_data_out   <= s_data_sampled;

end architecture rtl;