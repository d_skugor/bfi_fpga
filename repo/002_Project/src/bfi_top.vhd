----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             bfi_top.vhd
-- module name      BFI Top
-- author           Arturo Montufar Arreola (arturo.arreola@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Top Level Module
----------------------------------------------------------------------------------------------
-- create date      03.07.2019
-- Revision:
--      Revision 0.01 by arturo.arreola
--                      File Created
--                    - Heartbeat block connected to clock
--                    - PCI inputs from DAQs are hardcoded constants
--
--      Revision 0.02 by arturo.arreola
--                    - CRC support
--                    - PCI update
--
--      Revision 0.03 by pablo.velasco
--                    - Included sampleclkdiv
--
--      Revision 0.04 by danijela.skugor
--                    - added gate drivers monitor
--
--      Revision 0.05 by arturo.arreola
--                    - PCI refactoring changes
--                    - Removed CRC
--                    - Renamed signals
--
--      Revision 0.06 by arturo.arreola
--                    - Added resolver interface
--
--      Revision 0.07 by danijela.skugor
--                   - Added PWM Monitor
--
--      Revision 0.08 by danijela.skugor
--                   - Added High Voltage measurement
--
--      Revision 0.09 by marko.gulin
--                   - Added Currents ADC
--
--      Revision 0.10 by arturo.arreola
--                   - Disabled gate drivers and PWM monitors to test in SEFI power stage
--
--      Revision 0.11 by arturo.arreola
--                   - Included generic FIR filter
--
--      Revision 0.12 by arturo.arreola
--                   - Enabled gate drivers monitor
--
--      Revision 0.13 by arturo.arreola
--                   - added multiplex for RDY and FLT signals
--
--      Revision 0.14 by david.pavlovic
--                   - Added Intra-Inverter Error Detection module
--
--      Revision 0.15 by david.pavlovic
--                   - Added updated FIR filter
--                   - Added PWM synchronization module
--
--      Revision 0.16 by david.pavlovic
--                   - Added simple moving average filter
--                   - Removed PWM synchronization module
--                   - Integrated new ADC module designed by Jorge Sanches
--                   - Jorge's modifications:
--                      - Added missing ports into the ADC driver component declaration and instance
--                      - Added initialization values for all the declared signals
--                      - Added a reset synchronizer component, and modified reset routing
--
--      Revision 0.17 by david.pavlovic
--                   - Added ADC top module
--
--      Revision 0.18 by david.pavlovic
--                   - Added 24-bit speed calculator
--                   - Added updated PCI module
--                   - Removed FIR filter for DC current measurement
--
--      Revision 0.19 by david.pavlovic
--                   - Updated PWM monitor
--                   - Updated Gate Driver monitor
--                   - Added Diagnostics module
--
--      Revision 0.20.1 by jsanchez
--                  - Removed ADC driver from this module
--                  - Added a current-measurement-block module. This block includes the current ADC driver, the
--                    clock generator, the filters and the synchronizers.
--
--      Revision 0.21 by david.pavlovic
--                   - Updated Gate Driver Monitor component
--                   - Added power supply fault signals as outputs
--
--      Revision 0.22 by david.pavlovic
--                   - Updated PWM Monitor component with new deadtime measurement generics
--
--      Revision 0.23 by dario.drvenkar
--                   - Added output buffer in Resolver
--                   - FIR filter for hvADC is in a new block and output has buffer register
--
--      Revision 0.24 by dario.drvenkar
--                   - Median filters for current data are substituted by moving average
--                     filter with an odd number of samples
--
--      Revision 0.25 by david.pavlovic
--                   - Added SEM IP module and watchdog
--
--      Revision 0.26 by david.pavlovic
--                   - Update diagnostic module ports and signals
--                   - Update PWM monitor module ports and signals
--                   - Increased PWM feedback timeout to 20us
--                   - Update GDRV monitor module ports and signals
--                   - Removed SEM IP module and watchdog because of voltage level issue
--
--      Revision 0.27 by david.pavlovic
--                   - Updated speed estimation module with generic for selecting filter
--                     cutoff frequency
--
--      Revision 0.28 by dario.drvenkar
--                   - Updated speed_estimator module
--                   - resolverTop module replaced with new resolver_driver
--                   - Removed generic for selecting speed estimator's filter cutoff frequency
--
--      Revision 0.29 by dario.drvenkar
--                   - Added signals for ADC Interface
--
--      Revision 0.30 by dario.drvenkar
--                   - Added Delay Line for Resolver data
--                   - Added new signals between LPF Buffer and PCI Buffer
--                   - Removed initial values of the signals
--
--      Revision 0.31 by dario.drvenkar
--                   - Updated Heartbeat block
--                   - Added generate block for Reset override by SW3
--
--      Revision 0.32 by david.pavlovic
--                   - Code cleanup
--
--      Revision 0.33 by danijela.skugor
--                   - Updated hvADC component due to removing sample clock from it
--
--      Revision 0.34 by danijela.skugor
--                   - Updated speed estimator port names
--
--
--      Revision 0.35 by david.pavlovic
--                   - Updated pwm_monitor ports
--
--      Revision 0.36 by dario.drvenkar
--                   - removed buffer from hvADC filtering block
----------------------------------------------------------------------------------------------
-- TODO: - test gate drivers monitor
--       - test PWM monitor
--       - add error handler pins
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

library unisim;
use unisim.vcomponents.all;

entity bfi_top is
    generic (
        G_BITSTREAM_VERSION            : std_logic_vector(15 downto 0) := x"0000";
        G_BITSTREAM_TAG                : std_logic_vector( 7 downto 0) := x"00";
        G_DESIGN_HASH                  : std_logic_vector(27 downto 0) := x"0000000"
    );
    port (
        -- reset
        p_top_reset_n_in               : in    std_logic;

        -- CLK input
        p_top_clk_in                   : in    std_logic;   -- 100 MHz clock -> 10 ns clock ticks

        -- LED output
        p_top_led1_out                 : out   std_logic;
        p_top_led2_out                 : out   std_logic;
        p_top_led3_out                 : out   std_logic;

        -- Buttons
        p_top_sw1_in                   : in    std_logic;
        --p_top_sw2_in              : in std_logic;

        -- PCI interface
        p_top_req_in                   : in    std_logic;
        p_top_data_out                 : out   std_logic_vector(11 downto 0);
        p_top_address_out              : out   std_logic_vector(3 downto 0);
        p_top_clk_out                  : out   std_logic;

        -- Gate drivers monitor
        p_top_gdrv_flt_dcdc_in         : in    std_logic;
        p_top_gdrv_flt_vin_in          : in    std_logic;
        p_top_gdrv_flt_5v_in           : in    std_logic;
        p_top_gdrv_flt_hs_in           : in    std_logic;
        p_top_gdrv_flt_ls_in           : in    std_logic;
        p_top_gdrv_flt_gdrv_n_out      : out   std_logic;
        p_top_gdrv_flt_psup_n_out      : out   std_logic;
        p_top_gdrv_flt_vin_n_out       : out   std_logic;
        p_top_gdrv_flt_dcdc_n_out      : out   std_logic;
        p_top_gdrv_flt_5v_n_out        : out   std_logic;
        p_top_gdrv_flt_hs_n_out        : out   std_logic;
        p_top_gdrv_flt_ls_n_out        : out   std_logic;

        -- Resolver input and output signals
        p_top_ord_in                   : in    std_logic_vector (12 downto 0);
        p_top_ordclk_in                : in    std_logic;
        p_top_inhb_out                 : out   std_logic;
        p_top_va0_out                  : out   std_logic;
        p_top_va1_out                  : out   std_logic;

        -- PWM Monitor input signals
        p_top_aHs_in                   : in    std_logic;
        p_top_aLs_in                   : in    std_logic;
        p_top_bHs_in                   : in    std_logic;
        p_top_bLs_in                   : in    std_logic;
        p_top_cHs_in                   : in    std_logic;
        p_top_cLs_in                   : in    std_logic;

        --PWM Monitor output signals
        p_top_aHs_out                  : out   std_logic;
        p_top_aLs_out                  : out   std_logic;
        p_top_bHs_out                  : out   std_logic;
        p_top_bLs_out                  : out   std_logic;
        p_top_cHs_out                  : out   std_logic;
        p_top_cLs_out                  : out   std_logic;

        --PWM Monitor feedback input signals
        p_top_aHs_fb_in                : in    std_logic;
        p_top_aLs_fb_in                : in    std_logic;
        p_top_bHs_fb_in                : in    std_logic;
        p_top_bLs_fb_in                : in    std_logic;
        p_top_cHs_fb_in                : in    std_logic;
        p_top_cLs_fb_in                : in    std_logic;

        --Intra-inverter PWM synchronization
        p_top_iInvPWMSync_flt_in       : in    std_logic;

        -- Diagnostics signals
        p_top_diag_flt_gen_n_out       : out   std_logic;
        p_top_diag_flt_fpga_n_out      : out   std_logic;

        -- SPI signals
        p_top_spi_sck_in               : in    std_logic;
        p_top_spi_ss_n_in              : in    std_logic;
        p_top_spi_mosi_in              : in    std_logic;
        p_top_spi_miso_out             : inout std_logic;

        -- HV ADC Interface(MISO-only SPI)
        p_top_hvADC_miso_in            : in    std_logic;
        p_top_hvADC_sck_out            : out   std_logic;
        p_top_hvADC_meas_cs_n_out      : out   std_logic;

        -- Currents ADC Interface
        p_top_currADC_conv_n_out       : out   std_logic;
        p_top_currADC_cs0_n_out        : out   std_logic;
        p_top_currADC_cs1_out          : out   std_logic;
        p_top_currADC_rd_n_out         : out   std_logic;
        p_top_currADC_wr_n_out         : out   std_logic;
        p_top_currADC_dataAv_in        : in    std_logic;
        p_top_currADC_data_bi          : inout std_logic_vector(11 downto 0);

        -- Intra-inverter error detection
        p_top_iInvErrDet_inv_pwmi_in   : in    std_logic;
        p_top_iInvErrDet_inv_pwmo_out  : out   std_logic;
        p_top_iInvErrDet_inv_synci_in  : in    std_logic;
        p_top_iInvErrDet_inv_synco_out : out   std_logic;
        p_top_iInvErrDet_mcu_pwmi_in   : in    std_logic;
        p_top_iInvErrDet_mcu_pwmo_out  : out   std_logic;
        p_top_iInvErrDet_mcu_synci_in  : in    std_logic;
        p_top_iInvErrDet_mcu_synco_out : out   std_logic
    );
end entity bfi_top;

architecture structural of bfi_top is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component heartbeat is
        generic (
            G_HB_CLOCK_FREQ : integer := 100 -- Input frequency of Clock in MHz
        );
        port (
            p_hb_reset_n_in : in  std_logic;
            p_hb_signal_out : out std_logic;
            p_hb_clk_in     : in  std_logic
        );
    end component;

    component sampleclkdiv is
        generic (
            G_COUNT_LIM_HI         : std_logic_vector(7 downto 0) := "00110001";
            G_COUNT_LIM_LO         : std_logic_vector(7 downto 0) := "00110001"
        );
        port (
            -- main inputs
            p_sampleClk_reset_n_in : in  std_logic;
            p_sampleClk_clk_in     : in  std_logic;
            --outputs
            p_sampleClk_out        : out std_logic
        );
    end component;

    component reset_generator is
        generic (
            G_DEPTH       : natural := 3   -- Pipeline delay depth
        );
        port (
            clk_in        : in  std_logic; -- System clock
            asyn_rst_n_in : in  std_logic; -- Asynchronous negated reset input
            syn_rst_n_out : out std_logic  -- Synchronized negated reset output
        );
    end component;

    component gate_drivers_monitor is
        port (
            p_gdrvMon_clk_in        : in  std_logic;
            p_gdrvMon_rst_n_in      : in  std_logic;
            p_gdrvMon_sample_clk_in : in  std_logic;
            -- Gate Driver fault signals
            p_gdrvMon_flt_n_in      : in  std_logic_vector(4 downto 0);
            p_gdrvMon_gdrv_rdy_out  : out std_logic;
            p_gdrvMon_flt_n_out     : out std_logic_vector(6 downto 0)
        );
    end component;

    component pwm_monitor_top is
        generic (
            G_TIMEOUT                  : natural := 500; -- 5 us
            G_DEADTIME                 : natural := 300; -- 3 us
            G_DELTA                    : natural := 3;
            G_PULSE_LENGTH_PWM         : natural := 75;  -- 750 ns
            G_PULSE_LENGTH_PWM_FB      : natural := 50   -- 500 ns
        );
        port (
            -- Main signals
            p_pwm_m_top_clk_in         : in  std_logic;
            p_pwm_m_top_rst_n_in       : in  std_logic;
            p_pwm_m_top_mcu_req_in     : in  std_logic;
            -- Input PWM signal
            p_pwm_m_top_pwm_in         : in  std_logic_vector(5 downto 0);
            -- Input PWM feedback signal
            p_pwm_m_top_pwm_fb_in      : in  std_logic_vector(5 downto 0);
            -- Fault outputs
            p_pwm_m_top_flt_dt_n_out   : out std_logic_vector(2 downto 0);
            p_pwm_m_top_flt_fb_n_out   : out std_logic_vector(5 downto 0)
        );
    end component;

    component pci_top is
        port (
            -- main signals
            p_pcitop_reset_n_in          : in  std_logic;                     -- main reset / active low
            p_pcitop_clk_in              : in  std_logic;                     -- module clock
            -- MCU interface signals
            p_pcitop_dataReq_in          : in  std_logic;                     -- request input from MCU
            p_pcitop_address_out         : out std_logic_vector(3 downto 0);  -- address output to MCU
            p_pcitop_data_out            : out std_logic_vector(11 downto 0); -- data output to MCU
            p_pcitop_clk_out             : out std_logic;                     -- clock output to MCU
            -- DAQ blocks interface signals
            p_pcitop_angle_ready_p_in    : in  std_logic;
            p_pcitop_start_trans_p_in    : in  std_logic;
            p_pcitop_currents_ready_p_in : in  std_logic;
            p_pcitop_flt_n_out           : out std_logic;
            p_pcitop_phCurrA_in          : in  std_logic_vector(11 downto 0);
            p_pcitop_phCurrB_in          : in  std_logic_vector(11 downto 0);
            p_pcitop_phCurrC_in          : in  std_logic_vector(11 downto 0);
            p_pcitop_dcVolt_in           : in  std_logic_vector(11 downto 0);
            p_pcitop_angle_in            : in  std_logic_vector(11 downto 0);
            p_pcitop_speed_msb_in        : in  std_logic_vector(11 downto 0);
            p_pcitop_speed_lsb_in        : in  std_logic_vector(11 downto 0)
        );
    end component;

    component res_driver is
        generic (
            G_RES_DRIVER_CLK             : integer;                           -- input clock in MHz
            G_RES_DRIVER_RES_UPDATE_RATE : integer                            -- Resolver update rate in kHz
        );
        port (
            -- main inputs
            p_res_driver_rst_n_in        :  in std_logic;                     -- main reset line
            p_res_driver_clk_in          :  in std_logic;                     -- main system clock
            p_res_driver_sample_in       :  in std_logic;                     -- sampling pulse synchronized with ADC driver
            -- resolver inputs
            p_res_driver_ord_in          :  in std_logic_vector(11 downto 0); -- 12-bit parallel data in
            -- outputs
            p_res_driver_va0_out         : out std_logic;
            p_res_driver_va1_out         : out std_logic;
            p_res_driver_inhb_out        : out std_logic;
            p_res_driver_data_val_p_out  : out std_logic;
            p_res_driver_angle_out       : out std_logic_vector(11 downto 0); -- 12-bit angle value
            p_res_driver_velocity_out    : out std_logic_vector(11 downto 0)  -- 12-bit angle value
        );
    end component;

    component hvADC is
        port (
            -- system clock
            p_hvADC_clk_in       : in  std_logic;
            -- reset
            p_hvADC_rst_n_in     : in  std_logic;
            -- SPI MISO signals
            p_hvADC_meas_miso    : in  std_logic;
            p_hvADC_meas_sck     : out std_logic;
            p_hvADC_meas_cs_n    : out std_logic;
            -- output data
            p_hvADC_data_out     : out std_logic_vector(11 downto 0)
        );
    end component;

    component sw_freq_filter_top is
        generic (
            G_DATA                  : natural := 12;
            G_OFFSET                : natural := 100
        );
        port (
            p_top_sff_clk_in        : in  std_logic;
            p_top_sff_rst_n_in      : in  std_logic;
            p_top_sff_sample_clk_in : in  std_logic;
            p_top_sff_mcu_req_in    : in  std_logic;
            p_top_sff_data_in       : in  std_logic_vector(G_DATA-1 downto 0);
            p_top_sff_data_out      : out std_logic_vector(G_DATA-1 downto 0)
        );
    end component;

    component adc_driver_top is
        generic (
            G_ADC_DRIVER_TOP_TRIGGER_LEVEL : natural   := 0;                      -- Valid trigger levels range from 0 to 2 (see datasheet table 13, pp.28).
            G_ADC_DRIVER_TOP_CONV_MODE     : std_logic := '1';                    -- Conversion mode: continuous = 0, on-demand = 1
            G_ADC_DRIVER_TOP_OUTPUT_FORMAT : std_logic := '1';                    -- 2's complement = 0, binary = 1
            G_ADC_DRIVER_TOP_FIFO_RESET    : std_logic := '1';                    -- Enable automatic reset of the FIFO after each conversion.
            G_ADC_DRIVER_TOP_AUTO_OFFSET   : std_logic := '0';                    -- Automatic offset compensation
            G_ADC_DRIVER_TOP_REF_EXT       : std_logic := '0';                    -- Select the external ADC reference voltage
            G_ADC_DRIVER_TOP_WD_MAX_ERR    : natural   := 3                       -- Number of allowed initialization errors
        );
        port (
            -- Main signals
            p_adc_top_clk_in               : in    std_logic;                     -- main clock (expected 100 MHz)
            p_adc_top_rst_n_in             : in    std_logic;                     -- async reset
            -- ADC signals
            p_adc_top_conv_n_out           : out   std_logic;                     -- conversion trigger (active low)
            p_adc_top_cs0_n_out            : out   std_logic;                     -- control signal (active low)
            p_adc_top_cs1_out              : out   std_logic;                     -- control signal (active high)
            p_adc_top_rd_n_out             : out   std_logic;                     -- read trigger (active low)
            p_adc_top_wr_n_out             : out   std_logic;                     -- write trigger (active low)
            p_adc_top_dav_in               : in    std_logic;                     -- data available flag
            p_adc_top_data_bi              : inout std_logic_vector(11 downto 0); -- ADC data bus
            -- Interface
            p_top_adc_dataCh0_out          : out   std_logic_vector(11 downto 0); -- ADC data channel 0
            p_top_adc_dataCh1_out          : out   std_logic_vector(11 downto 0); -- ADC data channel 1
            p_top_adc_dataCh2_out          : out   std_logic_vector(11 downto 0); -- ADC data channel 2
            p_top_adc_dataCh3_out          : out   std_logic_vector(11 downto 0); -- ADC data channel 3
            p_top_adc_dataRdy_out          : out   std_logic;                     -- Sample available
            -- Fault
            p_top_adc_wd_flt_n_out         : out   std_logic                      -- ADC watchdog fault
        );
    end component;

    component mov_avg_filter_top is
        generic (
            G_MOV_AVG_FILTER_TOP_DATA            : natural := 12;                                                 -- Data size
            G_MOV_AVG_FILTER_TOP_AVG_LEN         : natural := 7                                                   -- Filter length
        );
        port (
            p_mov_avg_filter_top_rst_n_in        : in  std_logic;                                                 -- System reset input
            p_mov_avg_filter_top_clk_in          : in  std_logic;                                                 -- System clock input
            p_mov_avg_filter_top_sample_p_in     : in  std_logic;                                                 -- Sample pulse from ADC
            p_mov_avg_filter_top_mcu_req_in      : in  std_logic;                                                 -- MCU request (start of PWM cycle)
            p_mov_avg_filter_top_central_p_out   : out std_logic;                                                 -- Central sample detected pulse
            p_mov_avg_filter_top_start_pci_p_out : out std_logic;                                                 -- Start PCI transaction pulse
            p_mov_avg_filter_top_rdy_p_out       : out std_logic;                                                 -- Output ready pulse
            p_mov_avg_filter_top_phase_a_in      : in  std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Stream input 1
            p_mov_avg_filter_top_phase_b_in      : in  std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Stream input 2
            p_mov_avg_filter_top_phase_c_in      : in  std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Stream input 3
            p_mov_avg_filter_top_phase_a_out     : out std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Sample output 1
            p_mov_avg_filter_top_phase_b_out     : out std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0);  -- Sample output 2
            p_mov_avg_filter_top_phase_c_out     : out std_logic_vector(G_MOV_AVG_FILTER_TOP_DATA - 1 downto 0)   -- Sample output 3
        );
    end component;

    component intra_inv_err_detection is
        generic (
            G_NUM_VALID_PULSES         : natural := 3
        );
        port (
            -- Clock & Reset inputs
            p_iInvErrDet_clk_in        : in  std_logic;
            p_iInvErrDet_rst_n_in      : in  std_logic;
            -- IIED connector PWM synchronization
            p_iInvErrDet_inv_pwmi_in   : in  std_logic;
            p_iInvErrDet_inv_pwmo_out  : out std_logic;
            -- IIED connector error signals (active low)
            p_iInvErrDet_inv_synci_in  : in  std_logic;
            p_iInvErrDet_inv_synco_out : out std_logic;
            -- MCU PWM synchronization
            p_iInvErrDet_mcu_pwmi_in   : in  std_logic;
            p_iInvErrDet_mcu_pwmo_out  : out std_logic;
            -- MCU error signals (active low)
            p_iInvErrDet_mcu_synci_in  : in  std_logic;
            p_iInvErrDet_mcu_synco_out : out std_logic
        );
    end component;

    component speed_estimator is
        port (
            p_speed_est_clk_in         : in  std_logic;                     -- System clock input
            p_speed_est_rst_n_in       : in  std_logic;                     -- Synchronous reset
            p_speed_est_angle_val_p_in : in  std_logic;
            p_speed_est_angle_in       : in  std_logic_vector(11 downto 0); -- Angle input from resolver
            p_speed_est_speed_out      : out std_logic_vector(23 downto 0)  -- Speed output
        );
    end component;

    component diag_top is
        generic (
            G_SPI_DATA_W               : natural                       := 16;
            G_BITSTREAM_VERSION        : std_logic_vector(15 downto 0) := x"0000";
            G_BITSTREAM_TAG            : std_logic_vector( 7 downto 0) := x"00";
            G_DESIGN_HASH              : std_logic_vector(27 downto 0) := x"0000000"
        );
        port (
            -- Main signals
            p_diag_top_clk_in          : in    std_logic;
            p_diag_top_rst_n_in        : in    std_logic;
            p_diag_top_gdrv_rdy_in     : in    std_logic;
            -- Fault signals
            p_diag_top_flt_dt_n_in     : in    std_logic_vector(2 downto 0);
            p_diag_top_flt_fb_n_in     : in    std_logic_vector(5 downto 0);
            p_diag_top_flt_gdrv_n_in   : in    std_logic_vector(4 downto 0);
            p_diag_top_flt_pci_n_in    : in    std_logic;
            p_diag_top_flt_adc_n_in    : in    std_logic;
            p_diag_top_flt_gen_n_out   : out   std_logic;
            p_diag_top_flt_fpga_n_out  : out   std_logic;
            -- SPI interface
            p_diag_top_spi_sck_in      : in    std_logic;
            p_diag_top_spi_ss_n_in     : in    std_logic;
            p_diag_top_spi_mosi_in     : in    std_logic;
            p_diag_top_spi_miso_out    : inout std_logic
        );
    end component;

    component delay_line
        generic (
            G_DELAY_LINE_WIDTH     : integer := 8;
            G_DELAY_LINE_LENGTH    : integer := 2
        );
        port (
            p_delay_line_rst_n_in  :  in std_logic;
            p_delay_line_clk_in    :  in std_logic;
            p_delay_line_en_in     :  in std_logic;
            p_delay_line_data_in   :  in std_logic_vector(G_DELAY_LINE_WIDTH - 1 downto 0);
            p_delay_line_data_out  : out std_logic_vector(G_DELAY_LINE_WIDTH - 1 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    -- release version
    constant C_BITSTREAM_VERSION : std_logic_vector(15 downto 0) := G_BITSTREAM_VERSION;
    constant C_BITSTREAM_TAG     : std_logic_vector( 7 downto 0) := G_BITSTREAM_TAG;
    constant C_DESIGN_HASH       : std_logic_vector(27 downto 0) := G_DESIGN_HASH;
    -- manual reset on SW_3
    constant C_DBG_RST           : boolean := FALSE;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    -- main signals
    signal s_reset_n             : std_logic; -- reset input
    signal s_clk                 : std_logic; -- clock input

    -- heart beat
    signal s_heart_beat          : std_logic; -- to connect to LED port
    signal s_debugLed            : std_logic;

    -- interface <-> PCI connections
    signal s_mcuToPciDataReq     : std_logic;
    signal s_mcuToPciDataReqSync : std_logic;
    signal s_pciToMcuAddress     : std_logic_vector(3 downto 0);
    signal s_pciToMcuData        : std_logic_vector(11 downto 0);
    signal s_pciToMcuClk         : std_logic;
    signal s_pci_flt_n           : std_logic;

    -- Sample clock
    signal s_sampleClk           : std_logic;

    -- Gate Drivers Monitor
    signal s_gdrv_flt_meta_i     : std_logic_vector(4 downto 0);
    signal s_gdrv_flt_n_i        : std_logic_vector(4 downto 0);
    signal s_gdrv_flt_n_o        : std_logic_vector(6 downto 0);

    -- PWM Monitor
    signal s_pwm_meta_in         : std_logic_vector(5 downto 0);
    signal s_pwm_in              : std_logic_vector(5 downto 0);
    signal s_pwm_fb_meta_in      : std_logic_vector(5 downto 0);
    signal s_pwm_fb_in           : std_logic_vector(5 downto 0);
    signal s_pwm_mon_flt_dt_n    : std_logic_vector(2 downto 0);
    signal s_pwm_mon_flt_fb_n    : std_logic_vector(5 downto 0);

    -- Diagnostics signals
    signal s_gdrv_rdy            : std_logic;
    signal s_diag_flt_gen_n      : std_logic;
    signal s_diag_flt_fpga_n     : std_logic;

    -- Resolver signals
    signal s_resolverOrd         : std_logic_vector(12 downto 0);
    signal s_resolverInhb        : std_logic;
    signal s_resolverVa0         : std_logic;
    signal s_resolverVa1         : std_logic;
    signal s_resolverAngle       : std_logic_vector(11 downto 0);
    signal s_resolverAngle_delay : std_logic_vector(11 downto 0);
    signal s_angle_val_p         : std_logic;

    -- HV ADC measurement
    signal s_hvADC_sclk          : std_logic;
    signal s_hvADC_cs_n          : std_logic;
    signal s_hvADC_data          : std_logic_vector(11 downto 0);

    -- ADC chip interface
    signal s_top_currADC_conv_n  : std_logic;
    signal s_top_currADC_cs0_n   : std_logic;
    signal s_top_currADC_cs1     : std_logic;
    signal s_top_currADC_rd_n    : std_logic;
    signal s_top_currADC_wr_n    : std_logic;
    signal s_top_currADC_dataAv  : std_logic;
    signal s_adc_dataCh0         : std_logic_vector(11 downto 0);
    signal s_adc_dataCh1         : std_logic_vector(11 downto 0);
    signal s_adc_dataCh2         : std_logic_vector(11 downto 0);
    signal s_adc_dataCh3         : std_logic_vector(11 downto 0);
    signal s_adc_dataRdy_p       : std_logic;
    signal s_adc_flt_n           : std_logic;

      --filtered data
    signal s_bb_iPhA             : std_logic_vector(11 downto 0);
    signal s_bb_iPhB             : std_logic_vector(11 downto 0);
    signal s_bb_iPhC             : std_logic_vector(11 downto 0);
    signal s_firFilt_vDC         : std_logic_vector(11 downto 0);
    signal s_currents_rdy_p      : std_logic;
    signal s_start_pci_p         : std_logic;
    signal s_central_p           : std_logic;

    -- speed estimator
    signal s_sc_speed            : std_logic_vector(23 downto 0);

    -- debug reset
    signal s_rst_n_dbg           : std_logic;

begin

    ---------------------------------------------------------------------------------
    --                          external signals mapping
    ---------------------------------------------------------------------------------

    -- main signals
    s_clk <= p_top_clk_in;              -- Clock pin -> Reset signal

    -- LEDs
    -- heart beat signal
    p_top_led1_out <= s_heart_beat;      -- Heart beat signal -> LED pin
    p_top_led2_out <= s_mcuToPciDataReq;
    p_top_led3_out <= '0';

    -- MCU interface signals
    s_mcuToPciDataReq <= p_top_req_in;
    p_top_address_out <= s_pciToMcuAddress;
    p_top_data_out    <= s_pciToMcuData;
    p_top_clk_out     <= s_pciToMcuClk;
    s_debugLed        <= p_top_req_in;
    p_top_led2_out    <= s_debugLed;

    -- Gate driver interface signals
    s_gdrv_flt_meta_i(4) <= not p_top_gdrv_flt_vin_in;
    s_gdrv_flt_meta_i(3) <= not p_top_gdrv_flt_dcdc_in;
    s_gdrv_flt_meta_i(2) <= not p_top_gdrv_flt_5v_in;
    s_gdrv_flt_meta_i(1) <= not p_top_gdrv_flt_hs_in;
    s_gdrv_flt_meta_i(0) <= not p_top_gdrv_flt_ls_in;

    p_top_gdrv_flt_gdrv_n_out <= s_gdrv_flt_n_o(6);
    p_top_gdrv_flt_psup_n_out <= s_gdrv_flt_n_o(5);
    p_top_gdrv_flt_vin_n_out  <= s_gdrv_flt_n_o(4);
    p_top_gdrv_flt_dcdc_n_out <= s_gdrv_flt_n_o(3);
    p_top_gdrv_flt_5v_n_out   <= s_gdrv_flt_n_o(2);
    p_top_gdrv_flt_hs_n_out   <= s_gdrv_flt_n_o(1);
    p_top_gdrv_flt_ls_n_out   <= s_gdrv_flt_n_o(0);
    p_top_diag_flt_gen_n_out  <= s_diag_flt_gen_n;
    p_top_diag_flt_fpga_n_out <= s_diag_flt_fpga_n;

    -- PWM Monitor interface signals
    s_pwm_meta_in(5) <= p_top_aHs_in;
    s_pwm_meta_in(4) <= p_top_aLs_in;
    s_pwm_meta_in(3) <= p_top_bHs_in;
    s_pwm_meta_in(2) <= p_top_bLs_in;
    s_pwm_meta_in(1) <= p_top_cHs_in;
    s_pwm_meta_in(0) <= p_top_cLs_in;

    s_pwm_fb_meta_in(5) <= not p_top_aHs_fb_in;
    s_pwm_fb_meta_in(4) <= not p_top_aLs_fb_in;
    s_pwm_fb_meta_in(3) <= not p_top_bHs_fb_in;
    s_pwm_fb_meta_in(2) <= not p_top_bLs_fb_in;
    s_pwm_fb_meta_in(1) <= not p_top_cHs_fb_in;
    s_pwm_fb_meta_in(0) <= not p_top_cLs_fb_in;

    p_top_aHs_out <= p_top_aHs_in;
    p_top_aLs_out <= p_top_aLs_in;
    p_top_bHs_out <= p_top_bHs_in;
    p_top_bLs_out <= p_top_bLs_in;
    p_top_cHs_out <= p_top_cHs_in;
    p_top_cLs_out <= p_top_cLs_in;

    -- Resolver interface signals
    s_resolverOrd  <= p_top_ord_in;
    p_top_inhb_out <= s_resolverInhb;
    p_top_va0_out  <= s_resolverVa0;
    p_top_va1_out  <= s_resolverVa1;

    --  HV ADC Interface(MISO-only SPI) signals
    p_top_hvADC_sck_out       <= s_hvADC_sclk;
    p_top_hvADC_meas_cs_n_out <= s_hvADC_cs_n;

    -- Currents ADC Interface
    p_top_currADC_conv_n_out <= s_top_currADC_conv_n;
    p_top_currADC_cs0_n_out  <= s_top_currADC_cs0_n;
    p_top_currADC_cs1_out    <= s_top_currADC_cs1;
    p_top_currADC_rd_n_out   <= s_top_currADC_rd_n;
    p_top_currADC_wr_n_out   <= s_top_currADC_wr_n;
    s_top_currADC_dataAv     <= p_top_currADC_dataAv_in;

    ---------------------------------------------------------------------------------
    --                          internal signals mapping
    ---------------------------------------------------------------------------------

    -- when C_DBG_RST is enabled SW_3 can be used to reset the whole FPGA
    -- CLK will still be running but all registers will be set to the default value
    debug_reset :
    if C_DBG_RST = TRUE generate
        s_rst_n_dbg <= p_top_reset_n_in and p_top_sw1_in;
    end generate;
    no_debug_reset :
    if C_DBG_RST = FALSE generate
        s_rst_n_dbg <= p_top_reset_n_in;
    end generate;

    reset_sync : reset_generator -- Reset generator
        generic map (
            G_DEPTH         => 3
        )
        port map (
            clk_in        => s_clk,
            asyn_rst_n_in => s_rst_n_dbg,
            syn_rst_n_out => s_reset_n
        );

    mcu_req_sync_inst : xpm_cdc_sync_rst
        generic map (
           DEST_SYNC_FF   => 8,                     -- DECIMAL; range: 2-10
           INIT           => 1,                     -- DECIMAL; 0=initialize synchronization registers to 0, 1=initialize synchronization registers to 1
           INIT_SYNC_FF   => 1,                     -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
           SIM_ASSERT_CHK => 1                      -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        )
        port map (
           dest_rst       => s_mcuToPciDataReqSync, -- 1-bit output: src_rst synchronized to the destination clock domain. This output is registered.
           dest_clk       => s_clk,                 -- 1-bit input: Destination clock.
           src_rst        => s_mcuToPciDataReq      -- 1-bit input: Source reset signal.
        );

    pwm_sync : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF   => 2,                    -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,                    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 1,                    -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,                    -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 6                     -- DECIMAL; range: 1-1024
        )
        port map (
            dest_out       => s_pwm_in,             -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
            dest_clk       => s_clk,                -- 1-bit input: Clock signal for the destination clock domain.
            src_clk        => '0',                  -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in         => s_pwm_meta_in         -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain.
        );

    pwm_fb_sync : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF   => 2,                    -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,                    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 1,                    -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,                    -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 6                     -- DECIMAL; range: 1-1024
        )
        port map (
            dest_out       => s_pwm_fb_in,          -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
            dest_clk       => s_clk,                -- 1-bit input: Clock signal for the destination clock domain.
            src_clk        => '0',                  -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in         => s_pwm_fb_meta_in      -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain.
        );

    gdrv_flt_sync : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF   => 2,                    -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,                    -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 1,                    -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0,                    -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => 5                     -- DECIMAL; range: 1-1024
        )
        port map (
            dest_out       => s_gdrv_flt_n_i,       -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
            dest_clk       => s_clk,                -- 1-bit input: Clock signal for the destination clock domain.
            src_clk        => '0',                  -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in         => s_gdrv_flt_meta_i     -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain.
        );

    mdl_heartBeat : heartbeat
        generic map (
            G_HB_CLOCK_FREQ => 100
        )
        port map (
            p_hb_reset_n_in => s_reset_n,
            p_hb_signal_out => s_heart_beat,
            p_hb_clk_in     => s_clk
        );

    mdl_sampleClk : sampleclkdiv
        port map (
            p_sampleClk_reset_n_in => s_reset_n,
            p_sampleClk_clk_in     => s_clk,
            p_sampleClk_out        => s_sampleClk -- 1MHz Sample clock
        );

    mdl_pciTop : pci_top
        port map (
            -- main signals
            p_pcitop_reset_n_in          => s_reset_n,
            p_pcitop_clk_in              => s_clk,
            -- MCU interface signals
            p_pcitop_dataReq_in          => s_mcuToPciDataReqSync,
            p_pcitop_address_out         => s_pciToMcuAddress,
            p_pcitop_data_out            => s_pciToMcuData,
            p_pcitop_clk_out             => s_pciToMcuClk,
            -- DAQ blocks interface signals
            p_pcitop_angle_ready_p_in    => s_central_p,
            p_pcitop_start_trans_p_in    => s_start_pci_p,
            p_pcitop_currents_ready_p_in => s_currents_rdy_p,
            p_pcitop_flt_n_out           => s_pci_flt_n,
            p_pcitop_phCurrA_in          => s_bb_iPhA,
            p_pcitop_phCurrB_in          => s_bb_iPhB,
            p_pcitop_phCurrC_in          => s_bb_iPhC,
            p_pcitop_dcVolt_in           => s_firFilt_vDC,
            p_pcitop_angle_in            => s_resolverAngle_delay,
            p_pcitop_speed_msb_in        => s_sc_speed(23 downto 12),
            p_pcitop_speed_lsb_in        => s_sc_speed(11 downto 0)
        );

    mdl_pwmMonitor : pwm_monitor_top
        generic map (
            G_TIMEOUT                  => 2000, -- 20 us
            G_DEADTIME                 => 300,  -- 3 us
            G_DELTA                    => 5,
            G_PULSE_LENGTH_PWM         => 75,   -- 750 ns
            G_PULSE_LENGTH_PWM_FB      => 50    -- 500 ns
        )
        port map (
            -- Main signals
            p_pwm_m_top_clk_in         => s_clk,
            p_pwm_m_top_rst_n_in       => s_reset_n,
            p_pwm_m_top_mcu_req_in     => s_mcuToPciDataReqSync,
            -- Input PWM signals
            p_pwm_m_top_pwm_in         => s_pwm_in,
            -- Input PWM feedback signals
            p_pwm_m_top_pwm_fb_in      => s_pwm_fb_in,
            -- Fault outputs
            p_pwm_m_top_flt_dt_n_out   => s_pwm_mon_flt_dt_n,
            p_pwm_m_top_flt_fb_n_out   => s_pwm_mon_flt_fb_n
        );

    mdl_gdrvMonitor: gate_drivers_monitor
        port map (
            p_gdrvMon_clk_in        => s_clk,
            p_gdrvMon_rst_n_in      => s_reset_n,
            p_gdrvMon_sample_clk_in => s_sampleClk,
            -- Gate Driver fault signals
            p_gdrvMon_flt_n_in      => s_gdrv_flt_n_i,
            p_gdrvMon_gdrv_rdy_out  => s_gdrv_rdy,
            p_gdrvMon_flt_n_out     => s_gdrv_flt_n_o
        );

    mdl_diagTop : diag_top
        generic map (
            G_SPI_DATA_W              => 16,
            G_BITSTREAM_VERSION       => C_BITSTREAM_VERSION,
            G_BITSTREAM_TAG           => C_BITSTREAM_TAG,
            G_DESIGN_HASH             => C_DESIGN_HASH
        )
        port map (
            -- Main signals
            p_diag_top_clk_in         => s_clk,
            p_diag_top_rst_n_in       => s_reset_n,
            p_diag_top_gdrv_rdy_in    => s_gdrv_rdy,
            -- Fault signals
            p_diag_top_flt_dt_n_in    => s_pwm_mon_flt_dt_n,
            p_diag_top_flt_fb_n_in    => s_pwm_mon_flt_fb_n,
            p_diag_top_flt_gdrv_n_in  => s_gdrv_flt_n_o(4 downto 0),
            p_diag_top_flt_pci_n_in   => s_pci_flt_n,
            p_diag_top_flt_adc_n_in   => s_adc_flt_n,
            p_diag_top_flt_gen_n_out  => s_diag_flt_gen_n,
            p_diag_top_flt_fpga_n_out => s_diag_flt_fpga_n,
            -- SPI interface
            p_diag_top_spi_sck_in     => p_top_spi_sck_in,
            p_diag_top_spi_ss_n_in    => p_top_spi_ss_n_in,
            p_diag_top_spi_mosi_in    => p_top_spi_mosi_in,
            p_diag_top_spi_miso_out   => p_top_spi_miso_out
        );

    mdl_res_driver: res_driver
        generic map (
            G_RES_DRIVER_CLK             => 100,
            G_RES_DRIVER_RES_UPDATE_RATE => 800
        )
        port map (
            -- main inputs
            p_res_driver_rst_n_in        => s_reset_n,
            p_res_driver_clk_in          => s_clk,
            p_res_driver_sample_in       => s_top_currADC_conv_n,
            -- resolver inputs
            p_res_driver_ord_in          => s_resolverOrd(11 downto 0),
            -- ORDCLK has hardware deficiency
            -- p_res_driver_ordclk_in       :  in std_logic; -- sample ready clock in; not used (hardware deficiency)
            -- outputs
            p_res_driver_va0_out         => s_resolverVa0,
            p_res_driver_va1_out         => s_resolverVa1,
            p_res_driver_inhb_out        => s_resolverInhb,
            p_res_driver_data_val_p_out  => s_angle_val_p,
            p_res_driver_angle_out       => s_resolverAngle,
            p_res_driver_velocity_out    => open
        );

    mdl_res_delay: delay_line
        generic map (
            G_DELAY_LINE_WIDTH    => 12,
            G_DELAY_LINE_LENGTH   => 2
        )
        port map (
            p_delay_line_rst_n_in => s_reset_n,
            p_delay_line_clk_in   => s_clk,
            p_delay_line_en_in    => s_angle_val_p,
            p_delay_line_data_in  => s_resolverAngle,
            p_delay_line_data_out => s_resolverAngle_delay
        );

    mdl_hvADC_meas : hvADC
        port map (
            p_hvADC_clk_in       => s_clk,
            p_hvADC_rst_n_in     => s_reset_n,
            p_hvADC_meas_miso    => p_top_hvADC_miso_in,
            p_hvADC_meas_sck     => s_hvADC_sclk,
            p_hvADC_meas_cs_n    => s_hvADC_cs_n,
            p_hvADC_data_out     => s_hvADC_data
        );

    mdl_adc_interface : adc_driver_top
        generic map (
            G_ADC_DRIVER_TOP_TRIGGER_LEVEL => 0,
            G_ADC_DRIVER_TOP_CONV_MODE     => '1',
            G_ADC_DRIVER_TOP_OUTPUT_FORMAT => '1',
            G_ADC_DRIVER_TOP_FIFO_RESET    => '0',
            G_ADC_DRIVER_TOP_AUTO_OFFSET   => '0',
            G_ADC_DRIVER_TOP_REF_EXT       => '0',
            G_ADC_DRIVER_TOP_WD_MAX_ERR    => 3
        )
        port map (
            -- Main signals
            p_adc_top_clk_in               => s_clk,
            p_adc_top_rst_n_in             => s_reset_n,
            -- ADC signals
            p_adc_top_conv_n_out           => s_top_currADC_conv_n,
            p_adc_top_cs0_n_out            => s_top_currADC_cs0_n,
            p_adc_top_cs1_out              => s_top_currADC_cs1,
            p_adc_top_rd_n_out             => s_top_currADC_rd_n,
            p_adc_top_wr_n_out             => s_top_currADC_wr_n,
            p_adc_top_dav_in               => s_top_currADC_dataAv,
            p_adc_top_data_bi              => p_top_currADC_data_bi,
            -- Interface
            p_top_adc_dataCh0_out          => s_adc_dataCh0,
            p_top_adc_dataCh1_out          => s_adc_dataCh1,
            p_top_adc_dataCh2_out          => s_adc_dataCh2,
            p_top_adc_dataCh3_out          => open,
            p_top_adc_dataRdy_out          => s_adc_dataRdy_p,
            -- Fault
            p_top_adc_wd_flt_n_out         => s_adc_flt_n
        );

    LPF_block_AVG : mov_avg_filter_top
        generic map (
            G_MOV_AVG_FILTER_TOP_DATA            => 12,
            G_MOV_AVG_FILTER_TOP_AVG_LEN         => 13
        )
        port map (
            p_mov_avg_filter_top_rst_n_in        => s_reset_n,
            p_mov_avg_filter_top_clk_in          => s_clk,
            p_mov_avg_filter_top_sample_p_in     => s_adc_dataRdy_p,
            p_mov_avg_filter_top_mcu_req_in      => s_mcuToPciDataReqSync,
            p_mov_avg_filter_top_central_p_out   => s_central_p,
            p_mov_avg_filter_top_start_pci_p_out => s_start_pci_p,
            p_mov_avg_filter_top_rdy_p_out       => s_currents_rdy_p,
            p_mov_avg_filter_top_phase_a_in      => s_adc_dataCh0,
            p_mov_avg_filter_top_phase_b_in      => s_adc_dataCh1,
            p_mov_avg_filter_top_phase_c_in      => s_adc_dataCh2,
            p_mov_avg_filter_top_phase_a_out     => s_bb_iPhA,
            p_mov_avg_filter_top_phase_b_out     => s_bb_iPhB,
            p_mov_avg_filter_top_phase_c_out     => s_bb_iPhC
        );

    mdl_firFilt_vDC : sw_freq_filter_top
        generic map (
            G_DATA                  => 12,
            G_OFFSET                => 100
        )
        port map(
            p_top_sff_clk_in        => s_clk,
            p_top_sff_rst_n_in      => s_reset_n,
            p_top_sff_sample_clk_in => s_sampleClk,
            p_top_sff_mcu_req_in    => s_mcuToPciDataReqSync,
            p_top_sff_data_in       => s_hvADC_data,
            p_top_sff_data_out      => s_firFilt_vDC
        );

    mdl_intraInvErrDet : intra_inv_err_detection
        generic map (
            G_NUM_VALID_PULSES         => 3
        )
        port map (
            p_iInvErrDet_clk_in        => s_clk,
            p_iInvErrDet_rst_n_in      => s_reset_n,
            -- IIED connector PWM synchronization
            p_iInvErrDet_inv_pwmi_in   => p_top_iInvErrDet_inv_pwmi_in,
            p_iInvErrDet_inv_pwmo_out  => p_top_iInvErrDet_inv_pwmo_out,
            -- IIED connector error signals (active low)
            p_iInvErrDet_inv_synci_in  => p_top_iInvErrDet_inv_synci_in,
            p_iInvErrDet_inv_synco_out => p_top_iInvErrDet_inv_synco_out,
            -- MCU PWM synchronization
            p_iInvErrDet_mcu_pwmi_in   => p_top_iInvErrDet_mcu_pwmi_in,
            p_iInvErrDet_mcu_pwmo_out  => p_top_iInvErrDet_mcu_pwmo_out,
            -- MCU error signals (active low)
            p_iInvErrDet_mcu_synci_in  => p_top_iInvErrDet_mcu_synci_in,
            p_iInvErrDet_mcu_synco_out => p_top_iInvErrDet_mcu_synco_out
        );

    mdl_speed_estimator : speed_estimator
        port map (
            p_speed_est_clk_in         => s_clk,
            p_speed_est_rst_n_in       => s_reset_n,
            p_speed_est_angle_val_p_in => s_angle_val_p,
            p_speed_est_angle_in       => s_resolverAngle,
            p_speed_est_speed_out      => s_sc_speed
        );

end architecture structural;