----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             diag_top.vhd
-- module name      diag_top - structural
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Top module for FPGA diagnostics that connects all components
----------------------------------------------------------------------------------------------
-- create date      12.08.2020
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Added support for metadata
--                    - Changed signal names
--
--      Revision 0.03 by david.pavlovic
--                    - Code cleanup
--                    - Removed SEM IP port
--
--      Revision 0.04 by david.pavlovic
--                    - Removed ports for PWM ROC, PWM Timeout faults
--                    - Removed port for enabling PWM signals
----------------------------------------------------------------------------------------------
-- TODO: -
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity diag_top is
    generic (
        G_SPI_DATA_W               : natural                       := 16;
        G_BITSTREAM_VERSION        : std_logic_vector(15 downto 0) := x"0000";
        G_BITSTREAM_TAG            : std_logic_vector( 7 downto 0) := x"00";
        G_DESIGN_HASH              : std_logic_vector(27 downto 0) := x"0000000"
    );
    port (
        -- Main signals
        p_diag_top_clk_in          : in    std_logic;
        p_diag_top_rst_n_in        : in    std_logic;
        p_diag_top_gdrv_rdy_in     : in    std_logic;
        -- Fault signals
        p_diag_top_flt_dt_n_in     : in    std_logic_vector(2 downto 0);
        p_diag_top_flt_fb_n_in     : in    std_logic_vector(5 downto 0);
        p_diag_top_flt_gdrv_n_in   : in    std_logic_vector(4 downto 0);
        p_diag_top_flt_pci_n_in    : in    std_logic;
        p_diag_top_flt_adc_n_in    : in    std_logic;
        p_diag_top_flt_gen_n_out   : out   std_logic;
        p_diag_top_flt_fpga_n_out  : out   std_logic;
        -- SPI interface
        p_diag_top_spi_sck_in      : in    std_logic;
        p_diag_top_spi_ss_n_in     : in    std_logic;
        p_diag_top_spi_mosi_in     : in    std_logic;
        p_diag_top_spi_miso_out    : inout std_logic
    );
end entity diag_top;

architecture structural of diag_top is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component diag is
        generic (
            G_SPI_DATA_W              : natural                       := 16;
            G_CRC_DATA_W              : natural                       := 24;
            G_BITSTREAM_VERSION       : std_logic_vector(15 downto 0) := x"0000";
            G_BITSTREAM_TAG           : std_logic_vector( 7 downto 0) := x"00";
            G_DESIGN_HASH             : std_logic_vector(27 downto 0) := x"0000000"
        );
        port (
            -- Main signals
            p_diag_clk_in             : in  std_logic;
            p_diag_rst_n_in           : in  std_logic;
            p_diag_gdrv_rdy_in        : in  std_logic;
            -- Fault signals
            p_diag_flt_dt_n_in        : in  std_logic_vector(2 downto 0);
            p_diag_flt_fb_n_in        : in  std_logic_vector(5 downto 0);
            p_diag_flt_gdrv_n_in      : in  std_logic_vector(4 downto 0);
            p_diag_flt_pci_n_in       : in  std_logic;
            p_diag_flt_adc_n_in       : in  std_logic;
            p_diag_flt_gen_n_out      : out std_logic;
            p_diag_flt_fpga_n_out     : out std_logic;
            -- CRC signals
            p_diag_crc_req_out        : out std_logic;
            p_diag_crc_rdy_in         : in  std_logic;
            p_diag_crc_data_out       : out std_logic_vector(G_CRC_DATA_W-1 downto 0);
            p_diag_crc_val_in         : in  std_logic_vector(7 downto 0);
            -- SPI data and status signals
            p_diag_spi_busy_in        : in  std_logic;
            p_diag_spi_rx_vld_in      : in  std_logic;
            p_diag_spi_trans_flt_n_in : in  std_logic;
            p_diag_spi_data_tx_out    : out std_logic_vector(G_SPI_DATA_W-1 downto 0);
            p_diag_spi_data_rx_in     : in  std_logic_vector(G_SPI_DATA_W-1 downto 0)
    );
    end component;

    component spi_slave is
        generic (
            G_DATA_W              : natural := 8
        );
        port (
            -- Main signals
            p_spi_clk_in          : in    std_logic;
            p_spi_rst_n_in        : in    std_logic;
            -- SPI signals
            p_spi_sck_in          : in    std_logic;
            p_spi_ss_n_in         : in    std_logic;
            p_spi_mosi_in         : in    std_logic;
            p_spi_miso_out        : inout std_logic;
            -- TX and RX data
            p_spi_data_tx_in      : in    std_logic_vector(G_DATA_W-1 downto 0);
            p_spi_data_tx_out     : out   std_logic_vector(G_DATA_W-1 downto 0);
            p_spi_data_rx_out     : out   std_logic_vector(G_DATA_W-1 downto 0);
            -- Status signals
            p_spi_busy_out        : out   std_logic;
            p_spi_data_rx_vld_out : out   std_logic;
            p_spi_trans_flt_n_out : out   std_logic
        );
    end component;

    component crc is
        generic (
            G_CRC_POLYNOMIAL : std_logic_vector(8 downto 0) := "100011101";
            G_CRC_INPUT_SIZE : integer := 127
        );
        port (
            -- main signals input
            p_crc_reset_n_in : in  std_logic;
            p_crc_clk_in     : in  std_logic;
            -- interface input
            p_crc_req_in     : in  std_logic;
            p_crc_data_in    : in  std_logic_vector(G_CRC_INPUT_SIZE downto 0);
            -- interface output
            p_crc_rdy_out    : out std_logic;
            p_crc_value_out  : out std_logic_vector(7 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_CRC_W : integer := 32;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_data_tx   : std_logic_vector(G_SPI_DATA_W-1 downto 0);
    signal s_data_rx   : std_logic_vector(G_SPI_DATA_W-1 downto 0);
    signal s_spi_busy  : std_logic;
    signal s_spi_vld   : std_logic;
    signal s_spi_flt_n : std_logic;
    signal s_crc_req   : std_logic;
    signal s_crc_rdy   : std_logic;
    signal s_crc_data  : std_logic_vector(C_CRC_W-1 downto 0);
    signal s_crc_val   : std_logic_vector(7 downto 0);

begin

    mdl_diag : diag
        generic map (
            G_SPI_DATA_W              => G_SPI_DATA_W,
            G_CRC_DATA_W              => C_CRC_W,
            G_BITSTREAM_VERSION       => G_BITSTREAM_VERSION,
            G_BITSTREAM_TAG           => G_BITSTREAM_TAG,
            G_DESIGN_HASH             => G_DESIGN_HASH
        )
        port map (
            -- Main signals
            p_diag_clk_in             => p_diag_top_clk_in,
            p_diag_rst_n_in           => p_diag_top_rst_n_in,
            p_diag_gdrv_rdy_in        => p_diag_top_gdrv_rdy_in,
            -- Fault signals
            p_diag_flt_dt_n_in        => p_diag_top_flt_dt_n_in,
            p_diag_flt_fb_n_in        => p_diag_top_flt_fb_n_in,
            p_diag_flt_gdrv_n_in      => p_diag_top_flt_gdrv_n_in,
            p_diag_flt_pci_n_in       => p_diag_top_flt_pci_n_in,
            p_diag_flt_adc_n_in       => p_diag_top_flt_adc_n_in,
            p_diag_flt_gen_n_out      => p_diag_top_flt_gen_n_out,
            p_diag_flt_fpga_n_out     => p_diag_top_flt_fpga_n_out,
            -- CRC signals
            p_diag_crc_req_out        => s_crc_req,
            p_diag_crc_rdy_in         => s_crc_rdy,
            p_diag_crc_data_out       => s_crc_data,
            p_diag_crc_val_in         => s_crc_val,
            -- SPI data and status signals
            p_diag_spi_busy_in        => s_spi_busy,
            p_diag_spi_rx_vld_in      => s_spi_vld,
            p_diag_spi_trans_flt_n_in => s_spi_flt_n,
            p_diag_spi_data_tx_out    => s_data_tx,
            p_diag_spi_data_rx_in     => s_data_rx
        );

    mdl_spi_slave : spi_slave
        generic map (
            G_DATA_W              => G_SPI_DATA_W
        )
        port map (
            -- Main signals
            p_spi_clk_in          => p_diag_top_clk_in,
            p_spi_rst_n_in        => p_diag_top_rst_n_in,
            -- SPI signals
            p_spi_sck_in          => p_diag_top_spi_sck_in,
            p_spi_ss_n_in         => p_diag_top_spi_ss_n_in,
            p_spi_mosi_in         => p_diag_top_spi_mosi_in,
            p_spi_miso_out        => p_diag_top_spi_miso_out,
            -- TX and RX data
            p_spi_data_tx_in      => s_data_tx,
            p_spi_data_tx_out     => open,
            p_spi_data_rx_out     => s_data_rx,
            -- Status signals
            p_spi_busy_out        => s_spi_busy,
            p_spi_data_rx_vld_out => s_spi_vld,
            p_spi_trans_flt_n_out => s_spi_flt_n
        );

    mdl_crc : crc
        generic map (
            G_CRC_INPUT_SIZE => C_CRC_W-1
        )
        port map (
            -- main signals input
            p_crc_reset_n_in => p_diag_top_rst_n_in,
            p_crc_clk_in     => p_diag_top_clk_in,
            -- interface input
            p_crc_req_in     => s_crc_req,
            p_crc_data_in    => s_crc_data,
            -- interface output
            p_crc_rdy_out    => s_crc_rdy,
            p_crc_value_out  => s_crc_val
        );

end architecture structural;