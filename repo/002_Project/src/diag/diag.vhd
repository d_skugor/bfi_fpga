----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             diag.vhd
-- module name      diag - rtl
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Finite state machine for controling SPI transactions
----------------------------------------------------------------------------------------------
-- create date      10.08.2020
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Added support for metadata
--                    - Changed signal names
--
--      Revision 0.03 by david.pavlovic
--                    - Code cleanup
--                    - Removed SEM IP port
--
--      Revision 0.04 by david.pavlovic
--                    - Removed ports for PWM ROC, PWM Timeout faults
--                    - Removed port for enabling PWM signals
----------------------------------------------------------------------------------------------
-- TODO: -
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real."ceil";
use ieee.math_real."log2";

entity diag is
    generic (
        G_SPI_DATA_W              : natural                       := 16;
        G_CRC_DATA_W              : natural                       := 24;
        G_BITSTREAM_VERSION       : std_logic_vector(15 downto 0) := x"0000";
        G_BITSTREAM_TAG           : std_logic_vector( 7 downto 0) := x"00";
        G_DESIGN_HASH             : std_logic_vector(27 downto 0) := x"0000000"
    );
    port (
        -- Main signals
        p_diag_clk_in             : in  std_logic;
        p_diag_rst_n_in           : in  std_logic;
        p_diag_gdrv_rdy_in        : in  std_logic;
        -- Fault signals
        p_diag_flt_dt_n_in        : in  std_logic_vector(2 downto 0);
        p_diag_flt_fb_n_in        : in  std_logic_vector(5 downto 0);
        p_diag_flt_gdrv_n_in      : in  std_logic_vector(4 downto 0);
        p_diag_flt_pci_n_in       : in  std_logic;
        p_diag_flt_adc_n_in       : in  std_logic;
        p_diag_flt_gen_n_out      : out std_logic;
        p_diag_flt_fpga_n_out     : out std_logic;
        -- CRC signals
        p_diag_crc_req_out        : out std_logic;
        p_diag_crc_rdy_in         : in  std_logic;
        p_diag_crc_data_out       : out std_logic_vector(G_CRC_DATA_W-1 downto 0);
        p_diag_crc_val_in         : in  std_logic_vector(7 downto 0);
        -- SPI data and status signals
        p_diag_spi_busy_in        : in  std_logic;
        p_diag_spi_rx_vld_in      : in  std_logic;
        p_diag_spi_trans_flt_n_in : in  std_logic;
        p_diag_spi_data_tx_out    : out std_logic_vector(G_SPI_DATA_W-1 downto 0);
        p_diag_spi_data_rx_in     : in  std_logic_vector(G_SPI_DATA_W-1 downto 0)
    );
end entity diag;

architecture rtl of diag is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_FAULTS            : natural                               := 5;
    constant C_MEM_DW            : natural                               := 8;
    constant C_MEM_AW            : natural                               := 4;
    constant C_SPI_HEADER        : std_logic_vector( 3 downto 0)         := "0000";
    constant C_BITSTREAM_VERSION : std_logic_vector(15 downto 0)         := G_BITSTREAM_VERSION;
    constant C_BITSTREAM_TAG     : std_logic_vector( 7 downto 0)         := G_BITSTREAM_TAG;
    constant C_HASH_MSB          : std_logic_vector(15 downto 0)         := "0000" & G_DESIGN_HASH(27 downto 16);
    constant C_HASH_LSB          : std_logic_vector(15 downto 0)         := G_DESIGN_HASH(15 downto 0);
    constant C_NO_FAULTS         : std_logic_vector(C_MEM_DW-1 downto 0) := (others => '1');
    constant C_ID_BVER           : std_logic_vector(C_MEM_AW-1 downto 0) := "0000"; -- 0
    constant C_ID_HASH_MSB       : std_logic_vector(C_MEM_AW-1 downto 0) := "0001"; -- 1
    constant C_ID_HASH_LSB       : std_logic_vector(C_MEM_AW-1 downto 0) := "0010"; -- 2 (3 - 4 unused)
    constant C_ID_DT             : std_logic_vector(C_MEM_AW-1 downto 0) := "0101"; -- 5
    constant C_ID_FB             : std_logic_vector(C_MEM_AW-1 downto 0) := "0110"; -- 6
    constant C_ID_GDRV           : std_logic_vector(C_MEM_AW-1 downto 0) := "0111"; -- 7
    constant C_ID_PCI            : std_logic_vector(C_MEM_AW-1 downto 0) := "1000"; -- 8
    constant C_ID_ADC            : std_logic_vector(C_MEM_AW-1 downto 0) := "1001"; -- 9 (A - F unused)
    constant C_FLT_MEM_START     : natural                               := to_integer(unsigned(C_ID_DT));

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    type t_memory is array (C_FLT_MEM_START to 2**C_MEM_AW - 1) of std_logic_vector(C_MEM_DW - 1 downto 0);

    type t_state_diag is (
        t_state_diag_idle,
        t_state_diag_wait_data_trans_done,
        t_state_diag_wait_crc_trans_done
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk          : std_logic;
    signal s_rst_n        : std_logic;
    signal s_flt_dt_n     : std_logic_vector(2 downto 0);
    signal s_flt_fb_n     : std_logic_vector(5 downto 0);
    signal s_flt_gdrv_n   : std_logic_vector(4 downto 0);
    signal s_flt_pci_n    : std_logic;
    signal s_flt_adc_n    : std_logic;
    signal s_flt_vec_n    : std_logic_vector(C_FAULTS-1 downto 0);
    signal s_flt_mem      : t_memory;
    signal s_header       : std_logic_vector(11 downto 0);
    signal s_address      : std_logic_vector( 3 downto 0);
    signal s_address_reg  : std_logic_vector( 3 downto 0);
    signal s_address_int  : integer;
    signal s_spi_busy     : std_logic;
    signal s_spi_flt_n    : std_logic;
    signal s_spi_vld      : std_logic;
    signal s_spi_vld_re   : std_logic;
    signal s_data_tx      : std_logic_vector(G_SPI_DATA_W-1 downto 0);
    signal s_data_rx      : std_logic_vector(G_SPI_DATA_W-1 downto 0);
    signal s_crc_req      : std_logic;
    signal s_crc_rdy      : std_logic;
    signal s_crc_data     : std_logic_vector(G_CRC_DATA_W-1 downto 0);
    signal s_crc_val      : std_logic_vector(7 downto 0);
    signal s_state        : t_state_diag;
    signal s_flt_gen_n    : std_logic;
    signal s_flt_fpga_n   : std_logic;
    signal s_gdrv_rdy     : std_logic;

begin

    -- input signal assignments
    s_clk          <= p_diag_clk_in;
    s_rst_n        <= p_diag_rst_n_in;
    s_flt_dt_n     <= p_diag_flt_dt_n_in;
    s_flt_fb_n     <= p_diag_flt_fb_n_in;
    s_flt_gdrv_n   <= p_diag_flt_gdrv_n_in;
    s_flt_pci_n    <= p_diag_flt_pci_n_in;
    s_flt_adc_n    <= p_diag_flt_adc_n_in;
    s_spi_busy     <= p_diag_spi_busy_in;
    s_spi_vld      <= p_diag_spi_rx_vld_in;
    s_spi_flt_n    <= p_diag_spi_trans_flt_n_in;
    s_data_rx      <= p_diag_spi_data_rx_in;
    s_crc_rdy      <= p_diag_crc_rdy_in;
    s_crc_val      <= p_diag_crc_val_in;
    s_gdrv_rdy     <= p_diag_gdrv_rdy_in;

    -- output signal assignments
    p_diag_flt_gen_n_out   <= s_flt_gen_n;
    p_diag_flt_fpga_n_out  <= '1';
    p_diag_spi_data_tx_out <= s_data_tx;
    p_diag_crc_req_out     <= s_crc_req;
    p_diag_crc_data_out    <= s_crc_data;

    -- internal signal assignments
    s_header      <= s_data_rx(G_SPI_DATA_W-1 downto 4);
    s_address     <= s_data_rx(3 downto 0);
    s_address_int <= to_integer(unsigned(s_address));

    process (s_clk, s_rst_n) is
        begin
            if (s_rst_n = '0') then
                s_flt_vec_n <= (others => '1');
            elsif rising_edge(s_clk) then
                if (s_flt_mem(to_integer(unsigned(C_ID_DT))) /= C_NO_FAULTS) then -- PWM deadtime fault
                    s_flt_vec_n(to_integer(unsigned(C_ID_DT)) - C_FLT_MEM_START) <= '0';
                end if;
                if (s_flt_mem(to_integer(unsigned(C_ID_FB))) /= C_NO_FAULTS) then -- PWM feedback fault
                    s_flt_vec_n(to_integer(unsigned(C_ID_FB)) - C_FLT_MEM_START) <= '0';
                end if;
                if (s_flt_mem(to_integer(unsigned(C_ID_GDRV))) /= C_NO_FAULTS and s_gdrv_rdy = '1') then -- GDRV fault
                    s_flt_vec_n(to_integer(unsigned(C_ID_GDRV)) - C_FLT_MEM_START) <= '0';
                end if;
                if (s_flt_mem(to_integer(unsigned(C_ID_PCI))) /= C_NO_FAULTS) then -- PCI fault
                    s_flt_vec_n(to_integer(unsigned(C_ID_PCI)) - C_FLT_MEM_START) <= '0';
                end if;
                if (s_flt_mem(to_integer(unsigned(C_ID_ADC))) /= C_NO_FAULTS) then -- ADC fault
                    s_flt_vec_n(to_integer(unsigned(C_ID_ADC)) - C_FLT_MEM_START) <= '0';
                end if;
            end if;
        end process;

    -- rising edge detector on s_spi_vld signal
    spi_vld_ed: edge_detector_p
    generic map (
        G_REG            => 0
    )
    port map (
        -- Main signals
        p_edp_clk_in     => s_clk,
        p_edp_rst_n_in   => s_rst_n,
        -- Input signal
        p_edp_sig_in     => s_spi_vld,
        -- Output signals
        p_edp_sig_re_out => s_spi_vld_re,
        p_edp_sig_fe_out => open,
        p_edp_sig_rf_out => open
    );

    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_flt_gen_n <= '1';
        elsif rising_edge(s_clk) then
            if (s_flt_vec_n /= "11111") then
                s_flt_gen_n <= '0';
            end if;
        end if;
    end process;

    -- fault memory
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_flt_mem <= (others => (others => '1'));
        elsif rising_edge(s_clk) then
            s_flt_mem(to_integer(unsigned(C_ID_DT))) <= s_flt_mem(to_integer(unsigned(C_ID_DT))) and ("11111"   & s_flt_dt_n);
            s_flt_mem(to_integer(unsigned(C_ID_FB))) <= s_flt_mem(to_integer(unsigned(C_ID_FB))) and ("11"      & s_flt_fb_n);
            if (s_gdrv_rdy = '0') then
                s_flt_mem(to_integer(unsigned(C_ID_GDRV))) <= "111" & s_flt_gdrv_n;
            elsif (s_gdrv_rdy = '1') then
                s_flt_mem(to_integer(unsigned(C_ID_GDRV))) <= s_flt_mem(to_integer(unsigned(C_ID_GDRV))) and ("111" & s_flt_gdrv_n);
            end if;
            s_flt_mem(to_integer(unsigned(C_ID_PCI))) <= s_flt_mem(to_integer(unsigned(C_ID_PCI))) and ("1111111" & s_flt_pci_n);
            s_flt_mem(to_integer(unsigned(C_ID_ADC))) <= s_flt_mem(to_integer(unsigned(C_ID_ADC))) and ("1111111" & s_flt_adc_n);
        end if;
    end process;

    -- FSM
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_state       <= t_state_diag_idle;
            s_data_tx     <= (others => '0');
            s_address_reg <= (others => '0');
            s_crc_data    <= (others => '0');
            s_crc_req     <= '0';
            s_flt_fpga_n  <= '1';
        elsif rising_edge(s_clk) then
            s_flt_fpga_n  <= '1';
            case (s_state) is
                --------------------------------------------------------------------------------------------
                when t_state_diag_idle =>
                    -- check if SPI module reported fault during transaction and set SPI TX data to all 1's
                    if (s_spi_flt_n = '0') then
                        s_data_tx <= (others => '1');
                    -- check if first MCU read request is received
                    elsif (s_spi_vld_re = '1' and s_header = x"FFF") then
                        -- wait for next SPI transaction, store the address and start CRC
                        s_state       <= t_state_diag_wait_data_trans_done;
                        s_address_reg <= s_address;
                        s_crc_req     <= '1';
                        case (s_address) is
                            --------------------------------------------------------------------------------
                            when C_ID_BVER =>
                                s_data_tx  <= C_BITSTREAM_VERSION;
                                s_crc_data <= C_BITSTREAM_VERSION & C_BITSTREAM_TAG & x"00";
                            --------------------------------------------------------------------------------
                            when C_ID_HASH_MSB =>
                                s_data_tx  <= C_HASH_MSB;
                                s_crc_data <= C_HASH_MSB & x"0000";
                            --------------------------------------------------------------------------------
                            when C_ID_HASH_LSB =>
                                s_data_tx  <= C_HASH_LSB;
                                s_crc_data <= C_HASH_LSB & x"0000";
                            --------------------------------------------------------------------------------
                            when C_ID_DT =>
                                s_data_tx  <= C_SPI_HEADER & C_ID_DT & s_flt_mem(s_address_int);
                                s_crc_data <= C_SPI_HEADER & C_ID_DT & s_flt_mem(s_address_int) & x"0000";
                            --------------------------------------------------------------------------------
                            when C_ID_FB =>
                                s_data_tx  <= C_SPI_HEADER & C_ID_FB & s_flt_mem(s_address_int);
                                s_crc_data <= C_SPI_HEADER & C_ID_FB & s_flt_mem(s_address_int) & x"0000";
                            --------------------------------------------------------------------------------
                            when C_ID_GDRV =>
                                s_data_tx  <= C_SPI_HEADER & C_ID_GDRV & s_flt_mem(s_address_int);
                                s_crc_data <= C_SPI_HEADER & C_ID_GDRV & s_flt_mem(s_address_int) & x"0000";
                            --------------------------------------------------------------------------------
                            when C_ID_PCI =>
                                s_data_tx  <= C_SPI_HEADER & C_ID_PCI & s_flt_mem(s_address_int);
                                s_crc_data <= C_SPI_HEADER & C_ID_PCI & s_flt_mem(s_address_int) & x"0000";
                            --------------------------------------------------------------------------------
                            when C_ID_ADC =>
                                s_data_tx  <= C_SPI_HEADER & C_ID_ADC & s_flt_mem(s_address_int);
                                s_crc_data <= C_SPI_HEADER & C_ID_ADC & s_flt_mem(s_address_int) & x"0000";
                            --------------------------------------------------------------------------------
                            when others =>
                                -- stay in IDLE state and don't start CRC if address is not recognized
                                s_state   <= t_state_diag_idle;
                                s_crc_req <= '0';
                        end case;
                    end if;
                --------------------------------------------------------------------------------------------
                when t_state_diag_wait_data_trans_done =>
                    -- check if SPI module reported fault during transaction and set SPI TX data to all 1's
                    if (s_spi_flt_n = '0') then
                        s_data_tx <= (others => '1');
                    -- check if 2nd frame from MCU is received and CRC value is ready
                    elsif (s_spi_vld_re = '1' and s_crc_rdy = '1') then
                        s_state   <= t_state_diag_wait_crc_trans_done;
                        s_crc_req <= '0';
                        case (s_address_reg) is
                            --------------------------------------------------------------------------------
                            when C_ID_BVER =>
                                s_data_tx <= G_BITSTREAM_TAG & s_crc_val;
                            --------------------------------------------------------------------------------
                            when others =>
                                s_data_tx <= x"00" & s_crc_val;
                        end case;
                    end if;
                --------------------------------------------------------------------------------------------
                when others => -- t_state_diag_wait_crc_trans_done state
                    -- check if SPI module reported fault during transaction and set SPI TX data to all 1's
                    if (s_spi_flt_n = '0') then
                        s_data_tx <= (others => '1');
                    -- check if 3rd frame from MCU is received
                    elsif (s_spi_vld_re = '1') then
                        s_state   <= t_state_diag_idle;
                        s_data_tx <= (others => '0');
                    end if;
                --------------------------------------------------------------------------------------------
            end case;
        end if;
    end process;

end architecture rtl;