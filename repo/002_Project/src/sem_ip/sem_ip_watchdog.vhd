----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             sem_ip_watchdog.vhd
-- module name      sem_ip_watchdog
-- author           david.pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            SEM IP watchdog module
----------------------------------------------------------------------------------------------
-- create date      26.02.2021
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sem_ip_watchdog is

    port (
        -- main signals
        p_sem_ip_wd_clk_in                : in  std_logic;
        p_sem_ip_wd_rst_n_in              : in  std_logic;
        -- SEM IP status signals
        p_sem_ip_wd_status_heartbeat      : in  std_logic;
        p_sem_ip_wd_status_initialization : in  std_logic;
        p_sem_ip_wd_status_observation    : in  std_logic;
        p_sem_ip_wd_status_correction     : in  std_logic;
        p_sem_ip_wd_status_classification : in  std_logic;
        p_sem_ip_wd_status_injection      : in  std_logic;
        p_sem_ip_wd_status_essential      : in  std_logic;
        p_sem_ip_wd_status_uncorrectable  : in  std_logic;
        -- SEM IP ICAP grant
        p_sem_ip_wd_icap_grant_out        : out std_logic;
        -- fault output
        p_sem_ip_wd_flt_n_out             : out std_logic_vector(6 downto 0)
    );

end entity;

architecture rtl of sem_ip_watchdog is

    -- components
    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    -- constants
    constant C_BOOT_TIMEOUT           : unsigned(23 downto 0) := to_unsigned(13199999, 24); -- maximum allowed boot period is 132ms (approx. 20% more than specified in datasheet)
    constant C_INITIALIZATION_TIMEOUT : unsigned(21 downto 0) := to_unsigned(2999999, 22);  -- maximum allowed initialization period is 30ms (approx. 20% more than specified in datasheet)
    constant C_HEARTBEAT_INIT_TIMEOUT : unsigned(21 downto 0) := to_unsigned(2399999, 22);  -- heartbeat signal must start within three readback scan times (24ms)
    constant C_HEARTBEAT_TIMEOUT      : unsigned(21 downto 0) := to_unsigned(149, 22);      -- maximum number of cycles between heartbeat pulses is 150
    constant C_CORRECTION_TIMEOUT     : unsigned(26 downto 0) := to_unsigned(99999999, 27); -- SEM IP should not be in the correction state longer that 1s
    constant C_CLASSIFICATION_TIMEOUT : unsigned(26 downto 0) := to_unsigned(99999999, 27); -- SEM IP should not be in the classification state longer that 1s

    -- internal signals
    signal s_clk                      : std_logic;
    signal s_rst_n                    : std_logic;
    signal s_icap_grant               : std_logic;
    signal s_status_heartbeat         : std_logic;
    signal s_status_initialization    : std_logic;
    signal s_status_initialization_re : std_logic;
    signal s_status_initialization_fe : std_logic;
    signal s_status_observation       : std_logic;
    signal s_status_correction        : std_logic;
    signal s_status_correction_fe     : std_logic;
    signal s_status_classification    : std_logic;
    signal s_status_injection         : std_logic;
    signal s_status_essential         : std_logic;
    signal s_status_uncorrectable     : std_logic;
    signal s_status_state             : std_logic_vector(4 downto 0);
    signal s_flt_vec_n                : std_logic_vector(6 downto 0);
    signal s_boot_cnt                 : unsigned(23 downto 0);
    signal s_boot_done                : std_logic;
    signal s_boot_flt_n               : std_logic;
    signal s_initialization_cnt       : unsigned(21 downto 0);
    signal s_initialization_done      : std_logic;
    signal s_initialization_flt_n     : std_logic;
    signal s_heartbeat_cnt            : unsigned(21 downto 0);
    signal s_heartbeat_cnt_dbg        : unsigned(31 downto 0);
    signal s_heartbeat_active         : std_logic;
    signal s_heartbeat_flt_n          : std_logic;
    signal s_heartbeat_flt_n_fe       : std_logic;
    signal s_uncorrectable_flt_n      : std_logic;
    signal s_correction_cnt           : unsigned(26 downto 0);
    signal s_correction_flt_n         : std_logic;
    signal s_classification_cnt       : unsigned(26 downto 0);
    signal s_classification_flt_n     : std_logic;
    signal s_fatal_flt_n              : std_logic;

begin

    -- input signal assignments
    s_clk                   <= p_sem_ip_wd_clk_in;
    s_rst_n                 <= p_sem_ip_wd_rst_n_in;
    s_status_heartbeat      <= p_sem_ip_wd_status_heartbeat;
    s_status_initialization <= p_sem_ip_wd_status_initialization;
    s_status_observation    <= p_sem_ip_wd_status_observation;
    s_status_correction     <= p_sem_ip_wd_status_correction;
    s_status_classification <= p_sem_ip_wd_status_classification;
    s_status_injection      <= p_sem_ip_wd_status_injection;
    s_status_essential      <= p_sem_ip_wd_status_essential;
    s_status_uncorrectable  <= p_sem_ip_wd_status_uncorrectable;

    -- output signal assignments
    p_sem_ip_wd_icap_grant_out <= s_icap_grant;
    p_sem_ip_wd_flt_n_out      <= s_flt_vec_n;

    -- SEM IP watchdog fault vector
    s_flt_vec_n(0) <= s_boot_flt_n;
    s_flt_vec_n(1) <= s_initialization_flt_n;
    s_flt_vec_n(2) <= s_heartbeat_flt_n;
    s_flt_vec_n(3) <= s_uncorrectable_flt_n;
    s_flt_vec_n(4) <= s_correction_flt_n;
    s_flt_vec_n(5) <= s_classification_flt_n;
    s_flt_vec_n(6) <= s_fatal_flt_n;

    -- SEM IP state status
    s_status_state <= s_status_initialization &
                      s_status_observation    &
                      s_status_correction     &
                      s_status_classification &
                      s_status_injection;

    -- falling edge detector on s_status_initialization signal
    status_initialization_edge_detector: edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_status_initialization,
            p_edp_sig_re_out => s_status_initialization_re,
            p_edp_sig_fe_out => s_status_initialization_fe,
            p_edp_sig_rf_out => open
        );

    -- falling edge detector on s_status_correction signal
    status_correction_edge_detector: edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_status_correction,
            p_edp_sig_re_out => open,
            p_edp_sig_fe_out => s_status_correction_fe,
            p_edp_sig_rf_out => open
        );

    -- ICAP grant signal for SEM IP
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_icap_grant <= '0';
        elsif rising_edge(s_clk) then
            s_icap_grant <= '1';
        end if;
    end process;

    -- boot monitor
    -- boot time is defined as period between assertion of icap_grant signal and assertion of the s_status_initialization signal
    -- since icap_grant is hardcoded to high state, boot time starts after deasertion of the reset signal and it must not take
    -- more than C_BOOT_TIMEOUT clock cycles to boot (p69, UPG036)
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_boot_cnt   <= (others => '0');
            s_boot_done  <= '0';
            s_boot_flt_n <= '1';
        elsif rising_edge(s_clk) then
            if (s_boot_done = '0') then
                if (s_boot_cnt >= C_BOOT_TIMEOUT) then
                    s_boot_flt_n <= '0';
                elsif (s_status_initialization_re = '1') then
                    s_boot_done <= '1';
                else
                    s_boot_cnt <= s_boot_cnt + 1;
                end if;
            end if;
        end if;
    end process;

    -- initialization monitor
    -- initialzation state is active when signal s_status_initialization is high and it must not take more than C_INITIALIZATION_TIMEOUT
    -- clock cycles to initialize properly (p69; p17, Table 2-3, UPG036);
    -- initialization fault is asserted if observation state is not active immediately after the initialization state;
    -- initialization fault is asserted if both s_status_initialization and s_initialization_done are high which means that SEM IP is doing
    -- the initialization for the 2nd time
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_initialization_cnt   <= (others => '0');
            s_initialization_done  <= '0';
            s_initialization_flt_n <= '1';
        elsif rising_edge(s_clk) then
            if (s_initialization_done = '0') then
                if (s_status_initialization = '1') then
                    if (s_initialization_cnt >= C_INITIALIZATION_TIMEOUT) then
                        s_initialization_flt_n <= '0';
                    else
                        s_initialization_cnt <= s_initialization_cnt + 1;
                    end if;
                else
                    if (s_status_initialization_fe = '1' and s_status_observation = '1') then
                        s_initialization_done  <= '1';
                    elsif (s_status_initialization_fe = '1' and s_status_observation = '0') then
                        s_initialization_flt_n <= '0';
                    end if;
                end if;
            elsif (s_initialization_done = '1' and s_status_initialization = '1') then
                -- SEM IP is in the initialization state for the 2nd time
                s_initialization_flt_n <= '0';
            end if;
        end if;
    end process;

    -- heartbeat monitor
    -- SEM IP issues a single-cycle high pulse at least once every 150 clock cycles
    -- heartbeat fault is generated when there's more than 150 clock cycles between two consecutive heartbeat pulses
    -- behavior of the heartbeat signal is unspecified when s_status_observation is deasserted (p31, UPG036)
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_heartbeat_cnt    <= (others => '0');
            s_heartbeat_active <= '0';
            s_heartbeat_flt_n  <= '1';
        elsif rising_edge(s_clk) then
            if (s_status_observation = '1' and s_heartbeat_active = '0') then
                if (s_heartbeat_cnt >= C_HEARTBEAT_INIT_TIMEOUT) then
                    s_heartbeat_flt_n  <= '0';
                elsif (s_status_heartbeat = '1') then
                    s_heartbeat_active <= '1';
                    s_heartbeat_cnt    <= (others => '0');
                else
                    s_heartbeat_cnt <= s_heartbeat_cnt + 1;
                end if;
            elsif (s_status_observation = '1' and s_heartbeat_active = '1') then
                if (s_status_heartbeat = '1') then
                    s_heartbeat_cnt <= (others => '0');
                elsif (s_heartbeat_cnt >= C_HEARTBEAT_TIMEOUT) then
                    s_heartbeat_flt_n <= '0';
                else
                    s_heartbeat_cnt <= s_heartbeat_cnt + 1;
                end if;
            else
                s_heartbeat_cnt    <= (others => '0');
                s_heartbeat_active <= '0';
            end if;
        end if;
    end process;

    -- uncorrectable fault monitor
    -- uncorrectable fault is generated when there's falling edge of the s_status_correction signal simultaneously
    -- with s_status_uncorrectable being in HIGH state (p37, PG036)
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_uncorrectable_flt_n <= '1';
        elsif rising_edge(s_clk) then
            if (s_status_correction_fe = '1') then
                s_uncorrectable_flt_n <= not s_status_uncorrectable;
            end if;
        end if;
    end process;

    -- correction state monitor
    -- correction fault is generated when the controller dwells continously in correction state in excess of one second
    -- this is an uncorrectable, essential fault (p71, PG036)
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_correction_cnt   <= (others => '0');
            s_correction_flt_n <= '1';
        elsif rising_edge(s_clk) then
            if (s_status_correction = '1') then
                if (s_correction_cnt >= C_CORRECTION_TIMEOUT) then
                    s_correction_flt_n <= '0';
                else
                    s_correction_cnt <= s_correction_cnt + 1;
                end if;
            else
                s_correction_cnt <= (others => '0');
            end if;
        end if;
    end process;

    -- classification state monitor
    -- classification fault is generated when the controller dwells continously in classification state in excess of one second
    -- this is an uncorrectable, essential fault (p71, PG036)
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_classification_cnt   <= (others => '0');
            s_classification_flt_n <= '1';
        elsif rising_edge(s_clk) then
            if (s_status_classification = '1') then
                if (s_classification_cnt >= C_CLASSIFICATION_TIMEOUT) then
                    s_classification_flt_n <= '0';
                else
                    s_classification_cnt <= s_classification_cnt + 1;
                end if;
            else
                s_classification_cnt <= (others => '0');
            end if;
        end if;
    end process;

    -- fatal fault monitor
    -- fatal fault is indicated by the assertion of all five state bits on the Status Interface;
    -- this condition is non-recoverable, and the FPGA must be reconfigured (p61, UPG036)
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_fatal_flt_n <= '1';
        elsif rising_edge(s_clk) then
            if (s_status_state = "11111") then
                s_fatal_flt_n <= '0';
            end if;
        end if;
    end process;

end architecture rtl;
