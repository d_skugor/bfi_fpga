----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             sem_ip_top.vhd
-- module name      sem_ip_top
-- author           david.pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            SEM IP top module file with all additional modules requeired for proper
--                  working of SEM IP
----------------------------------------------------------------------------------------------
-- create date      09.02.2021
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

entity sem_ip_top is

    port (
        -- main signals
        p_sem_ip_top_clk_in    : in  std_logic;
        p_sem_ip_top_rst_n_in  : in  std_logic;
        -- SEM IP faults vector
        p_sem_ip_top_flt_n_out : out std_logic_vector(6 downto 0)
    );

end entity sem_ip_top;

architecture structural of sem_ip_top is

    -- components
    component sem_0
        port (
            status_heartbeat      : out std_logic;
            status_initialization : out std_logic;
            status_observation    : out std_logic;
            status_correction     : out std_logic;
            status_classification : out std_logic;
            status_injection      : out std_logic;
            status_essential      : out std_logic;
            status_uncorrectable  : out std_logic;
            monitor_txdata        : out std_logic_vector(7 downto 0);
            monitor_txwrite       : out std_logic;
            monitor_txfull        : in  std_logic;
            monitor_rxdata        : in  std_logic_vector(7 downto 0);
            monitor_rxread        : out std_logic;
            monitor_rxempty       : in  std_logic;
            icap_o                : in  std_logic_vector(31 downto 0);
            icap_csib             : out std_logic;
            icap_rdwrb            : out std_logic;
            icap_i                : out std_logic_vector(31 downto 0);
            icap_clk              : in  std_logic;
            icap_request          : out std_logic;
            icap_grant            : in  std_logic;
            fecc_crcerr           : in  std_logic;
            fecc_eccerr           : in  std_logic;
            fecc_eccerrsingle     : in  std_logic;
            fecc_syndromevalid    : in  std_logic;
            fecc_syndrome         : in  std_logic_vector(12 downto 0);
            fecc_far              : in  std_logic_vector(25 downto 0);
            fecc_synbit           : in  std_logic_vector(4 downto 0);
            fecc_synword          : in  std_logic_vector(6 downto 0)
        );
    end component;

    component sem_ip_watchdog is
      port (
          -- main signals
          p_sem_ip_wd_clk_in                : in  std_logic;
          p_sem_ip_wd_rst_n_in              : in  std_logic;
          -- SEM IP status signals
          p_sem_ip_wd_status_heartbeat      : in  std_logic;
          p_sem_ip_wd_status_initialization : in  std_logic;
          p_sem_ip_wd_status_observation    : in  std_logic;
          p_sem_ip_wd_status_correction     : in  std_logic;
          p_sem_ip_wd_status_classification : in  std_logic;
          p_sem_ip_wd_status_injection      : in  std_logic;
          p_sem_ip_wd_status_essential      : in  std_logic;
          p_sem_ip_wd_status_uncorrectable  : in  std_logic;
          -- SEM IP ICAP grant
          p_sem_ip_wd_icap_grant_out        : out std_logic;
          -- fault output
          p_sem_ip_wd_flt_n_out             : out std_logic_vector(6 downto 0)
      );
    end component;

    -- internal signals
    signal s_clk                   : std_logic;
    signal s_rst_n                 : std_logic;
    signal s_uart_tx               : std_logic;
    signal s_uart_rx               : std_logic;
    signal s_sem_ip_vec_flt_n      : std_logic_vector(6 downto 0);
    signal s_status_heartbeat      : std_logic;
    signal s_status_initialization : std_logic;
    signal s_status_observation    : std_logic;
    signal s_status_correction     : std_logic;
    signal s_status_classification : std_logic;
    signal s_status_injection      : std_logic;
    signal s_status_essential      : std_logic;
    signal s_status_uncorrectable  : std_logic;
    signal s_monitor_txdata        : std_logic_vector(7 downto 0);
    signal s_monitor_txwrite       : std_logic;
    signal s_monitor_txfull        : std_logic := '0';
    signal s_monitor_rxdata        : std_logic_vector(7 downto 0) := x"00";
    signal s_monitor_rxread        : std_logic;
    signal s_monitor_rxempty       : std_logic := '0';
    signal s_icap_o                : std_logic_vector(31 downto 0);
    signal s_icap_csib             : std_logic;
    signal s_icap_rdwrb            : std_logic;
    signal s_icap_i                : std_logic_vector(31 downto 0);
    signal s_icap_clk              : std_logic;
    signal s_icap_request_unused   : std_logic;
    signal s_icap_grant            : std_logic;
    signal s_fecc_crcerr           : std_logic;
    signal s_fecc_eccerr           : std_logic;
    signal s_fecc_eccerrsingle     : std_logic;
    signal s_fecc_syndromevalid    : std_logic;
    signal s_fecc_syndrome         : std_logic_vector(12 downto 0);
    signal s_fecc_far              : std_logic_vector(25 downto 0);
    signal s_fecc_synbit           : std_logic_vector(4 downto 0);
    signal s_fecc_synword          : std_logic_vector(6 downto 0);

begin

    --input signal assignments
    s_clk   <= p_sem_ip_top_clk_in;
    s_rst_n <= p_sem_ip_top_rst_n_in;

    -- output signal assignments
    p_sem_ip_top_flt_n_out <= s_sem_ip_vec_flt_n;

    -- Monitor interface signal assignments
    s_monitor_txfull  <= '0';
    s_monitor_rxdata  <= x"00";
    s_monitor_rxempty <= '1';

    -- SEM IP watchdog
    mdl_sem_ip_wd : sem_ip_watchdog
      port map (
          -- main signals
          p_sem_ip_wd_clk_in                => s_clk,
          p_sem_ip_wd_rst_n_in              => s_rst_n,
          -- SEM IP status signals
          p_sem_ip_wd_status_heartbeat      => s_status_heartbeat,
          p_sem_ip_wd_status_initialization => s_status_initialization,
          p_sem_ip_wd_status_observation    => s_status_observation,
          p_sem_ip_wd_status_correction     => s_status_correction,
          p_sem_ip_wd_status_classification => s_status_classification,
          p_sem_ip_wd_status_injection      => s_status_injection,
          p_sem_ip_wd_status_essential      => s_status_essential,
          p_sem_ip_wd_status_uncorrectable  => s_status_uncorrectable,
          -- SEM IP ICAP grant
          p_sem_ip_wd_icap_grant_out        => s_icap_grant,
          -- fault output
          p_sem_ip_wd_flt_n_out             => s_sem_ip_vec_flt_n
    );

    -- SEM IP
    mdl_sem_ip : sem_0
        port map (
            status_heartbeat      => s_status_heartbeat,
            status_initialization => s_status_initialization,
            status_observation    => s_status_observation,
            status_correction     => s_status_correction,
            status_classification => s_status_classification,
            status_injection      => s_status_injection,
            status_essential      => s_status_essential,
            status_uncorrectable  => s_status_uncorrectable,
            monitor_txdata        => s_monitor_txdata,
            monitor_txwrite       => s_monitor_txwrite,
            monitor_txfull        => s_monitor_txfull,
            monitor_rxdata        => s_monitor_rxdata,
            monitor_rxread        => s_monitor_rxread,
            monitor_rxempty       => s_monitor_rxempty,
            icap_o                => s_icap_o,
            icap_csib             => s_icap_csib,
            icap_rdwrb            => s_icap_rdwrb,
            icap_i                => s_icap_i,
            icap_clk              => s_clk,
            icap_request          => s_icap_request_unused,
            icap_grant            => s_icap_grant,
            fecc_crcerr           => s_fecc_crcerr,
            fecc_eccerr           => s_fecc_eccerr,
            fecc_eccerrsingle     => s_fecc_eccerrsingle,
            fecc_syndromevalid    => s_fecc_syndromevalid,
            fecc_syndrome         => s_fecc_syndrome,
            fecc_far              => s_fecc_far,
            fecc_synbit           => s_fecc_synbit,
            fecc_synword          => s_fecc_synword
        );

    frame_ecce2_inst : frame_ecce2
        generic map (
            farsrc                => "efar",        -- determines if the output of far[25:0] configuration register points
                                                    -- to the far or efar. sets configuration option register bit ctl0[7].
            frame_rbt_in_filename => "none"         -- this file is output by the icap_e2 model and it contains frame data
                                                    -- information for the raw bitstream (rbt) file. the frame_ecce2 model
                                                    -- will parse this file, calculate ecc and output any error conditions.
        )
        port map (
            crcerror       => s_fecc_crcerr,        -- 1-bit output: output indicating a crc error
            eccerror       => s_fecc_eccerr,        -- 1-bit output: output indicating an ecc error
            eccerrorsingle => s_fecc_eccerrsingle,  -- 1-bit output: output indicating single-bit frame ecc error detected
            far            => s_fecc_far,           -- 26-bit output: frame address register value output
            synbit         => s_fecc_synbit,        -- 5-bit output: output bit address of error
            syndrome       => s_fecc_syndrome,      -- 13-bit output: output location of erroneous bit
            syndromevalid  => s_fecc_syndromevalid, -- 1-bit output: frame ecc output indicating the syndrome output is valid
            synword        => s_fecc_synword        -- 7-bit output: word output in the frame where an ecc error has been detected
        );

    icape2_inst : icape2
        generic map (
            device_id         => x"03651093",       -- specifies the pre-programmed device id value to be used for simulation purposes
            icap_width        => "x32",             -- specifies the input and output data width
            sim_cfg_file_name => "none"             -- specifies the raw bitstream (rbt) file to be parsed by the simulation model
        )
        port map (
            o                 => s_icap_o,          -- 32-bit output: configuration data output bus
            clk               => s_clk,             -- 1-bit input: clock input
            csib              => s_icap_csib,       -- 1-bit input: active-low icap enable
            i                 => s_icap_i,          -- 32-bit input: configuration data input bus
            rdwrb             => s_icap_rdwrb       -- 1-bit input: read/write select input
        );

end architecture structural;