----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pwm_feedback_monitor.vhd
-- module name      pwm_feedback_monitor - rtl
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Module for monitoring PWM feedback signals
----------------------------------------------------------------------------------------------
-- create date      27.08.2020
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.03 by david.pavlovic
--                    - Removed deprecated ports
----------------------------------------------------------------------------------------------
-- TODO: -
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity pwm_feedback_monitor is
    generic (
        G_TIMEOUT             : natural := 500; -- 5 us
        G_PULSE_LENGTH_PWM    : natural := 75;  -- 500 ns
        G_PULSE_LENGTH_PWM_FB : natural := 50   -- 350 ns
    );
    port (
        -- Main signals
        p_pwm_fm_clk_in       : in  std_logic;
        p_pwm_fm_rst_n_in     : in  std_logic;
        p_pwm_fm_start_p_in   : in  std_logic;
        -- Input PWM signals
        p_pwm_fm_pwm_in       : in  std_logic_vector(5 downto 0);
        -- Input PWM feedback signals
        p_pwm_fm_pwm_fb_in    : in  std_logic_vector(5 downto 0);
        -- Output PWM fault signals
        p_pwm_fm_fb_flt_n_out : out std_logic_vector(5 downto 0)
    );
end entity;

architecture rtl of pwm_feedback_monitor is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component pwm_feedback_monitor_fsm is
        generic (
            G_TIMEOUT                 : natural := 500; -- 5 us
            G_PULSE_LENGTH_PWM        : natural := 75;  -- 500 ns
            G_PULSE_LENGTH_PWM_FB     : natural := 50   -- 350 ns
        );
        port (
            -- Main signals
            p_pwm_fm_fsm_clk_in       : in  std_logic;
            p_pwm_fm_fsm_rst_n_in     : in  std_logic;
            p_pwm_fm_fsm_start_p_in   : in  std_logic;
            -- Input PWM & PWM_FB signals
            p_pwm_fm_fsm_pwm_in       : in  std_logic;
            p_pwm_fm_fsm_pwm_fb_in    : in  std_logic;
            -- Fault signal
            p_pwm_fm_fsm_fb_flt_n_out : out std_logic
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk          : std_logic;
    signal s_rst_n        : std_logic;
    signal s_start_p      : std_logic;
    signal s_pwm          : std_logic_vector(5 downto 0);
    signal s_pwm_fb       : std_logic_vector(5 downto 0);
    signal s_pwm_fb_flt_n : std_logic_vector(5 downto 0);

begin

    -- input signal assignments
    s_clk     <= p_pwm_fm_clk_in;
    s_rst_n   <= p_pwm_fm_rst_n_in;
    s_start_p <= p_pwm_fm_start_p_in;
    s_pwm     <= p_pwm_fm_pwm_in;
    s_pwm_fb  <= p_pwm_fm_pwm_fb_in;

    -- output signal assignments
    p_pwm_fm_fb_flt_n_out <= s_pwm_fb_flt_n;

    -- FSM instances
    fsm_gen:
    for i in 0 to 5 generate
        fsm_i : pwm_feedback_monitor_fsm
            generic map (
                G_TIMEOUT                 => G_TIMEOUT,
                G_PULSE_LENGTH_PWM        => G_PULSE_LENGTH_PWM,
                G_PULSE_LENGTH_PWM_FB     => G_PULSE_LENGTH_PWM_FB
            )
            port map (
                p_pwm_fm_fsm_clk_in       => s_clk,
                p_pwm_fm_fsm_rst_n_in     => s_rst_n,
                p_pwm_fm_fsm_start_p_in   => s_start_p,
                p_pwm_fm_fsm_pwm_in       => s_pwm(i),
                p_pwm_fm_fsm_pwm_fb_in    => s_pwm_fb(i),
                p_pwm_fm_fsm_fb_flt_n_out => s_pwm_fb_flt_n(i)
            );
    end generate;

end architecture rtl;
