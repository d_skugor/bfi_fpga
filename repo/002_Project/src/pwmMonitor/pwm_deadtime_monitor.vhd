----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pwm_deadtime_monitor.vhd
-- module name      pwm_deadtime_monitor - rtl
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Module for monitoring PWM waveforms
----------------------------------------------------------------------------------------------
-- create date      26.08.2020
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Added generics for deadtime measurement
--                    - Added XPM macro for synchronization
--
--      Revision 0.03 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.04 by david.pavlovic
--                    - Removed deprecated ports
----------------------------------------------------------------------------------------------
-- TODO: -
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real."ceil";
use ieee.math_real."log2";

library xpm;
use xpm.vcomponents.all;

entity pwm_deadtime_monitor is
    generic (
        G_DEADTIME             : natural := 300; -- 3 us
        G_DELTA                : natural := 5
    );
    port (
        -- Main signals
        p_pwm_dtm_clk_in       : in  std_logic;
        p_pwm_dtm_rst_n_in     : in  std_logic;
        p_pwm_dtm_start_p_in   : in  std_logic;
        -- MCU PWM input signals
        p_pwm_dtm_pwm_in       : in  std_logic_vector(5 downto 0);
        -- Faults
        p_pwm_dtm_flt_dt_n_out : out std_logic_vector(2 downto 0)
    );
end entity pwm_deadtime_monitor;

architecture rtl of pwm_deadtime_monitor is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component pwm_deadtime_monitor_fsm is
        generic (
            G_DEADTIME               : natural := 300;
            G_DELTA                  : natural := 5
        );
        port (
            -- Main signals
            p_pwm_dtm_fsm_clk_in     : in  std_logic;
            p_pwm_dtm_fsm_rst_n_in   : in  std_logic;
            p_pwm_dtm_fsm_start_p_in : in  std_logic;
            -- Input PWM signals
            p_pwm_dtm_fsm_hs         : in  std_logic;
            p_pwm_dtm_fsm_ls         : in  std_logic;
            -- Output fault
            p_pwm_dtm_fsm_flt_n_out  : out std_logic
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk      : std_logic;
    signal s_rst_n    : std_logic;
    signal s_start_p  : std_logic;
    signal s_en       : std_logic;
    signal s_pwm      : std_logic_vector(5 downto 0);
    signal s_flt_dt_n : std_logic_vector(2 downto 0);

begin

    s_clk     <= p_pwm_dtm_clk_in;
    s_rst_n   <= p_pwm_dtm_rst_n_in;
    s_start_p <= p_pwm_dtm_start_p_in;
    s_pwm     <= p_pwm_dtm_pwm_in;

    -- output assignments
    p_pwm_dtm_flt_dt_n_out <= s_flt_dt_n;

    --Deadtime monitor instances
    dt_gen:
    for i in 0 to 2 generate
        dt_i : pwm_deadtime_monitor_fsm
            generic map (
                G_DEADTIME               => G_DEADTIME,
                G_DELTA                  => G_DELTA
            )
            port map (
                -- Main signals
                p_pwm_dtm_fsm_clk_in     => s_clk,
                p_pwm_dtm_fsm_rst_n_in   => s_rst_n,
                p_pwm_dtm_fsm_start_p_in => s_start_p,
                -- Input PWM signals
                p_pwm_dtm_fsm_hs         => s_pwm(2*i+1),
                p_pwm_dtm_fsm_ls         => s_pwm(2*i),
                -- Output fault
                p_pwm_dtm_fsm_flt_n_out  => s_flt_dt_n(i)
            );
    end generate;

end architecture rtl;