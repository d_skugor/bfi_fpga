----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pwm_deadtime_monitor_fsm.vhd
-- module name      pwm_deadtime_monitor_fsm - rtl
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Module for monitoring PWM deadtime timing
----------------------------------------------------------------------------------------------
-- create date      10.11.2020
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Added logicfor start of PWM deadtime monitoring
--
--      Revision 0.03 by danijela.skugor
--                    - Changes in accordance with changed gray_counter component:
--                          -- reset changed from active-high to active-low
--                          -- update of port and generic names of gray_counter
--
--      Revision 0.04 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.05 by david.pavlovic
--                    - Changed name to pwm_deadtime_monitor_fsm
----------------------------------------------------------------------------------------------
-- TODO: -
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real."ceil";
use ieee.math_real."log2";

entity pwm_deadtime_monitor_fsm is
    generic (
        G_DEADTIME               : natural := 300;
        G_DELTA                  : natural := 5
    );
    port (
        -- Main signals
        p_pwm_dtm_fsm_clk_in     : in  std_logic;
        p_pwm_dtm_fsm_rst_n_in   : in  std_logic;
        p_pwm_dtm_fsm_start_p_in : in  std_logic;
        -- Input PWM signals
        p_pwm_dtm_fsm_hs         : in  std_logic;
        p_pwm_dtm_fsm_ls         : in  std_logic;
        -- Output fault
        p_pwm_dtm_fsm_flt_n_out  : out std_logic
    );
end entity;

architecture rtl of pwm_deadtime_monitor_fsm is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component edge_detector_p
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    component gray_counter is
        generic (
            G_GRAYCNT_WIDTH         : integer := 4;
            G_GRAYCNT_CORRECTION_ON : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port (
            p_grayCnt_clk_in        : in std_logic;
            p_grayCnt_rst_n_in      : in std_logic;     -- sync reset
            p_grayCnt_en_in         : in std_logic;     -- enable
            p_grayCnt_err_p_out     : out std_logic;    -- pulsed error
            p_grayCnt_res_out       : out std_logic_vector(G_GRAYCNT_WIDTH -1 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------

    function bin_to_gray (bin : std_logic_vector) return std_logic_vector is
        variable v_gray : std_logic_vector(bin'range);
    begin
        v_gray(bin'left) := bin(bin'left);
        for i in bin'left-1 downto 0 loop
            v_gray(i) := bin(i+1) xor bin(i);
        end loop;
        return v_gray;
    end function bin_to_gray;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_CNT_VAL   : integer                              := G_DEADTIME - G_DELTA;
    constant C_CNT_W     : integer                              := integer(ceil(log2(real(C_CNT_VAL))));
    constant C_CNT_VAL_B : std_logic_vector(C_CNT_W-1 downto 0) := std_logic_vector(to_unsigned(C_CNT_VAL-1, C_CNT_W));
    constant C_CNT_VAL_G : std_logic_vector(C_CNT_W-1 downto 0) := bin_to_gray(C_CNT_VAL_B);

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    type t_state is (
        t_state_idle,
        t_state_ready,
        t_state_wait_hs_re,
        t_state_wait_ls_re,
        t_state_fault
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk        : std_logic;
    signal s_rst_n      : std_logic;
    signal s_start_p    : std_logic;
    signal s_hs         : std_logic;
    signal s_hs_re      : std_logic;
    signal s_hs_re_d    : std_logic;
    signal s_hs_fe      : std_logic;
    signal s_ls         : std_logic;
    signal s_ls_re      : std_logic;
    signal s_ls_re_d    : std_logic;
    signal s_ls_fe      : std_logic;
    signal s_hs_ls_high : std_logic;
    signal s_state      : t_state;
    signal s_cnt        : std_logic_vector(C_CNT_W-1 downto 0);
    signal s_cnt_clr    : std_logic;
    signal s_cnt_en     : std_logic;
    signal s_cnt_err    : std_logic;
    signal s_cnt_rst_n  : std_logic;
    signal s_flt_n      : std_logic;

begin

    -- input signal assignments
    s_clk     <= p_pwm_dtm_fsm_clk_in;
    s_rst_n   <= p_pwm_dtm_fsm_rst_n_in;
    s_start_p <= p_pwm_dtm_fsm_start_p_in;
    s_hs      <= p_pwm_dtm_fsm_hs;
    s_ls      <= p_pwm_dtm_fsm_ls;

    -- output signal assignment
    p_pwm_dtm_fsm_flt_n_out <= s_flt_n;

    -- rising and falling edge detector for HS signal
    mdl_ed_hs : edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_hs,
            p_edp_sig_re_out => s_hs_re,
            p_edp_sig_fe_out => s_hs_fe,
            p_edp_sig_rf_out => open
        );

    -- rising and falling edge detector for LS signal
    mdl_ed_ls : edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_ls,
            p_edp_sig_re_out => s_ls_re,
            p_edp_sig_fe_out => s_ls_fe,
            p_edp_sig_rf_out => open
        );

    -- Gray counter
    mdl_gray_cnt : gray_counter
        generic map (
            G_GRAYCNT_WIDTH         => C_CNT_W,
            G_GRAYCNT_CORRECTION_ON => true
        )
        port map (
            p_grayCnt_clk_in        => s_clk,
            p_grayCnt_rst_n_in      => s_cnt_rst_n,
            p_grayCnt_en_in         => s_cnt_en,
            p_grayCnt_err_p_out     => s_cnt_err,
            p_grayCnt_res_out       => s_cnt
        );

    -- Gray counter reset and enable signals
    s_cnt_rst_n  <= s_rst_n and (not s_cnt_clr);

    -- check if both HS and LS signals are high
    s_hs_ls_high <= s_hs and s_ls;

    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_hs_re_d <= '0';
            s_ls_re_d <= '0';
        elsif rising_edge(s_clk) then
            s_hs_re_d <= s_hs_re;
            s_ls_re_d <= s_ls_re;
        end if;
    end process;

    -- FSM
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_state   <= t_state_idle;
            s_cnt_clr <= '0';
            s_cnt_en  <= '0';
            s_flt_n   <= '1';
        elsif rising_edge(s_clk) then
            -- default values
            s_cnt_clr <= '0';
            s_cnt_en  <= '1';
            s_flt_n   <= '1';
            case s_state is
                ------------------------------------------------
                when t_state_idle =>
                    s_cnt_en  <= '0';
                    if (s_hs_ls_high = '1') then
                        s_state   <= t_state_fault;
                        s_flt_n   <= '0';
                    elsif (s_start_p = '1') then
                        s_state <= t_state_ready;
                    end if;
                ------------------------------------------------
                when t_state_ready =>
                    s_cnt_en  <= '0';
                    if (s_hs_ls_high = '1') then
                        s_state   <= t_state_fault;
                        s_flt_n   <= '0';
                    elsif (s_ls_fe = '1') then
                        s_state   <= t_state_wait_hs_re;
                        s_cnt_en  <= '1';
                    elsif (s_hs_fe = '1') then
                        s_state   <= t_state_wait_ls_re;
                        s_cnt_en  <= '1';
                    end if;
                ------------------------------------------------
                when t_state_wait_hs_re =>
                    if (s_hs_ls_high = '1') then
                        s_state   <= t_state_fault;
                        s_cnt_en  <= '0';
                        s_flt_n   <= '0';
                    elsif (s_cnt = C_CNT_VAL_G) then
                        s_state   <= t_state_ready;
                        s_cnt_clr <= '1';
                        s_cnt_en  <= '0';
                    elsif (s_hs_re = '1' or s_hs_re_d = '1') then
                        s_state   <= t_state_fault;
                        s_cnt_en  <= '0';
                        s_flt_n   <= '0';
                    end if;
                ------------------------------------------------
                when t_state_wait_ls_re =>
                    if (s_hs_ls_high = '1') then
                        s_state   <= t_state_fault;
                        s_cnt_en  <= '0';
                        s_flt_n   <= '0';
                    elsif (s_cnt = C_CNT_VAL_G) then
                        s_state   <= t_state_ready;
                        s_cnt_clr <= '1';
                        s_cnt_en  <= '0';
                    elsif (s_ls_re = '1' or s_ls_re_d = '1') then
                        s_state   <= t_state_fault;
                        s_cnt_en  <= '0';
                        s_flt_n   <= '0';
                    end if;
                ------------------------------------------------
                when others =>
                    -- t_state_fault state
                    s_cnt_en <= '0';
                    s_flt_n  <= '0';
                ------------------------------------------------
            end case;
        end if;
    end process;

end architecture rtl;