----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pwm_monitor.vhd
-- module name      PWM Monitor - Behavioral
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief
----------------------------------------------------------------------------------------------
-- create date     11.09.2019
-- Revision:
--      Revision 0.01 - File Created -
--
--      Revision 0.02 by david.pavlovic
--                    - Code cleanup
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity pwm_check_cycles is
    generic (
        G_NUM_OF_CYCLES    : integer := 4;
        G_FROZEN_CYCLE_DEV : integer := 1;
        G_MAX_PWM_PERIOD   : integer := 100000; -- 1 kHz -> period 1 000 000 ns/10 ns = 100 000
        G_SYSTEM_PERIOD    : integer := 10      -- 10 ns
    );
    port(
        p_pwmMon_clk_in    : in  std_logic;     -- module clock
        p_pwmMon_rst_n_in  : in  std_logic;     -- main reset / active low
        -- PWM line inputs
        p_pwmMon_hs_in     : in std_logic;      -- pwm input hs line
        p_pwmMon_ls_in     : in std_logic;      -- pwm input ls line
        -- Gray counter error
        p_pwmMon_gCErr_out : out std_logic;     -- gray counter error - pulsed signal
        -- PWM error output
        p_pwmMon_err_out   : out std_logic      -- pulsed error signal
    );
end pwm_check_cycles;

architecture structural of pwm_check_cycles is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component gray_counter is
        generic (
            G_WIDTH            : integer := 4;
            G_CORRECTION_ON    : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port (
            p_grayCnt_clk_in   : in  std_logic;
            p_grayCnt_rst_in   : in  std_logic;    -- sync reset
            p_grayCnt_en_in    : in  std_logic;    -- enable
            p_grayCnt_err_out  : out std_logic;    -- pulsed error
            p_grayCnt_res_out  : out std_logic_vector(G_WIDTH -1 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_PWM_MAX_CLOCKS : integer := (G_MAX_PWM_PERIOD/G_SYSTEM_PERIOD); -- maximum period

    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------

    function gray_to_bin ( gray : std_logic_vector ) return std_logic_vector is
        variable v_temp : std_logic_vector(gray'range);
    begin
        v_temp(gray'left) := gray(gray'left);
        for i in gray'left - 1 downto 0 loop
            v_temp(i) := gray(i) xor v_temp(i+1);
        end loop;
        return v_temp;
    end gray_to_bin;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    -- interal pwm input signals
    signal s_hs_i                : std_logic;
    signal s_ls_i                : std_logic;
    signal s_hs_t_i              : std_logic;
    signal s_ls_t_i              : std_logic;

    signal s_cnt_period_start_d  : std_logic;
    signal s_cnt_period_start_dd : std_logic;

    -- signals to count num of clocks in pwm signals
    signal s_cnt_period_en       : std_logic;
    signal s_cnt_period_clr      : std_logic;
    signal s_clock_ls_cnt        : std_logic_vector(15 downto 0);

    signal s_ls_rising           : std_logic;
    signal s_cycle_stop          : std_logic;
    signal s_cycle_stop_d        : std_logic;

    signal s_pwm_period          : std_logic_vector(15 downto 0) :=  std_logic_vector(to_unsigned(C_PWM_MAX_CLOCKS, 16));
    signal s_pwm_period_tmp      : std_logic_vector(15 downto 0) :=  std_logic_vector(to_unsigned(C_PWM_MAX_CLOCKS-2, 16));

    signal s_cycle_cnt           : std_logic_vector(3 downto 0);
    signal s_cycle_cnt_en        : std_logic;
    signal s_cycle_cnt_clr       : std_logic;

    signal s_clk_cnt             : std_logic_vector(15 downto 0);
    signal s_clk_cnt_en          : std_logic;
    signal s_clk_cnt_clr         : std_logic;

    signal s_cycle_err           : std_logic;

    signal s_gC_err1             : std_logic;
    signal s_gC_err2             : std_logic;
    signal s_gC_err3             : std_logic;

begin

    -- syncronize pwm lines with system clock for sync monitoring
    -- input signal sampled two times due to metastability
    process (p_pwmMon_clk_in)
    begin
        if rising_edge(p_pwmMon_clk_in) then
            s_hs_t_i <= p_pwmMon_hs_in;
            s_ls_t_i <= p_pwmMon_ls_in;
            s_hs_i   <= s_hs_t_i;
            s_ls_i   <= s_ls_t_i;
        end if;
    end process;

    -- syncronize enable signal for counting period with input pwm signal
    process (p_pwmMon_clk_in)
    begin
        if rising_edge(p_pwmMon_clk_in) then
            if (p_pwmMon_rst_n_in = '0') then
                s_cnt_period_start_d  <= '0';
                s_cnt_period_start_dd <= '0';
                s_cnt_period_en       <= '0';
            else
                s_cnt_period_start_d  <= '1';
                s_cnt_period_start_dd <= s_cnt_period_start_d;
                s_cnt_period_en       <= s_cnt_period_start_dd;
            end if;
        end if;
    end process;

    -- sig_pwm_period used to check if period count is beyond max
    process (p_pwmMon_clk_in)
    begin
        if rising_edge(p_pwmMon_clk_in) then
            if (p_pwmMon_rst_n_in = '0') then
                s_pwm_period    <=  std_logic_vector(to_unsigned(C_PWM_MAX_CLOCKS, 16));
            else
                if (s_cnt_period_clr = '1') then
                    s_pwm_period <= s_pwm_period_tmp + 2; -- add 2 extra clock cycles to get full value of period
                end if;
            end if;
        end if;
    end process;

    -- generate signal to stop counting pwm period
    process (p_pwmMon_clk_in)
    begin
        if rising_edge(p_pwmMon_clk_in) then
            if (p_pwmMon_rst_n_in = '0') then
                s_cycle_stop <= '0';
            else
                if (s_hs_i = '0' and s_ls_i = '1') then
                    s_cycle_stop <= '1';
                else
                    s_cycle_stop <= '0';
                end if;
            end if;
        end if;
    end process;

    process (p_pwmMon_clk_in)
    begin
        if rising_edge(p_pwmMon_clk_in) then
            if (p_pwmMon_rst_n_in = '0') then
                s_cycle_stop_d <= '0';
                s_pwm_period_tmp <=  (others => '0' );
            else
                s_cycle_stop_d <= s_cycle_stop;
                s_pwm_period_tmp <= gray_to_bin(s_clock_ls_cnt);
            end if;
        end if;
    end process;

    s_ls_rising <= s_cycle_stop and not  s_cycle_stop_d;
    s_cnt_period_clr <= not s_cycle_stop and s_cycle_stop_d;

    -- count number of clocks in period
    ls_gray_cnt_block : gray_counter
        generic map (
            G_WIDTH           => 16,
            G_CORRECTION_ON   => true
        )
        port map (
            p_grayCnt_clk_in  => p_pwmMon_clk_in,
            p_grayCnt_rst_in  => s_cnt_period_clr,
            p_grayCnt_en_in   => s_cnt_period_en,
            p_grayCnt_err_out => s_gC_err1,
            p_grayCnt_res_out => s_clock_ls_cnt
        );

    --enable signal for counting ticks every system period
    process (p_pwmMon_clk_in )
    begin
        if (rising_edge(p_pwmMon_clk_in)) then
            if (p_pwmMon_rst_n_in = '0') then
                s_clk_cnt_en <= '0';
            else
                if (s_cnt_period_clr = '1') then
                    s_clk_cnt_en <= '0';
                else
                    if (gray_to_bin(s_clk_cnt)  < s_pwm_period ) then
                        s_clk_cnt_en <= '1';
                    else
                        s_clk_cnt_en <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- check if cycles are active
    process(p_pwmMon_clk_in)
    begin
        if rising_edge(p_pwmMon_clk_in) then
            if (p_pwmMon_rst_n_in = '0') then
                s_cycle_cnt_en <= '0';
            else
                if (gray_to_bin(s_clk_cnt)  < s_pwm_period ) then
                    s_cycle_cnt_en <= '0';
                else
                    s_cycle_cnt_en <= '1';
                end if;
           end if;
        end if;
    end process;

    -- restart counting after sig_clk_cnt_en goes to zero
    s_clk_cnt_clr <= not s_clk_cnt_en;

    -- count ls line number of ticks
    clk_counter_block : gray_counter
        generic map (
            G_WIDTH            => 16,
            G_CORRECTION_ON    => true
        )
        port map (
            p_grayCnt_clk_in   => p_pwmMon_clk_in,
            p_grayCnt_rst_in   => s_clk_cnt_clr,
            p_grayCnt_en_in    => s_clk_cnt_en,
            p_grayCnt_err_out  => s_gC_err2,
            p_grayCnt_res_out  => s_clk_cnt
        );

    -- reset counting cycles when change od pwm signals occurs- this means that lines are not frozen any more
    s_cycle_cnt_clr <= s_cnt_period_clr or s_ls_rising;

    cycle_counter_block : gray_counter
        generic map (
           G_WIDTH            => 4,
           G_CORRECTION_ON    => true
        )
        port map (
            p_grayCnt_clk_in  => p_pwmMon_clk_in,
            p_grayCnt_rst_in  => s_cycle_cnt_clr,
            p_grayCnt_en_in   => s_cycle_cnt_en,
            p_grayCnt_err_out => s_gC_err3,
            p_grayCnt_res_out => s_cycle_cnt
        );

    -- generate error signal high if pwm lines are frozen for more than gen_num_of_cycles + gen_frozen_cycle_dev cycles
    process(p_pwmMon_clk_in)
    begin
        if rising_edge(p_pwmMon_clk_in) then
            if (p_pwmMon_rst_n_in = '0') then
                s_cycle_err <= '0';
            else
                if ( gray_to_bin(s_cycle_cnt) >= G_NUM_OF_CYCLES + G_FROZEN_CYCLE_DEV) then
                    s_cycle_err <= '1';
                else
                    s_cycle_err <= '0';
                end if;
            end if;
        end if;
    end process;

    -- proces Gray counter errors output
    p_pwmMon_gCErr_out <= s_gC_err1 or s_gC_err2 or s_gC_err3;

    --process  error output (pulsed)
    p_pwmMon_err_out <= s_cycle_err;

end structural;
