----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pwm_feedback_monitor_fsm.vhd
-- module name      pwm_feedback_monitor_fsm - rtl
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            FSM module for monitoring PWM feedback signals
----------------------------------------------------------------------------------------------
-- create date      27.08.2020
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Added logic for start of PWM feedback monitoring
--
--      Revision 0.03 by danijela.skugor
--                    - Changes in accordance with changed gray_counter component:
--                          -- reset changed from active-high to active-low
--                          -- update of port and generic names of gray_counter
--
--      Revision 0.04 by david.pavlovic
--                    - Code cleanup
----------------------------------------------------------------------------------------------
-- TODO: -
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real."ceil";
use ieee.math_real."log2";

entity pwm_feedback_monitor_fsm is
    generic (
        G_TIMEOUT                 : natural := 500; -- 5 us
        G_PULSE_LENGTH_PWM        : natural := 75;  -- 500 ns
        G_PULSE_LENGTH_PWM_FB     : natural := 50   -- 350 ns
    );
    port (
        -- Main signals
        p_pwm_fm_fsm_clk_in       : in  std_logic;
        p_pwm_fm_fsm_rst_n_in     : in  std_logic;
        p_pwm_fm_fsm_start_p_in   : in  std_logic;
        -- Input PWM & PWM_FB signals
        p_pwm_fm_fsm_pwm_in       : in  std_logic;
        p_pwm_fm_fsm_pwm_fb_in    : in  std_logic;
        -- Fault signal
        p_pwm_fm_fsm_fb_flt_n_out : out std_logic
    );
end entity;

architecture rtl of pwm_feedback_monitor_fsm is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    component gray_counter is
        generic (
            G_GRAYCNT_WIDTH         : integer := 4;
            G_GRAYCNT_CORRECTION_ON : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port (
            p_grayCnt_clk_in        : in std_logic;
            p_grayCnt_rst_n_in      : in std_logic;     -- sync reset
            p_grayCnt_en_in         : in std_logic;     -- enable
            p_grayCnt_err_p_out     : out std_logic;    -- pulsed error
            p_grayCnt_res_out       : out std_logic_vector(G_GRAYCNT_WIDTH - 1 downto 0)
        );
    end component;

    component pulse_filter is
        generic (
            G_PULSE_FILTER_LENGTH   : natural := 10
        );
        port (
            -- main signals
            p_pulse_filter_clk_in   : in  std_logic;
            p_pulse_filter_rst_n_in : in  std_logic;
            p_pulse_filter_en_in    : in  std_logic;
            -- input signal
            p_pulse_filter_sig_in   : in  std_logic;
            p_pulse_filter_sig_out  : out std_logic
        );
    end component;

    component delay_line is
        generic (
            G_DELAY_LINE_WIDTH    : integer := 8;
            G_DELAY_LINE_LENGTH   : integer := 2
        );
        port (
            p_delay_line_rst_n_in :  in std_logic;
            p_delay_line_clk_in   :  in std_logic;
            p_delay_line_en_in    :  in std_logic;
            p_delay_line_data_in  :  in std_logic_vector(G_DELAY_LINE_WIDTH - 1 downto 0);
            p_delay_line_data_out : out std_logic_vector(G_DELAY_LINE_WIDTH - 1 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------

    function bin_to_gray (bin : std_logic_vector) return std_logic_vector is
        variable v_gray : std_logic_vector(bin'range);
    begin
        v_gray(bin'left) := bin(bin'left);
        for i in bin'left-1 downto 0 loop
            v_gray(i) := bin(i+1) xor bin(i);
        end loop;
        return v_gray;
    end function bin_to_gray;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_TIMEOUT_PWM   : integer                              := 12500; --125us
    constant C_CNT_W         : integer                              := integer(ceil(log2(real(C_TIMEOUT_PWM))));
    constant C_TIMEOUT_PWM_B : std_logic_vector(C_CNT_W-1 downto 0) := std_logic_vector(to_unsigned(C_TIMEOUT_PWM-1, C_CNT_W));
    constant C_TIMEOUT_PWM_G : std_logic_vector(C_CNT_W-1 downto 0) := bin_to_gray(C_TIMEOUT_PWM_B);
    constant C_TIMEOUT_B     : std_logic_vector(C_CNT_W-1 downto 0) := std_logic_vector(to_unsigned(G_TIMEOUT-1, C_CNT_W));
    constant C_TIMEOUT_G     : std_logic_vector(C_CNT_W-1 downto 0) := bin_to_gray(C_TIMEOUT_B);
    constant C_PWM_FB_DELAY  : natural                              := G_PULSE_LENGTH_PWM - G_PULSE_LENGTH_PWM_FB;

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    type t_state_re is (
        t_state_re_pwm_idle,
        t_state_re_pwm_ready,
        t_state_re_pwm_wait_pwm_re,
        t_state_re_pwm_wait_fb_re,
        t_state_re_pwm_flt
    );

    type t_state_fe is (
        t_state_fe_pwm_idle,
        t_state_fe_pwm_ready,
        t_state_fe_pwm_wait_pwm_fe,
        t_state_fe_pwm_wait_fb_fe,
        t_state_fe_pwm_flt
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk            : std_logic;
    signal s_rst_n          : std_logic;
    signal s_start_p        : std_logic;
    signal s_pwm            : std_logic;
    signal s_pwm_in         : std_logic;
    signal s_pwm_re         : std_logic;
    signal s_pwm_fe         : std_logic;
    signal s_pwm_fb         : std_logic;
    signal s_pwm_fb_in      : std_logic;
    signal s_pwm_fb_in_filt : std_logic;
    signal s_pwm_fb_re      : std_logic;
    signal s_pwm_fb_fe      : std_logic;
    signal s_state_re       : t_state_re;
    signal s_pwm_flt_re_n   : std_logic;
    signal s_cnt_re         : std_logic_vector(C_CNT_W-1 downto 0);
    signal s_cnt_rst_n_re   : std_logic;
    signal s_cnt_clr_re     : std_logic;
    signal s_cnt_en_re      : std_logic;
    signal s_state_fe       : t_state_fe;
    signal s_pwm_flt_fe_n   : std_logic;
    signal s_cnt_fe         : std_logic_vector(C_CNT_W-1 downto 0);
    signal s_cnt_rst_n_fe   : std_logic;
    signal s_cnt_clr_fe     : std_logic;
    signal s_cnt_en_fe      : std_logic;

begin

    -- internal signal assignments
    s_clk       <= p_pwm_fm_fsm_clk_in;
    s_rst_n     <= p_pwm_fm_fsm_rst_n_in;
    s_start_p   <= p_pwm_fm_fsm_start_p_in;
    s_pwm_in    <= p_pwm_fm_fsm_pwm_in;
    s_pwm_fb_in <= p_pwm_fm_fsm_pwm_fb_in;

    -- output register
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            p_pwm_fm_fsm_fb_flt_n_out <= '1';
        elsif rising_edge(s_clk) then
            p_pwm_fm_fsm_fb_flt_n_out <= s_pwm_flt_re_n and s_pwm_flt_fe_n;
        end if;
    end process;

    -- Gray counter reset signals
    s_cnt_rst_n_re <= s_rst_n and (not s_cnt_clr_re);
    s_cnt_rst_n_fe <= s_rst_n and (not s_cnt_clr_fe);

    -- pulse filters
    mdl_pulse_filter_pwm : pulse_filter
        generic map (
            G_PULSE_FILTER_LENGTH   => G_PULSE_LENGTH_PWM
        )
        port map (
            -- main signals
            p_pulse_filter_clk_in   => s_clk,
            p_pulse_filter_rst_n_in => s_rst_n,
            p_pulse_filter_en_in    => '1',
            -- input signal
            p_pulse_filter_sig_in   => s_pwm_in,
            p_pulse_filter_sig_out  => s_pwm
        );

    mdl_pulse_filter_pwm_fb : pulse_filter
        generic map (
            G_PULSE_FILTER_LENGTH   => G_PULSE_LENGTH_PWM_FB
        )
        port map (
            -- main signals
            p_pulse_filter_clk_in   => s_clk,
            p_pulse_filter_rst_n_in => s_rst_n,
            p_pulse_filter_en_in    => '1',
            -- input signal
            p_pulse_filter_sig_in   => s_pwm_fb_in,
            p_pulse_filter_sig_out  => s_pwm_fb_in_filt
        );

    mdl_delay_line_pwm_fb : delay_line
        generic map (
            G_DELAY_LINE_WIDTH        => 1,
            G_DELAY_LINE_LENGTH       => C_PWM_FB_DELAY
        )
        port map (
            p_delay_line_rst_n_in     => s_rst_n,
            p_delay_line_clk_in       => s_clk,
            p_delay_line_en_in        => '1',
            p_delay_line_data_in(0)   => s_pwm_fb_in_filt,
            p_delay_line_data_out(0)  => s_pwm_fb
        );

    -- edge detectors
    mdl_ed_pwm : edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_pwm,
            p_edp_sig_re_out => s_pwm_re,
            p_edp_sig_fe_out => s_pwm_fe,
            p_edp_sig_rf_out => open
        );

    mdl_ed_pwm_fb : edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_pwm_fb,
            p_edp_sig_re_out => s_pwm_fb_re,
            p_edp_sig_fe_out => s_pwm_fb_fe,
            p_edp_sig_rf_out => open
        );

    -- Gray counters
    mdl_g_cnt_re : gray_counter
        generic map (
           G_GRAYCNT_WIDTH         => s_cnt_re'length,
           G_GRAYCNT_CORRECTION_ON => true
        )
        port map (
            p_grayCnt_clk_in    => s_clk,
            p_grayCnt_rst_n_in  => s_cnt_rst_n_re,
            p_grayCnt_en_in     => s_cnt_en_re,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_cnt_re
        );

    mdl_g_cnt_fe : gray_counter
        generic map (
           G_GRAYCNT_WIDTH         => s_cnt_fe'length,
           G_GRAYCNT_CORRECTION_ON => true
        )
        port map (
            p_grayCnt_clk_in    => s_clk,
            p_grayCnt_rst_n_in  => s_cnt_rst_n_fe,
            p_grayCnt_en_in     => s_cnt_en_fe,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_cnt_fe
        );

    -- FSM process
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_cnt_clr_re <= '0';
            s_cnt_en_re  <= '0';
            s_pwm_flt_re_n <= '1';
        elsif rising_edge(s_clk) then
            s_cnt_clr_re <= '0';
            s_cnt_en_re  <= '1';
            s_pwm_flt_re_n <= '1';
            case (s_state_re) is
                -----------------------------------------------------------------
                when t_state_re_pwm_idle =>
                    s_cnt_en_re <= '0';
                    if (s_start_p = '1') then
                        s_state_re <= t_state_re_pwm_ready;
                    end if;
                -----------------------------------------------------------------
                when t_state_re_pwm_ready =>
                    s_cnt_en_re <= '0';
                    if (s_pwm_re = '1') then
                        s_state_re   <= t_state_re_pwm_wait_fb_re;
                        s_cnt_en_re  <= '1';
                    end if;
                -----------------------------------------------------------------
                when t_state_re_pwm_wait_fb_re =>
                    if (s_cnt_re = C_TIMEOUT_G) then
                        s_state_re   <= t_state_re_pwm_flt;
                        s_cnt_en_re  <= '0';
                        s_pwm_flt_re_n <= '0';
                    elsif (s_pwm_fb_re = '1') then
                        s_state_re   <= t_state_re_pwm_wait_pwm_re;
                        s_cnt_clr_re <= '1';
                        s_cnt_en_re  <= '0';
                    end if;
                -----------------------------------------------------------------
                when t_state_re_pwm_wait_pwm_re =>
                    s_cnt_en_re  <= '0';
                    if (s_pwm_re = '1') then
                        s_state_re   <= t_state_re_pwm_wait_fb_re;
                        s_cnt_clr_re <= '1';
                        s_cnt_en_re  <= '0';
                    end if;
                -----------------------------------------------------------------
                when others => -- fault state
                    s_cnt_en_re    <= '0';
                    s_pwm_flt_re_n <= '0';
                -----------------------------------------------------------------
            end case;
        end if;
    end process;

    -- falling edge checker
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_cnt_clr_fe   <= '0';
            s_cnt_en_fe    <= '0';
            s_pwm_flt_fe_n <= '1';
        elsif rising_edge(s_clk) then
            s_cnt_clr_fe   <= '0';
            s_cnt_en_fe    <= '1';
            s_pwm_flt_fe_n <= '1';
            case (s_state_fe) is
                -----------------------------------------------------------------
                when t_state_fe_pwm_idle =>
                    s_cnt_en_fe <= '0';
                    if (s_start_p = '1') then
                        s_state_fe   <= t_state_fe_pwm_ready;
                    end if;
                -----------------------------------------------------------------
                when t_state_fe_pwm_ready =>
                    s_cnt_en_fe <= '0';
                    if (s_pwm_fe = '1') then
                        s_state_fe   <= t_state_fe_pwm_wait_fb_fe;
                        s_cnt_en_fe  <= '1';
                    end if;
                -----------------------------------------------------------------
                when t_state_fe_pwm_wait_fb_fe =>
                    if (s_cnt_fe = C_TIMEOUT_G) then
                        s_state_fe     <= t_state_fe_pwm_flt;
                        s_cnt_en_fe    <= '0';
                        s_pwm_flt_fe_n <= '0';
                    elsif (s_pwm_fb_fe = '1') then
                        s_state_fe   <= t_state_fe_pwm_wait_pwm_fe;
                        s_cnt_clr_fe <= '1';
                    end if;
                -----------------------------------------------------------------
                when t_state_fe_pwm_wait_pwm_fe =>
                    s_cnt_en_fe <= '0';
                    if (s_pwm_fe = '1') then
                        s_state_fe   <= t_state_fe_pwm_wait_fb_fe;
                        s_cnt_clr_fe <= '1';
                    end if;
                -----------------------------------------------------------------
                when others => -- fault state
                    s_cnt_en_fe    <= '0';
                    s_pwm_flt_fe_n <= '0';
                -----------------------------------------------------------------
            end case;
        end if;
    end process;

end architecture rtl;