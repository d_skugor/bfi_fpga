----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pwm_monitor.vhd
-- module name      PWM Monitor - Behavioral
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Checks if dead-time violation occcured and if both h and l lines are high
--                  at the same time
----------------------------------------------------------------------------------------------
-- create date     04.09.2019
-- Revision:
--      Revision 0.01 - File Created -
--
--      Revision 0.02 by david.pavlovic
--                    - Code cleanup
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity pwm_monitor is
    generic (
        G_DEAD_TIME           : integer := 100; -- dead time in ns
        G_SYSTEM_PERIOD_NS    : integer := 10;  -- system period in ns
        G_DEADTIME_MAX_DEV_NS : integer := 10   -- max deadtime deviation
    );
    port(
        p_pwmMon_clk_in       : in  std_logic;  -- module clock
        p_pwmMon_rst_n_in     : in  std_logic;  -- main reset / active low
        -- PWM line inputs
        p_pwmMon_hs_in        : in  std_logic;  --  pwm input hs line
        p_pwmMon_ls_in        : in  std_logic;  --  pwm input ls line
        -- Gray counter error
        p_pwmMon_gCErr_out    : out std_logic;  -- gray counter error -pulsed signal
        -- PWM error output
        p_pwmMon_errDt_out    : out std_logic;  -- deadtime error - pulsed signal
        p_pwmMon_errSt_out    : out std_logic   -- shoout-trough error - pulsed signal
    );
end pwm_monitor;

architecture structural of pwm_monitor is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component gray_counter is
        generic (
            G_WIDTH            : integer := 4;
            G_CORRECTION_ON    : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port (
            p_grayCnt_clk_in   : in  std_logic;
            p_grayCnt_rst_in   : in  std_logic;    -- clear
            p_grayCnt_en_in    : in  std_logic;    -- enable
            p_grayCnt_err_out  : out std_logic;    -- pulsed error
            p_grayCnt_res_out  : out std_logic_vector(G_WIDTH -1 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_DEAD_TIME          : integer := G_DEAD_TIME/ G_SYSTEM_PERIOD_NS ;
    constant C_DEAD_TIME_MAX_DEV  : integer := G_DEADTIME_MAX_DEV_NS/ G_SYSTEM_PERIOD_NS;
    constant C_NUM_OF_PAST_STATES : integer := 2;

    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------

    function gray_to_bin ( gray : std_logic_vector ) return std_logic_vector is
      variable v_temp : std_logic_vector(gray'range);
    begin
      v_temp(gray'left) := gray(gray'left);
      for i in gray'left - 1 downto 0 loop
         v_temp(i) := gray(i) xor v_temp(i+1);
      end loop;
      return v_temp;
    end gray_to_bin;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

     -- interal pwm input signals
    signal s_hs_i             : std_logic;
    signal s_ls_i             : std_logic;
    signal s_hs_t_i           : std_logic;
    signal s_ls_t_i           : std_logic;

    signal s_en_monitor_i     : std_logic;
    signal s_en_monitor_t_i   : std_logic;

    signal s_err_st           : std_logic; -- error shoot-through
    signal s_err_dt           : std_logic; -- error dead-time

    -- hs and ls edge detector signals
    signal s_pwm_high_ed      : std_logic_vector(C_NUM_OF_PAST_STATES-1 downto 0);
    signal s_pwm_low_ed       : std_logic_vector(C_NUM_OF_PAST_STATES-1 downto 0);

    -- deadtime counter signals
    signal s_deadtime_cnt_en  : std_logic;
    signal s_deadtime_cnt_clr : std_logic;
    signal s_dead_time_cnt    : std_logic_vector(3 downto 0);

    signal s_gC_err           : std_logic;

begin

    -- synchronize PWM lines with system clock for sync monitoring
    -- input signal sampled two times due to metastability
    process (p_pwmMon_clk_in)
    begin
        if rising_edge(p_pwmMon_clk_in) then
            s_hs_t_i <= p_pwmMon_hs_in;
            s_ls_t_i <= p_pwmMon_ls_in;
            s_hs_i   <= s_hs_t_i;
            s_ls_i   <= s_ls_t_i;
        end if;
    end process;

    process (p_pwmMon_clk_in)
    begin
        if rising_edge(p_pwmMon_clk_in) then
            if (p_pwmMon_rst_n_in = '0') then
                s_en_monitor_t_i <= '0';
                s_en_monitor_i   <= '0';
            else
                s_en_monitor_t_i <= '1';
                s_en_monitor_i <= s_en_monitor_t_i;
            end if;
        end if;
    end process;

    -- watch past states to catch rising and falling edge of input signals
    process (p_pwmMon_clk_in)
        variable v_pwm_high : std_logic_vector(C_NUM_OF_PAST_STATES-1 downto 0);
        variable v_pwm_low  : std_logic_vector(C_NUM_OF_PAST_STATES-1 downto 0);
    begin
        if rising_edge(p_pwmMon_clk_in) then
            if (p_pwmMon_rst_n_in = '0') then
                s_pwm_high_ed <= "01";
                s_pwm_low_ed  <= "10";
            else
                if (s_en_monitor_i = '1') then
                    -- set PWM vector with new sample
                    v_pwm_high(C_NUM_OF_PAST_STATES-1 downto 0) := s_pwm_high_ed(C_NUM_OF_PAST_STATES-2 downto 0 ) & s_hs_i;
                    v_pwm_low(C_NUM_OF_PAST_STATES-1  downto 0) := s_pwm_low_ed(C_NUM_OF_PAST_STATES-2 downto 0 ) & s_ls_i;
                    -- get edge detector value
                    s_pwm_high_ed  <= v_pwm_high;
                    s_pwm_low_ed   <= v_pwm_low;
                end if;
            end if;
        end if;
    end process;

    process (p_pwmMon_clk_in)
    begin
        if rising_edge(p_pwmMon_clk_in) then
            if (p_pwmMon_rst_n_in = '0') then
                s_err_dt <= '0';
            else
                if (s_en_monitor_i = '1') then
                    --detect falling edge before deadtime count
                    if ((s_pwm_high_ed = "10" and s_pwm_low_ed = "00" )  or  (s_pwm_low_ed = "10" and s_pwm_high_ed = "00")) then
                        s_err_dt <= '0';
                    -- detect raising edge after deadtime
                    elsif((s_pwm_high_ed = "01" and s_pwm_low_ed = "00") or  (s_pwm_low_ed = "01" and s_pwm_high_ed = "00")) then
                        if (s_deadtime_cnt_en /= '1') then -- if deadtime count didn't start
                            s_err_dt <= '1';
                        else
                            -- if deadtime is different drom allowed deadtime, error occured
                            if (gray_to_bin(s_dead_time_cnt) < (C_DEAD_TIME - C_DEAD_TIME_MAX_DEV-1) or
                              gray_to_bin(s_dead_time_cnt) > (C_DEAD_TIME + C_DEAD_TIME_MAX_DEV-1)) then
                                s_err_dt <= '1';
                            else
                                s_err_dt <= '0';
                            end if;
                        end if;
                    -- if there's no detection and deadtime count is bigger than allowed deadtime
                    else
                        if (gray_to_bin(s_dead_time_cnt) > (C_DEAD_TIME + C_DEAD_TIME_MAX_DEV)) then
                            s_err_dt <= '1';
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

    process (p_pwmMon_clk_in)
    begin
        if rising_edge(p_pwmMon_clk_in) then
            if (p_pwmMon_rst_n_in = '0') then
                s_deadtime_cnt_en  <= '0';
            else
                if (s_en_monitor_i = '1') then
                    --detect falling edge with deadtime
                    if ((s_pwm_high_ed = "10" and s_pwm_low_ed = "00" )  or  (s_pwm_low_ed = "10" and s_pwm_high_ed = "00")) then
                        s_deadtime_cnt_en <= '1';
                    -- detect raising edge after deadtime
                    elsif((s_pwm_high_ed = "01" and s_pwm_low_ed = "00") or  (s_pwm_low_ed = "01" and s_pwm_high_ed = "00")) then
                        s_deadtime_cnt_en <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- async clear for grey counter  : reset counting  each time after enable goes to zero
    s_deadtime_cnt_clr <= not s_deadtime_cnt_en;

    -- count clocks passed from detected falling edge - grey counter
    gray_cnt_block : gray_counter
        generic map(
           G_WIDTH             => 4,
           G_CORRECTION_ON     => true
        )
        port map(
            p_grayCnt_clk_in   => p_pwmMon_clk_in,
            p_grayCnt_rst_in   => s_deadtime_cnt_clr,
            p_grayCnt_en_in    => s_deadtime_cnt_en,
            p_grayCnt_err_out  => s_gC_err,
            p_grayCnt_res_out  => s_dead_time_cnt
        );

    -- check for shoot-trough condition
    process (p_pwmMon_rst_n_in, s_hs_i, s_ls_i)
    begin
       if (p_pwmMon_rst_n_in = '0') then
            s_err_st <= '0';
       else
            if (s_hs_i = '1' and s_ls_i = '1') then -- if hs and ls are active at the same time - generate an error
                s_err_st <= '1';
            else
                s_err_st <= '0';
            end if;
        end if;
    end process;

    -- process gray counter error signal
    p_pwmMon_gCErr_out <= s_gC_err;

    -- process error output
    p_pwmMon_errSt_out <= s_err_st; -- shoot trough error
    p_pwmMon_errDt_out <= s_err_dt; -- dead time violation error

end structural;
