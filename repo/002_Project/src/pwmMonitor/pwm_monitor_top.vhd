----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pwm_monitor_top.vhd
-- module name      pwm_monitor_top - structural
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            PWM Monitor checks if delay between PWM and PWM feedback signals
--                  is less than TIMEOUT (5 us)
----------------------------------------------------------------------------------------------
-- create date     09.09.2019
-- Revision:
--      Revision 0.01 - File Created -
--
--      Revision 0.02 by david.pavlovic
--                   - New implementation of PWM monitor for BFI Gate Driver v4.0
--
--      Revision 0.03 by david.pavlovic
--                   - Changed PWM feedback timeout to 20us
--
--      Revision 0.04 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.05 by david.pavlovic
--                    - Removed deprecated ports
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library xpm;
use xpm.vcomponents.all;

entity pwm_monitor_top is
    generic (
        G_TIMEOUT                : natural := 500; -- 5 us
        G_DEADTIME               : natural := 300; -- 3 us
        G_DELTA                  : natural := 5;
        G_PULSE_LENGTH_PWM       : natural := 75;  -- 750 ns
        G_PULSE_LENGTH_PWM_FB    : natural := 50   -- 500 ns
    );
    port (
        -- Main signals
        p_pwm_m_top_clk_in       : in  std_logic;
        p_pwm_m_top_rst_n_in     : in  std_logic;
        p_pwm_m_top_mcu_req_in   : in  std_logic;
        -- Input PWM signal
        p_pwm_m_top_pwm_in       : in  std_logic_vector(5 downto 0);
        -- Input PWM feedback signal
        p_pwm_m_top_pwm_fb_in    : in  std_logic_vector(5 downto 0);
        -- Fault outputs
        p_pwm_m_top_flt_dt_n_out : out std_logic_vector(2 downto 0);
        p_pwm_m_top_flt_fb_n_out : out std_logic_vector(5 downto 0)
    );
end entity pwm_monitor_top;

architecture structural of pwm_monitor_top is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component edge_detector_p
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    component pwm_deadtime_monitor is
        generic (
            G_DEADTIME             : natural := 300; -- 3 us
            G_DELTA                : natural := 5
        );
        port (
            -- Main signals
            p_pwm_dtm_clk_in       : in  std_logic;
            p_pwm_dtm_rst_n_in     : in  std_logic;
            p_pwm_dtm_start_p_in   : in  std_logic;
            -- MCU PWM input signals
            p_pwm_dtm_pwm_in       : in  std_logic_vector(5 downto 0);
            -- Faults
            p_pwm_dtm_flt_dt_n_out : out std_logic_vector(2 downto 0)
        );
    end component;

    component pwm_feedback_monitor is
        generic (
            G_TIMEOUT             : natural := 500; -- 5 us
            G_PULSE_LENGTH_PWM    : natural := 75;  -- 750 ns
            G_PULSE_LENGTH_PWM_FB : natural := 50   -- 500 ns
        );
        port (
            -- Main signals
            p_pwm_fm_clk_in       : in  std_logic;
            p_pwm_fm_rst_n_in     : in  std_logic;
            p_pwm_fm_start_p_in   : in  std_logic;
            -- Input PWM signals
            p_pwm_fm_pwm_in       : in  std_logic_vector(5 downto 0);
            -- Input PWM feedback signals
            p_pwm_fm_pwm_fb_in    : in  std_logic_vector(5 downto 0);
            -- Output PWM fault signals
            p_pwm_fm_fb_flt_n_out : out std_logic_vector(5 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk          : std_logic;
    signal s_rst_n        : std_logic;
    signal s_en           : std_logic;
    signal s_mcu_req      : std_logic;
    signal s_mcu_req_re   : std_logic;
    signal s_pwm_in       : std_logic_vector(5 downto 0);
    signal s_pwm_fb_in    : std_logic_vector(5 downto 0);
    signal s_pwm_flt_dt_n : std_logic_vector(2 downto 0);
    signal s_pwm_flt_fb_n : std_logic_vector(5 downto 0);

begin

    -- input signal assignments
    s_clk       <= p_pwm_m_top_clk_in;
    s_rst_n     <= p_pwm_m_top_rst_n_in;
    s_mcu_req   <= p_pwm_m_top_mcu_req_in;
    s_pwm_in    <= p_pwm_m_top_pwm_in;
    s_pwm_fb_in <= p_pwm_m_top_pwm_fb_in;

    -- output signal assignments
    p_pwm_m_top_flt_dt_n_out   <= s_pwm_flt_dt_n;
    p_pwm_m_top_flt_fb_n_out   <= s_pwm_flt_fb_n;

    -- rising edge detector for MCU request signal
    mdl_ed_mcu_req : edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_mcu_req,
            p_edp_sig_re_out => s_mcu_req_re,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
        );

    mdl_pwm_dt_mon : pwm_deadtime_monitor
        generic map (
            G_DEADTIME             => G_DEADTIME,
            G_DELTA                => G_DELTA
        )
        port map (
            p_pwm_dtm_clk_in       => s_clk,
            p_pwm_dtm_rst_n_in     => s_rst_n,
            p_pwm_dtm_start_p_in   => s_mcu_req_re,
            p_pwm_dtm_pwm_in       => s_pwm_in,
            p_pwm_dtm_flt_dt_n_out => s_pwm_flt_dt_n
        );

    mdl_pwm_fb_mon : pwm_feedback_monitor
        generic map (
            G_TIMEOUT             => G_TIMEOUT,
            G_PULSE_LENGTH_PWM    => G_PULSE_LENGTH_PWM,
            G_PULSE_LENGTH_PWM_FB => G_PULSE_LENGTH_PWM_FB
        )
        port map (
            p_pwm_fm_clk_in       => s_clk,
            p_pwm_fm_rst_n_in     => s_rst_n,
            p_pwm_fm_start_p_in   => s_mcu_req_re,
            p_pwm_fm_pwm_in       => s_pwm_in,
            p_pwm_fm_pwm_fb_in    => s_pwm_fb_in,
            p_pwm_fm_fb_flt_n_out => s_pwm_flt_fb_n
        );

end architecture structural;