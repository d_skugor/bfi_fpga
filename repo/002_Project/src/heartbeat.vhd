----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             heart_beat.vhd
-- module name      heart_beat - rtl
-- author           Arturo Montufar Arreola (arturo.arreola@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100T-1CSG324Q
-- brief            LED blinking as device heart beat
----------------------------------------------------------------------------------------------
-- create date      10.06.2019
-- Revision:
--      Revision 0.01 by arturo.arreola
--                    - File Created
--
--      Revision 0.02 by arturo.arreola
--                    -  Modified to comply with coding standard

--      Revision 0.03 by dario.drvenkar
--                    - Added "heartbeat" behavioral
--                    - Heart has cardiac arrest while under reset
--
--      Revision 0.04 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.05 by danijela.skugor
--                    - Substitute binary counter with gray code counter
--                    - Constants changed to std_logic_vector instead integer
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real."ceil";
use ieee.math_real."log2";

Library xpm;
use xpm.vcomponents.all;

-- entity definition
entity heartbeat is
    generic (
        G_HB_CLOCK_FREQ   : integer := 100 -- Input frequency of Clock in MHz
    );
    port (
        p_hb_clk_in       : in  std_logic;
        p_hb_reset_n_in   : in  std_logic;
        p_hb_signal_out   : out std_logic
    );
end heartbeat;

-- architecture definition
architecture rtl of heartbeat is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component gray_counter is
        generic(
            G_GRAYCNT_WIDTH             : integer := 4;
            G_GRAYCNT_CORRECTION_ON     : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port(
            p_grayCnt_clk_in    : in std_logic;
            p_grayCnt_rst_n_in  : in std_logic;  -- sync reset
            p_grayCnt_en_in     : in std_logic;  -- enable
            p_grayCnt_err_p_out : out std_logic; -- pulsed error
            p_grayCnt_res_out   : out std_logic_vector(G_GRAYCNT_WIDTH -1 downto 0)
        );
    end component;
    
    ---------------------------------------------------------------------------------
    --                                   functions
    ---------------------------------------------------------------------------------
    -- Convert binary to  gray code
    function bin_to_gray (bin : std_logic_vector) return std_logic_vector is
        variable v_gray : std_logic_vector(bin'range);
    begin
        v_gray(bin'left) := bin(bin'left);
        for i in bin'left-1 downto 0 loop
            v_gray(i) := bin(i+1) xor bin(i);
        end loop;
        return v_gray;
    end function bin_to_gray;

    -- Convert gray code to binary
    function gray_to_bin( gray : std_logic_vector ) return std_logic_vector is
        variable v_bin : std_logic_vector(gray'range);
    begin
        v_bin(v_bin'high) := gray(gray'high);
        for i in gray'high-1 downto 0 loop
            v_bin(i) := gray(i) xor v_bin(i+1);
        end loop;
      return v_bin;
    end function;  
      
    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_HB_PERIOD_COUNTER_LEN : integer := 32;
    constant C_HB_PERIOD_COUNTER_I   : integer := G_HB_CLOCK_FREQ * 1_250_000;
    constant C_HB_PERIOD_COUNTER     : std_logic_vector(C_HB_PERIOD_COUNTER_LEN-1 downto 0) := std_logic_vector(to_unsigned(C_HB_PERIOD_COUNTER_I, C_HB_PERIOD_COUNTER_LEN));


    -- Ventricular fibrillation (cardiac arrest)
    constant C_HB_CARDIAC_ARREST_0   : std_logic_vector(C_HB_PERIOD_COUNTER_LEN-1 downto 0) := (others => '0');
    constant C_HB_CARDIAC_ARREST_HI  : std_logic_vector(C_HB_PERIOD_COUNTER_LEN-1 downto 0) := std_logic_vector(to_unsigned(C_HB_PERIOD_COUNTER_I / 8, C_HB_PERIOD_COUNTER_LEN));
    constant C_HB_CARDIAC_ARREST_LO  : std_logic_vector(C_HB_PERIOD_COUNTER_LEN-1 downto 0) := std_logic_vector(to_unsigned(C_HB_PERIOD_COUNTER_I * 2, C_HB_PERIOD_COUNTER_LEN));

    -- Cardiac cycle has 5 phases: P, Q, R, S, T
    constant C_HB_R_START_I          : integer := C_HB_PERIOD_COUNTER_I * 0;
    constant C_HB_R_STOP_I           : integer := C_HB_PERIOD_COUNTER_I / 8;
    constant C_HB_T_START_I          : integer := C_HB_PERIOD_COUNTER_I * 10 / 25;
    constant C_HB_T_STOP_I           : integer := C_HB_T_START_I + C_HB_R_STOP_I * 15 / 10;
    constant C_HB_R_START            : std_logic_vector(C_HB_PERIOD_COUNTER_LEN-1 downto 0) := std_logic_vector(to_unsigned(C_HB_R_START_I, C_HB_PERIOD_COUNTER_LEN));
    constant C_HB_R_STOP             : std_logic_vector(C_HB_PERIOD_COUNTER_LEN-1 downto 0) := std_logic_vector(to_unsigned(C_HB_R_STOP_I, C_HB_PERIOD_COUNTER_LEN));
    constant C_HB_T_START            : std_logic_vector(C_HB_PERIOD_COUNTER_LEN-1 downto 0) := std_logic_vector(to_unsigned(C_HB_T_START_I, C_HB_PERIOD_COUNTER_LEN));
    constant C_HB_T_STOP             : std_logic_vector(C_HB_PERIOD_COUNTER_LEN-1 downto 0) := std_logic_vector(to_unsigned(C_HB_T_STOP_I, C_HB_PERIOD_COUNTER_LEN));
    constant C_PWM_COUNTER           : integer := 1024;
    constant C_PWM_REG               : integer := integer(ceil(log2(real(C_PWM_COUNTER))));
    constant C_PWM_DC                : integer := C_PWM_COUNTER * 3 / 10;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk           : std_logic;
    signal s_rst_n         : std_logic;
    signal s_rst_sync_n    : std_logic;
    signal s_reset_counter : std_logic;
    signal s_pwm_counter   : unsigned (C_PWM_REG-1 downto 0);
    signal s_heartbeat     : std_logic;
    
    signal s_cycleCnt       : std_logic_vector(C_HB_PERIOD_COUNTER_LEN-1 downto 0);
    signal s_cycleCnt_en    : std_logic;
    signal s_cycleCnt_rst_n : std_logic;

begin

    -- input signal assignments
    s_clk <= p_hb_clk_in;
    s_rst_n <= p_hb_reset_n_in;

    -- output signal assignment
    p_hb_signal_out <= s_heartbeat;

    -- reset synchronization
    xpm_cdc_single_inst : xpm_cdc_single
        generic map (
            DEST_SYNC_FF   => 2,            -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,            -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 1,            -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0             -- DECIMAL; 0=do not register input, 1=register input
        )
        port map (
            dest_out       => s_rst_sync_n, -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
            dest_clk       => s_clk,        -- 1-bit input: Clock signal for the destination clock domain.
            src_clk        => '0',          -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in         => s_rst_n       -- 1-bit input: Input signal to be synchronized to dest_clk domain.
        );
        
    gray_cnt : gray_counter
        generic map(
            G_GRAYCNT_WIDTH            => C_HB_PERIOD_COUNTER_LEN,
            G_GRAYCNT_CORRECTION_ON    => true
        )
        port map(
            p_grayCnt_clk_in    => s_clk,
            p_grayCnt_rst_n_in  => s_cycleCnt_rst_n,
            p_grayCnt_en_in     => s_cycleCnt_en,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_cycleCnt
        );

    process (s_clk)
    begin
         if (rising_edge(s_clk)) then
            -- PWM counter
            s_pwm_counter <= s_pwm_counter + 1;
            -- Hartbeat counter
            if (s_reset_counter = '1') then
                s_cycleCnt_rst_n <= '0';
                s_cycleCnt_en <= '0';
            else
                s_cycleCnt_rst_n <= '1';
                s_cycleCnt_en <= '1';
            end if;
         end if;
    end process;

    process (s_clk)
    begin
        if (rising_edge(s_clk)) then
            if(s_rst_sync_n = '0') then
                s_reset_counter <= '0';
                if (gray_to_bin(s_cycleCnt) >= C_HB_CARDIAC_ARREST_0 and gray_to_bin(s_cycleCnt) < C_HB_CARDIAC_ARREST_HI) then
                    s_heartbeat <= '1';
                elsif (gray_to_bin(s_cycleCnt) >= C_HB_CARDIAC_ARREST_HI and gray_to_bin(s_cycleCnt) < C_HB_CARDIAC_ARREST_LO) then
                    s_heartbeat <= '0';
                else
                    s_reset_counter <= '1';
                end if;
            else
                if (gray_to_bin(s_cycleCnt) >= C_HB_PERIOD_COUNTER) then
                    s_reset_counter <= '1';
                else
                    s_reset_counter <= '0';
                end if;
                if (gray_to_bin(s_cycleCnt) >= C_HB_R_START and gray_to_bin(s_cycleCnt) <= C_HB_R_STOP) then
                    s_heartbeat <= '1';
                elsif (gray_to_bin(s_cycleCnt) > C_HB_R_STOP and gray_to_bin(s_cycleCnt) < C_HB_T_START) then
                    s_heartbeat <= '0';
                elsif (gray_to_bin(s_cycleCnt) >= C_HB_T_START and gray_to_bin(s_cycleCnt) <= C_HB_T_STOP) then
                    if (s_pwm_counter >= 0 and s_pwm_counter < C_PWM_DC) then
                        s_heartbeat <= '1';
                    else
                        s_heartbeat <= '0';
                    end if;
                elsif (gray_to_bin(s_cycleCnt) > C_HB_T_STOP and gray_to_bin(s_cycleCnt) < C_HB_PERIOD_COUNTER) then
                    s_heartbeat <= '0';
                end if;
            end if;
        end if;
    end process;

end architecture rtl;
