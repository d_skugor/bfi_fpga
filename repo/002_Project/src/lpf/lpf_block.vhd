----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             lpf_block.vhd
-- module name      Low Pass Filter block
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Top Level Module
----------------------------------------------------------------------------------------------
-- create date      19.11.2019
-- Revision:
--      Revision 0.01 by jsq
--                   File Created
--      Revision 0.02 by jsq
--                   Updated internal scaling to avoid overflow in case of overshoot
--      Revision 0.04 by jsq
--                   Corrected filter coefficients.
--                   Added a bypass architecture.
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity lpf_block is
    port (
        clk_i         : in  std_logic;                     --! System clock input
        rst_n_i       : in  std_logic;                     --! Synchronous reset input
        sample_clk_i  : in  std_logic;                     --! Sample clock input
        filter_data_i : in  std_logic_vector(11 downto 0); --! Filter data input
        filter_data_o : out std_logic_vector(11 downto 0)  --! Filter data output
    );
end entity lpf_block;

architecture BYPASS of lpf_block is
begin
    -- Just bypass the input to the output (no filter...)
    process (clk_i)
    begin
        if (rst_n_i = '0') then
            filter_data_o <= (others => '0');
        elsif Rising_edge(clk_i) then
            if (sample_clk_i = '1') then
                filter_data_o <= filter_data_i;
            end if;
        end if;
    end process;

end architecture BYPASS;

architecture IIR of lpf_block is

    constant C_FILTER_DATA_SIZE : integer := 13;

    signal s1to2 : std_logic_vector(C_FILTER_DATA_SIZE - 1 downto 0);

    signal sample_p        : std_logic;                    -- Internally generated sample pulse
    signal sample_p_filter : std_logic_vector(1 downto 0); -- Edge detector pulse filter

    signal scaled_input    : signed(C_FILTER_DATA_SIZE - 1 downto 0);
    signal filter_output   : std_logic_vector(C_FILTER_DATA_SIZE - 1 downto 0);

begin

    scaled_input  <= resize(signed(filter_data_i),C_FILTER_DATA_SIZE);
    filter_data_o <= filter_output(filter_data_i'RANGE);

    section1 : entity work.biquad_bb
        generic map (
            DATA_SIZE      => C_FILTER_DATA_SIZE,
            COEFF_SIZE     => 21,
            A1             => -1309304,
            A2             => 431457,
            B0             => 23999,
            B1             => -727,
            B2             => 23999
        )
        port map (
            clk_i          => clk_i,
            rst_n_i        => rst_n_i,
            sample_pulse_i => sample_p,
            filter_data_i  => std_logic_vector(scaled_input),
            filter_data_o  => s1to2
        );

    section2 : entity work.biquad_bb
        generic map (
            DATA_SIZE      => C_FILTER_DATA_SIZE,
            COEFF_SIZE     => 21,
            A1             => -1683307,
            A2             => 800453,
            B0             => 1048576,
            B1             => -1498629,
            B2             => 1048576
        )
        port map (
            clk_i          => clk_i,
            rst_n_i        => rst_n_i,
            sample_pulse_i => sample_p,
            filter_data_i  => s1to2,
            filter_data_o  => filter_output
        );
    --TODO: THE FILTER OUTPUT NEEDS TO BE SATURATED. DETECT SATURATION TAKING MODULE AND FIX OUTPUT TO +-FS

    -- Edge detector to extract the sampling clock rising edge
    edge_detector : process (clk_i)
    begin
        if rising_edge(clk_i) then
            if (rst_n_i = '0') then
                sample_p_filter <= (others => '0');
                sample_p        <= '0';
            else
                sample_p        <= '0';
                sample_p_filter <= sample_p_filter(0) & sample_clk_i;
                if (sample_p_filter = "01") then
                    sample_p <= '1';
                end if;
            end if;
        end if;
    end process edge_detector;

end architecture IIR;

architecture MVA of lpf_block is

    constant C_FILTER_DATA_SIZE : integer := 50;

    signal sample_p        : std_logic;                    -- Internally generated sample pulse
    signal sample_p_filter : std_logic_vector(1 downto 0); -- Edge detector pulse filter

begin

    filter : entity work.mva_b
        generic map (
            F_LENGTH       => C_FILTER_DATA_SIZE,
            DATA_SIZE      => 12
        )
        port map (
            clk_i          => clk_i,
            rst_n_i        => rst_n_i,
            sample_pulse_i => sample_p,
            filter_data_i  => filter_data_i,
            filter_data_o  => filter_data_o
        );

    -- Edge detector to extract the sampling clock rising edge
    edge_detector : process (clk_i)
    begin
        if rising_edge(clk_i) then
            if (rst_n_i = '0') then
                sample_p_filter <= (others => '0');
                sample_p        <= '0';
            else
                sample_p        <= '0';
                sample_p_filter <= sample_p_filter(0) & sample_clk_i;
                if (sample_p_filter = "01") then
                    sample_p <= '1';
                end if;
            end if;
        end if;
    end process edge_detector;

end architecture MVA;
