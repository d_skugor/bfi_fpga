----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             medfilt_sequencer.vhd
-- module name      Median filter sequencer
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Intra-cycle sequencer module
--
----------------------------------------------------------------------------------------------
-- create date      01.10.2020
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
--      Revision 1.02 by jsq
--                      Filter modification: the new filter takes three consecutive samples
--                      at the ADC rate, at each start_p (synchro PWM pulse), with the middle
--                      sample aligned with the start_p. The alignment is obtained in the sampler
--                      block.
--                      This block is not compatible with the previous filter implementation, and
--                      the PWM frequency measurement is not a requirement anymore.
--
--                      Replaced original edge detector block, with the new edge_detector_p block.
--
--      Revision 1.03 by david.pavlovic
--                      FSM removed
--                      Module waits for start pulse (MCU request) nd after that wait for
--                      one more sample pulse from ADC (sample_p of 1MHz)
--
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;
use work.pwm_pack.all;

entity medfilt_sequencer is
    port (
        clk_i      : in  std_logic; --! System clock input
        rst_n_i    : in  std_logic; --! System reset input
        rdy_p_o    : out std_logic; --! New frame ready output pulse
        sample_p_o : out std_logic; --! Sample pulse output
        sample_p_i : in  std_logic; --! Sample pulse input
        start_p_i  : in  std_logic  --! Frame start marker
    );
end entity medfilt_sequencer;

architecture rtl of medfilt_sequencer is

     signal s_rdy_p         : std_logic; -- Internal ready pulse
     signal s_sync_detected : std_logic; -- Start pulse received flag

begin

    rdy_p_o <= s_rdy_p;

    buff_control : process(clk_i, rst_n_i)
    begin
        if (rst_n_i = '0') then
            s_rdy_p         <= '0';
            s_sync_detected <= '0';
        elsif rising_edge(clk_i) then
            s_rdy_p <= '0';
            if (start_p_i = '1') then
                s_sync_detected <= '1';
            elsif (s_sync_detected = '1' and sample_p_i = '1') then
                s_rdy_p <= '1';
                s_sync_detected <= '0';
            end if;
        end if;
    end process;

    sample_output : process(clk_i, rst_n_i)
    begin
        if (rst_n_i = '0') then
            sample_p_o <= '0';
        elsif rising_edge(clk_i) then
            sample_p_o <= sample_p_i;
        end if;
    end process;

end architecture rtl;
