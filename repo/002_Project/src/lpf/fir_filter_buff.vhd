----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             fir_filter_buff.vhd
-- module name      firFilter_buff
-- author           Dario Drvenkar (dario.drvenkar@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Wrapper for FIR filter with output buffer
----------------------------------------------------------------------------------------------
-- create date      15.12.2019
-- Revision:
--      Revision 0.01 by dario.drvenkar
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Code cleanup
--
----------------------------------------------------------------------------------------------
-- TODO: - nothing
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity firFilter_buff is
    generic (
        G_DATA                            : natural := 12;
        G_OFFSET                          : natural := 100
    );
    port (
        p_firFilter_buff_reset_n_in       :  in std_logic;
        p_firFilter_buff_clk_in           :  in std_logic;
        p_firFilter_buff_sampleClk_in     :  in std_logic;
        p_firFilter_buff_req_in           :  in std_logic; -- MCU request (start of PWM cycle)
        p_firFilter_buff_rawData_in       :  in std_logic_vector(G_DATA-1 downto 0);
        p_firFilter_buff_filteredData_out : out std_logic_vector(G_DATA-1 downto 0)
    );
end firFilter_buff;

architecture structural of firFilter_buff is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component sw_freq_filter_top is
        generic (
            G_DATA                  : natural := 12;
            G_OFFSET                : natural := 100
        );
        port (
            p_top_sff_clk_in        : in  std_logic;
            p_top_sff_rst_n_in      : in  std_logic;
            p_top_sff_sample_clk_in : in  std_logic;
            p_top_sff_mcu_req_in    : in  std_logic;
            p_top_sff_data_in       : in  std_logic_vector(G_DATA-1 downto 0);
            p_top_sff_data_out      : out std_logic_vector(G_DATA-1 downto 0)
        );
    end component;

    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk                   : std_logic;
    signal s_rst_n                 : std_logic;
    signal s_sample_clk            : std_logic;
    signal s_mcu_req_sync          : std_logic;
    signal s_rawData               : std_logic_vector(G_DATA-1 downto 0);
    signal s_filteredData_out      : std_logic_vector(G_DATA-1 downto 0);
    signal s_filteredData_out_sync : std_logic_vector(G_DATA-1 downto 0);
    signal s_mcu_req               : std_logic;

begin

    -- input signals assignments
    s_clk        <= p_firFilter_buff_clk_in;
    s_rst_n      <= p_firFilter_buff_reset_n_in;
    s_sample_clk <= p_firFilter_buff_sampleClk_in;
    s_rawData    <= p_firFilter_buff_rawData_in;
    s_mcu_req    <= p_firFilter_buff_req_in;

    -- output signals assignments
    p_firFilter_buff_filteredData_out <= s_filteredData_out_sync;

   mdl_firFilt_vDC : sw_freq_filter_top
        generic map (
            G_DATA                  => G_DATA,
            G_OFFSET                => G_OFFSET
        )
        port map (
            p_top_sff_rst_n_in      => s_rst_n,
            p_top_sff_clk_in        => s_clk,
            p_top_sff_sample_clk_in => s_sample_clk,
            p_top_sff_mcu_req_in    => s_mcu_req,
            p_top_sff_data_in       => s_rawData,
            p_top_sff_data_out      => s_filteredData_out
        );

    pedgeflt : edge_detector_p
        generic map (
            G_REG            => 1
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_mcu_req,
            p_edp_sig_re_out => s_mcu_req_sync,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
        );

    output_update_process : process (s_rst_n, s_clk)
    begin
        -- reset
        if(s_rst_n = '0') then
            s_filteredData_out_sync <= (others => '0');
        elsif (rising_edge(s_clk)) then
            if(s_mcu_req_sync = '1') then
                s_filteredData_out_sync <= s_filteredData_out;
            end if;
        end if;
    end process;

end architecture structural;
