
----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             frame_retimer.vhd
-- module name      frame_retimer - Behavioral
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Retime the frames at the ADC LPF output, which are synchronous to
--                  the ADC clock, into the system clock. Double-buffers the output to
--                  update the frames at the pulse-train (1 MHz typically) rate, before
--                  sending them into the PCI block.
----------------------------------------------------------------------------------------------
-- create date      03.11.2020
-- Revision:
--      Revision 0.01 jsanchez
--                - Initial version
----------------------------------------------------------------------------------------------
--
-- NOTE: This is a VHDL2008 file
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity frame_retimer is
    port (
        -- Clock and reset ports
        dom1_clk_in       : in  std_logic;                     -- Domain 1 clock
        dom1_rst_n_in     : in  std_logic;                     -- Domain 1 reset
        dom2_clk_in       : in  std_logic;                     -- Domain 2 clock
        dom2_rst_n_in     : in  std_logic;                     -- Domain 2 reset

        -- Domain 1 (input from sampler) ports
        dom1_dataRdy_p_in : in  std_logic;                     -- ADC data ready input
        dom1_ch1_in       : in  std_logic_vector(11 downto 0); -- Channel 1 measured current
        dom1_ch2_in       : in  std_logic_vector(11 downto 0); -- Channel 2 measured current
        dom1_ch3_in       : in  std_logic_vector(11 downto 0); -- Channel 3 measured current

        -- Domain 2 (output to system) ports
        dom2_sample_p_in  : in  std_logic; -- System sample pulse train
        dom2_ch1_out      : out std_logic_vector(11 downto 0); -- Channel 1 measured current
        dom2_ch2_out      : out std_logic_vector(11 downto 0); -- Channel 2 measured current
        dom2_ch3_out      : out std_logic_vector(11 downto 0)  -- Channel 3 measured current
    );
end entity frame_retimer;

architecture rtl of frame_retimer is

    signal s_rdy_2_p         : std_logic; -- Domain 2 filter output data ready pulse
    signal s_buffer_strobe_p : std_logic; -- Output buffer strobe pulse

    type state is (S00, S10, S20);
    signal state_curr, state_next : state;

begin

    xross_domain : entity work.pulse_synchronizer
        port map (
            clk_domain1_in   => dom1_clk_in,
            rst_n_domain1_in => dom1_rst_n_in,
            clk_domain2_in   => dom2_clk_in,
            rst_n_domain2_in => dom2_rst_n_in,
            pulse_in         => dom1_dataRdy_p_in,
            pulse_out        => s_rdy_2_p
        );

    interlock : process (dom2_clk_in, dom2_rst_n_in)
    begin
        if (dom2_rst_n_in = '0') then
            state_curr <= S00;
        elsif rising_edge(dom2_clk_in) then
            state_curr <= state_next;
        end if;
    end process;

    interlock_cm : process(all)
    begin
        s_buffer_strobe_p <= '0';
        case state_curr is
            when S00 =>
                state_next <= S00;
                if (s_rdy_2_p = '1') then -- New data available
                    state_next <= S10;
                end if;
            when S10 =>
                state_next <= S10;
                if (s_rdy_2_p = '1') then -- Error! Previous available data wasn't read. Overrun condition.
                    state_next <= S20;
                elsif (dom2_sample_p_in = '1') then
                    s_buffer_strobe_p <= '1'; -- Toggle buffer strobe and return to idle
                    state_next        <= S00;
                end if;
            when S20 =>
                -- Overrun condition signal
                -- Nothing to do for the moment.
                state_next <= S00;
        end case;
    end process;

    output_buffer : process (dom2_clk_in, dom2_rst_n_in)
    begin
        if (dom2_rst_n_in = '0') then
            dom2_ch1_out <= (others => '0');
            dom2_ch2_out <= (others => '0');
            dom2_ch3_out <= (others => '0');
        elsif rising_edge(dom2_clk_in) then
            if (s_buffer_strobe_p = '1') then -- Store tue current filter output data
                dom2_ch1_out <= dom1_ch1_in;
                dom2_ch2_out <= dom1_ch2_in;
                dom2_ch3_out <= dom1_ch3_in;
            end if;
        end if;
    end process;

end architecture rtl;
