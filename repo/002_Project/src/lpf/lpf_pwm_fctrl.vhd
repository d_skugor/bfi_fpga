----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             lpf_pwm_fctrl.vhd
-- module name      LPF PWM frequency control
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Continuously monitors PWM synchronous pulse train to measure the PWM
--                  switching frequency.
--
----------------------------------------------------------------------------------------------
-- create date      01.10.2020
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;
use work.pwm_pack.all;

entity lpf_pwm_fctrl is
    port (
        clk_i      : in  std_logic; --! System clock input
        rst_n_i    : in  std_logic; --! System reset input
        sample_p_i : in  std_logic; --! Sample pulse from ADC
        sync_p_i   : in  std_logic; --! PWM synchronism pulse input
        pwm_freq_o : out t_pwm_freq --! PWM frequency value
    );

end entity lpf_pwm_fctrl;

architecture rtl of lpf_pwm_fctrl is

    signal c_sample_0 : unsigned(8 downto 0);         -- Interval counter samples
    signal c_sdiff    : integer range 0 to 255;       -- Interval counter difference
    signal c_intcnt   : unsigned(8 downto 0);         -- Interval counter
    signal ffsync0    : std_logic_vector(2 downto 0);

begin

    -- Rolling counter
    fcnt : process (clk_i, rst_n_i)
    begin
        if (rst_n_i = '0') then
            c_intcnt <= (others => '0');
        elsif rising_edge(clk_i) then
            if (sample_p_i = '1') then
                c_intcnt <= c_intcnt + 1;
            end if;
        end if;
    end process;

    -- Differntiator
    fdiff : process (clk_i, rst_n_i)
    begin
        if (rst_n_i = '0') then
            c_sdiff    <= 0;
            c_sample_0 <= (others => '0');
        elsif Rising_edge(clk_i) then
            if (sync_p_i = '1') then
                c_sdiff    <= to_integer(c_intcnt - c_sample_0); -- Difference
                c_sample_0 <= c_intcnt;
            end if;
        end if;
    end process;

    -- Comparator
    cmp : process (clk_i, rst_n_i)
    begin
        if (rst_n_i = '0') then
            pwm_freq_o <= LOW;
        elsif Rising_edge(clk_i) then
            -- Update the detected frequency value
            if (c_sdiff > C_FM_PWM1_MIN and c_sdiff < C_FM_PWM1_MAX) then
                pwm_freq_o <= LOW;
            elsif (c_sdiff >= C_FM_PWM2_MIN and c_sdiff <= C_FM_PWM2_MAX) then
                pwm_freq_o <= MED;
            elsif (c_sdiff >= C_FM_PWM3_MIN and c_sdiff <= C_FM_PWM3_MAX) then
                pwm_freq_o <= HIGH;
            end if;
        end if;
    end process;

end architecture rtl;
