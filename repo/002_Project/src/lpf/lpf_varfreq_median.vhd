----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             lpf_varfreq_med.vhd
-- module name      Variable frequency median LPF
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Variable frequency median LPF module.
--                  This filter block has to be clocked in the ADC clock domain
--                  All signals are synchronous with the clock, except for the sync input, which is expected
--                  to be asynchronous. This pulse coming from the MCU has to be, at least, three ADC clock cycles long.
--
----------------------------------------------------------------------------------------------
-- create date      01.10.2020
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
--      Revision 0.02 by jsq
--                      Expanding input to 3 phases, to reuse resources.
--      Revision 0.03 by jsq
--                      Removing PWM frequency control, as it not required anymore by the medfilt block.
--                      Updating medfilt block.
--                      Replacing positive edge detector with the generic edge_detector_p block.
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;
use work.pwm_pack.all;

entity lpf_varfreq_median is
    generic (
        BSIZE      : natural := 12                             -- Data size
    );
    port (
        clk_i      : in  std_logic;                            -- Clock input (ADC clock)
        rst_n_i    : in  std_logic;                            -- Reset input (ADC reset)
        sample_p_i : in  std_logic;                            -- Sample pulse from ADC
        sync_p_i   : in  std_logic;                            -- PWM synchronism pulse input (synchronous)
        rdy_p_o    : out std_logic;                            -- New ready output sample pulse flag
        ch1_i      : in  std_logic_vector(BSIZE - 1 downto 0); -- Filter channel 1 input
        ch2_i      : in  std_logic_vector(BSIZE - 1 downto 0); -- Filter channel 2 input
        ch3_i      : in  std_logic_vector(BSIZE - 1 downto 0); -- Filter channel 3 input
        ch1_o      : out std_logic_vector(BSIZE - 1 downto 0); -- Filter channel 1 output
        ch2_o      : out std_logic_vector(BSIZE - 1 downto 0); -- Filter channel 2 output
        ch3_o      : out std_logic_vector(BSIZE - 1 downto 0)  -- Filter channel 3 output
    );
end entity lpf_varfreq_median;

architecture rtl of lpf_varfreq_median is

    signal pwm_freq        : t_pwm_freq; -- Current detected switching frequency
    signal pwm_sync_p      : std_logic;  -- Synchronized MCU PWM synchro pulse
    signal pwm_sync_edge_p : std_logic;  -- Rising edge of PWM synchro pulse marker
    signal s_rst           : std_logic;  -- Internal positive logic reset

begin

    pedgeflt : entity work.edge_detector_p
        generic map (
            G_REG            => 1
        )
        port map(
            p_edp_clk_in     => clk_i,
            p_edp_rst_n_in   => rst_n_i,
            p_edp_sig_in     => sync_p_i,
            p_edp_sig_re_out => pwm_sync_edge_p,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
        );

    median_filter : entity work.medfilt_block
        generic map (
            BSIZE      => BSIZE
        )
        port map(
            clk_i      => clk_i,
            rst_n_i    => rst_n_i,
            sample_p_i => sample_p_i,
            sync_p_i   => pwm_sync_edge_p,
            rdy_p_o    => rdy_p_o,
            x_1_i      => ch1_i,
            x_2_i      => ch2_i,
            x_3_i      => ch3_i,
            y_1_o      => ch1_o,
            y_2_o      => ch2_o,
            y_3_o      => ch3_o
        );

end architecture rtl;
