----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pwm_pack.vhd
-- module name      PWM Pack
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Types, definitions and constants related to the PWM automatic frequency
--                  measurement, and sampling.
--
----------------------------------------------------------------------------------------------
-- create date      01.10.2020
-- Revision:
--      Revision 0.01 by jsq
--                  File Created
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pwm_pack is

    -- PWM Types
    -- PWM frequency type
    -- LOW = 10 KHz switching
    -- MED = 16 KHz switching
    -- HIGH = 20 KHz switching
    type t_pwm_freq is (LOW, MED, HIGH); --! PWM frequency definitions

    -- PWM Constants
    -- Variable frequency filter intervals
    constant C_NS_PWM1   : integer range 0 to 255 := 120; -- Samples per PWM cycle for LOW frequency
    constant C_NS_F_PWM1 : integer range 0 to 255 := 40;  -- 1/3 cycle length for LOW frequency
    constant C_NS_PWM2   : integer range 0 to 255 := 75;  -- Samples per PWM cycle for MED frequency
    constant C_NS_F_PWM2 : integer range 0 to 255 := 25;  -- 1/3 cycle length for MED frequency
    constant C_NS_PWM3   : integer range 0 to 255 := 60;  -- Samples per PWM cycle for HIGH frequency
    constant C_NS_F_PWM3 : integer range 0 to 255 := 20;  -- 1/3 cycle length for HIGH frequency

    -- Frequency measurement constants, for ~1% deviation tolerance
    constant C_FM_PWM1_MIN : integer range 0 to 255 := 116;
    constant C_FM_PWM1_MAX : integer range 0 to 255 := 122;
    constant C_FM_PWM2_MIN : integer range 0 to 255 := 73;
    constant C_FM_PWM2_MAX : integer range 0 to 255 := 77;
    constant C_FM_PWM3_MIN : integer range 0 to 255 := 58;
    constant C_FM_PWM3_MAX : integer range 0 to 255 := 62;

end package pwm_pack;

package body pwm_pack is

end package body pwm_pack;
