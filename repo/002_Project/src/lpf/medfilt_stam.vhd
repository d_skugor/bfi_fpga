----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             medfilt_stam.vhd
-- module name      Median filter systolic array
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Median systolic array calculator
--
----------------------------------------------------------------------------------------------
-- create date      01.10.2020
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;
use work.pwm_pack.all;

entity medfilt_stam is
    generic (
        BSIZE   : natural := 12                              -- Data size
    );
    port (
        clk_i   : in  std_logic;                             -- System clock input
        rst_n_i : in  std_logic;                             -- System reset input
        rdy_i   : in  std_logic;                             -- Data ready input
        rdy_o   : out std_logic;                             -- Data ready output
        x1_i    : in  std_logic_vector(BSIZE - 1 downto 0);  -- Stream input 1
        x2_i    : in  std_logic_vector(BSIZE - 1 downto 0);  -- Stream input 2
        x3_i    : in  std_logic_vector(BSIZE - 1 downto 0);  -- Stream input 3
        y_o     : out std_logic_vector(BSIZE - 1 downto 0)   -- Median output
    );
end entity medfilt_stam;

architecture rtl of medfilt_stam is

    signal C1a, C1b, C2a, C2b, C3a, C3b : std_logic_vector(BSIZE - 1 downto 0); -- Intermediate chain data
    signal rdy_c1, rdy_c2               : std_logic;                            -- Carry ready chain

begin

    -- Median calculator systolic array
    C1 : entity work.medfilt_bblock
        generic map (
            BSIZE   => BSIZE
        )
        port map (
            clk_i   => clk_i,
            rst_n_i => rst_n_i,
            rdy_i   => rdy_i,
            rdy_o   => rdy_c1,
            A1i     => x1_i,
            A2i     => x2_i,
            B1i     => C1a,
            B2i     => C1b
        );

    C2 : entity work.medfilt_bblock
        generic map (
            BSIZE   => BSIZE
        )
        port map (
            clk_i   => clk_i,
            rst_n_i => rst_n_i,
            rdy_i   => rdy_c1,
            rdy_o   => rdy_c2,
            A1i     => C1b,
            A2i     => x3_i,
            B1i     => C2a,
            B2i     => C2b
        );

    C3 : entity work.medfilt_bblock
        generic map (
            BSIZE   => BSIZE
        )
        port map (
            clk_i   => clk_i,
            rst_n_i => rst_n_i,
            rdy_i   => rdy_c2,
            rdy_o   => rdy_o,
            A1i     => C1a,
            A2i     => C2a,
            B1i     => C3a,
            B2i     => C3b
        );

    -- Output
    y_o <= C3b;

end architecture rtl;
