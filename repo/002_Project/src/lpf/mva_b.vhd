----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             mva_b.vhd
-- module name      Moving average filter block
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Top Level Module
----------------------------------------------------------------------------------------------
-- create date      04.03.2020
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mva_b is
    generic (
        F_LENGTH       : integer := 50;                                -- Filter length
        DATA_SIZE      : integer := 12                                 -- Filter input/output data size
    );
    port(
        clk_i          : in  std_logic;                                -- System clock input
        rst_n_i        : in  std_logic;                                -- Synchronous reset input
        sample_pulse_i : in  std_logic;                                -- Sample clock input
        filter_data_i  : in  std_logic_vector(DATA_SIZE - 1 downto 0); -- Filter data input
        filter_data_o  : out std_logic_vector(DATA_SIZE - 1 downto 0)  -- Filter data output
    );
end entity mva_b;

architecture rtl of mva_b is

    constant C_WF      : integer := 15;                                              -- Fractional number of bits
    constant C_DIVIDER : integer := ((2**C_WF) / F_LENGTH);                          -- 1/L factor in fixed point

    subtype t_fx12wf is signed(DATA_SIZE + C_WF - 1 downto 0);                       -- Fixed point subtype definition
    subtype t_fx24wf is signed(DATA_SIZE * 2 + C_WF - 1 downto 0);                   -- Fixed point subtype definition 2

    type t_filter_vector is array (0 to F_LENGTH - 1) of signed(DATA_SIZE downto 0); -- Array type for the FIR structure
    signal accum : t_filter_vector;                                                  -- Fixed point summing vector

begin

    --TODO: REDO STRUCTURE TO REDUCE TRUNCATION TO AVOID EXCESSIVE GAIN ERROR AND BIAS

    process (clk_i)
        variable mult_0 : signed((DATA_SIZE + 1) + C_WF - 1 downto 0) := (others => '0');
    begin
        if rising_edge(clk_i) then
            if (rst_n_i = '0') then
                accum <= (others => (others => '0'));
            elsif (sample_pulse_i = '1') then
                mult_0        := SIGNED(filter_data_i)* TO_SIGNED(C_DIVIDER, C_WF + 1);
                FIR1 : for i in (F_LENGTH - 1) downto 0 loop
                    if i = 0 then
                        accum(0) <= mult_0(DATA_SIZE + C_WF downto C_WF);-- resize(signed(filter_data_i),DATA_SIZE + 1);
                    else
                        accum(i) <= accum(i - 1) + mult_0(DATA_SIZE + C_WF downto C_WF); --resize(signed(filter_data_i),DATA_SIZE + 1);
                    end if;
                end loop FIR1;
                filter_data_o <= std_logic_vector(accum(F_LENGTH - 1)(DATA_SIZE - 1 downto 0));
            end if;
        end if;
    end process;

end architecture rtl;

