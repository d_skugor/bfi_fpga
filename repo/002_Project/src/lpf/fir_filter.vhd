----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             fir_filter.vhd
-- module name      Finite Input Response Filter
-- author           Arturo Montufar (arturo.arreola@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Moving average filter
----------------------------------------------------------------------------------------------
-- create date      15.12.2019
-- Revision:
--      Revision 0.01 by arturo.arreola
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Filter is rewritten to support variable swithcing frequencies
--                    - Added 2 new input signals: pwm_sync and pwm_freq
--                        - pwm_sync is 1 clock cycle pulse with same frequency as currently
--                          active switching frequency
--                        - pwm_freq carries information which switching frequency is
--                          currently active
--
--      Revision 0.03 by david.pavlovic
--                    - Implemented simple moving average filter without support for
--                      variable switching frequency
--
--      Revision 0.04 by dario.drvenkar
--                    - update of moving average filter
--                    - Moving average filter with an odd number of samples (>=3)
--
--      Revision 0.05 by david.pavlovic
--                    - Code cleanup
--
----------------------------------------------------------------------------------------------
-- TODO: - nothing
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real."ceil";
use ieee.math_real."log2";

library unisim;
use unisim.vcomponents.all;

library unimacro;
use unimacro.vcomponents.all;

entity firFilter is
    generic (
        G_FIRFILTER_DATA_W           : natural := 12; -- Data width
        G_FIRFILTER_AVG_LEN          : natural := 7   -- Number of averaged samples (must be >= 3)
    );
    port (
        p_firFilter_reset_n_in       : in  std_logic;
        p_firFilter_clk_in           : in  std_logic;
        p_firFilter_sampleClk_in     : in  std_logic;
        p_firFilter_rawData_in       : in  std_logic_vector(11 downto 0);
        p_firFilter_filteredData_out : out std_logic_vector(11 downto 0)
    );
end entity firFilter;

architecture rtl of firFilter is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_FIRFILTER_DIV_CONST : integer := 262144 / G_FIRFILTER_AVG_LEN;   -- shifted by 2^18, if G_FIRFILTER_AVG_LEN is >= 3 , 2^-1 bit will always be 0
    constant C_FIRFILTER_DIV_W     : integer := 17;
    constant C_FIRFILTER_ACC_W     : integer := integer(ceil(log2(real(G_FIRFILTER_AVG_LEN * 2 ** G_FIRFILTER_DATA_W))));
    constant C_FIRFILTER_MUL_LAT   : integer := 3;

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    type t_delay_array is array (0 to G_FIRFILTER_AVG_LEN-1) of signed(G_FIRFILTER_DATA_W-1 downto 0);

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk           : std_logic;
    signal s_rst_n         : std_logic;
    signal s_rst           : std_logic;
    signal s_sample_clk    : std_logic;
    signal s_sample_clk_d  : std_logic;
    signal s_sample_clk_re : std_logic;
    signal s_data_i        : signed(G_FIRFILTER_DATA_W-1 downto 0);
    signal s_acc           : signed(C_FIRFILTER_ACC_W -1 downto 0);
    signal s_delay         : t_delay_array;
    signal s_mult_r        : signed(11 downto 0);
    signal s_mul_ce        : std_logic;
    signal s_mul_ce_d      : std_logic_vector(C_FIRFILTER_MUL_LAT-1 downto 0);
    signal s_mul_const     : std_logic_vector(C_FIRFILTER_DIV_W downto 0);  -- C_FIRFILTER_DIV_W + 1 bit for sign
    signal s_mul_acc       : std_logic_vector(s_acc'high downto s_acc'low);
    signal s_mul_out       : std_logic_vector(s_mul_const'high + s_mul_acc'high +1 downto 0 );

begin

    -- input signals assignments
    s_clk        <= p_firFilter_clk_in;
    s_rst_n      <= p_firFilter_reset_n_in;
    s_rst        <= not s_rst_n;
    s_sample_clk <= p_firFilter_sampleClk_in;

    -- s_sample_clk rising edge detector
    pedgeflt : edge_detector_p
        generic map (
            G_REG            => 1
        )
        port map (
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_sample_clk,
            p_edp_sig_re_out => s_sample_clk_re,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
        );

    -- SOB to TC conversion
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n <= '0') then
            s_data_i <= (others => '0');
        elsif rising_edge(s_clk) then
            s_data_i <= signed(not p_firFilter_rawData_in(G_FIRFILTER_DATA_W - 1) & p_firFilter_rawData_in(G_FIRFILTER_DATA_W - 2 downto 0));
        end if;
    end process;

    p_average : process(s_rst_n, s_clk)
    begin
        if(s_rst_n = '0') then
            s_acc   <= (others=>'0');
            s_delay <= (others=>(others=>'0'));
        elsif rising_edge(s_clk) then
            if (s_sample_clk_re = '1') then
                s_delay <= signed(s_data_i) & s_delay(0 to s_delay'length-2);
                s_acc   <= s_acc + signed(s_data_i)-s_delay(s_delay'length-1);
            end if;
        end if;
    end process p_average;

    process (s_rst_n, s_clk)
    begin
        if (s_rst_n = '0') then
            s_mul_ce_d <= (others => '0');
        elsif rising_edge(s_clk) then
            for i in 0 to s_mul_ce_d'length-2 loop
                s_mul_ce_d(i+1) <= s_mul_ce_d(i);
            end loop;
            s_mul_ce_d(0) <= s_sample_clk_re;
        end if;
    end process;

    s_mul_ce    <= s_mul_ce_d(s_mul_ce_d'high) OR s_mul_ce_d(s_mul_ce_d'high-1) OR s_mul_ce_d(s_mul_ce_d'high-2);
    s_mul_const <= '0' & std_logic_vector(to_unsigned(C_FIRFILTER_DIV_CONST,C_FIRFILTER_DIV_W));
    s_mul_acc   <= std_logic_vector(s_acc);

   -- MULT_MACRO: Multiply Function implemented in a DSP48E
   --             Artix-7
   -- Xilinx HDL Language Template, version 2018.3

    MULT_MACRO_inst : MULT_MACRO
    generic map (
        DEVICE  => "7SERIES",           -- Target Device: "VIRTEX5", "7SERIES", "SPARTAN6"
        LATENCY => C_FIRFILTER_MUL_LAT, -- Desired clock cycle latency, 0-4
        WIDTH_A => s_mul_acc'length,    -- Multiplier A-input bus width, 1-25
        WIDTH_B => s_mul_const'length   -- Multiplier B-input bus width, 1-18
    )
    port map (
        P       => s_mul_out,           -- Multiplier ouput bus, width determined by WIDTH_P generic
        A       => s_mul_acc,           -- Multiplier input A bus, width determined by WIDTH_A generic
        B       => s_mul_const,         -- Multiplier input B bus, width determined by WIDTH_B generic
        CE      => s_mul_ce,            -- 1-bit active high input clock enable
        CLK     => s_clk,               -- 1-bit positive edge clock input
        RST     => s_rst                -- 1-bit input active high reset
    );

    -- rounding process
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_mult_r <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_mul_out(C_FIRFILTER_DIV_W) = '1') then
                s_mult_r <= signed(s_mul_out(G_FIRFILTER_DATA_W + C_FIRFILTER_DIV_W downto C_FIRFILTER_DIV_W + 1)) + 1;
            else
                s_mult_r <= signed(s_mul_out(29 downto 18));
            end if;
        end if;
    end process;

    -- TC to SOB conversion
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            p_firFilter_filteredData_out <= (others => '0');
        elsif rising_edge(s_clk) then
            p_firFilter_filteredData_out <= std_logic_vector(unsigned(not s_mult_r(11) & s_mult_r(10 downto 0)));
        end if;
    end process;

end architecture rtl;