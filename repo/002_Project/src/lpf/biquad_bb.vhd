----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             biquad_bb.vhd
-- module name      Basic parametrizable biquad filter building block
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Top Level Module
----------------------------------------------------------------------------------------------
-- create date      19.11.2019
-- Revision:
--      Revision 0.01 by jsq
--                    File Created
--      Revision 0.02 by jsq
--                    Added more commends and header description.
--      Revision 0.03 by jsq
--                    Modified scaling and generic list.
--                    Coefficients do not need to be divided by two externally.
----------------------------------------------------------------------------------------------
-- Description
--
-- This is a BiQuad filtering structure, built using a modified graph to reduce the register length.
-- The module expects a sampling pulse, of a single clk duration.
--------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity biquad_bb is
    generic (
        DATA_SIZE      : integer := 16;
        COEFF_SIZE     : integer := 16;
        A1             : integer := 0;
        A2             : integer := 0;
        B0             : integer := 32767;
        B1             : integer := 0;
        B2             : integer := 0
    );
    port (
        clk_i          : in  std_logic;                                --! System clock input
        rst_n_i        : in  std_logic;                                --! Synchronous reset input
        sample_pulse_i : in  std_logic;                                --! Sample clock input
        filter_data_i  : in  std_logic_vector(DATA_SIZE - 1 downto 0); --! Filter data input
        filter_data_o  : out std_logic_vector(DATA_SIZE - 1 downto 0)  --! Filter data output
    );
end entity biquad_bb;

architecture rtl of biquad_bb is

    -- Filter constants
    -- Coefficients have to be encoded with a 1/2 factor, so they are all normalized to 1
    constant A1_COEFF : signed(COEFF_SIZE - 1 downto 0) := to_signed(A1/2, COEFF_SIZE);
    constant A2_COEFF : signed(COEFF_SIZE - 1 downto 0) := to_signed(A2/2, COEFF_SIZE);
    constant B0_COEFF : signed(COEFF_SIZE - 1 downto 0) := to_signed(B0/2, COEFF_SIZE);
    constant B1_COEFF : signed(COEFF_SIZE - 1 downto 0) := to_signed(B1/2, COEFF_SIZE);
    constant B2_COEFF : signed(COEFF_SIZE - 1 downto 0) := to_signed(B2/2, COEFF_SIZE);

    -- Accumulators
    signal zb0 : signed(COEFF_SIZE - 1 downto 0);
    signal zb1 : signed(COEFF_SIZE - 1 downto 0);
    signal zb2 : signed(COEFF_SIZE - 1 downto 0);
    signal za0 : signed(COEFF_SIZE - 1 downto 0);
    signal za1 : signed(COEFF_SIZE - 1 downto 0);
    signal za2 : signed(COEFF_SIZE - 1 downto 0);

    -- Input / output vectors
    signal xi : signed(filter_data_i'range);
    signal yo : signed(filter_data_o'range);

begin

    xi <= signed(filter_data_i);

    filter_zero : process (clk_i)
        -- Products
        variable pb0 : signed(COEFF_SIZE + DATA_SIZE - 1 downto 0) := (others => '0');
        variable pb1 : signed(COEFF_SIZE + DATA_SIZE - 1 downto 0) := (others => '0');
        variable pb2 : signed(COEFF_SIZE + DATA_SIZE - 1 downto 0) := (others => '0');
    begin
        if rising_edge(clk_i) then
            if (rst_n_i = '0') then
                zb0 <= (others => '0');
                zb1 <= (others => '0');
                zb2 <= (others => '0');
            elsif (sample_pulse_i = '1') then
                pb0 := xi * B0_COEFF;
                pb1 := xi * B1_COEFF;
                pb2 := xi * B2_COEFF;

                zb0 <= pb0(pb0'LEFT - 1 downto DATA_SIZE - 1) + zb1;
                zb1 <= pb1(pb0'LEFT - 1 downto DATA_SIZE - 1) + zb2;
                zb2 <= pb2(pb0'LEFT - 1 downto DATA_SIZE - 1);
            end if;
        end if;
    end process;

    filter_pole : process (clk_i)
        -- Products
        variable pa1 : signed(2*COEFF_SIZE - 1 downto 0) := (others => '0');
        variable pa2 : signed(2*COEFF_SIZE - 1 downto 0) := (others => '0');
        variable pa0 : signed(COEFF_SIZE - 1 downto 0) := (others => '0');
    begin
        if rising_edge(clk_i) then
            if (rst_n_i = '0') then
                za1 <= (others => '0');
                za2 <= (others => '0');
            elsif (sample_pulse_i = '1') then
                pa1 := za1 * A1_COEFF;
                pa2 := za1 * A2_COEFF;
                za2 <= zb0 - pa2(pa2'LEFT - 1 downto COEFF_SIZE - 1);
                pa0 := za2 - pa1(pa1'LEFT - 1 downto COEFF_SIZE - 1);
                za1 <= pa0(pa0'LEFT - 1 downto 0)&'0';
            end if;
        end if;
    end process;

    yo            <= za1(za1'LEFT downto za1'LEFT - DATA_SIZE + 1);
    filter_data_o <= std_logic_vector(yo);

end architecture rtl;
