----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             medfilt_bblock.vhd
-- module name      Median filter systolic array building block
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Building block definition for the median calculator systolic array.
--
----------------------------------------------------------------------------------------------
-- create date      01.10.2020
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
--      Revision 0.02 by dario.drvenkar
--                      Comparison of numbers is done in unsigned arithmetic because
--                      output from ADC is in offset binary format
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity medfilt_bblock is
    generic (
        BSIZE   : natural := 12                             -- Data size
    );
    port(
        clk_i   : in  std_logic;                            -- System clock input
        rst_n_i : in  std_logic;                            -- System reset input
        rdy_i   : in  std_logic;                            -- Data ready input
        rdy_o   : out std_logic;                            -- Data ready output
        A1i     : in  std_logic_vector(BSIZE - 1 downto 0); -- A1 input
        A2i     : in  std_logic_vector(BSIZE - 1 downto 0); -- A2 input
        B1i     : out std_logic_vector(BSIZE - 1 downto 0); -- B1 output
        B2i     : out std_logic_vector(BSIZE - 1 downto 0)  -- B2 output
    );

end entity medfilt_bblock;

architecture rtl of medfilt_bblock is

begin

    maincmp : process(clk_i, rst_n_i)
        variable x1, x2 : unsigned(BSIZE - 1 downto 0);
    begin
        if (rst_n_i = '0') then
            B1i <= (others => '0');
            B2i <= (others => '0');
        elsif rising_edge(clk_i) then
            rdy_o <= '0';
            if (rdy_i = '1') then
                rdy_o <= '1';
                x1    := unsigned(A1i);
                x2    := unsigned(A2i);
                if (x1 >= x2) then
                    B1i <= A1i;
                    B2i <= A2i;
                else
                    B1i <= A2i;
                    B2i <= A1i;
                end if;
            end if;
        end if;
    end process;

end architecture rtl;
