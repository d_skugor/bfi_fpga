----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             medfilt_block.vhd
-- module name      Median filter block
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Median filter basic block
--
----------------------------------------------------------------------------------------------
-- create date      01.10.2020
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
--      Revision 0.02 by jsq
--                      Expanding input to 3 phases, to reuse resources.
--      Revision 1.00 by jsq
--                      Updated structure.
--                      Removing pwm frequency input port, as it is not required anymore.
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;
use work.pwm_pack.all;

entity medfilt_block is
    generic (
        BSIZE      : natural := 12                              --! Data size
    );
    port (
        clk_i      : in  std_logic;                             --! System clock input
        rst_n_i    : in  std_logic;                             --! System reset input
        sample_p_i : in  std_logic;                             --! Sample pulse from ADC
        sync_p_i   : in  std_logic;                             --! PWM synchronism pulse input
        rdy_p_o    : out std_logic;                             --! Output ready pulse
        x_1_i      : in  std_logic_vector(BSIZE - 1 downto 0);  --! Stream input 1
        x_2_i      : in  std_logic_vector(BSIZE - 1 downto 0);  --! Stream input 2
        x_3_i      : in  std_logic_vector(BSIZE - 1 downto 0);  --! Stream input 3
        y_1_o      : out std_logic_vector(BSIZE - 1 downto 0);  --! Sample output 1
        y_2_o      : out std_logic_vector(BSIZE - 1 downto 0);  --! Sample output 2
        y_3_o      : out std_logic_vector(BSIZE - 1 downto 0)   --! Sample output 3
    );
end entity medfilt_block;

architecture rtl of medfilt_block is

    signal x11, x12, x13  : std_logic_vector(BSIZE - 1 downto 0);
    signal x21, x22, x23  : std_logic_vector(BSIZE - 1 downto 0);
    signal x31, x32, x33  : std_logic_vector(BSIZE - 1 downto 0);
    signal rdy_1, rdy_2   : std_logic;
    signal cycle_sample_p : std_logic;

begin

    -- Sample sequencer
    SS1 : entity work.medfilt_sequencer
        port map (
            clk_i      => clk_i,
            rst_n_i    => rst_n_i,
            rdy_p_o    => rdy_1,
            sample_p_o => cycle_sample_p,
            sample_p_i => sample_p_i,
            start_p_i  => sync_p_i
        );

    -- Sample frame buffer 1
    SF1 : entity work.medfilt_sampler
        generic map (
            BSIZE      => BSIZE
        )
        port map(
            clk_i      => clk_i,
            rst_n_i    => rst_n_i,
            rdy_p_o    => rdy_2,
            x_i        => x_1_i,
            y1_o       => x11,
            y2_o       => x12,
            y3_o       => x13,
            sample_p_i => cycle_sample_p,
            rdy_p_i    => rdy_1
        );

    -- Sample frame buffer 2
    SF2 : entity work.medfilt_sampler
        generic map (
            BSIZE      => BSIZE
        )
        port map (
            clk_i      => clk_i,
            rst_n_i    => rst_n_i,
            rdy_p_o    => open,
            x_i        => x_2_i,
            y1_o       => x21,
            y2_o       => x22,
            y3_o       => x23,
            sample_p_i => cycle_sample_p,
            rdy_p_i    => rdy_1
        );

    -- Sample frame buffer 3
    SF3 : entity work.medfilt_sampler
        generic map (
            BSIZE      => BSIZE
        )
        port map (
            clk_i      => clk_i,
            rst_n_i    => rst_n_i,
            rdy_p_o    => open,
            x_i        => x_3_i,
            y1_o       => x31,
            y2_o       => x32,
            y3_o       => x33,
            sample_p_i => cycle_sample_p,
            rdy_p_i    => rdy_1
        );

    -- Median calculator 1
    MC1 : entity work.medfilt_stam
        generic map (
            BSIZE => BSIZE
        )
        port map (
            clk_i   => clk_i,
            rst_n_i => rst_n_i,
            rdy_i   => rdy_2,
            rdy_o   => rdy_p_o,
            x1_i    => x11,
            x2_i    => x12,
            x3_i    => x13,
            y_o     => y_1_o
        );

    -- Median calculator 2
    MC2 : entity work.medfilt_stam
        generic map(
            BSIZE   => BSIZE
        )
        port map(
            clk_i   => clk_i,
            rst_n_i => rst_n_i,
            rdy_i   => rdy_2,
            rdy_o   => open,
            x1_i    => x21,
            x2_i    => x22,
            x3_i    => x23,
            y_o     => y_2_o
        );

    -- Median calculator 3
    MC3 : entity work.medfilt_stam
        generic map (
            BSIZE   => BSIZE
        )
        port map (
            clk_i   => clk_i,
            rst_n_i => rst_n_i,
            rdy_i   => rdy_2,
            rdy_o   => open,
            x1_i    => x31,
            x2_i    => x32,
            x3_i    => x33,
            y_o     => y_3_o
        );

end architecture rtl;
