----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             medfilt_sampler.vhd
-- module name      Median filter sampler
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Median filter sampling double buffer
--
----------------------------------------------------------------------------------------------
-- create date      01.10.2020
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity medfilt_sampler is
    generic (
        BSIZE      : natural := 12                             --! Data size
    );
    port (
        clk_i      : in  std_logic;                            --! System clock input
        rst_n_i    : in  std_logic;                            --! System reset input
        rdy_p_o    : out std_logic;                            --! New frame ready pulse output
        x_i        : in  std_logic_vector(BSIZE - 1 downto 0); --! Sample input
        y1_o       : out std_logic_vector(BSIZE - 1 downto 0); --! Frame sample 1 output
        y2_o       : out std_logic_vector(BSIZE - 1 downto 0); --! Frame sample 2 output
        y3_o       : out std_logic_vector(BSIZE - 1 downto 0); --! Frame sample 3 output
        sample_p_i : in  std_logic;                            --! Sample pulse input
        rdy_p_i    : in  std_logic                             --! Frame ready pulse input
    );

end entity medfilt_sampler;

architecture rtl of medfilt_sampler is

    signal newFrame_p          : std_logic;
    signal spb_0, spb_1, spb_2 : std_logic_vector(BSIZE - 1 downto 0);

begin

    sample_buffer : process (clk_i, rst_n_i)
    begin
        if (rst_n_i = '0') then
            y1_o  <= (others => '0');
            y2_o  <= (others => '0');
            y3_o  <= (others => '0');
            spb_0 <= (others => '0');
            spb_1 <= (others => '0');
            spb_2 <= (others => '0');
        elsif rising_edge(clk_i) then
            rdy_p_o <= '0';
            if (sample_p_i = '1' and rdy_p_i = '1') then
                spb_2   <= spb_1;
                spb_1   <= spb_0;
                spb_0   <= x_i;
                y1_o    <= spb_1;
                y2_o    <= spb_0;
                y3_o    <= x_i;
                rdy_p_o <= '1';
            elsif (sample_p_i = '1') then
                -- Get a new sample into de buffer
                spb_2 <= spb_1;
                spb_1 <= spb_0;
                spb_0 <= x_i;
            elsif (rdy_p_i = '1') then
                -- Transfer new frame to the output
                y1_o    <= spb_0;
                y2_o    <= spb_1;
                y3_o    <= spb_2;
                rdy_p_o <= '1';
            end if;
        end if;
    end process;

end architecture rtl;