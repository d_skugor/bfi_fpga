
----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             synchronous_filter.vhd
-- module name      Synchronous filter - Behavioral
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            This block is part of the current measurement ADC control block.
--                  This block contains the ADC clock generator, filter and resampler.
----------------------------------------------------------------------------------------------
-- create date      05.11.2020
-- Revision:
--      Revision 0.01 jsanchez
--                  - Initial version
----------------------------------------------------------------------------------------------
--
-- NOTE: This is a VHDL2008 file
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity synchronous_filter is
    port (
        -- Clock and reset (system clock)
        clk_in            : in  std_logic;
        rst_n_in          : in  std_logic;
        -- Output interface
        sample_p_in       : in  std_logic;                     -- System sample pulse train
        ch1_out           : out std_logic_vector(11 downto 0); -- Channel 1 measured current, filtered
        ch2_out           : out std_logic_vector(11 downto 0); -- Channel 2 measured current, filtered
        ch3_out           : out std_logic_vector(11 downto 0); -- Channel 3 measured current, filtered
        -- ADC driver interface
        adc_clk_out       : out std_logic;                     -- ADC clk output
        adc_rst_out       : out std_logic;                     -- ADC reset
        ch1_in            : in  std_logic_vector(11 downto 0); -- ADC channel 1 samples
        ch2_in            : in  std_logic_vector(11 downto 0); -- ADC channel 2 samples
        ch3_in            : in  std_logic_vector(11 downto 0); -- ADC channel 3 samples
        adc_data_rdy_p_in : in  std_logic;                     -- ADC data ready pulse
        -- System
        sync_p_a_in       : in  std_logic                      -- PWM Synchro pulse (asynchronous)
    );
end entity synchronous_filter;

architecture rtl of synchronous_filter is

    -- System interconnect
    signal s_adc_clk : std_logic;                       -- ADC clock
    signal s_rdy     : std_logic;                       -- Ready flag
    signal s_irst_n  : std_logic;                       -- Internal reset synchronous to the ADC clock
    signal s_rst_n_a : std_logic;                       -- Asynchronous reset

    -- Control signals
    signal s_adc_rdy_p : std_logic;                     -- New frame ready flag

    -- Baseband processed signals
    signal s_ch1_lpf   : std_logic_vector(11 downto 0); -- Filtered phase A
    signal s_ch2_lpf   : std_logic_vector(11 downto 0); -- Filtered phase B
    signal s_ch3_lpf   : std_logic_vector(11 downto 0); -- Filtered phase C
    signal s_lpf_rdy_p : std_logic;                     -- Filtered data frame ready

begin

    adc_clk_out <= s_adc_clk;
    adc_rst_out <= s_irst_n;

    PLL_ClkGenerator : entity work.pll_clk_gen
        generic map (
            CLK_PERIOD    => 10.0,
            CLK_MULT      => 12,   -- Use VCO = 1200 MHz, then divide by 10 to reach 120 MHz
            CLK1_DIV      => 10,
            CLK2_DIV      => 12,
            CLK3_DIV      => 12
        )
        port map (
            clk_system_in => clk_in,
            rst_n_in      => rst_n_in,
            ready_out     => s_rdy,
            clk1_out      => s_adc_clk,
            clk2_out      => open,
            clk3_out      => open
        );

    s_rst_n_a <= rst_n_in and s_rdy;     -- Keep reset active if PLL is not ready

    Reset_gen : entity work.reset_generator
        port map (
            clk_in        => s_adc_clk,
            asyn_rst_n_in => s_rst_n_a,
            syn_rst_n_out => s_irst_n
        );

    s_adc_rdy_p <= adc_data_rdy_p_in;

    Retimer : entity work.frame_retimer
        port map(
            dom1_clk_in       => s_adc_clk,
            dom1_rst_n_in     => s_irst_n,
            dom2_clk_in       => clk_in,
            dom2_rst_n_in     => rst_n_in,
            dom1_dataRdy_p_in => s_lpf_rdy_p,
            dom1_ch1_in       => s_ch1_lpf,
            dom1_ch2_in       => s_ch2_lpf,
            dom1_ch3_in       => s_ch3_lpf,
            dom2_sample_p_in  => sample_p_in,
            dom2_ch1_out      => ch1_out,
            dom2_ch2_out      => ch2_out,
            dom2_ch3_out      => ch3_out
        );

end architecture rtl;
