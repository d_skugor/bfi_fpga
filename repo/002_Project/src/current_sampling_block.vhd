
----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             current_sampling_block.vhd
-- module name      current_sampling_block - Behavioral
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Current measurement ADC control block. This block generates the ADC sampling
--                  train, filters the ADC samples and resynchronizes the output to the system
--                  clock domain.
----------------------------------------------------------------------------------------------
-- create date      03.11.2020
-- Revision:
--      Revision 0.01 jsanchez
--                  - Initial version
--
--      Revision 0.02 by jsanchez
--                  - Removing rdy_out signal, as it is not used finally.
--                  - Use adc_driver_top module instead of adc_driver
--
--      Revision 0.03 by jsanchez
--                  - Corrected output error ADC output, which was inverted erroneously signaling
--                      a fault when the ADC was operating properly.
----------------------------------------------------------------------------------------------
--
-- NOTE: This is a VHDL2008 file
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity current_sampling_block is
    port (
        -- Clock and reset (system clock)
        clk_in           : in    std_logic;
        rst_n_in         : in    std_logic;
        -- ADC interface
        p_adc_conv_n_out : out   std_logic;                     -- conversion trigger (active low)
        p_adc_cs0_n_out  : out   std_logic;                     -- control signal (active low)
        p_adc_cs1_out    : out   std_logic;                     -- control signal (active high)
        p_adc_rd_n_out   : out   std_logic;                     -- read trigger (active low)
        p_adc_wr_n_out   : out   std_logic;                     -- write trigger (active low)
        p_adc_dav_in     : in    std_logic;                     -- data available flag
        p_adc_data_bi    : inout std_logic_vector(11 downto 0); -- ADC data bus
        -- Output interface
        sample_p_in      : in    std_logic;                     -- System sample pulse train
        ch1_out          : out   std_logic_vector(11 downto 0); -- Channel 1 measured current, filtered
        ch2_out          : out   std_logic_vector(11 downto 0); -- Channel 2 measured current, filtered
        ch3_out          : out   std_logic_vector(11 downto 0); -- Channel 3 measured current, filtered
        -- Status
        err_adc_out      : out   std_logic;                     -- ADC error flag (initialization error)
        -- System
        sync_p_a_in      : in    std_logic                      -- PWM Synchro pulse (asynchronous)
    );
end entity current_sampling_block;

architecture rtl of current_sampling_block is

    -- System interconnect
    signal s_adc_clk          : std_logic;                     -- ADC clock
    signal s_irst_n           : std_logic;                     -- Internal reset synchronous to the ADC clock

    -- Sampled frame data
    signal s_ch0_adc          : std_logic_vector(11 downto 0); -- Phase A
    signal s_ch1_adc          : std_logic_vector(11 downto 0); -- Phase B
    signal s_ch2_adc          : std_logic_vector(11 downto 0); -- Phase C
    signal s_ch3_adc          : std_logic_vector(11 downto 0); -- <unused>

    -- Control and status signals
    signal s_adc_rdy_p        : std_logic;                     -- New frame ready flag
    signal s_adc_init_fault_n : std_logic;                     -- ADC initialization fault (inverted logic)

begin

    SYNC_LPF : entity work.synchronous_filter
        port map (
            clk_in            => clk_in,
            rst_n_in          => rst_n_in,
            sample_p_in       => sample_p_in,
            ch1_out           => ch1_out,
            ch2_out           => ch2_out,
            ch3_out           => ch3_out,
            adc_clk_out       => s_adc_clk,
            adc_rst_out       => s_irst_n,
            ch1_in            => s_ch0_adc,
            ch2_in            => s_ch1_adc,
            ch3_in            => s_ch2_adc,
            adc_data_rdy_p_in => s_adc_rdy_p,
            sync_p_a_in       => sync_p_a_in
        );

    ADC_interface : entity work.adc_driver_top
        generic map (
            G_TRIGGER_LEVEL          => 0,
            G_CONV_MODE              => '1',
            G_OUTPUT_FORMAT          => '1',
            G_FIFO_RESET             => '0',
            G_ADC_AUTO_OFFSET        => '0',
            G_ADC_REF_EXT            => '0',
            G_ADC_WD_MAX_ERR         => 3
        )
        port map (
            p_adc_top_clk_in         => s_adc_clk,
            p_adc_top_rst_n_in       => s_irst_n,
            p_adc_top_conv_n_out     => p_adc_conv_n_out,
            p_adc_top_cs0_n_out      => p_adc_cs0_n_out,
            p_adc_top_cs1_out        => p_adc_cs1_out,
            p_adc_top_rd_n_out       => p_adc_rd_n_out,
            p_adc_top_wr_n_out       => p_adc_wr_n_out,
            p_adc_top_dav_in         => p_adc_dav_in,
            p_adc_top_data_bi        => p_adc_data_bi,
            p_top_adc_dataCh0_out    => s_ch0_adc,
            p_top_adc_dataCh1_out    => s_ch1_adc,
            p_top_adc_dataCh2_out    => s_ch2_adc,
            p_top_adc_dataCh3_out    => s_ch3_adc,
            p_top_adc_dataRdy_out    => s_adc_rdy_p,
            p_top_adc_flt_init_n_out => s_adc_init_fault_n
        );

    err_adc_out <= not s_adc_init_fault_n;

end architecture rtl;
