----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             clkdiv.vhd
-- module name      Clock divider - Behavioral
-- author           Pablo Velasco (pablo.velasco@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100T-1CSG324Q
-- brief            clock divider for INHB
-- note             used to generate 5MHz @50% duty cycle clock for INHB of resolver
----------------------------------------------------------------------------------------------
-- create date      29.08.2019
-- Revision:
--      Revision 0.01 by pablo.velasco
--                    - File Created
--
--      Revision 0.02 by arturo.montufar
--                    - Updated naming and removed tabs
----------------------------------------------------------------------------------------------
-- TODO: - use gray counters
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity res_clkDiv is
    generic (
        G_COUNT_LIM       : unsigned(7 downto 0):= "00001001"  -- number of main clk cycles on each polarity; count starts at 0; a limit of 9 = "00001001" will result in a 20 times division which will output a 5MHz clock
    );
    port (
        -- main inputs
        p_clkdiv_rst_n_in : in std_logic; -- main reset line
        p_clkdiv_clk_in   : in std_logic; -- main system clock (100 MHz)
        --outputs
        p_clkdiv_clk_out  : out std_logic -- output of divided clock
    );
end res_clkDiv;


architecture behavioral of res_clkDiv is

    signal s_count  : unsigned(7 downto 0);
    signal s_clkval : std_logic;

begin

    -- clock divider process
    process (p_clkdiv_clk_in, p_clkdiv_rst_n_in)
    begin
        -- reset
        if (p_clkdiv_rst_n_in = '0') then
            s_count  <= (others=>'0');
            s_clkval <= '0';
            p_clkdiv_clk_out <= '0';
        -- clock increment and output toggle
        elsif(rising_edge (p_clkdiv_clk_in)) then
            s_count <= s_count + 1;
            if (s_count = G_COUNT_LIM) then
                s_clkval <= not s_clkval;
                s_count  <= (others=>'0');
            end if;
            p_clkdiv_clk_out <= s_clkval;
        end if;
    end process;

end behavioral;
