----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             res_driver.vhd
-- module name      res_driver
-- author           Dario Drvenkar (dario.drvenkar@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100T-1CSG324Q
-- brief            Resolver interface driver
-- note             Based on section 7.3.10.1 (p.38) of the PGA411-Q1 Resolver Sensor
--                  this block receives output data and output clock from resolver
--                  and will update angle on every read
----------------------------------------------------------------------------------------------
-- create date      25.03.2021
-- Revision:
--      Revision 0.01 by dario.drvenkar
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Code cleanup
--
--      Revision 0.03 by danijela.skugor
--                    - Substitute binary counter with gray code counter
--
----------------------------------------------------------------------------------------------
-- TODO: - none
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity res_driver is
    generic (
        G_RES_DRIVER_CLK             : integer;                           -- Input clock in MHz
        G_RES_DRIVER_RES_UPDATE_RATE : integer                            -- Resolver update rate in kHz
    );
    port (
        -- main inputs
        p_res_driver_rst_n_in        :  in std_logic;                     -- main reset line
        p_res_driver_clk_in          :  in std_logic;                     -- main system clock
        p_res_driver_sample_in       :  in std_logic;                     -- signal controlling ADC sampling
        -- resolver inputs
        p_res_driver_ord_in          :  in std_logic_vector(11 downto 0); -- 12-bit parallel data in
--      p_res_driver_ordclk_in       :  in std_logic;                     -- sample ready clock in; not used (hardware deficiency)
        -- outputs
        p_res_driver_va0_out         : out std_logic;
        p_res_driver_va1_out         : out std_logic;
        p_res_driver_inhb_out        : out std_logic;
        p_res_driver_data_val_p_out  : out std_logic;
        p_res_driver_angle_out       : out std_logic_vector(11 downto 0); -- 12-bit angle value
        p_res_driver_velocity_out    : out std_logic_vector(11 downto 0)  -- 12-bit angle value
    );
end entity res_driver;

architecture rtl of res_driver is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;
    
    component gray_counter is
        generic(
            G_GRAYCNT_WIDTH             : integer := 4;
            G_GRAYCNT_CORRECTION_ON     : boolean := false  -- can gray counter fix itself if an error occurs
        );
        port(
            p_grayCnt_clk_in    : in std_logic;
            p_grayCnt_rst_n_in  : in std_logic;  -- sync reset
            p_grayCnt_en_in     : in std_logic;  -- enable
            p_grayCnt_err_p_out : out std_logic; -- pulsed error
            p_grayCnt_res_out   : out std_logic_vector(G_GRAYCNT_WIDTH -1 downto 0)
        );
    end component;    
    
---------------------------------------------------------------------------------
--                                   functions
---------------------------------------------------------------------------------

    function bin_to_gray (bin : std_logic_vector) return std_logic_vector is
        variable v_gray : std_logic_vector(bin'range);
    begin
        v_gray(bin'left) := bin(bin'left);
        for i in bin'left-1 downto 0 loop
            v_gray(i) := bin(i+1) xor bin(i);
        end loop;
        return v_gray;
    end function bin_to_gray;
    
    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_COUNT_RES_UPDATE : integer := (G_RES_DRIVER_CLK * 1000) / (G_RES_DRIVER_RES_UPDATE_RATE);
    constant C_INHB_PULSE       : integer := (G_RES_DRIVER_CLK * 100 / 1000);   -- INHB puls must be > 100 ns; 10^6*10^-9 = 10^-3 => /1000
    constant C_ORD_STABLE       : integer := (G_RES_DRIVER_CLK * 125 / 1000) + 1;   -- delay after falling edge of INHB must be > 125 ns
    constant C_CNT_WIDTH        : integer := 4;

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------
    -- state machine type definition
    type t_resolverStateMachine is (
        t_resolverStateMachine_idle,
        t_resolverStateMachine_set_inhb,
        t_resolverStateMachine_wait_ord_stable,
        t_resolverStateMachine_sample_ord
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_rst_n    : std_logic;
    signal s_clk      : std_logic;
    signal s_sample   : std_logic;
    signal s_sample_p : std_logic;
    signal s_resState : t_resolverStateMachine; -- state machine variable

    signal s_va0            : std_logic;
    signal s_va1            : std_logic;
    signal s_inhb           : std_logic;
    signal s_ord            : std_logic_vector(11 downto 0);
    signal s_val_p          : std_logic;
    signal s_angle          : std_logic_vector(11 downto 0);

    signal s_count_en       : std_logic;
    signal s_count_rst_n    : std_logic;
    signal s_count          : std_logic_vector(C_CNT_WIDTH-1 downto 0);      --counter for setting up delay between transactions

begin

      -- input signals assignments
    s_rst_n  <= p_res_driver_rst_n_in;
    s_clk    <= p_res_driver_clk_in;
    s_sample <= p_res_driver_sample_in;
    s_ord    <= p_res_driver_ord_in;

    -- output signals assignments
    p_res_driver_va0_out        <= s_va0;
    p_res_driver_va1_out        <= s_va1;
    p_res_driver_data_val_p_out <= s_val_p;
    p_res_driver_angle_out      <= s_angle;
    p_res_driver_velocity_out   <= (others => '0');
    p_res_driver_inhb_out       <= s_inhb;

    -- s_mcu_req rising edge detector
    pedgeflt : edge_detector_p
        generic map (
            G_REG            => 1
        )
        port map(
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            p_edp_sig_in     => s_sample,
            p_edp_sig_re_out => open,
            p_edp_sig_fe_out => s_sample_p,
            p_edp_sig_rf_out => open
    );
    
    -- Gray code pulse counter
    gray_cnt_res : gray_counter
        generic map(
            G_GRAYCNT_WIDTH            => C_CNT_WIDTH,
            G_GRAYCNT_CORRECTION_ON    => true
        )
        port map(
            p_grayCnt_clk_in    => s_clk,
            p_grayCnt_rst_n_in  => s_count_rst_n,
            p_grayCnt_en_in     => s_count_en,
            p_grayCnt_err_p_out => open,
            p_grayCnt_res_out   => s_count
        );    

    resolver_fsm : process(s_rst_n, s_clk)
    begin
        if(s_rst_n = '0') then
            s_resState    <= t_resolverStateMachine_idle;
            s_count_rst_n <= '0';
            s_count_en    <= '0';
            s_va0         <= '0';
            s_va1         <= '0';
            s_inhb        <= '0';
            s_angle       <= (others => '0');
            s_val_p      <= '0';
        elsif rising_edge(s_clk) then
            s_count_rst_n <= '1';
            s_va0   <= '0';
            s_va1   <= '1';
            s_inhb  <= '0';
            s_val_p <= '0';
            case s_resState is
                -----------------------------------------------------------------------
                when t_resolverStateMachine_idle =>
                    -- wait for signal from counter
                    if (s_sample_p = '1') then
                         s_count_en <= '1';
                        s_resState <= t_resolverStateMachine_set_inhb;
                    end if;
                -----------------------------------------------------------------------
                when t_resolverStateMachine_set_inhb =>
                    s_inhb <= '1';
                    if (s_count = bin_to_gray(std_logic_vector((to_unsigned((C_INHB_PULSE -1 ),C_CNT_WIDTH))))) then
                        s_count_rst_n <= '0';
                        s_count_en    <= '0';                      
                    elsif (s_count = bin_to_gray(std_logic_vector((to_unsigned((C_INHB_PULSE ),C_CNT_WIDTH))))) then
                        s_resState <= t_resolverStateMachine_wait_ord_stable; 
                        s_count_en    <= '1';
                    end if;
                -----------------------------------------------------------------------
                when t_resolverStateMachine_wait_ord_stable =>
                    s_inhb <= '0';
                    if (s_count = bin_to_gray(std_logic_vector((to_unsigned((C_ORD_STABLE - 1),C_CNT_WIDTH))))) then
                        s_count_rst_n <= '0';
                        s_count_en    <= '0';
                    elsif (s_count = bin_to_gray(std_logic_vector((to_unsigned((C_ORD_STABLE),C_CNT_WIDTH))))) then
                        s_resState <= t_resolverStateMachine_sample_ord;
                    end if;
                -----------------------------------------------------------------------
                when t_resolverStateMachine_sample_ord =>
                    s_angle <= s_ord;
                    s_val_p <= '1';
                    s_resState <= t_resolverStateMachine_idle;
                -----------------------------------------------------------------------
            end case;
        end if;
    end process;

end architecture rtl;