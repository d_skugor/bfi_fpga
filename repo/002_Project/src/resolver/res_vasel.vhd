----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             vasel.vhd
-- module name      Velocity/Angle selector - Behavioral
-- author           Pablo Velasco (pablo.velasco@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100T-1CSG324Q
-- brief            Generate VA0 and VA1 signals for the PGA411-Q1 Resolver interface
-- note             Based on section 7.3.10.1 (p.38) of the PGA411-Q1 Resolver Sensor
--                  Interface datasheet (SLASE76E, November 2015, Revised August 2017).
--                  To enable the parallel output interface, OMODE pin must be high.
--                  VA0 and VA1 signals select velocity or angle output from sensor
--                  this module receives 5MHz clock input (INHB) and will switch VA values
--                  after 25ns of INHB falling edge and generate a trigger 125ns
--                  after INHB falling
----------------------------------------------------------------------------------------------
-- create date      30.08.2019
-- Revision:
--      Revision 0.01 by pablo.velasco
--                    - File Created
--
--      Revision 0.02 by arturo.montufar
--                    - Updated naming and removed tabs
--                    - moved port value assignment outside of the process
--
----------------------------------------------------------------------------------------------
-- TODO: - use Gray counter
--       - implement INHB edge detector in another block
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity res_vasel is
    generic (
        G_CYCLE_DELAY     : unsigned(3 downto 0) := "0010"; -- clock cycles to wait for toggle VA
                                                            -- 3 clock cycles = 30ns, need at least
                                                            -- 25ns before change
        G_TRIGG_DELAY     : unsigned(3 downto 0) := "1101"  -- clock cycles to wait for trigger
                                                            -- 13 clock cycles = 130ns, need at least
                                                            -- 125ns before sampling
    );
    port (
        -- main inputs
        p_vasel_rst_n_in  : in  std_logic;                  -- main reset line
        p_vasel_clk_in    : in  std_logic;                  -- main system clock (100 MHz)
        p_vasel_inhb_in   : in  std_logic;                  -- INHB clock (5 MHz)
        --outputs
        p_vasel_VA_out    : out std_logic;                  -- output of VA value (0 Angle 1 Velocity)
        p_vasel_VA0_out   : out std_logic;                  -- output of VA0 pin
        p_vasel_VA1_out   : out std_logic;                  -- output of VA1 pin
        p_vasel_trigg_out : out std_logic                   -- output 125ns after inhb to trigger a sample
    );
end res_vasel;

architecture behavioral of res_vasel is

    signal s_delayCount       : unsigned(3 downto 0);
    signal s_triggCount       : unsigned(3 downto 0);
    signal s_inhbReg          : std_logic_vector (1 downto 0);
    signal s_enableDelayCount : std_logic;
    signal s_enableTriggCount : std_logic;
    signal s_Va0Val           : std_logic;
    signal s_Va1Val           : std_logic;
    signal s_trigg            : std_logic;

begin

    process (p_vasel_clk_in, p_vasel_rst_n_in)
    begin
        -- reset
        if (p_vasel_rst_n_in = '0') then

            s_delayCount       <= (others => '0');
            s_triggCount       <= (others => '0');
            s_inhbReg          <= (others => '0');

            s_enableDelayCount <= '0';
            s_enableTriggCount <= '0';

            s_Va0Val           <= '0';
            s_Va1Val           <= '1';

            s_trigg            <= '0';

        -- delay and output
        elsif (rising_edge (p_vasel_clk_in)) then
            s_inhbReg <= (s_inhbReg(0)& p_vasel_inhb_in);

            if (s_inhbReg = "10") then                 -- inhb has fallen, delay count can start
                s_enableDelayCount <= '1';
                s_enableTriggCount <= '1';
            end if;

            if ((s_enableDelayCount = '1') or (s_enableTriggCount = '1')) then

                if (s_enableDelayCount = '1') then
                    s_delayCount <= s_delayCount + 1;
                end if;

                if (s_enableTriggCount = '1') then
                    s_triggCount <= s_triggCount + 1;
                end if;

                if (s_delayCount = G_CYCLE_DELAY) then
                    s_enableDelayCount <= '0';        -- stop counting on delay completion
                    s_delayCount       <= (others => '0'); -- reset counter
                    s_Va0Val           <= not s_Va0Val; -- toggle VA0
                    s_Va1Val           <= not s_Va1Val; -- toggle VA1
                end if;

                if (s_triggCount = G_TRIGG_DELAY) then
                    s_trigg <= '1';
                end if;

                if (s_triggCount = G_TRIGG_DELAY + G_CYCLE_DELAY) then
                    s_enableTriggCount <= '0'; -- stop counting on delay completion
                    s_trigg            <= '0';
                end if;

            end if;

        end if;

    end process;

    p_vasel_VA0_out   <= s_Va0Val; -- maintain VA0 on every clk cycle
    p_vasel_VA1_out   <= s_Va1Val; -- maintain VA1 on every clk cycle
    p_vasel_VA_out    <= s_Va0Val; -- output 0 for Angle, 1 for Velocity
    p_vasel_trigg_out <= s_trigg;

end behavioral;
