----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             resolver.vhd
-- module name      Resolver - rtl
-- author           Pablo Velasco (pablo.velasco@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100T-1CSG324Q
-- brief            Resolver interface block
-- note             Based on section 7.3.10.1 (p.38) of the PGA411-Q1 Resolver Sensor
--                  this block receives the INHB clock (5 MHz) and the VA value
--                  and will update the outputs every falling edge of sample clock
----------------------------------------------------------------------------------------------
-- create date      30.08.2019
-- Revision:
--      Revision 0.01 by pablo.velasco
--                    - File Created
--
--      Revision 0.02 by arturo.montufar
--                    - Added synchronous edge detectors for INHB and SampleClk signals
--                    - Updated naming and removed tabs
--
--      Revision 0.03 by dario.drvenkar
--                    - Added input for MCU request puls
--                    - Update of output data is done on MCU request pulse
--
----------------------------------------------------------------------------------------------
-- TODO: - check register size for ORD
--       - test in C-Sample hardware
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity res_input is
    port (
        -- main inputs
        p_resolver_rst_n_in      :  in std_logic;                     -- main reset line
        p_resolver_clk_in        :  in std_logic;                     -- main system clock
        p_resolver_sample_clk_in :  in std_logic;                     -- sample clock (1MHz)
        p_resolver_req_in        :  in std_logic;                     -- MCU request (start of PWM cycle)
        -- resolver inputs
        p_resolver_ord_in        :  in std_logic_vector(12 downto 0); -- 12-bit parallel data in
        p_resolver_ord_clk_in    :  in std_logic;                     -- sample ready clock in
        p_resolver_inhb_in       :  in std_logic;                     -- sample clock/hold clock (5MHz)
        p_resolver_va_in         :  in std_logic;                     -- VA selector (0 Angle 1 Velocity)
        p_resolver_trigg_in      :  in std_logic;                     -- Sample trigger
        -- outputs
        p_resolver_angle_out     : out std_logic_vector(11 downto 0); -- 12-bit angle value
        p_resolver_vel_out       : out std_logic_vector(11 downto 0)  -- 12-bit velocity value
    );
end res_input;

architecture rtl of res_input is

    -- components
    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    -- internal signals
    signal s_angle             : std_logic_vector(11 downto 0);
    signal s_vel               : std_logic_vector(11 downto 0);
    signal s_trigPrevState     : std_logic;
    signal s_trigCurrState     : std_logic;
    signal s_resolver_req_sync : std_logic; -- used to synchronize angle update with MCU request

begin

        pedgeflt : edge_detector_p
        generic map (
            G_REG            => 1
        )
        port map(
            p_edp_clk_in     => p_resolver_clk_in,
            p_edp_rst_n_in   => p_resolver_rst_n_in,
            p_edp_sig_in     => p_resolver_req_in,
            p_edp_sig_re_out => s_resolver_req_sync,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
        );

    ord_sampling_process : process (p_resolver_rst_n_in, p_resolver_clk_in)
    begin
        -- reset
        if (p_resolver_rst_n_in = '0') then
            s_angle         <= (others => '0');
            s_vel           <= (others => '0');
            s_trigPrevState <= '0';
            s_trigCurrState <= '0';

        elsif(rising_edge(p_resolver_clk_in)) then

            -- store trigger values to detect the rising edge
            s_trigPrevState <= s_trigCurrState;
            s_trigCurrState <= p_resolver_trigg_in;

            -- get values on rising edges of inhb
            if(s_trigPrevState = '0') then
                if(s_trigCurrState = '1') then
                    if (p_resolver_va_in = '0') then
                        s_angle <= p_resolver_ord_in(11 downto 0); -- get angle sample
                    else
                        s_vel <= p_resolver_ord_in(11 downto 0); -- get velocity sample
                    end if;
                end if; -- s_trigCurrState = '1'
            end if; -- s_trigPrevState = '0'

        end if;
    end process; -- ord_sampling_process

    output_update_process : process (p_resolver_rst_n_in, p_resolver_clk_in)
    begin
        -- reset
        if(p_resolver_rst_n_in = '0') then
            p_resolver_angle_out <= (others => '0');
            p_resolver_vel_out   <= (others => '0');
        elsif(rising_edge(p_resolver_clk_in)) then
            if(s_resolver_req_sync='1') then
                p_resolver_angle_out <= s_angle;
                p_resolver_vel_out   <= s_vel;
            end if;
        end if;
    end process;

end rtl;
