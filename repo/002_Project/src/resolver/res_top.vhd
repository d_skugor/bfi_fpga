----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             res_top.vhd
-- module name      Resolver top level - Behavioral
-- author           Pablo Velasco (pablo.velasco@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100T-1CSG324Q
-- brief            Resolver interface block - top level
-- note             Based on section 7.3.10.1 (p.38) of the PGA411-Q1 Resolver Sensor
----------------------------------------------------------------------------------------------
-- create date      30.08.2019
-- Revision:
--      Revision 0.01 by pablo.velasco
--                    - File Created
--
--      Revision 0.02 by arturo.montufar
--                    - Updated naming and removed tabs
--
--      Revision 0.03 by dario.drvenkar
--                    - Added input fpr MCU request
--
--      Revision 0.04 by dario.drvenkar
--                    - Major redesign
--                    - Macro for dciclk sinchronyzation
--                    - New Resolver Driver
--
----------------------------------------------------------------------------------------------
-- TODO: - use grey counters to avoid SEU corruption
--       - test in C-Sample hardware
--
----------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

entity res_top is
    port (
        -- main inputs
        p_resTop_rst_n_in        :  in std_logic;                     -- main reset line
        p_resTop_clk_in          :  in std_logic;                     -- main system clock
        p_resTop_sampleClk_in    :  in std_logic;                     -- sample clock in (1MHz)
        p_resTop_req_in          :  in std_logic;                     -- MCU request (start of PWM cycle)
        -- resolver inputs
        p_resTop_ord_in	         :  in std_logic_vector(12 downto 0); -- 12-bit parallel data in
        p_resTop_ordclk_in       :  in std_logic;                     -- sample ready clock in (not used)
        -- outputs
        p_resTop_inhb_out        : out std_logic;                     -- sample clock/hold clock (5MHz)
        p_resTop_va0_out         : out std_logic;                     -- output of VA0 pin
        p_resTop_va1_out         : out std_logic;                     -- output of VA1 pin
        p_resTop_angle_val_p_out : out std_logic;
        p_resTop_angle_out       : out std_logic_vector(11 downto 0); -- 12-bit angle value
        p_resTop_vel_out         : out std_logic_vector(11 downto 0)  -- 12-bit velocity value
    );
end res_top;

architecture structural of res_top is

    -- components
    component res_driver is
    generic (
        G_RES_DRIVER_CLK             : integer;                           -- input clock in MHz
        G_RES_DRIVER_RES_UPDATE_RATE : integer                            -- Resolver update rate in kHz
    );
    port (
        -- main inputs
        p_res_driver_rst_n_in        :  in std_logic;                     -- main reset line
        p_res_driver_clk_in          :  in std_logic;                     -- main system clock
        -- resolver inputs
        p_res_driver_ord_in          :  in std_logic_vector(11 downto 0); -- 12-bit parallel data in
        p_res_driver_ordclk_in       :  in std_logic;                     -- sample ready clock in
        -- outputs
        p_res_driver_va0_out         : out std_logic;
        p_res_driver_va1_out         : out std_logic;
        p_res_driver_inhb_out        : out std_logic;
        p_res_driver_data_val_p_out  : out std_logic;
        p_res_driver_angle_out       : out std_logic_vector(11 downto 0); -- 12-bit angle value
        p_res_driver_velocity_out    : out std_logic_vector(11 downto 0)  -- 12-bit angle value
    );
    end component;

-- signals
    signal s_rst_n        : std_logic;
    signal s_clk          : std_logic;

    signal s_ord_async    : std_logic_vector(11 downto 0); -- MSB is discarded for angle
    signal s_ordclk_async : std_logic;
    signal s_ordclk_sync  : std_logic;


    signal s_inhb         : std_logic;
    signal s_va           : std_logic;
    signal s_vaselTrigg   : std_logic;
    signal s_va0          : std_logic;
    signal s_va1          : std_logic;

    signal s_angle_val_p  : std_logic;
    signal s_angle        : std_logic_vector(11 downto 0);

begin

            -- input signals assignments
    s_rst_n        <= p_resTop_rst_n_in;
    s_clk          <= p_resTop_clk_in;
    s_ord_async    <= p_resTop_ord_in(11 downto 0);
    s_ordclk_async <= p_resTop_ordclk_in;
            -- output signals assignments
    p_resTop_va0_out         <= s_va0;
    p_resTop_va1_out         <= s_va1;
    p_resTop_inhb_out        <= s_inhb;
    p_resTop_angle_val_p_out <= s_angle_val_p;
    p_resTop_angle_out       <= s_angle;
    p_resTop_vel_out         <= (others => '0');

    mdl_res_driver: res_driver
        generic map (
            G_RES_DRIVER_CLK             => 100,
            G_RES_DRIVER_RES_UPDATE_RATE => 80
        )
        port map (
            -- main inputs
            p_res_driver_rst_n_in        => s_rst_n,
            p_res_driver_clk_in          => s_clk,
            -- resolver inputs
            p_res_driver_ord_in          => s_ord_async,
            p_res_driver_ordclk_in       => s_ordclk_sync,
            -- outputs
            p_res_driver_va0_out         => s_va0,
            p_res_driver_va1_out         => s_va1,
            p_res_driver_inhb_out        => s_inhb,
            p_res_driver_data_val_p_out  => s_angle_val_p,
            p_res_driver_angle_out       => s_angle,
            p_res_driver_velocity_out    => open
        );

   xpm_cdc_single_inst : xpm_cdc_single
   generic map (
      DEST_SYNC_FF   => 2,                 -- DECIMAL; range: 2-10
      INIT_SYNC_FF   => 1,                 -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
      SIM_ASSERT_CHK => 1,                 -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      SRC_INPUT_REG  => 0                  -- DECIMAL; 0=do not register input, 1=register input
   )
   port map (
      dest_out       => s_ordclk_sync,     -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
      dest_clk       => s_clk,             -- 1-bit input: Clock signal for the destination clock domain.
      src_clk        => '0',               -- 1-bit input: optional; required when SRC_INPUT_REG = 1
      src_in         => p_resTop_ordclk_in -- 1-bit input: Input signal to be synchronized to dest_clk domain.
   );

end structural;
