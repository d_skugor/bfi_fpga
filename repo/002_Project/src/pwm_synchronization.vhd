----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pwm_synchronization.vhd
-- module name      PWM synchronization module
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Module estimates PWM switching frequency and outputs pulses with same frequency
--                  which are used in moving average filters; implementation details can be found
--                  at https://bit.ly/3fILIiF
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pwm_synchronization is
    port (
        p_pwmSync_clk_in   : in  std_logic;
        p_pwmSync_rst_n_in : in  std_logic;
        p_pwmSync_mcu_req  : in  std_logic;
        p_pwmSync_pwm_sync : out std_logic;
        p_pwmSync_pwm_freq : out std_logic_vector(2 downto 0)
    );
end entity;

architecture rtl of pwm_synchronization is

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_FREQ_8_PERIOD  : unsigned(13 downto 0) := to_unsigned(12500, 14); -- 125 us
    constant C_FREQ_10_PERIOD : unsigned(13 downto 0) := to_unsigned(10000, 14); -- 100 us
    constant C_FREQ_12_PERIOD : unsigned(13 downto 0) := to_unsigned( 8333, 14); -- 83.33 us
    constant C_FREQ_16_PERIOD : unsigned(13 downto 0) := to_unsigned( 6250, 14); -- 62.5 us
    constant C_FREQ_20_PERIOD : unsigned(13 downto 0) := to_unsigned( 5000, 14); -- 50 us
    constant C_OFFSET         : unsigned(13 downto 0) := to_unsigned(   50, 14); -- 0.5 us

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    -- MCU request counter FSM
    type t_state_mcu is (
        t_state_mcu_idle,
        t_state_mcu_count,
        t_state_mcu_error
    );

    -- PWM synchronization FSM signals
    type t_state_sync is (
        t_state_sync_idle,
        t_state_sync_init,
        t_state_sync_delay,
        t_state_sync_8_kHz,
        t_state_sync_10_kHz,
        t_state_sync_12_kHz,
        t_state_sync_16_kHz,
        t_state_sync_20_kHz,
        t_state_sync_error
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk          : std_logic;
    signal s_rst_n        : std_logic;
    signal s_mcu_req_meta : std_logic;
    signal s_mcu_req      : std_logic;
    signal s_mcu_req_old  : std_logic;
    signal s_mcu_req_re   : std_logic;
    signal s_pwm_sync     : std_logic;
    signal s_pwm_freq     : std_logic_vector(2 downto 0);
    signal s_state_sync   : t_state_sync;
    signal s_cnt          : unsigned(13 downto 0);

begin

    -- input signals assignments
    s_clk   <= p_pwmSync_clk_in;
    s_rst_n <= p_pwmSync_rst_n_in;

    -- output signal assignments
    p_pwmSync_pwm_sync <= s_pwm_sync;
    p_pwmSync_pwm_freq <= s_pwm_freq;

    -- 2 flip-flop synchronizer for MCU request signal
    process (s_clk, s_rst_n) is
    begin
        if rising_edge(s_clk) then
            if (s_rst_n = '0') then
                s_mcu_req_meta <= '0';
                s_mcu_req      <= '0';
            else
                s_mcu_req_meta <= p_pwmSync_mcu_req;
                s_mcu_req      <= s_mcu_req_meta;
            end if;
        end if;
    end process;

    -- rising edge detector on s_mcu_req signal
    process (s_clk) is
    begin
        if rising_edge(s_clk) then
            if (s_rst_n = '0') then
                s_mcu_req_old <= '0';
                s_mcu_req_re  <= '0';
            else
                s_mcu_req_old <= s_mcu_req;
                if (s_mcu_req_old = '0' and s_mcu_req = '1') then
                    s_mcu_req_re <= '1';
                else
                    s_mcu_req_re <= '0';
                end if;
            end if;
        end if;
    end process;

    -- MCU request counter FSM
    -- FSM checks if MCU request period counter is inside valid interval for switching frequencies
    -- of 8, 10, 12, 16 and 20 kHz and increments MCU request counter if MCU request is asserted
    -- while inside valid interval
    process (s_clk) is
    begin
        if rising_edge(s_clk) then
            if (s_rst_n = '0') then
                s_state_mcu     <= t_state_mcu_idle;
                s_mcu_req_cnt   <= (others => '0');
                s_mcu_req_p_cnt <= (others => '0');
            else
                case (s_state_mcu) is
                    -----------------------------------------------
                    when t_state_mcu_idle =>
                        if (s_mcu_req_re = '1') then
                            s_state_mcu <= t_state_mcu_count;
                        end if;
                    -----------------------------------------------
                    when t_state_mcu_count =>
                        -- MCU request period counter
                        if (s_mcu_req_re = '1') then
                            s_mcu_req_p_cnt <= (others => '0');
                        else
                            s_mcu_req_p_cnt <= s_mcu_req_p_cnt + 1;
                        end if;

                        -- check if MCU request period is inside valid intervals
                        if (s_pwm_sync = '1') then
                            -- reset MCU request counter if s_pwm_sync signal is asserted
                            s_mcu_req_cnt <= (others => '0');
                        elsif (s_mcu_req_p_cnt >= C_FREQ_8_PERIOD - C_OFFSET - 1 and s_mcu_req_p_cnt <= C_FREQ_8_PERIOD + C_OFFSET - 1) then
                            -- 8 kHz
                            if (s_mcu_req_re = '1') then
                                s_mcu_req_cnt <= s_mcu_req_cnt + 1;
                            end if;
                        elsif (s_mcu_req_p_cnt >= C_FREQ_10_PERIOD - C_OFFSET - 1 and s_mcu_req_p_cnt <= C_FREQ_10_PERIOD + C_OFFSET - 1) then
                            -- 10 kHz
                            if (s_mcu_req_re = '1') then
                                s_mcu_req_cnt <= s_mcu_req_cnt + 1;
                            end if;
                        elsif (s_mcu_req_p_cnt >= C_FREQ_12_PERIOD - C_OFFSET - 1 and s_mcu_req_p_cnt <= C_FREQ_12_PERIOD + C_OFFSET - 1) then
                            -- 12 kHz
                            if (s_mcu_req_re = '1') then
                                s_mcu_req_cnt <= s_mcu_req_cnt + 1;
                            end if;
                        elsif (s_mcu_req_p_cnt >= C_FREQ_16_PERIOD - C_OFFSET - 1 and s_mcu_req_p_cnt <= C_FREQ_16_PERIOD + C_OFFSET - 1) then
                            -- 16 kHz
                            if (s_mcu_req_re = '1') then
                                s_mcu_req_cnt <= s_mcu_req_cnt + 1;
                            end if;
                        elsif (s_mcu_req_p_cnt >= C_FREQ_20_PERIOD - C_OFFSET - 1 and s_mcu_req_p_cnt <= C_FREQ_20_PERIOD + C_OFFSET - 1) then
                            -- 20 kHz
                            if (s_mcu_req_re = '1') then
                                s_mcu_req_cnt <= s_mcu_req_cnt + 1;
                            end if;
                        elsif (s_mcu_req_p_cnt >= C_FREQ_8_PERIOD + C_OFFSET) then
                            -- error
                            s_state_mcu <= t_state_mcu_error;
                        end if;
                    -----------------------------------------------
                    when others =>
                        -- t_state_mcu_error state
                        if (s_mcu_req_re = '1') then
                            s_mcu_req_cnt   <= (others => '0');
                            s_mcu_req_p_cnt <= (others => '0');
                            s_state_mcu     <= t_state_mcu_count;
                        end if;
                    -----------------------------------------------
                end case;
            end if;
        end if;
    end process;

    -- PWM synchronization FSM
    -- valid sequence of changing frequencies is 10 - 16 - 20
    process (s_clk) is
    begin
        if rising_edge(s_clk) then
            if (s_rst_n = '0') then
                s_cnt      <= (others => '0');
                s_pwm_sync <= '0';
                s_pwm_freq <= (others => '1');
                s_state_sync <= t_state_sync_idle;
            else
                -- default values
                s_pwm_sync <= '0';
                s_pwm_freq <= (others => '1');
                case (s_state_sync) is
                    -----------------------------------------------
                    when t_state_sync_idle =>
                        if (s_mcu_req_re = '1') then
                            s_state_sync <= t_state_sync_delay;
                        end if;
                    -----------------------------------------------
                    when t_state_sync_delay =>
                        if (s_cnt = C_OFFSET) then
                            s_cnt   <= (others => '0');
                            s_state_sync <= t_state_sync_20_kHz;
                        else
                            s_cnt <= s_cnt + 1;
                        end if;
                    -----------------------------------------------
                    when t_state_sync_10_kHz =>
                        s_pwm_freq <= "001";
                        if (s_cnt = 0) then
                            s_pwm_sync <= '1';
                            s_cnt      <= s_cnt + 1;
                        elsif (s_cnt = C_FREQ_16_PERIOD - 1) then
                            -- check if new switching frequency is 16 kHz
                            if (s_mcu_req_cnt = 1) then
                                s_cnt   <= (others => '0');
                                s_state_sync <= t_state_sync_16_kHz;
                            elsif (s_mcu_req_cnt >= 2) then
                                -- error because too much MCU requests are asserted
                                s_state_sync <= t_state_sync_error;
                            else
                                s_cnt <= s_cnt + 1;
                            end if;
                        elsif (s_cnt = C_FREQ_10_PERIOD - 1) then
                            -- check if switching frequency stays 10 kHz
                            if (s_mcu_req_cnt = 1) then
                                s_cnt   <= (others => '0');
                                s_state_sync <= t_state_sync_10_kHz;
                            else
                                s_state_sync <= t_state_sync_error;
                            end if;
                        else
                            s_cnt <= s_cnt + 1;
                        end if;
                    -----------------------------------------------
                    when t_state_sync_16_kHz =>
                        s_pwm_freq <= "011";
                        if (s_cnt = 0) then
                            s_pwm_sync <= '1';
                            s_cnt      <= s_cnt + 1;
                        elsif (s_cnt = C_FREQ_20_PERIOD - 1) then
                            -- check if new switching frequency is 20 kHz
                            if (s_mcu_req_cnt = 1) then
                                s_cnt   <= (others => '0');
                                s_state_sync <= t_state_sync_20_kHz;
                            elsif (s_mcu_req_cnt >= 2) then
                                -- error because too much MCU requests are asserted
                                s_state_sync <= t_state_sync_error;
                            else
                                s_cnt <= s_cnt + 1;
                            end if;
                        elsif (s_cnt = C_FREQ_16_PERIOD - 1) then
                            -- check if switching frequency stays 16 kHz
                            if (s_mcu_req_cnt = 1) then
                                s_cnt   <= (others => '0');
                                s_state_sync <= t_state_sync_16_kHz;
                            elsif (s_mcu_req_cnt >= 2) then
                                s_state_sync <= t_state_sync_error;
                            else
                                s_cnt <= s_cnt + 1;
                            end if;
                        elsif (s_cnt = C_FREQ_10_PERIOD - 1) then
                            -- check if new switching frequency is 10 kHz
                            if (s_mcu_req_cnt = 1) then
                                s_cnt   <= (others => '0');
                                s_state_sync <= t_state_sync_10_kHz;
                            else
                                s_state_sync <= t_state_sync_error;
                            end if;
                        else
                            s_cnt <= s_cnt + 1;
                        end if;
                    -----------------------------------------------
                    when t_state_sync_20_kHz =>
                        s_pwm_freq <= "100";
                        if (s_cnt = 0) then
                            s_pwm_sync <= '1';
                            s_cnt      <= s_cnt + 1;
                        elsif (s_cnt = C_FREQ_20_PERIOD - 1) then
                            -- check if switching frequency stays 20 kHz
                            if (s_mcu_req_cnt = 1) then
                                s_cnt        <= (others => '0');
                                s_state_sync <= t_state_sync_20_kHz;
                            elsif (s_mcu_req_cnt >= 2) then
                                s_state_sync <= t_state_sync_error;
                            else
                                s_cnt <= s_cnt + 1;
                            end if;
                        elsif (s_cnt = C_FREQ_16_PERIOD - 1) then
                            -- check if new switching frequency is 16 kHz
                            if (s_mcu_req_cnt = 1) then
                                s_cnt   <= (others => '0');
                                s_state_sync <= t_state_sync_16_kHz;
                            else
                                s_state_sync <= t_state_sync_error;
                            end if;
                        else
                            s_cnt <= s_cnt + 1;
                        end if;
                    -----------------------------------------------
                    when others =>
                        -- t_state_sync_error state
                        if (s_mcu_req_re = '1') then
                            s_cnt        <= (others => '0');
                            s_state_sync <= t_state_sync_delay;
                        end if;
                    -----------------------------------------------
                end case;
            end if;
        end if;
    end process;

end architecture rtl;