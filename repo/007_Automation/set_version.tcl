# Set design version, tag and hash into generic

#get git commit hash id  - 28 bits long
if { [catch {exec git rev-parse --short=7 HEAD}] } {
    puts "No git version control in the directory"
    set git_hash 0000000
} else {
    set git_hash [exec git rev-parse --short=7 HEAD]
}

#get last git tag  on all the brances
set allTags [exec git rev-list --tags --max-count=1]
set versionfull [exec git describe --tags $allTags]
set versionLen [string length  $versionfull]

#set bitstream tag 
#Count the number of the commits from the last tag
set tag [exec git rev-list $versionfull..HEAD --count]

# if the tag is release candidate than it should be long 13 characters (s3-v4.0.0-rc2)
# if it's not release candidate than it should be long 9 characters
if {$versionLen <= 9 } {
	lassign [split $versionfull v ] step result
	set rc 0
} elseif {$versionLen == 13} {
	lassign [split $versionfull - ] step version_tmp release_cand
	lassign [split $version_tmp v ] v result 
	lassign [split $release_cand rc] r c rc
} else {
	puts " The branch is not tagged correctly - wrong tag naming. It should be : sX-vA.B.C-rcY or sX-vA.B.C" 
	exit 1
	}

#remove "."
lassign [split $result . ] high middle low
set version $high$middle$low$rc

#open_proj ./BFI/BFI.xpr

# Add version, tag and hash to design generics
set_property generic "G_BITSTREAM_VERSION=16'h$high$middle$low$rc G_BITSTREAM_TAG=8'h$tag G_DESIGN_HASH=28'h$git_hash" [current_fileset]
