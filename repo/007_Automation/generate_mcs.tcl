# Generate Memory Configuration File
# Return exit code 1 on error, else 0

open_proj ./BFI/BFI.xpr

set run_name impl_1

write_cfgmem  -format mcs -size 16 -interface SPIx4 -loadbit {up 0x00000000 "./BFI/BFI.runs/impl_1/bfi_top.bit" } -checksum -force -disablebitswap -file "./BFI/BFI.runs/impl_1/bfi_top.mcs"

#set status [get_property STATUS [get_runs $run_name]]
#puts $status
#if {$status != "write_cfgmem completed successfully"} {
#  exit 1
#}
#exit 0
