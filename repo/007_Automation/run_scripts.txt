C:\Xilinx\Vivado\2018.3\bin\vivado.bat -mode batch -source BFI.tcl 
echo $?
C:\Xilinx\Vivado\2018.3\bin\vivado.bat -mode batch -source repo/007_Automation/check_syntax.tcl
echo $?
C:\Xilinx\Vivado\2018.3\bin\vivado.bat -mode batch -source repo/007_Automation/set_version.tcl
echo $?
C:\Xilinx\Vivado\2018.3\bin\vivado.bat -mode batch -source repo/007_Automation/run_synthesis.tcl
echo $?
C:\Xilinx\Vivado\2018.3\bin\vivado.bat -mode batch -source repo/007_Automation/run_implementation.tcl
echo $?
C:\Xilinx\Vivado\2018.3\bin\vivado.bat -mode batch -source repo/007_Automation/generate_bitstream.tcl
echo $?
C:\Xilinx\Vivado\2018.3\bin\vivado.bat -mode batch -source repo/007_Automation/generate_mcs.tcl
echo $?