----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             spi_slave.vhd
-- module name      spi_slave - Behavioral
-- author           Marko Gulin (marko.gulin@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            SPI slave (CPOL = 0, CPHA = 0)
----------------------------------------------------------------------------------------------
-- create date      25.10.2019
-- Revision:
--      Revision 0.01 by marko.gulin
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Refactored version of SPI slave module
--                    - Implemented FSM for handling SPI transactions
--
--      Revision 0.03 by david.pavlovic
--                    - Added XPM macro for SPI signals synchronization
--                    - Changed MISO pin to inout
--
--      Revision 0.04 by david.pavlovic
--                    - Code cleanup
----------------------------------------------------------------------------------------------
-- TODO: -
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real."ceil";
use ieee.math_real."log2";

library xpm;
use xpm.vcomponents.all;

entity spi_slave is
    generic (
        G_DATA_W              : natural := 8
    );
    port (
        -- Main signals
        p_spi_clk_in          : in    std_logic;
        p_spi_rst_n_in        : in    std_logic;
        -- SPI signals
        p_spi_sck_in          : in    std_logic;
        p_spi_ss_n_in         : in    std_logic;
        p_spi_mosi_in         : in    std_logic;
        p_spi_miso_out        : inout std_logic;
        -- TX and RX data
        p_spi_data_tx_in      : in    std_logic_vector(G_DATA_W-1 downto 0);
        p_spi_data_tx_out     : out   std_logic_vector(G_DATA_W-1 downto 0);
        p_spi_data_rx_out     : out   std_logic_vector(G_DATA_W-1 downto 0);
        -- Status signals
        p_spi_busy_out        : out   std_logic;
        p_spi_data_rx_vld_out : out   std_logic;
        p_spi_trans_flt_n_out : out   std_logic
    );
end entity spi_slave;

architecture rtl of spi_slave is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component edge_detector_p is
        generic (
            G_REG            : natural := 1
        );
        port (
            -- Main signals
            p_edp_clk_in     : in  std_logic;
            p_edp_rst_n_in   : in  std_logic;
            -- Input signal
            p_edp_sig_in     : in  std_logic;
            -- Output signals
            p_edp_sig_re_out : out std_logic;
            p_edp_sig_fe_out : out std_logic;
            p_edp_sig_rf_out : out std_logic
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_CNT_W : integer := integer(ceil(log2(real(G_DATA_W))));

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    type t_state_spi is (
        t_state_spi_idle,
        t_state_spi_load_tx,
        t_state_spi_ss_active,
        t_state_spi_wait_sck_le, -- leading edge
        t_state_spi_wait_sck_te, -- trailing edge
        t_state_spi_trans_done,
        t_state_spi_error
    );

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk         : std_logic;
    signal s_rst_n       : std_logic;
    signal s_spi_vec_in  : std_logic_vector(2 downto 0);
    signal s_spi_vec_out : std_logic_vector(2 downto 0);
    signal s_spi_sck     : std_logic;
    signal s_spi_sck_re  : std_logic;
    signal s_spi_sck_fe  : std_logic;
    signal s_spi_ss_n    : std_logic;
    signal s_spi_mosi    : std_logic;
    signal s_spi_miso    : std_logic;
    signal s_state       : t_state_spi;
    signal s_busy        : std_logic;
    signal s_load_en     : std_logic;
    signal s_shift_en    : std_logic;
    signal s_sample_en   : std_logic;
    signal s_miso_en     : std_logic;
    signal s_data_vld    : std_logic;
    signal s_done        : std_logic;
    signal s_flt_n       : std_logic;  -- active low
    signal s_cnt_bit     : unsigned(C_CNT_W downto 0);
    signal s_data_tx     : std_logic_vector(G_DATA_W-1 downto 0);
    signal s_data_rx     : std_logic_vector(G_DATA_W-1 downto 0);

begin

    -- input signal assignments
    s_clk   <= p_spi_clk_in;
    s_rst_n <= p_spi_rst_n_in;

    -- tri-state buffer
    s_spi_miso     <= s_data_tx(G_DATA_W-1);
    p_spi_miso_out <= s_spi_miso when s_miso_en = '1' else 'Z';

    -- output signal assignment
    p_spi_busy_out        <= s_busy;
    p_spi_trans_flt_n_out <= s_flt_n;

    -- SPI signal vectors
    s_spi_vec_in <= p_spi_sck_in & p_spi_ss_n_in & p_spi_mosi_in;
    s_spi_sck    <= s_spi_vec_out(2);
    s_spi_ss_n   <= s_spi_vec_out(1);
    s_spi_mosi   <= s_spi_vec_out(0);

    -- xpm_cdc_array_single: Single-bit Array Synchronizer
    -- Xilinx Parameterized Macro, version 2020.1
    xpm_cdc_array_single_inst : xpm_cdc_array_single
    generic map (
       DEST_SYNC_FF   => 2,             -- DECIMAL; range: 2-10
       INIT_SYNC_FF   => 0,             -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
       SIM_ASSERT_CHK => 1,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
       SRC_INPUT_REG  => 0,             -- DECIMAL; 0=do not register input, 1=register input
       WIDTH          => 3              -- DECIMAL; range: 1-1024
    )
    port map (
       dest_out       => s_spi_vec_out, -- WIDTH-bit output: src_in synchronized to the destination clock domain. This output is registered.
       dest_clk       => s_clk,         -- 1-bit input: Clock signal for the destination clock domain.
       src_clk        => '0',           -- 1-bit input: optional; required when SRC_INPUT_REG = 1
       src_in         => s_spi_vec_in   -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock domain.
    );

    -- rising edge detector on s_spi_sck signal
    spi_sck_ed: edge_detector_p
        generic map (
            G_REG            => 0
        )
        port map (
            -- Main signals
            p_edp_clk_in     => s_clk,
            p_edp_rst_n_in   => s_rst_n,
            -- Input signal
            p_edp_sig_in     => s_spi_sck,
            -- Output signals
            p_edp_sig_re_out => s_spi_sck_re,
            p_edp_sig_fe_out => s_spi_sck_fe,
            p_edp_sig_rf_out => open
        );

    -- SPI FSM
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_state     <= t_state_spi_idle;
            s_busy      <= '0';
            s_load_en   <= '0';
            s_shift_en  <= '0';
            s_sample_en <= '0';
            s_miso_en   <= '0';
            s_data_vld  <= '0';
            s_done      <= '0';
            s_flt_n     <= '1';
            s_cnt_bit   <= (others => '0');
        elsif rising_edge(s_clk) then
            -- default values
            s_busy      <= '1';
            s_load_en   <= '0';
            s_shift_en  <= '0';
            s_sample_en <= '0';
            s_miso_en   <= '1';
            s_done      <= '0';
            s_flt_n     <= '1';
            case (s_state) is
                ---------------------------------------------
                when t_state_spi_idle =>
                    s_busy    <= '0';
                    s_miso_en <= '0';
                    if (s_spi_ss_n = '0') then
                        s_state    <= t_state_spi_load_tx;
                        s_load_en  <= '1';
                        s_data_vld <= '0';
                    end if;
                ---------------------------------------------
                when t_state_spi_load_tx =>
                    s_busy    <= '0';
                    s_miso_en <= '0';
                    if (s_spi_ss_n = '0') then
                        s_state   <= t_state_spi_ss_active;
                        s_busy    <= '1';
                        s_miso_en <= '1';
                    else
                        s_state   <= t_state_spi_idle;
                        s_busy    <= '0';
                        s_miso_en <= '0';
                    end if;
                ---------------------------------------------
                when t_state_spi_ss_active =>
                    if (s_spi_sck_re = '1') then
                        s_state     <= t_state_spi_wait_sck_te;
                        s_sample_en <= '1';
                        s_cnt_bit   <= s_cnt_bit + 1;
                    elsif (s_spi_ss_n = '1') then
                        s_state     <= t_state_spi_idle;
                        s_busy      <= '0';
                        s_miso_en   <= '0';
                    end if;
                ---------------------------------------------
                when t_state_spi_wait_sck_te =>
                    if (s_spi_ss_n = '1') then
                        s_state        <= t_state_spi_error;
                        s_flt_n        <= '0';
                    elsif (s_spi_sck_fe = '1') then
                        if (s_cnt_bit /= G_DATA_W) then
                            s_state    <= t_state_spi_wait_sck_le;
                            s_shift_en <= '1';
                        else
                            s_state    <= t_state_spi_trans_done;
                            s_data_vld <= '1';
                            s_done     <= '1';
                            s_cnt_bit  <= (others => '0');
                        end if;
                    end if;
                ---------------------------------------------
                when t_state_spi_wait_sck_le =>
                    if (s_spi_ss_n = '1') then
                        s_state     <= t_state_spi_error;
                        s_flt_n     <= '0';
                    elsif (s_spi_sck_re = '1') then
                        s_state     <= t_state_spi_wait_sck_te;
                        s_sample_en <= '1';
                        s_cnt_bit   <= s_cnt_bit + 1;
                    end if;
                ---------------------------------------------
                when t_state_spi_trans_done =>
                    if (s_spi_ss_n = '1') then
                        s_state   <= t_state_spi_idle;
                        s_busy    <= '0';
                        s_miso_en <= '0';
                    elsif (s_spi_sck_re = '1') then
                        s_state   <= t_state_spi_error;
                        s_flt_n   <= '0';
                    end if;
                ---------------------------------------------
                when others => -- t_state_spi_error state
                    s_busy     <= '0';
                    s_miso_en  <= '0';
                    s_data_vld <= '0';
                    s_flt_n    <= '0';
                    if (s_spi_ss_n = '0') then
                        s_state    <= t_state_spi_ss_active;
                        s_busy     <= '1';
                        s_miso_en  <= '1';
                        s_data_vld <= '0';
                        s_flt_n    <= '1';
                    end if;
                ---------------------------------------------
            end case;
        end if;
    end process;

    -- TX shift register
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_data_tx <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_load_en = '1') then
                s_data_tx <= p_spi_data_tx_in;
            elsif (s_busy = '1' and s_shift_en = '1') then
                s_data_tx <= s_data_tx(G_DATA_W-2 downto 0) & '0';
            end if;
        end if;
    end process;

    -- RX shift register
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_data_rx <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_sample_en = '1') then
                s_data_rx(G_DATA_W-1 downto 1) <= s_data_rx(G_DATA_W-2 downto 0);
                s_data_rx(0)                   <= s_spi_mosi;
            end if;
        end if;
    end process;

    -- output register
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            p_spi_data_tx_out     <= (others => '0');
            p_spi_data_rx_out     <= (others => '0');
            p_spi_data_rx_vld_out <= '0';
        elsif rising_edge(s_clk) then
            p_spi_data_rx_vld_out <= s_data_vld;
            if (s_load_en = '1') then
                p_spi_data_tx_out <= p_spi_data_tx_in;
            end if;
            if (s_done = '1') then
                p_spi_data_rx_out <= s_data_rx;
            end if;
        end if;
    end process;

end architecture rtl;