----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pulse_synchronizer.vhd
-- module name      Simple pulse synchronizer with positive edge detection
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            The pulse synchronizer will pass through a clock domain edge a pulse
--                  active on positive-edge, producing a single clock pulse at the output
--                  clock domain. The input clock domain pulse duration is expected to be
--                  not more than 3 clock cycles.
--                  This is a resource-consuming and slow function.
--
----------------------------------------------------------------------------------------------
-- create date      01.10.2020
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
--      Revision 0.02 by jsq
--                      Replace positive edge detector by the new generic block edge_detector_p
----------------------------------------------------------------------------------------------
-- TODO
-- Ensure that module will not generate more than one pulse in second domain independently
-- from pulse duration in the first domain
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity pulse_synchronizer is
    port (
        clk_domain1_in   : in  std_logic; -- Clock domain 1 clock
        rst_n_domain1_in : in  std_logic; -- Clock domain 1 reset
        clk_domain2_in   : in  std_logic; -- Clock domain 2 clock
        rst_n_domain2_in : in  std_logic; -- Clock domain 2 reset
        pulse_in         : in  std_logic; -- Clock domain 1 pulse input
        pulse_out        : out std_logic  -- Clock domain 2 pulse output
    );
end entity pulse_synchronizer;

architecture rtl of pulse_synchronizer is

    signal q_o_1, r_i_1 : std_logic;
    signal q_o_2, r_i_2 : std_logic;

begin

    interlock_reg : process(clk_domain1_in, rst_n_domain1_in)
    begin
        if (rst_n_domain1_in = '0') then
            q_o_1 <= '0';
        elsif Rising_edge(clk_domain1_in) then
            if (r_i_1 = '1') then
                q_o_1 <= '0';
            elsif (pulse_in = '1') then
                q_o_1 <= '1';
            end if;
        end if;
    end process;

    -- Forward synchronizer and edge detector
    fwd_synchro : entity work.cdc_simple_synchro
        port map (
            clk_i   => clk_domain2_in,
            async_i => q_o_1,
            sync_o  => q_o_2
        );

    fwd_pdet : entity work.edge_detector_p
        generic map (
            G_REG            => 1
        )
        port map (
            p_edp_clk_in     => clk_domain2_in,
            p_edp_rst_n_in   => rst_n_domain2_in,
            p_edp_sig_in     => q_o_2,
            p_edp_sig_re_out => pulse_out,
            p_edp_sig_fe_out => open,
            p_edp_sig_rf_out => open
        );

    -- Feedback synchronizer and edge detector
    fbk_syncrho : entity work.cdc_simple_synchro
        port map (
            clk_i   => clk_domain1_in,
            async_i => q_o_2,
            sync_o  => r_i_2
        );

    r_i_1 <= r_i_2;

end architecture rtl;
