----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             edge_detector_p.vhd
-- module name      edge_detector_p - rtl
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Rising and falling edge detector with option to register pulses
----------------------------------------------------------------------------------------------
-- create date      27.08.2020
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                    - Code cleanup
----------------------------------------------------------------------------------------------
-- TODO: -
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity edge_detector_p is

    generic (
        G_REG            : natural := 1
    );
    port (
        -- Main signals
        p_edp_clk_in     : in  std_logic;
        p_edp_rst_n_in   : in  std_logic;
        -- Input signal
        p_edp_sig_in     : in  std_logic;
        -- Output signals
        p_edp_sig_re_out : out std_logic;
        p_edp_sig_fe_out : out std_logic;
        p_edp_sig_rf_out : out std_logic
    );

end entity;

architecture rtl of edge_detector_p is

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk    : std_logic;
    signal s_rst_n  : std_logic;
    signal s_sig    : std_logic;
    signal s_sig_d  : std_logic;
    signal s_sig_re : std_logic;
    signal s_sig_fe : std_logic;

begin

    -- input signal assignments
    s_clk   <= p_edp_clk_in;
    s_rst_n <= p_edp_rst_n_in;
    s_sig   <= p_edp_sig_in;

    -- combinatorial logic
    s_sig_re <=  s_sig and (not s_sig_d);
    s_sig_fe <= (not s_sig) and s_sig_d;

    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_sig_d <= '0';
        elsif rising_edge(s_clk) then
            s_sig_d <= s_sig;
        end if;
    end process;

    no_output_register :
    if G_REG = 0 generate
        p_edp_sig_re_out <= s_sig_re;
        p_edp_sig_fe_out <= s_sig_fe;
        p_edp_sig_rf_out <= s_sig_re or s_sig_fe;
    end generate;

    output_register :
    if G_REG = 1 generate
        process (s_clk, s_rst_n) is
        begin
            if (s_rst_n = '0') then
                p_edp_sig_re_out <= '0';
                p_edp_sig_fe_out <= '0';
                p_edp_sig_rf_out <= '0';
            elsif rising_edge(s_clk) then
                p_edp_sig_re_out <= s_sig_re;
                p_edp_sig_fe_out <= s_sig_fe;
                p_edp_sig_rf_out <= s_sig_re or s_sig_fe;
            end if;
        end process;
    end generate;

end architecture rtl;