----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2021, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pulse_filter.vhd
-- module name      pulse_filter - rtl
-- author           David Pavlovic (david.pavlovic@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100T-1CSG324Q
-- brief            Filters out pulses shorter than generic threshold
----------------------------------------------------------------------------------------------
-- create date      06.07.2021
-- Revision:
--      Revision 0.01 by david.pavlovic
--                    - File Created
----------------------------------------------------------------------------------------------
-- TODO:
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pulse_filter is
    generic (
        G_PULSE_FILTER_LENGTH   : natural := 10
    );
    port (
        -- main signals
        p_pulse_filter_clk_in   : in  std_logic;
        p_pulse_filter_rst_n_in : in  std_logic;
        p_pulse_filter_en_in    : in  std_logic;
        -- input signal
        p_pulse_filter_sig_in   : in  std_logic;
        p_pulse_filter_sig_out  : out std_logic
    );
end entity pulse_filter;

architecture rtl of pulse_filter is

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_ZEROS : std_logic_vector(G_PULSE_FILTER_LENGTH - 1 downto 0) := (others => '0');
    constant C_ONES  : std_logic_vector(G_PULSE_FILTER_LENGTH - 1 downto 0) := (others => '1');

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk       : std_logic;
    signal s_rst_n     : std_logic;
    signal s_en        : std_logic;
    signal s_sig_in    : std_logic;
    signal s_sig_out   : std_logic;
    signal s_shift_reg : std_logic_vector(G_PULSE_FILTER_LENGTH - 1 downto 0);

begin

    -- input signal assignments
    s_clk    <= p_pulse_filter_clk_in;
    s_rst_n  <= p_pulse_filter_rst_n_in;
    s_en     <= p_pulse_filter_en_in;
    s_sig_in <= p_pulse_filter_sig_in;

    -- output signal assignments
    p_pulse_filter_sig_out <= s_sig_out;

    -- shift register
    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_shift_reg <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_en = '1') then
                s_shift_reg(0) <= s_sig_in;
                for i in 1 to G_PULSE_FILTER_LENGTH - 1 loop
                    s_shift_reg(i) <= s_shift_reg(i - 1);
                end loop;
            end if;
        end if;
    end process;

    process (s_clk, s_rst_n) is
    begin
        if (s_rst_n = '0') then
            s_sig_out <= '0';
        elsif rising_edge(s_clk) then
            if (s_shift_reg = C_ZEROS) then
                s_sig_out <= '0';
            elsif (s_shift_reg = C_ONES) then
                s_sig_out <= '1';
            end if;
        end if;
    end process;

end architecture rtl;
