----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             delay_line.vhd
-- module name      delay_line
-- author           Dario Drvenkar (dario.drvenkar@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100T-1CSG324Q
-- brief            Delay line with generic for data and length for delay
----------------------------------------------------------------------------------------------
-- create date      10.05.2021
-- Revision:
--      Revision 0.01 by dario.drvenkar
--                    - File Created
--
--      Revision 0.02 by david.pavlovic
--                   - Code cleanup
----------------------------------------------------------------------------------------------
-- TODO: - none
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity delay_line is
    generic (
        G_DELAY_LINE_WIDTH    : integer := 8;
        G_DELAY_LINE_LENGTH   : integer := 2
    );
    port (
        p_delay_line_rst_n_in :  in std_logic;
        p_delay_line_clk_in   :  in std_logic;
        p_delay_line_en_in    :  in std_logic;
        p_delay_line_data_in  :  in std_logic_vector(G_DELAY_LINE_WIDTH - 1 downto 0);
        p_delay_line_data_out : out std_logic_vector(G_DELAY_LINE_WIDTH - 1 downto 0)
    );
end entity delay_line;

architecture rtl of delay_line is

    ---------------------------------------------------------------------------------
    --                                     types
    ---------------------------------------------------------------------------------

    type delay_array is array (0 to G_DELAY_LINE_LENGTH-1) of std_logic_vector(G_DELAY_LINE_WIDTH - 1 downto 0);

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_rst_n       : std_logic;
    signal s_clk         : std_logic;
    signal s_en          : std_logic;
    signal s_input       : std_logic_vector(G_DELAY_LINE_WIDTH - 1 downto 0);
    signal s_output      : std_logic_vector(G_DELAY_LINE_WIDTH - 1 downto 0);
    signal s_delay_array : delay_array;

begin

    -- input signal assignments
    s_rst_n <= p_delay_line_rst_n_in;
    s_clk   <= p_delay_line_clk_in;
    s_en    <= p_delay_line_en_in;
    s_input <= p_delay_line_data_in;

    -- output signal assignments
    p_delay_line_data_out <= s_output;

    process (s_rst_n, s_clk)
    begin
        if (s_rst_n = '0') then
            s_delay_array <= (others => (others => '0'));
            s_output      <= (others => '0');
        elsif rising_edge(s_clk) then
            if (s_en = '1') then
                s_delay_array(0) <= s_input;
                for i in 1 to G_DELAY_LINE_LENGTH-1 loop
                    s_delay_array(i) <= s_delay_array(i-1);
                end loop;
                s_output <= s_delay_array(G_DELAY_LINE_LENGTH - 1);
            end if;
        end if;
    end process;

end architecture rtl;