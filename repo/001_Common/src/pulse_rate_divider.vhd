----------------------------------------------------------------------------------------------
--                                   ALTAM SYSTEMS SL
--
--          (c) Copyright 2018 - 2020, ALTAM SYSTEMS SL, Barcelona, Spain
--
--                                 All rights reserved
--
--
-- COPYRIGHT NOTICE:
-- All information contained herein is, and remains the property of ALTAM Systems S.L.
-- and its suppliers, if any.
-- The intellectual and technical concepts contained herein are proprietary to
-- ALTAM Systems S.L. and its suppliers and may be covered by Spanish and Foreign Patents,
-- patents in process, and are protected by trade secret or copyright law.
-- Dissemination of this information, modification or reproduction of this material
-- is strictly forbidden unless prior written permission is obtained from ALTAM Systems S.L.
--
----------------------------------------------------------------------------------------------
--! @file       pulse_rate_divider.vhd
--! @brief      A generic pulse rate divider block
--! @details    A generic pulse train rate divider, based on a Fibonacci LFSR.
--! The LFSR sequence for the x�?� + x³ + 1 polynomial initialized with all ones, is:
--! LFSR        Sequence item
--! ========================
--! 0x F --     1
--! 0x 7 --     2
--! 0x 3 --     3
--! 0x11 --     4
--! 0x18 --     5
--! 0x C --     6
--! 0x16 --     7
--! 0x1B --     8
--! 0x1D --     9
--! 0x E --    10
--! 0x17 --    11
--! 0x B --    12
--! 0x15 --    13
--! 0x A --    14
--! 0x 5 --    15
--! 0x 2 --    16
--! 0x 1 --    17
--! 0x10 --    18
--! 0x 8 --    19
--! 0x 4 --    20
--! 0x12 --    21
--! 0x 9 --    22
--! 0x14 --    23
--! 0x1A --    24
--! 0x D --    25
--! 0x 6 --    26
--! 0x13 --    27
--! 0x19 --    28
--! 0x1C --    29
--! 0x1E --    30
--! 0x1F --    31
--!
--! Update rate behavior:
--! After a reset, the divider will take the rate_i value present at the input port.
--! If a new rate is desired, rate_load_p_i has to be pulsed. The sync_o output will go to zero. At the end of the current
--! cycle, the new rate value will be applied and the output sync_o will transition back to '1'.

--! @author     Jorge Sanchez (jsanchez@altamsys.com)
--! @date       17/12/2018
----------------------------------------------------------------------------------------------
-- CHANGES:
--      17.12.2018 (JSQ) - Creation date
--      26.05.2020 (JSQ) - Licensed to RIMAC to be used within the BFI project
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PulseRateDivider is
    port (
        clk_i         : in  std_logic;                    --! Input clock
        rst_i         : in  std_logic;                    --! Reset input. Synchronous reset, atcive high!
        rate_load_p_i : in  std_logic;                    --! Load new pulse rate input
        rate_i        : in  std_logic_vector(4 downto 0); --! Pulse rate selector input
        enable_i      : in  std_logic;                    --! Enable input
        trig_i        : in  std_logic;                    --! Pulse train input
        sync_o        : out std_logic;                    --! Sychro ready output. Pulsed when the new selected rate is active.
        pulse_o       : out std_logic                     --! Pulse train output
    );
end entity PulseRateDivider;

architecture Behavioral of PulseRateDivider is

    -- constants
    constant N_SHIFTER   : natural                          := 5;
    constant LFSR_PRESET : unsigned(N_SHIFTER - 1 downto 0) := (others => '1');

    -- internal signals
    signal lfsr_reg      : unsigned(N_SHIFTER - 1 downto 0);
    signal current_rate  : natural;
    signal pulse_output  : std_logic;
    signal new_rate_flag : std_logic;
begin

    --=============================================================================
    --! @brief Main process
    --! @param  clk_i
    --=============================================================================
    lfsr_proc : process (clk_i)
        variable bits : std_logic;
    begin
        if (clk_i'event and clk_i = '1') then
            pulse_output <= '0';
            -- Synchronous reset
            if (rst_i = '1') then
                lfsr_reg <= LFSR_PRESET;
            else
                -- Progress in the LFSR sequence on an input trigger event
                if (enable_i = '1' and trig_i = '1') then
                    -- LFSR process
                    bits     := (lfsr_reg(0)) xor (lfsr_reg(2));
                    lfsr_reg <= bits & lfsr_reg(N_SHIFTER - 1 downto 1);
                end if;

                -- Output pulse discriminator
                if ((current_rate =  1 and lfsr_reg = X"0F") or
                    (current_rate =  2 and lfsr_reg = X"07") or
                    (current_rate =  3 and lfsr_reg = X"03") or
                    (current_rate =  4 and lfsr_reg = X"11") or
                    (current_rate =  5 and lfsr_reg = X"18") or
                    (current_rate =  6 and lfsr_reg = X"0C") or
                    (current_rate =  7 and lfsr_reg = X"16") or
                    (current_rate =  8 and lfsr_reg = X"1B") or
                    (current_rate =  9 and lfsr_reg = X"1D") or
                    (current_rate = 10 and lfsr_reg = X"0E") or
                    (current_rate = 11 and lfsr_reg = X"17") or
                    (current_rate = 12 and lfsr_reg = X"0B") or
                    (current_rate = 13 and lfsr_reg = X"15") or
                    (current_rate = 14 and lfsr_reg = X"0A") or
                    (current_rate = 15 and lfsr_reg = X"05") or
                    (current_rate = 16 and lfsr_reg = X"02") or
                    (current_rate = 17 and lfsr_reg = X"01") or
                    (current_rate = 18 and lfsr_reg = X"10") or
                    (current_rate = 19 and lfsr_reg = X"08") or
                    (current_rate = 20 and lfsr_reg = X"04") or
                    (current_rate = 21 and lfsr_reg = X"12") or
                    (current_rate = 22 and lfsr_reg = X"09") or
                    (current_rate = 23 and lfsr_reg = X"14") or
                    (current_rate = 24 and lfsr_reg = X"1A") or
                    (current_rate = 25 and lfsr_reg = X"0D") or
                    (current_rate = 26 and lfsr_reg = X"06") or
                    (current_rate = 27 and lfsr_reg = X"13") or
                    (current_rate = 28 and lfsr_reg = X"19") or
                    (current_rate = 29 and lfsr_reg = X"1C") or
                    (current_rate = 30 and lfsr_reg = X"1E") or
                    (current_rate = 31 and lfsr_reg = X"1F")) then
                    lfsr_reg     <= LFSR_PRESET;
                    pulse_output <= '1';
                end if;

            end if;
        end if;
    end process lfsr_proc;

    --=============================================================================
    --! @brief rate_i selector process
    --! @param  clk_i
    --=============================================================================
    load_rate : process(clk_i)
    begin
        if (clk_i'event and clk_i = '1') then
            if (rst_i = '1') then
                current_rate  <= to_integer(unsigned(rate_i)); -- After reset, current rate is the read rate from the input port
                new_rate_flag <= '0';
            else
                if (new_rate_flag = '1' and lfsr_reg = LFSR_PRESET) then
                    -- Priority to the previous update_rate event
                    current_rate  <= to_integer(unsigned(rate_i)); -- Load new rate from the input port
                    new_rate_flag <= '0'; -- Reset flag
                elsif (rate_load_p_i = '1') then
                    new_rate_flag <= '1'; -- Raise rate update flag
                end if;
            end if;
        end if;
    end process load_rate;

    --=============================================================================
    -- Concurrent assignments
    pulse_o <= pulse_output;
    sync_o  <= not new_rate_flag;

end Behavioral;