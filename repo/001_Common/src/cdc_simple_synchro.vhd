----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             cdc_simple_synchro.vhd
-- module name      Simple double FF synchronizer
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Simple clock domain synchronizer, with double FF, without register.
--
----------------------------------------------------------------------------------------------
-- create date      01.10.2020
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

entity cdc_simple_synchro is
    port (
        clk_i   : in  std_logic; --! Destination clock domain
        async_i : in  std_logic; --! Asynchronous input
        sync_o  : out std_logic  --! Synchronous output
    );
end entity cdc_simple_synchro;

architecture rtl of cdc_simple_synchro is

begin

    -- xpm_cdc_single: Single-bit Synchronizer
    -- Xilinx Parameterized Macro, version 2018.1

    xpm_cdc_single_inst : xpm_cdc_single
        generic map(
            DEST_SYNC_FF   => 2,       -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 1,       -- DECIMAL; integer; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,       -- DECIMAL; integer; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0        -- DECIMAL; integer; 0=do not register input, 1=register input
        )
        port map(
            dest_out       => sync_o,  -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
            dest_clk       => clk_i,   -- 1-bit input: Clock signal for the destination clock domain.
            src_clk        => '0',     -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in         => async_i  -- 1-bit input: Input signal to be synchronized to dest_clk domain.
        );

end architecture rtl;
