----------------------------------------------------------------------------------------------
--                                   ALTAM SYSTEMS SL
--
--          (c) Copyright 2018 - 2020, ALTAM SYSTEMS SL, Barcelona, Spain
--
--                                 All rights reserved
--
--
-- COPYRIGHT NOTICE:
-- All information contained herein is, and remains the property of ALTAM Systems S.L.
-- and its suppliers, if any.
-- The intellectual and technical concepts contained herein are proprietary to
-- ALTAM Systems S.L. and its suppliers and may be covered by Spanish and Foreign Patents,
-- patents in process, and are protected by trade secret or copyright law.
-- Dissemination of this information, modification or reproduction of this material
-- is strictly forbidden unless prior written permission is obtained from ALTAM Systems S.L.
--
----------------------------------------------------------------------------------------------
--! @file       pulse_rate_divider.vhd
--! @brief      A generic pulse rate divider block
--! @details    A generic pulse train rate divider, based on a Fibonacci LFSR.
--! The LFSR sequence for the x�?� + x³ + 1 polynomial initialized with all ones, is:
--! LFSR        Sequence item
--! ========================
--! 0x F --     1
--! 0x 7 --     2
--! 0x 3 --     3
--! 0x11 --     4
--! 0x18 --     5
--! 0x C --     6
--! 0x16 --     7
--! 0x1B --     8
--! 0x1D --     9
--! 0x E --    10
--! 0x17 --    11
--! 0x B --    12
--! 0x15 --    13
--! 0x A --    14
--! 0x 5 --    15
--! 0x 2 --    16
--! 0x 1 --    17
--! 0x10 --    18
--! 0x 8 --    19
--! 0x 4 --    20
--! 0x12 --    21
--! 0x 9 --    22
--! 0x14 --    23
--! 0x1A --    24
--! 0x D --    25
--! 0x 6 --    26
--! 0x13 --    27
--! 0x19 --    28
--! 0x1C --    29
--! 0x1E --    30
--! 0x1F --    31
--!
--! Update rate behavior:
--!    After a reset, the divider will take the p_pulse_rate_divider_rate_in value present at the input port.
--! If a new rate is desired, p_pulse_rate_divider_rate_load_p_in has to be pulsed. The p_pulse_rate_divider_sync_out output will go to zero. At the end of the current
--! cycle, the new rate value will be applied and the output p_pulse_rate_divider_sync_out will transition back to '1'.

--! @author     Jorge Sanchez (jsanchez@altamsys.com)
--! @date        17/12/2018
----------------------------------------------------------------------------------------------
-- CHANGES:
--        17.12.2018 (JSQ) - Creation date
--
--        26.05.2020 (JSQ) - Licensed to RIMAC to be used within the BFI project
--
--        Revision 0.03 by david.pavlovic
--                         - Code cleanup
--                         - Chagned logic to use asynchronous active-low reset
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PulseRateDivider is
    port (
        p_pulse_rate_divider_clk_in         : in  std_logic;                    -- Input clock
        p_pulse_rate_divider_rst_n_in       : in  std_logic;                    -- Reset input. Synchronous reset, atcive high!
        p_pulse_rate_divider_rate_load_p_in : in  std_logic;                    -- Load new pulse rate input
        p_pulse_rate_divider_rate_in        : in  std_logic_vector(4 downto 0); -- Pulse rate selector input
        p_pulse_rate_divider_enable_in      : in  std_logic;                    -- Enable input
        p_pulse_rate_divider_trig_in        : in  std_logic;                    -- Pulse train input
        p_pulse_rate_divider_sync_out       : out std_logic;                    -- Sychro ready output. Pulsed when the new selected rate is active.
        p_pulse_rate_divider_pulse_out      : out std_logic                     -- Pulse train output
    );
end entity PulseRateDivider;

architecture rtl of PulseRateDivider is

    ---------------------------------------------------------------------------------
    --                                   constants
    ---------------------------------------------------------------------------------

    constant C_SHIFT       : natural                        := 5;
    constant C_LFSR_PRESET : unsigned(C_SHIFT - 1 downto 0) := (others => '1');

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_clk           : std_logic;
    signal s_rst_n         : std_logic;
    signal s_lfsr_reg      : unsigned(C_SHIFT - 1 downto 0);
    signal s_rate_load_p   : std_logic;
    signal s_rate          : std_logic_vector(4 downto 0);
    signal s_enable        : std_logic;
    signal s_trig          : std_logic;
    signal s_current_rate  : natural;
    signal s_pulse_output  : std_logic;
    signal s_new_rate_flag : std_logic;

begin

    -- input signal assignments
    s_clk         <= p_pulse_rate_divider_clk_in;
    s_rst_n       <= p_pulse_rate_divider_rst_n_in;
    s_rate_load_p <= p_pulse_rate_divider_rate_load_p_in;
    s_rate        <= p_pulse_rate_divider_rate_in;
    s_enable      <= p_pulse_rate_divider_enable_in;
    s_trig        <= p_pulse_rate_divider_trig_in;

    -- output signal assignments
    p_pulse_rate_divider_pulse_out <= s_pulse_output;
    p_pulse_rate_divider_sync_out  <= not s_new_rate_flag;

    --=============================================================================
    --! @brief Main process
    --! @param    s_clk
    --=============================================================================
    lfsr_proc : process (s_clk, s_rst_n)
        variable v_bits : std_logic;
    begin
        if (s_rst_n = '0') then
            s_lfsr_reg <= C_LFSR_PRESET;
        elsif rising_edge(s_clk) then
            s_pulse_output <= '0';
            -- Progress in the LFSR sequence on an input trigger event
            if (s_enable = '1' and s_trig = '1') then
                -- LFSR process
                v_bits     := (s_lfsr_reg(0)) xor (s_lfsr_reg(2));
                s_lfsr_reg <= v_bits & s_lfsr_reg(C_SHIFT - 1 downto 1);
            end if;
            -- Output pulse discriminator
            if ((s_current_rate =  1 and s_lfsr_reg = X"0F") or
                (s_current_rate =  2 and s_lfsr_reg = X"07") or
                (s_current_rate =  3 and s_lfsr_reg = X"03") or
                (s_current_rate =  4 and s_lfsr_reg = X"11") or
                (s_current_rate =  5 and s_lfsr_reg = X"18") or
                (s_current_rate =  6 and s_lfsr_reg = X"0C") or
                (s_current_rate =  7 and s_lfsr_reg = X"16") or
                (s_current_rate =  8 and s_lfsr_reg = X"1B") or
                (s_current_rate =  9 and s_lfsr_reg = X"1D") or
                (s_current_rate = 10 and s_lfsr_reg = X"0E") or
                (s_current_rate = 11 and s_lfsr_reg = X"17") or
                (s_current_rate = 12 and s_lfsr_reg = X"0B") or
                (s_current_rate = 13 and s_lfsr_reg = X"15") or
                (s_current_rate = 14 and s_lfsr_reg = X"0A") or
                (s_current_rate = 15 and s_lfsr_reg = X"05") or
                (s_current_rate = 16 and s_lfsr_reg = X"02") or
                (s_current_rate = 17 and s_lfsr_reg = X"01") or
                (s_current_rate = 18 and s_lfsr_reg = X"10") or
                (s_current_rate = 19 and s_lfsr_reg = X"08") or
                (s_current_rate = 20 and s_lfsr_reg = X"04") or
                (s_current_rate = 21 and s_lfsr_reg = X"12") or
                (s_current_rate = 22 and s_lfsr_reg = X"09") or
                (s_current_rate = 23 and s_lfsr_reg = X"14") or
                (s_current_rate = 24 and s_lfsr_reg = X"1A") or
                (s_current_rate = 25 and s_lfsr_reg = X"0D") or
                (s_current_rate = 26 and s_lfsr_reg = X"06") or
                (s_current_rate = 27 and s_lfsr_reg = X"13") or
                (s_current_rate = 28 and s_lfsr_reg = X"19") or
                (s_current_rate = 29 and s_lfsr_reg = X"1C") or
                (s_current_rate = 30 and s_lfsr_reg = X"1E") or
                (s_current_rate = 31 and s_lfsr_reg = X"1F")) then
                s_lfsr_reg     <= C_LFSR_PRESET;
                s_pulse_output <= '1';
            end if;
        end if;
    end process lfsr_proc;

    --=============================================================================
    --! @brief s_rate selector process
    --! @param    s_clk, s_rst_n
    --=============================================================================
    load_rate : process (s_clk, s_rst_n)
    begin
        if (s_rst_n = '0') then
            s_current_rate  <= to_integer(unsigned(s_rate)); -- After reset, current rate is the read rate from the input port
            s_new_rate_flag <= '0';
        elsif rising_edge(s_clk) then
            if (s_new_rate_flag = '1' and s_lfsr_reg = C_LFSR_PRESET) then
                -- Priority to the previous update_rate event
                s_current_rate  <= to_integer(unsigned(s_rate)); -- Load new rate from the input port
                s_new_rate_flag <= '0'; -- Reset flag
            elsif (s_rate_load_p = '1') then
                s_new_rate_flag <= '1'; -- Raise rate update flag
            end if;
        end if;
    end process load_rate;

end architecture rtl;