----------------------------------------------------------------------------------------------
--                                   ALTAM SYSTEMS SL
--
--          (c) Copyright 2018 - 2020, ALTAM SYSTEMS SL, Barcelona, Spain
--
--                                 All rights reserved
--
-- COPYRIGHT NOTICE:
-- All information contained herein is, and remains the property of ALTAM Systems S.L.
-- and its suppliers, if any.
-- The intellectual and technical concepts contained herein are proprietary to
-- ALTAM Systems S.L. and its suppliers and may be covered by Spanish and Foreign Patents,
-- patents in process, and are protected by trade secret or copyright law.
-- Dissemination of this information, modification or reproduction of this material
-- is strictly forbidden unless prior written permission is obtained from ALTAM Systems S.L.
--
----------------------------------------------------------------------------------------------
--! @file    edge_detector.vhd
--! @brief   Rising edge detector
--! @details
--! @author  Jorge Sanchez (jsanchez@altamsys.com)
--! @date    15.01.2019
----------------------------------------------------------------------------------------------
-- CHANGES:
--        05.01.2019 (JSQ) - Creation date
--        26.05.2020 (JSQ) - Licensed to RIMAC to be used within the BFI project
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity edge_detector is
    port (
        clk_i : in  std_logic; --! Clock input
        rst_i : in  std_logic; --! Reset input
        d_i   : in  std_logic; --! Data input
        q_p_o : out std_logic; --! Edge detected pulse output
        en_i  : in  std_logic  --! Enable input
    );
end entity edge_detector;

architecture rtl of edge_detector is

    signal pfilter_0, pfilter_1 : std_logic; --! Filter taps

begin

    --==================================================================
    --! @brief Filter process.
    --! @param clk_i
    --! @param rst_i
    --==================================================================
    sdr_register_p : process (clk_i, rst_i)
    begin
        if (rst_i = '1') then
            q_p_o     <= '0';
            pfilter_1 <= '0';
            pfilter_0 <= '0';
        elsif rising_edge(clk_i) then
            if (en_i = '1') then
                pfilter_1 <= pfilter_0;
                pfilter_0 <= d_i;
                -- Decisor
                if ((pfilter_1 = '0') and (pfilter_0 = '1')) then
                    q_p_o <= '1';
                else
                    q_p_o <= '0';
                end if;
            end if;
        end if;
    end process;
end architecture rtl;
