----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             spi_wrapper.vhd
-- module name      spi_wrapper - Behavioral
-- author           Marko Gulin (marko.gulin@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Wrapper for SPI slave to behave as master
----------------------------------------------------------------------------------------------
-- create date      28.10.2019
-- Revision:
--      Revision 0.01 by marko.gulin
--                    - File Created
----------------------------------------------------------------------------------------------
-- TODO: -
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Entity definition
entity spi_wrapper is
    generic (
        G_SPI_FREQ         : natural := 50 -- SPI clock frequency (2*50*10ns=1us)
    );
    port (
        -- Main signals
        p_spi_clk_in       : in    std_logic;
        p_spi_reset_n_in   : in    std_logic;  -- async reset

        -- SPI lines
        p_spi_ss_n_out     : out   std_logic;
        p_spi_sck_out      : out   std_logic;
        p_spi_mosi_out     : out   std_logic;

        -- ADC data
        p_spi_dataCh0_in   : in    std_logic_vector(15 downto 0);
        p_spi_dataCh1_in   : in    std_logic_vector(15 downto 0);
        p_spi_dataCh2_in   : in    std_logic_vector(15 downto 0);
        p_spi_dataCh3_in   : in    std_logic_vector(15 downto 0);

        -- Echo trigger
        p_spi_trigger_p_in : in    std_logic
    );
end spi_wrapper;

-- Entity implementation
architecture Behavioral of spi_wrapper is

    -- Time constants
    -- (expected time base: 10 ns)
    constant C_TIME_BLANKING : natural := 5000;     -- 50 us

    -- State machine type definition
    type t_spiStateMachine is (
        t_spiStateMachine_idle,
        t_spiStateMachine_activateSlave,
        t_spiStateMachine_echoData,
        t_spiStateMachine_echoBlanking
    );

    -- State machine states
    signal s_stateLvl0 : t_spiStateMachine;   -- level 0 state

    -- SPI signals
    signal s_ss_n : std_logic;
    signal s_sck  : std_logic;

    -- Serial clock
    signal s_sckCounter : natural range 0 to G_SPI_FREQ;

    -- Data vector
    signal s_data : std_logic_vector(63 downto 0);

    -- Number of echoed bytes
    signal s_byteCounter : natural range 0 to 8;

    -- Echo blanking counter
    signal s_echoBlankingCounter : natural range 0 to C_TIME_BLANKING;

    -- SPI done flag from slave module
    signal s_spiDone_p : std_logic;

begin

    -- ### MODULE OUTPUTS ### --

    p_spi_ss_n_out <= s_ss_n;
    p_spi_sck_out <= s_sck;

    -- ### SPI MASTER ### --

    process (p_spi_reset_n_in, p_spi_clk_in)
    begin

        if (p_spi_reset_n_in = '0') then

            s_stateLvl0             <= t_spiStateMachine_idle;

            s_ss_n                  <= '1';
            s_sck                   <= '0';

            s_sckCounter            <= 0;

            s_data                  <= (others=>'0');

            s_byteCounter           <= 0;

            s_echoBlankingCounter   <= 0;

        elsif rising_edge(p_spi_clk_in) then

            -- ### STATE MACHINE ### --

            -- LEVEL 0 STATE MACHINE
            case s_stateLvl0 is

                when t_spiStateMachine_idle =>
                    -- Reset SPI signals
                    s_ss_n <= '1';
                    s_sck <= '0';

                    -- Reset counters
                    s_sckCounter <= 0;
                    s_byteCounter <= 0;
                    s_echoBlankingCounter <= 0;

                    -- Check for echo trigger
                    if (p_spi_trigger_p_in = '1') then
                        s_data <= p_spi_dataCh0_in & p_spi_dataCh1_in & p_spi_dataCh2_in & p_spi_dataCh3_in;
                        s_stateLvl0 <= t_spiStateMachine_activateSlave;
                    end if;

                when t_spiStateMachine_activateSlave =>
                    -- Activate SPI slave
                    s_ss_n <= '0';

                    -- Update serial clock counter
                    s_sckCounter <= s_sckCounter + 1;

                    -- Wait half clock cycle before starting
                    if (s_sckCounter = G_SPI_FREQ) then
                        -- Reset serial clock counter
                        s_sckCounter <= 0;

                        -- Transition to next state
                        s_stateLvl0 <= t_spiStateMachine_echoData;

                        -- Initial data shift
                        s_data <= s_data(55 downto 0) & X"00";

                    end if;

                when t_spiStateMachine_echoData =>
                    -- Update serial clock counter
                    s_sckCounter <= s_sckCounter + 1;

                    -- Check for serial clock counter overflow
                    if (s_sckCounter = G_SPI_FREQ) then
                        -- Reset serial clock counter
                        s_sckCounter <= 0;

                        -- Transition serial clock
                        s_sck <= not s_sck;

                        -- Stop SPI transaction on serial clock falling edge
                        if ((s_sck = '1') and (s_byteCounter = 8)) then
                            -- Deactivate SPI slave
                            s_ss_n <= '1';

                            -- Transition to next state
                            s_stateLvl0 <= t_spiStateMachine_echoBlanking;

                        end if;

                    end if;

                    -- Shift data when SPI slave is done with a byte
                    if (s_spiDone_p = '1') then
                        -- Shift data vector
                        s_data <= s_data(55 downto 0) & X"00";

                        -- Update byte counter
                        s_byteCounter <= s_byteCounter + 1;

                    end if;

                when t_spiStateMachine_echoBlanking =>
                    -- Update echo blanking counter
                    s_echoBlankingCounter <= s_echoBlankingCounter + 1;

                    -- Check for blanking counter overflow
                    if (s_echoBlankingCounter = C_TIME_BLANKING) then
                        s_stateLvl0 <= t_spiStateMachine_idle;
                    end if;

            end case;

        end if;

    end process;

    -- ### SPI SLAVE COMPONENT ### --

    mdl_spiSlave_0 : entity work.spi_slave(Behavioral)
        Port map (
            p_spi_clk_in        => p_spi_clk_in,
            p_spi_reset_n_in    => p_spi_reset_n_in,
            p_spi_ss_n_in       => s_ss_n,
            p_spi_mosi_in       => '0',
            p_spi_miso_out      => p_spi_mosi_out, -- slave behaves as master
            p_spi_sck_in        => s_sck,
            p_spi_misoEn_in     => '1',
            p_spi_done_p_out    => s_spiDone_p,
            p_spi_byteTx_in     => s_data(63 downto 56)
        );

end Behavioral;
