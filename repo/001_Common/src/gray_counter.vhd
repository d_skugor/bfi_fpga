----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             gray_counter.vhd
-- module name      Gray Counter - Behavioral
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Gray counter with N width with feature to correct itself
----------------------------------------------------------------------------------------------
-- create date     27.09.2019
-- Revision:
--      Revision 0.01 - File Created -
--
--      Revision 0.02 by danijela.skugor
--                  - Refactor gray counter : Instead of instantiating itself , two gray counting
--                     is done in parallel and counting logic is removed in separeted component
--                  - Input reset is now active low instead active high
--                  - Rename generic names to follow coding rules
--
--      Revision 0.03 by david.pavlovic
--                  - Code cleanup
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity gray_counter is
    generic (
        G_GRAYCNT_WIDTH         : integer := 4;
        G_GRAYCNT_CORRECTION_ON : boolean := false  -- can gray counter fix itself if an error occurs
    );
    port (
        p_grayCnt_clk_in        : in std_logic;
        p_grayCnt_rst_n_in      : in std_logic;     -- sync reset
        p_grayCnt_en_in         : in std_logic;     -- enable
        p_grayCnt_err_p_out     : out std_logic;    -- pulsed error
        p_grayCnt_res_out       : out std_logic_vector(G_GRAYCNT_WIDTH -1 downto 0)
    );
end entity gray_counter;

architecture structural of gray_counter is

    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------

    component gray_counter_block is
        generic (
            G_GRAYCNTBLOCK_WIDTH       : integer := 4
        );
        port (
            p_grayCntBlock_clk_in      : in  std_logic;
            p_grayCntBlock_rst_n_in    : in  std_logic;                                           -- sync reset
            p_grayCntBlock_en_in       : in  std_logic;                                           -- enable
            p_grayCntBlock_prev_res_in : in  std_logic_vector(G_GRAYCNTBLOCK_WIDTH - 1 downto 0); -- valid count
            p_grayCntBlock_err_p_out   : out std_logic;                                           -- pulsed error
            p_grayCntBlock_res_out     : out std_logic_vector(G_GRAYCNTBLOCK_WIDTH - 1 downto 0)
        );
    end component;

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_res_valid_i     : std_logic_vector (G_GRAYCNT_WIDTH-1 downto 0);
    signal s_err_i           : std_logic;

    -- output signals from main counting block
    signal s_swap_err_main_p : std_logic;
    signal s_res_main        : std_logic_vector (G_GRAYCNT_WIDTH-1 downto 0);

    -- output signals from correction counting block
    signal s_swap_err_corr_p : std_logic;
    signal s_res_corr        : std_logic_vector (G_GRAYCNT_WIDTH-1 downto 0);

begin

   main_cnt_block : gray_counter_block
        generic map(
            G_GRAYCNTBLOCK_WIDTH       => G_GRAYCNT_WIDTH
        )
        port map(
            p_grayCntBlock_clk_in      => p_grayCnt_clk_in,
            p_grayCntBlock_rst_n_in    => p_grayCnt_rst_n_in,
            p_grayCntBlock_en_in       => p_grayCnt_en_in,
            p_grayCntBlock_prev_res_in => s_res_valid_i,
            p_grayCntBlock_err_p_out   => s_swap_err_main_p,
            p_grayCntBlock_res_out     => s_res_main
        );
  ---------------------------------------------------------------------------------------------------------------
   correction_on :  if (G_GRAYCNT_CORRECTION_ON = true) generate

    correction_cnt_block : gray_counter_block
        generic map(
            G_GRAYCNTBLOCK_WIDTH       => G_GRAYCNT_WIDTH
        )
        port map(
            p_grayCntBlock_clk_in      => p_grayCnt_clk_in,
            p_grayCntBlock_rst_n_in    => p_grayCnt_rst_n_in,
            p_grayCntBlock_en_in       => p_grayCnt_en_in,
            p_grayCntBlock_prev_res_in => s_res_valid_i,
            p_grayCntBlock_err_p_out   => s_swap_err_corr_p,
            p_grayCntBlock_res_out     => s_res_corr
        );

    --------------------------------------------------------------------
    -- if parallel correction gray counter is implemented -> fix the value, if error happened
    -- the idea is that the bit swapping would occur only on one gray counter component
    -- so if error happens on main counter -  the result from the correction counter
    -- would be driven out. if error doesn't happen on main counter -  the result from
    -- the main counter is taken. But if the error happens on correction counter,
    -- the error would be drive out to notify that something wrong happened

        s_res_valid_i <= s_res_main when s_swap_err_main_p = '0' else s_res_corr;

        s_err_i <= s_swap_err_main_p or s_swap_err_corr_p;
    end generate;

    -- if parallel gray counter is not implemented
    no_correct_result : if (G_GRAYCNT_CORRECTION_ON = false) generate
         s_res_valid_i <= s_res_main;
         s_err_i <= s_swap_err_main_p;
    end generate;

    -- drive the outputs

    -- put error in register so that it last one clock
    process(p_grayCnt_clk_in)
    begin
        if (rising_edge(p_grayCnt_clk_in)) then
            if (p_grayCnt_rst_n_in = '0') then
                p_grayCnt_err_p_out <= '0';
            else
                p_grayCnt_err_p_out <= s_err_i;
            end if;
        end if;
    end process;

    p_grayCnt_res_out   <= s_res_valid_i;

end architecture structural;
