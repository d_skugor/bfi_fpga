
----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     all rights reserved
----------------------------------------------------------------------------------------------
-- file             pll_clk_gen.vhd
-- module name      pll_clk_gen - Behavioral
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            PLL-Based clock generator module. Instantiates the primitives to generate
--                  clock based on a PLL module (Artix-7 family).
----------------------------------------------------------------------------------------------
-- create date      03.11.2020
-- Revision:
--      Revision 0.01 jsanchez
--                    - Initial version
----------------------------------------------------------------------------------------------
--
-- NOTE: This is a VHDL2008 file
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

library work;
use work.all;

entity pll_clk_gen is
    generic (
        CLK_PERIOD    : real    := 10.0;
        CLK_MULT      : natural := 5;
        CLK1_DIV      : natural := 5;
        CLK2_DIV      : natural := 5;
        CLK3_DIV      : natural := 5
        );
    port (
        clk_system_in : in  std_logic;
        rst_n_in      : in  std_logic;
        ready_out     : out std_logic;  -- Synchronous to the system clock
        clk1_out      : out std_logic;
        clk2_out      : out std_logic;
        clk3_out      : out std_logic
    );
end entity pll_clk_gen;

architecture rtl of pll_clk_gen is

    signal s_clk1_out : std_logic; -- PLL clk1 internal output
    signal s_clk2_out : std_logic; -- PLL clk2 internal output
    signal s_clk3_out : std_logic; -- PLL clk3 internal output

    signal s_clk_fbk  : std_logic; -- PLL feedback
    signal s_rdy_a    : std_logic; -- Asnychronous ready output from PLL locked output
    signal s_rdy      : std_logic; -- Synchronized ready

    signal s_rst      : std_logic;

begin

    s_rst  <= not rst_n_in;

    -- PLLE2_BASE: Base Phase Locked Loop (PLL)
    --             Artix-7
    -- Xilinx HDL Language Template, version 2018.3

    PLLE2_BASE_inst : PLLE2_BASE
        generic map(
            BANDWIDTH          => "OPTIMIZED",   -- OPTIMIZED, HIGH, LOW
            CLKFBOUT_MULT      => CLK_MULT,      -- Multiply value for all CLKOUT, (2-64)
            CLKFBOUT_PHASE     => 0.0,           -- Phase offset in degrees of CLKFB, (-360.000-360.000).
            CLKIN1_PERIOD      => CLK_PERIOD,    -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
            -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
            CLKOUT0_DIVIDE     => CLK1_DIV,
            CLKOUT1_DIVIDE     => CLK2_DIV,
            CLKOUT2_DIVIDE     => CLK3_DIV,
            CLKOUT3_DIVIDE     => 1,
            CLKOUT4_DIVIDE     => 1,
            CLKOUT5_DIVIDE     => 1,
            -- CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
            CLKOUT0_DUTY_CYCLE => 0.5,
            CLKOUT1_DUTY_CYCLE => 0.5,
            CLKOUT2_DUTY_CYCLE => 0.5,
            CLKOUT3_DUTY_CYCLE => 0.5,
            CLKOUT4_DUTY_CYCLE => 0.5,
            CLKOUT5_DUTY_CYCLE => 0.5,
            -- CLKOUT0_PHASE - CLKOUT5_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
            CLKOUT0_PHASE      => 0.0,
            CLKOUT1_PHASE      => 0.0,
            CLKOUT2_PHASE      => 0.0,
            CLKOUT3_PHASE      => 0.0,
            CLKOUT4_PHASE      => 0.0,
            CLKOUT5_PHASE      => 0.0,
            DIVCLK_DIVIDE      => 1,             -- Master division value, (1-56)
            REF_JITTER1        => 0.0,           -- Reference input jitter in UI, (0.000-0.999).
            STARTUP_WAIT       => "FALSE"        -- Delay DONE until PLL Locks, ("TRUE"/"FALSE")
        )
        port map(
            -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
            CLKOUT0            => s_clk1_out,    -- 1-bit output: CLKOUT0
            CLKOUT1            => s_clk2_out,    -- 1-bit output: CLKOUT1
            CLKOUT2            => s_clk3_out,    -- 1-bit output: CLKOUT2
            CLKOUT3            => open,          -- 1-bit output: CLKOUT3
            CLKOUT4            => open,          -- 1-bit output: CLKOUT4
            CLKOUT5            => open,          -- 1-bit output: CLKOUT5
            -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
            CLKFBOUT           => s_clk_fbk,     -- 1-bit output: Feedback clock
            LOCKED             => s_rdy_a,       -- 1-bit output: LOCK
            CLKIN1             => clk_system_in, -- 1-bit input: Input clock
            -- Control Ports: 1-bit (each) input: PLL control ports
            PWRDWN             => '0',           -- 1-bit input: Power-down
            RST                => s_rst,         -- 1-bit input: Reset
            -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
            CLKFBIN            => s_clk_fbk      -- 1-bit input: Feedback clock
        );

    -- End of PLLE2_BASE_inst instantiation

    -- xpm_cdc_single: Single-bit Synchronizer
    -- Xilinx Parameterized Macro, version 2018.1

    xpm_cdc_single_inst : xpm_cdc_single
        generic map (
            DEST_SYNC_FF   => 8,          -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 1,          -- DECIMAL; integer; 0=disable simulation init values, 1=enable simulation init
                                          -- values
            SIM_ASSERT_CHK => 0,          -- DECIMAL; integer; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0           -- DECIMAL; integer; 0=do not register input, 1=register input
        )
        port map (
            dest_out       => s_rdy,      -- 1-bit output: src_in synchronized to the destination clock domain. This output is registered.
            dest_clk       => s_clk1_out, -- 1-bit input: Clock signal for the destination clock domain.
            src_clk        => '0',        -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in         => s_rdy_a     -- 1-bit input: Input signal to be synchronized to dest_clk domain.
        );

    -- End of xpm_cdc_single_inst instantiation

    clkout1_buf : unisim.vcomponents.BUFGCTRL
        generic map (
            INIT_OUT     => 0,
            PRESELECT_I0 => true,
            PRESELECT_I1 => false
        )
        port map (
            CE0          => s_rdy,
            CE1          => '0',
            I0           => s_clk1_out,
            I1           => '1',
            IGNORE0      => '0',
            IGNORE1      => '1',
            O            => clk1_out,
            S0           => '1',
            S1           => '0'
        );

    clkout2_buf : unisim.vcomponents.BUFGCTRL
        generic map (
            INIT_OUT     => 0,
            PRESELECT_I0 => true,
            PRESELECT_I1 => false
        )
        port map (
            CE0          => s_rdy,
            CE1          => '0',
            I0           => s_clk2_out,
            I1           => '1',
            IGNORE0      => '0',
            IGNORE1      => '1',
            O            => clk2_out,
            S0           => '1',
            S1           => '0'
        );

    clkout3_buf : unisim.vcomponents.BUFGCTRL
        generic map (
            INIT_OUT     => 0,
            PRESELECT_I0 => true,
            PRESELECT_I1 => false
        )
        port map (
            CE0          => s_rdy,
            CE1          => '0',
            I0           => s_clk3_out,
            I1           => '1',
            IGNORE0      => '0',
            IGNORE1      => '1',
            O            => clk3_out,
            S0           => '1',
            S1           => '0'
        );

    ready_out <= s_rdy;

end architecture rtl;
