//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             spi_if.sv
// module name      spi_if
// author           David Palvovic (david.pavlovic@rimac-automobili.com)
// project name     BFI
// target device    XA7A100T-1CSG324Q
// brief            SPI interface with master and slave modports
//--------------------------------------------------------------------------------------------
// create date      18.08.2020.
// Revision:
//      Revision 0.01 by david.pavlovic
//                    - File Created
//--------------------------------------------------------------------------------------------
// NOTE: This is a SystemVerilog file
//--------------------------------------------------------------------------------------------

interface spi_if #(int N = 1);

    logic         sck;
    logic [N-1:0] ss_n;
    logic         mosi;
    wire          miso;

    /* modport master
    (
        output sck,
        output ss_n_m,
        output mosi,
        input  miso
    );

    modport slave
    (
        input  sck,
        input  ss_n,
        input  mosi,
        output miso
    ); */

endinterface : spi_if