//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             pwm_if.sv
// module name      pwm_if
// author           David Palvovic (david.pavlovic@rimac-automobili.com)
// project name     BFI
// target device    XA7A100T-1CSG324Q
// brief            PWM interface for Gate Driver
//--------------------------------------------------------------------------------------------
// create date      01.09.2020.
// Revision:
//      Revision 0.01 by david.pavlovic
//                    - File Created
//--------------------------------------------------------------------------------------------
// NOTE: This is a SystemVerilog file
//--------------------------------------------------------------------------------------------

interface pwm_if #(real DT = 3000.0);

    timeunit 1ns / 1ps;

    logic [5:0] pwm_int;
    logic [5:0] pwm_int_d;
    logic [5:0] pwm_i;
    logic [5:0] pwm_o;
    logic [5:0] pwm_fb;
    logic [4:0] gdrv_flt;

    //const real deadtime = 3000.0;
    int  fb_delay;
    int  gdrv_delay;

    initial begin
        gdrv_flt <= '1;
        if (!std::randomize(gdrv_delay) with { gdrv_delay inside {[2000:5000]}; })
            $fatal(1, "*** RANDOMIZATION ERROR (gdrv_delay) ***");
        #gdrv_delay;
        gdrv_flt <= '0;
    end

    genvar i;
    generate
        for (i=0; i <= $left(pwm_int); i++) begin
            always @(pwm_int[i]) pwm_int_d[i] <= #DT pwm_int[i];
            assign pwm_i[i] = pwm_int[i] & pwm_int_d[i];
            always @(pwm_o[i]) pwm_fb[i] <= #fb_delay ~pwm_o[i];
        end
    endgenerate

endinterface : pwm_if