----------------------------------------------------------------------------------------------
--                                   ALTAM SYSTEMS SL
--
--          (c) Copyright 2018 - ${year}, ALTAM SYSTEMS SL, Barcelona, Spain
--
--                                 All rights reserved
--
--
-- COPYRIGHT NOTICE:
-- All information contained herein is, and remains the property of ALTAM Systems S.L.
-- and its suppliers, if any.
-- The intellectual and technical concepts contained herein are proprietary to
-- ALTAM Systems S.L. and its suppliers and may be covered by Spanish and Foreign Patents,
-- patents in process, and are protected by trade secret or copyright law.
-- Dissemination of this information, modification or reproduction of this material
-- is strictly forbidden unless prior written permission is obtained from ALTAM Systems S.L.
--
----------------------------------------------------------------------------------------------
--! @file            reset_generator.vhd
--! @brief           Reset synchronizer
--! @details         Synchronizes and debounces the asynchronous reset input to the FPGA
--! @author          J.Sanchez
--! @date            02.06.2020
----------------------------------------------------------------------------------------------
--
-- create date      02.06.2020
-- Revision:
--      Revision 0.01 by (JSQ)
--                    - Creation date
--
--      Revision 0.02 by dario.drvenkar
--                    - Added generic for pipeline delay depth
--                    - Added xpm_cdc_async_rst macro
--
--      Revision 0.03 by david.pavlovic
--                    - Code cleanup
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

entity reset_generator is
    generic (
        G_DEPTH       : natural := 3   -- Pipeline delay depth
    );
    port (
        clk_in        : in  std_logic; -- System clock
        asyn_rst_n_in : in  std_logic; -- Asynchronous negated reset input
        syn_rst_n_out : out std_logic  -- Synchronized negated reset output
    );
end entity;

architecture rtl of reset_generator is

begin

    xpm_cdc_async_rst_inst : xpm_cdc_async_rst
        generic map (
            DEST_SYNC_FF    => G_DEPTH,       -- DECIMAL; range: 2-10
            INIT_SYNC_FF    => 1,             -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            RST_ACTIVE_HIGH => 0              -- DECIMAL; 0=active low reset, 1=active high reset
        )
        port map (
            dest_arst       => syn_rst_n_out, -- 1-bit output: src_arst asynchronous reset signal synchronized to destination
                                              -- clock domain. This output is registered. NOTE: Signal asserts asynchronously
                                              -- but deasserts synchronously to dest_clk. Width of the reset signal is at least
                                              -- (DEST_SYNC_FF*dest_clk) period.
            dest_clk        => clk_in,        -- 1-bit input: Destination clock.
            src_arst        => asyn_rst_n_in  -- 1-bit input: Source asynchronous reset signal.
        );

end architecture;
