----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             gray_counter_block.vhd
-- module name      Gray Counter Block - Behavioral
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- target device    XA7A100T-1CSG324Q
-- brief            Gray counter with N width. Implemented checking if result is vaild and fixing the value
--                  if enabled by generic gen_correction_on.
--                  Result correction: First check if result is ok :
--                  If only one bit is changed than sig_bit_swapp value should be power of 2
--                  e.g result 1100 , prev result 0100
--                      0100 xor 1100 => 1000
--                      sig_bit_min1 = 1000 - 0001 = 0111
--                      sig_swappw2 = 1000 and 0111 => 0000 !! it's is power of 2 ! only one bit is changed!!
--
----------------------------------------------------------------------------------------------
-- create date     20.05.2021.
-- Revision:
--      Revision 0.01 - File Created -
--                    -- Logic inherited from gray_counter.vhd
--                    -- Added p_gray_cnt_prev_res_in port to continue counting from it
--
--      Revision 0.02 by david.pavlovic
--                    -- Code cleanup
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity gray_counter_block is
    generic (
        G_GRAYCNTBLOCK_WIDTH       : integer := 4
    );
    port (
        p_grayCntBlock_clk_in      : in  std_logic;
        p_grayCntBlock_rst_n_in    : in  std_logic;                                           -- sync reset
        p_grayCntBlock_en_in       : in  std_logic;                                           -- enable
        p_grayCntBlock_prev_res_in : in  std_logic_vector(G_GRAYCNTBLOCK_WIDTH - 1 downto 0); -- valid count
        p_grayCntBlock_err_p_out   : out std_logic;                                           -- pulsed error
        p_grayCntBlock_res_out     : out std_logic_vector(G_GRAYCNTBLOCK_WIDTH - 1 downto 0)
    );
end entity gray_counter_block;

architecture structural of gray_counter_block is

    ---------------------------------------------------------------------------------
    --                                    signals
    ---------------------------------------------------------------------------------

    signal s_res_i         : std_logic_vector (G_GRAYCNTBLOCK_WIDTH downto 0);
    signal s_res_valid     : std_logic_vector (G_GRAYCNTBLOCK_WIDTH downto 0);
    signal s_prev_res      : std_logic_vector (G_GRAYCNTBLOCK_WIDTH - 1 downto 0);
    signal s_z             : std_logic_vector (G_GRAYCNTBLOCK_WIDTH downto 0);
    signal s_qmsb          : std_logic;

    -- signals to validate result and fix if needed
    signal s_bit_swapp     : std_logic_vector (G_GRAYCNTBLOCK_WIDTH - 1 downto 0);
    signal s_bit_min1      : std_logic_vector (G_GRAYCNTBLOCK_WIDTH - 1 downto 0);
    signal s_swap_pw2      : std_logic_vector (G_GRAYCNTBLOCK_WIDTH - 1 downto 0);
    signal s_swap_err      : std_logic;

begin

    -- valid result
    s_res_valid(G_GRAYCNTBLOCK_WIDTH downto 1) <= p_grayCntBlock_prev_res_in;

    s_z(0) <= '1';

    --less significant bits
    g_create_lsb_bits: for i in 1 to G_GRAYCNTBLOCK_WIDTH - 1 generate
        process(p_grayCntBlock_clk_in)
        begin
            if(rising_edge(p_grayCntBlock_clk_in)) then
                if (p_grayCntBlock_rst_n_in = '0') then
                    s_res_i(i) <= '0';
                else
                    if (p_grayCntBlock_en_in = '1') then
                        s_res_i(i) <= s_res_valid(i) xor (s_res_valid(i-1) and s_z(i-1));
                    end if;
                end if;
            end if;
        end process;

        s_z(i) <= s_z(i-1) and not s_res_valid(i-1);

     end generate;

    p_create_msb: process(p_grayCntBlock_clk_in)
    begin
        if(rising_edge(p_grayCntBlock_clk_in)) then
            if ( p_grayCntBlock_rst_n_in = '0') then
                s_res_i(G_GRAYCNTBLOCK_WIDTH) <= '0';
            else
                if (p_grayCntBlock_en_in = '1') then
                    s_res_i(G_GRAYCNTBLOCK_WIDTH) <= s_res_valid(G_GRAYCNTBLOCK_WIDTH) xor (s_qmsb and s_z(G_GRAYCNTBLOCK_WIDTH - 1));
                end if;
            end if;
        end if;
    end process;

   s_z(G_GRAYCNTBLOCK_WIDTH) <= s_z(G_GRAYCNTBLOCK_WIDTH - 1) and not s_qmsb;

   -- auxiliary signal for msb
   s_qmsb <= s_res_valid(G_GRAYCNTBLOCK_WIDTH - 1) or s_res_valid(G_GRAYCNTBLOCK_WIDTH);

   process(p_grayCntBlock_clk_in)
   begin
       if(rising_edge(p_grayCntBlock_clk_in)) then
            if ( p_grayCntBlock_rst_n_in = '0') then
                s_res_i(0) <= '1';
                s_res_valid(0) <= '1';

            else
                if (p_grayCntBlock_en_in = '1') then
                    s_res_i(0) <= not  s_res_i(0);
                    s_res_valid(0) <= not  s_res_valid(0);
                end if;
            end if;
        end if;
   end process;


   --------------------------------------------------------------------------------------------------------------

    -- register current result as previous for checking single bit swapped
    process(p_grayCntBlock_clk_in)
    begin
        if (rising_edge(p_grayCntBlock_clk_in)) then
            if (p_grayCntBlock_rst_n_in = '0') then
                s_prev_res <= ( (G_GRAYCNTBLOCK_WIDTH - 1 ) => '1', others => '0');
            else
               s_prev_res <= s_res_i(G_GRAYCNTBLOCK_WIDTH downto 1);
            end if;
        end if;
    end process;

   -- do xor operation over result and previous result
   s_bit_swapp <= s_prev_res xor s_res_i(G_GRAYCNTBLOCK_WIDTH downto 1);

   --check if bit swap result is power of two -> which means only one bit is changed
   s_bit_min1 <= s_bit_swapp-1;
   s_swap_pw2 <= s_bit_swapp and s_bit_min1;

   -- if it's different than zero - it's not power of two
   s_swap_err <= '0' when (s_swap_pw2 = std_logic_vector(to_unsigned(0, s_swap_pw2'length))) else '1';

   -- drive error out
   p_grayCntBlock_err_p_out <= s_swap_err;

   -- drive result out
   p_grayCntBlock_res_out <= s_res_i(G_GRAYCNTBLOCK_WIDTH downto 1);

end architecture structural;
