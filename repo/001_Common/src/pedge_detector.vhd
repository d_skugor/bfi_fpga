----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             pedge_detector.vhd
-- module name      Positive edge detector
-- author           Jorge Sanchez Quesada (jsanchez@altamsys.com), ALTAM SYSTEMS SL
-- project name     BFI
-- target device    XA7A100T-CSG324Q1
-- brief            Positive edge detector block.
--
----------------------------------------------------------------------------------------------
-- create date      01.10.2020
-- Revision:
--      Revision 0.01 by jsq
--                      File Created
----------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pedge_detector is
    port(
        clk_i   : in  std_logic; --! Clock input
        rst_n_i : in  std_logic; --! Reset input
        d_i     : in  std_logic; --! Data input
        q_p_o   : out std_logic; --! Edge detected pulse output
        en_i    : in  std_logic  --! Enable input
    );
end entity pedge_detector;

architecture rtl of pedge_detector is

    signal pfilter_0, pfilter_1 : std_logic; --! Filter taps

begin

    sdr_register_p : process(clk_i, rst_n_i)
    begin
        if (rst_n_i = '0') then
            q_p_o     <= '0';
            pfilter_1 <= '0';
            pfilter_0 <= '0';
        elsif rising_edge(clk_i) then
            if (en_i = '1') then
                pfilter_1 <= pfilter_0;
                pfilter_0 <= d_i;
                -- Decisor
                if ((pfilter_1 = '0') and (pfilter_0 = '1')) then
                    q_p_o <= '1';
                else
                    q_p_o <= '0';
                end if;
            end if;
        end if;
    end process;

end architecture rtl;

