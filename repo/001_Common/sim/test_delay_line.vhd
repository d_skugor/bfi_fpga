----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_delay_line.vhd
-- module name      delay_line_tb
-- author           Dario Drvenkar (dario.drvenkar@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100T-1CSG324Q
-- brief            TB for Delay line with generic for data and length for delay
----------------------------------------------------------------------------------------------
-- create date      10.05.2021
-- Revision:
--      Revision 0.01 by dario.drvenkar
--                    - File Created
--
----------------------------------------------------------------------------------------------
-- TODO: - none
--
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity delay_line_tb is
end;

architecture bench of delay_line_tb is

  component delay_line
      generic (
          G_DELAY_LINE_WIDTH  : integer := 8;
          G_DELAY_LINE_LENGTH : integer := 2
      );
      port (
          p_delay_line_rst_n_in  :  in std_logic;
          p_delay_line_clk_in    :  in std_logic;
          p_delay_line_en_in     :  in std_logic;
          p_delay_line_data_in   :  in std_logic_vector(G_DELAY_LINE_WIDTH - 1 downto 0);
          p_delay_line_data_out  : out std_logic_vector(G_DELAY_LINE_WIDTH - 1 downto 0)
      );
  end component;
  
  constant C_DELAY_LINE_WIDTH  : natural := 12;
  constant C_DELAY_LINE_LENGTH : natural := 2;
 
  signal p_delay_line_rst_n_in: std_logic;
  signal p_delay_line_clk_in: std_logic;
  signal p_delay_line_en_in: std_logic;
  signal p_delay_line_data_in: std_logic_vector(C_DELAY_LINE_WIDTH - 1 downto 0);
  signal p_delay_line_data_out: std_logic_vector(C_DELAY_LINE_WIDTH - 1 downto 0) ;
  
  signal s_data : unsigned(C_DELAY_LINE_WIDTH - 1 downto 0) := to_unsigned(0,C_DELAY_LINE_WIDTH);

  constant clock_period: time := 10 ns;
  constant en_period: time := 1 us;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: delay_line generic map ( G_DELAY_LINE_WIDTH    => C_DELAY_LINE_WIDTH,
                                G_DELAY_LINE_LENGTH   => C_DELAY_LINE_LENGTH)
                     port map ( p_delay_line_rst_n_in => p_delay_line_rst_n_in,
                                p_delay_line_clk_in   => p_delay_line_clk_in,
                                p_delay_line_en_in    => p_delay_line_en_in,
                                p_delay_line_data_in  => p_delay_line_data_in,
                                p_delay_line_data_out => p_delay_line_data_out );

  stimulus: process
  begin
  
    -- Put initialisation code here
    p_delay_line_rst_n_in <= '1';
    wait for 100 ns;    
    p_delay_line_rst_n_in <= '0';
    wait for 100 ns;
    p_delay_line_rst_n_in <= '1';
    -- Put test bench stimulus code here
    wait for 5 ms;
    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      p_delay_line_clk_in <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;
  
  data_valid_signal: process
  begin
    while not stop_the_clock loop
      wait until rising_edge(p_delay_line_clk_in); 
      p_delay_line_en_in <= '0';
      for i in 1 to 99 loop
        wait until rising_edge(p_delay_line_clk_in);
      end loop;
      p_delay_line_en_in <= '1';
    end loop;
    wait;
  end process;
  
  data_generator: process
  begin
    while not stop_the_clock loop
      wait until rising_edge(p_delay_line_clk_in); 
        s_data <= s_data + 1;
    end loop;
  end process;
  
  p_delay_line_data_in <= std_logic_vector(s_data);

end;