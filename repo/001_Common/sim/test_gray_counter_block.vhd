----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_gray_counter_block.vhd
-- module name      Gray Counter Block Simulation
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100TCSG324-1L
-- brief            Top Level Module
----------------------------------------------------------------------------------------------
-- create date      25.05.2021
-- Revision:
--      Revision 0.01 - File Created
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity test_gray_counter_block is
--  Port ( );
end test_gray_counter_block;

architecture tb of test_gray_counter_block is
    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------
    component gray_counter_block is
       generic(
          G_GRAYCNTBLOCK_WIDTH             : integer := 4
       );
       port(
          p_grayCntBlock_clk_in       : in  std_logic;
          p_grayCntBlock_rst_n_in     : in  std_logic;                                           -- sync reset
          p_grayCntBlock_en_in        : in  std_logic;                                           -- enable
          p_grayCntBlock_prev_res_in  : in  std_logic_vector(G_GRAYCNTBLOCK_WIDTH - 1 downto 0); -- valid count
          p_grayCntBlock_err_p_out    : out std_logic;                                           -- pulsed error
          p_grayCntBlock_res_out      : out std_logic_vector(G_GRAYCNTBLOCK_WIDTH - 1 downto 0)
       );
     end component;
    ---------------------------------------------------------------------------------
    --                                  constants
    ---------------------------------------------------------------------------------
    constant SYSTEM_PERIOD   : time := 5 ns ;      -- 100 MHz
    constant C_GRAYCNT_WIDTH : integer := 4;

    ---------------------------------------------------------------------------------
    --                                  signals
    ---------------------------------------------------------------------------------

    signal sig_clk         : std_logic := '0';
    signal sig_clr         : std_logic := '0';

    signal sig_enable_cnt  : std_logic := '0';
    signal sig_counter_err : std_logic := '0';
    signal sig_counter_res : std_logic_vector(C_GRAYCNT_WIDTH-1 downto 0);

    signal sig_en_cnt_par  : std_logic := '0';
    signal sig_cnt_err_par : std_logic := '0';
    signal sig_cnt_res_par : std_logic_vector(C_GRAYCNT_WIDTH-1 downto 0);


    signal sig_cnt_valid   : std_logic_vector(C_GRAYCNT_WIDTH-1 downto 0);
    signal sig_swap_test   : std_logic_vector(C_GRAYCNT_WIDTH-1 downto 0) := ((1) => '1', others => '0');
    signal s_change_cnt    : std_logic;

begin

    sig_clr <='0', '1' after 50 ns , '0' after 905 ns, '1' after 915 ns;
    sig_enable_cnt <= '0' after 300 ns, '1' after 500 ns;
    sig_en_cnt_par <= '0' after 300 ns, '1' after 520 ns;


    clk_generate: process
    begin
        wait for 5 ns; -- 100 Mhz
        sig_clk <= not sig_clk;
    end process;

  gray_cnt_block : gray_counter_block
        generic map(
           G_GRAYCNTBLOCK_WIDTH => C_GRAYCNT_WIDTH
        )
        port map(
            p_grayCntBlock_clk_in   => sig_clk,
            p_grayCntBlock_rst_n_in   => sig_clr,
            p_grayCntBlock_en_in    => sig_enable_cnt,
            p_grayCntBlock_prev_res_in => sig_cnt_valid,
            p_grayCntBlock_err_p_out  => sig_counter_err,
            p_grayCntBlock_res_out  => sig_counter_res
        );

  gray_cnt_block_parallel : gray_counter_block
        generic map(
           G_GRAYCNTBLOCK_WIDTH => C_GRAYCNT_WIDTH
        )
        port map(
            p_grayCntBlock_clk_in   => sig_clk,
            p_grayCntBlock_rst_n_in   => sig_clr,
            p_grayCntBlock_en_in    => sig_en_cnt_par,
            p_grayCntBlock_prev_res_in => sig_cnt_res_par,
            p_grayCntBlock_err_p_out  => sig_cnt_err_par,
            p_grayCntBlock_res_out  => sig_cnt_res_par
        );

   -- test if change counting is ok
    s_change_cnt <= '0', '1' after 605 ns, '0' after 615 ns;
    sig_cnt_valid <= sig_counter_res when (s_change_cnt = '0') else sig_cnt_res_par;

end tb;
