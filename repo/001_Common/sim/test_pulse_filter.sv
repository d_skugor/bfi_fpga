//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2021, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             test_pulse_filter.sv
// module name      test_pulse_filter
// author           David Pavlović (david.pavlovic@rimac-automobili.com)
// project name     BFI
// target device    XA7A100TCSG324-1Q
// brief            Testbench for pulse_filter module
//--------------------------------------------------------------------------------------------
// create date      06.07.2021
// Revision:
//      Revision 0.01 by david.pavlovic
//                    - File Created
//--------------------------------------------------------------------------------------------

module test_pulse_filter;

    timeunit 1ns/1ps;

    // parameters
    parameter C_CLK_PER = 10.0;
    parameter G_PULSE_FILTER_LENGTH = 10;
    parameter G_PUSLE_ABOVE_THRESHOLD = 5;
    parameter C_EN_PERIOD = 100;

    // signals
    logic s_clk = 1'b0;
    logic s_rst_n = 1'b0;
    logic s_en = 1'b0;
    logic s_sig_in = 1'b0;
    logic s_sig_out;

    //-------------------------------------------------------------------------

    // DUT instance
    pulse_filter
        #(
            .G_PULSE_FILTER_LENGTH   (G_PULSE_FILTER_LENGTH)
        )
    dut
        (
            // main signals
            .p_pulse_filter_clk_in   (s_clk),
            .p_pulse_filter_rst_n_in (s_rst_n),
            .p_pulse_filter_en_in    (s_en),
            // input signal
            .p_pulse_filter_sig_in   (s_sig_in),
            .p_pulse_filter_sig_out  (s_sig_out)
        );

    //-------------------------------------------------------------------------

    // clock generator
    always begin
        #(C_CLK_PER/2) s_clk = ~s_clk;
    end

    // enable gemerator
    always begin
        repeat (C_EN_PERIOD-1) @(posedge s_clk);
        s_en <= 1'b1;
        @(posedge s_clk);
        s_en <= 1'b0;
    end

    //-----------------------------------
    // Tasks
    //-----------------------------------

    // generates N cycle active-low reset signal synchronous with clock
    task reset_gen(input int N);
        s_rst_n <= 1'b1;
        #20ns;
        s_rst_n <= 1'b0;
        repeat (N+1) @(posedge s_clk);
        s_rst_n <= 1'b1;
    endtask : reset_gen;

    // waits for N clock cycles
    task wait_clk(input int N);
        repeat (N)
            @(posedge s_clk);
    endtask : wait_clk

    // generates high and low pulse of duration N clock cycles
    task gen_pulse(input int N);
        s_sig_in <= 1'b1;
        wait_clk(N * C_EN_PERIOD);
        s_sig_in <= 1'b0;
        wait_clk(N * C_EN_PERIOD);
    endtask : gen_pulse

    initial begin
        reset_gen(3);
        wait_clk(2);
        for (int i = 0; i <= G_PULSE_FILTER_LENGTH + G_PUSLE_ABOVE_THRESHOLD - 1; i++)
            gen_pulse(i);
        wait_clk(10);
        for (int i = G_PULSE_FILTER_LENGTH + G_PUSLE_ABOVE_THRESHOLD - 1; i > 0; i--)
            gen_pulse(i);
        #50ns;
        $finish;
    end

    //initial begin
    //    reset_gen(3);
    //    wait_clk(2);
    //    for (int i = 0; i <= (G_PULSE_FILTER_LENGTH + G_PUSLE_ABOVE_THRESHOLD) - 1; i++)
    //        gen_pulse(i);
    //    wait_clk(10);
    //    for (int i = (G_PULSE_FILTER_LENGTH + G_PUSLE_ABOVE_THRESHOLD) * C_EN_PERIOD - 1; i > 0; i--)
    //        gen_pulse(i);
    //    #50ns;
    //    $finish;
    //end

    // final block
    final begin
        $display("*** End of simulation! ***");
        $display("*** Simulation was successful! ***");
    end

endmodule : test_pulse_filter