//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             test_spi_slave.vhd
// module name      SPI slave module testbench
// author           David Palvovic (david.pavlovic@rimac-automobili.com)
// project name     BFI
// target device    XA7A100T-1CSG324Q
// brief            SPI slave module testbench environment
//--------------------------------------------------------------------------------------------
// create date      22.07.2020.
// Revision:
//      Revision 0.01 by david.pavlovic
//                    - File Created
//
//      Revision 0.02 by david.pavlovic
//                    - Rewritten in SystemVerilog
//
//      Revision 0.03 by david.pavlovic
//                    - Added task for multiple SPI transactions
//--------------------------------------------------------------------------------------------
// NOTE: This is a SystemVerilog
//--------------------------------------------------------------------------------------------

import bfi_tb_pkg::*;

module test_spi_slave();

    timeunit 1ns / 1ps;

    parameter C_CLK_PER       = 10.0;
    parameter G_DATA_W        = 16;
    parameter C_NO_OF_SLV     = 1;
    parameter C_SPI_CLK_FREQ  = 5.0;
    parameter C_NO_OF_TRANS   = 1000;
    parameter C_NO_OF_FRAMES  = 2;

    logic                s_clk       = '0;
    logic                s_rst_n     = '0;
    logic [G_DATA_W-1:0] s_data_tx_i [C_NO_OF_SLV] = '{default:0};
    logic [G_DATA_W-1:0] s_data_tx_o [C_NO_OF_SLV];
    logic [G_DATA_W-1:0] s_data_rx   [C_NO_OF_SLV];
    logic                s_busy      [C_NO_OF_SLV];
    logic                s_valid     [C_NO_OF_SLV];
    logic                s_flt       [C_NO_OF_SLV];
    int                  delay;
    int                  slv_sel;

    // SPI interface
    spi_if #(C_NO_OF_SLV) spi_if_i();

    // SPI master class handle
    spi_master #(G_DATA_W, C_NO_OF_SLV) spi_m;

    genvar i;
    generate
        for (i = 0; i < C_NO_OF_SLV; i++) begin
        spi_slave
            #(
                .G_DATA_W              (G_DATA_W)
            )
        dut
            (
                // Main signals
                .p_spi_clk_in          (s_clk),
                .p_spi_rst_n_in        (s_rst_n),
                // SPI signals
                .p_spi_sck_in          (spi_if_i.sck),
                .p_spi_ss_n_in         (spi_if_i.ss_n[i]),
                .p_spi_mosi_in         (spi_if_i.mosi),
                .p_spi_miso_out        (spi_if_i.miso),
                // TX and RX data
                .p_spi_data_tx_in      (s_data_tx_i[i]),
                .p_spi_data_tx_out     (s_data_tx_o[i]),
                .p_spi_data_rx_out     (s_data_rx[i]),
                // Status signals
                .p_spi_busy_out        (s_busy[i]),
                .p_spi_data_rx_vld_out (s_valid[i]),
                .p_spi_trans_flt_out   (s_flt[i])
            );
        end
    endgenerate

    //-------------------------------------------------------------------------

    // clock generator
    always begin
        #(C_CLK_PER/2) s_clk = ~s_clk;
    end

    //-----------------------------------
    // Functions
    //-----------------------------------

    function void check_trans_data(input int slv);
        logic [G_DATA_W-1:0] miso;
        logic [G_DATA_W-1:0] mosi;
        miso = spi_m.get_miso();
        mosi = spi_m.get_mosi();
        // check MISO
        if (miso != s_data_tx_i[slv])
            $fatal(1, "*** Master received wrong data! ***\nReceived = %b\nExpected = %b", miso, s_data_tx_i[slv]);
        if (miso != s_data_tx_o[slv])
            $fatal(1, "*** Master received wrong data! ***\nReceived = %b\nExpected = %b", miso, s_data_tx_o[slv]);
        // check MOSI
        if (mosi != s_data_rx[slv])
            $fatal(1, "*** Slave received wrong data ***\nReceived = %b\nExpected = %b", s_data_rx[slv], mosi);
    endfunction : check_trans_data

    //-----------------------------------
    // Tasks
    //-----------------------------------

    // generates N cycle active-low reset signal synchronous with clock
    task reset_gen(input int N);
        s_rst_n <= 1'b0;
        repeat (N+1) @(posedge s_clk);
        s_rst_n <= 1'b1;
    endtask : reset_gen;

    // waits for N clock cycles
    task wait_clk(input int N);
        repeat (N) @(posedge s_clk);
    endtask : wait_clk

    // randomizes slave and slave's data and executes one SPI transaction
    task spi_trans();
        if(!std::randomize(slv_sel) with { slv_sel inside {[0:C_NO_OF_SLV-1]}; } )
            $fatal(1, "*** RANDOMIZATION ERROR (slv_sel) ***");
        if(!std::randomize(s_data_tx_i[slv_sel]))
            $fatal(1, "*** RANDOMIZATION ERROR (s_data_tx_i) ***");
        spi_m.randomize();
        fork
            spi_m.drive(slv_sel);
            @(posedge s_valid[slv_sel]);
        join
        check_trans_data(slv_sel);
        if(!std::randomize(delay) with { delay inside {[25:50]}; })
            $fatal(1, "*** RANDOMIZATION ERROR (delay) ***");
        wait_clk(delay);
    endtask : spi_trans

    // randomizes slave and slave's data and executes num_of_trans SPI transactions
    task spi_trans_rand_n(input int num_of_trans);
        if(!std::randomize(slv_sel) with { slv_sel inside {[0:C_NO_OF_SLV-1]}; } )
            $fatal(1, "*** RANDOMIZATION ERROR (slv_sel) ***");
        if(!std::randomize(s_data_tx_i[slv_sel]))
            $fatal(1, "*** RANDOMIZATION ERROR (s_data_tx_i) ***");
        if (!spi_m.randomize() with { mosi_a.size() == num_of_trans; })
            $fatal(1, "*** RANDOMIZATION ERROR (mosi_a.size) ***");
        //$display("%p", spi_m.mosi_a);
        spi_m.drive_n(slv_sel);
        if(!std::randomize(delay) with { delay inside {[25:50]}; })
            $fatal(1, "*** RANDOMIZATION ERROR (delay) ***");
        wait_clk(delay);
    endtask : spi_trans_rand_n

    // Main TB block
    initial begin
        spi_m = new(spi_if_i);
        spi_m.set_freq(C_SPI_CLK_FREQ);
        spi_m.set_tcsc(spi_m.get_period());
        reset_gen(5);
        repeat (C_NO_OF_TRANS) begin
            spi_trans();
            spi_trans_rand_n(C_NO_OF_FRAMES);
        end
        $finish;
    end

    final begin
        $display("*** End of simulation! ***");
        $display("*** Simulation was successful! ***");
    end

endmodule : test_spi_slave