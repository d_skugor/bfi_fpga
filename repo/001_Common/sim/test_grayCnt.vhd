----------------------------------------------------------------------------------------------
--                                   Rimac Automobili d.o.o.
--                             Ljubljanska 7, 10431 Sveta Nedelja
--
--            (c) Copyright 2019, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
--                                     All rights reserved
----------------------------------------------------------------------------------------------
-- file             test_grayCnt.vhd
-- module name      Gray Counter Simulation
-- author           Danijela Skugor Markovic (danijela.skugor@rimac-automobili.com)
-- project name     BFI
-- tarjet device    XA7A100TCSG324-1L
-- brief            Top Level Module
----------------------------------------------------------------------------------------------
-- create date      27.09.2019
-- Revision:
--      Revision 0.01 - File Created
--      Revision 0.02 by danijela.skugor
--                    - changes in accordance with gray_counter component changes :  
--                          -- reset changed from active-high to active-low
--                          -- update of port names of gray_counter
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity test_grayCnt is
--  Port ( );
end test_grayCnt;

architecture tb of test_grayCnt is
    ---------------------------------------------------------------------------------
    --                                  components
    ---------------------------------------------------------------------------------
    component gray_counter is
       generic(
           G_GRAYCNT_WIDTH             : integer := 4;
           G_GRAYCNT_CORRECTION_ON     : boolean := false  -- can gray counter fix itself if an error occurs
       );
       port(
           p_grayCnt_clk_in    : in std_logic;
           p_grayCnt_rst_n_in  : in std_logic;  -- sync reset
           p_grayCnt_en_in     : in std_logic;  -- enable 
           p_grayCnt_err_p_out : out std_logic; -- pulsed error
           p_grayCnt_res_out   : out std_logic_vector(G_GRAYCNT_WIDTH -1 downto 0) 
       );
    end component;
    ---------------------------------------------------------------------------------
    --                                  constants
    ---------------------------------------------------------------------------------
    constant SYSTEM_PERIOD   : time := 5 ns ;      -- 100 MHz
    constant C_GRAYCNT_WIDTH : integer := 15;

    ---------------------------------------------------------------------------------
    --                                  signals
    ---------------------------------------------------------------------------------
    
    signal sig_clk         : std_logic := '0';
    signal sig_rst_n       : std_logic := '1';
    signal sig_enable_cnt  : std_logic := '0';
    signal sig_counter_err : std_logic := '0';
    signal sig_counter_res : std_logic_vector(C_GRAYCNT_WIDTH-1 downto 0);
    
begin

    sig_rst_n <='0', '1' after 50 ns , '0' after 605 ns, '1' after 615 ns;
    sig_enable_cnt <= '0' after 300 ns, '1' after 500 ns;
  

    clk_generate: process
    begin
        wait for 5 ns; -- 100 Mhz
        sig_clk <= not sig_clk;
    end process;

  gray_cnt_block : gray_counter
        generic map(
           G_GRAYCNT_WIDTH => C_GRAYCNT_WIDTH,
           G_GRAYCNT_CORRECTION_ON => true
        )
        port map(
            p_grayCnt_clk_in     => sig_clk,
            p_grayCnt_rst_n_in   => sig_rst_n, 
            p_grayCnt_en_in      => sig_enable_cnt,
            p_grayCnt_err_p_out  => sig_counter_err,
            p_grayCnt_res_out    => sig_counter_res
        );

end tb;
