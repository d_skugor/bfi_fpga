//--------------------------------------------------------------------------------------------
//                                   Rimac Automobili d.o.o.
//                             Ljubljanska 7, 10431 Sveta Nedelja
//
//            (c) Copyright 2020, Rimac Automobili d.o.o., Sveta Nedelja, Croatia
//                                     All rights reserved
//--------------------------------------------------------------------------------------------
// file             bfi_tb_pkg.sv
// module name      bfi_tb_pkg (package)
// author           David Pavlovic (david.pavlovic@rimac-automobili.com)
// project name     BFI
// target device    XA7A100T-1CSG324Q
// brief            Reusable class library for building BFI testbenches
//--------------------------------------------------------------------------------------------
// create date      13.08.2020.
// Revision:
//      Revision 0.01 by david.pavlovic
//                    - File Created
//
//      Revision 0.02 by david.pavlovic
//                    - Added tas in SPI class for multiple transactions
//--------------------------------------------------------------------------------------------
// NOTE: This is a SystemVerilog file
//--------------------------------------------------------------------------------------------

package bfi_tb_pkg;

    timeunit 1ns / 1ps;

    import "DPI-C" function real sin(input real in);

    //=====================================================================================
    // Types
    //=====================================================================================

    typedef enum {PWM_OK, PWM_NOK} t_pwm_status;


    //=====================================================================================
    // Classes
    //=====================================================================================


    //-------------------------------------------------------------------------------------
    // SPI master class
    // Parameters:
    //   W = width of SPI data frame (default 8)
    //   N = number of slave devices (default 1)
    //-------------------------------------------------------------------------------------
    class spi_master #(int W = 8, int N = 1);

        // --------------------------------------------
        // Properties
        // --------------------------------------------

        // configuration properties
        bit  cpol;
        bit  cpha;
        real freq;
        real tcsc;

        // transaction properties
        rand logic [W-1:0] mosi;
        rand logic [W-1:0] mosi_a[];
             logic [W-1:0] miso;
             logic [W-1:0] miso_a[];

        // auxiliary properties
        real period;

        // virtual SPI interface
        virtual interface spi_if #(N) vif;

        // --------------------------------------------
        // Constraints
        // --------------------------------------------
        constraint c_mosi_a { mosi_a.size() inside {[1:16]}; }

        // --------------------------------------------
        // Methods
        // --------------------------------------------

        // calculates SPI clock period (in nanoseconds)
        function void calc_period();
            period = 1000.0/freq;
        endfunction;

        // initializes master's output signals
        function void init();
            vif.sck  <= cpol;
            vif.ss_n <= '1;
            vif.mosi <= 1'b0;
        endfunction : init;

        // constructor
        function new (virtual spi_if #(N) vif);
            // initialization
            this.cpol   = 0;
            this.cpha   = 0;
            this.freq   = 1.0;
            this.mosi   = '0;
            this.miso   = '0;
            this.vif    = vif;
            calc_period();
            this.tcsc   = period/2;
            init();
        endfunction : new

        // sets clock polarity property to 0 (idle low) or 1 (idle high)
        // and initializes the SPI sck signal (default 0)
        function void set_cpol(bit cpol);
            this.cpol = cpol;
            init();
        endfunction : set_cpol

        // returns the value of clock polarity property
        function bit get_cpol();
            get_cpol = cpol;
        endfunction : get_cpol

        // sets clock phase property
        function void set_cpha(input bit cpha);
            this.cpha = cpha;
        endfunction : set_cpha

        // returns the value of clock phase property
        function bit get_cpha();
            get_cpha = cpha;
        endfunction : get_cpha

        // sets SPI clock frequency (in MHz) and calculates clock
        // period for the new frequency
        // (default 1 MHz)
        function void set_freq(input real freq);
            this.freq = freq;
            calc_period();
        endfunction : set_freq

        // returns the value of SPI clock frequency
        function real get_freq();
            get_freq = freq;
        endfunction : get_freq

        // returns the value of SPI clock period (in nanoseconds)
        function real get_period();
            get_period = period;
        endfunction : get_period

        // sets chip-select to clock time (in ns)
        // (default is half of SCK period)
        function void set_tcsc(input real tcsc);
            this.tcsc = tcsc;
        endfunction : set_tcsc

        // returns the value
        function real get_tcsc();
            get_tcsc = tcsc;
        endfunction : get_tcsc

        // sets the MOSI data word which will be sent to the slave
        // (for random data word use SV randomize() function)
        function void set_mosi(input logic [W-1:0] mosi);
            this.mosi = mosi;
        endfunction : set_mosi

        // returns the value of latest MOSI data word
        function bit [W-1:0] get_mosi();
            get_mosi = mosi;
        endfunction : get_mosi

        // returns the value of latest received MISO data word
        function bit [W-1:0] get_miso();
            get_miso = miso;
        endfunction : get_miso

        // post randomize function
        function void post_randomize();
            $display("mosi_a = %p", mosi_a);
            $display("miso_a = %p", miso_a);
            if (miso_a.size() == 0)
                miso_a = new[mosi_a.size()];
            else begin
                miso_a.delete();
                miso_a = new[mosi_a.size()];
            end
        endfunction : post_randomize

        // executes one SPI transaction
        task drive(input int slv_sel = 0);
            vif.ss_n[slv_sel] <= 1'b0;
            if (!cpha)
                vif.mosi <= mosi[W-1];
            #tcsc;
            foreach (mosi[i]) begin
                vif.sck = ~vif.sck;
                if (cpha)
                    vif.mosi <= mosi[i];
                else
                    miso[i] = vif.miso;
                #(period/2);
                vif.sck <= ~vif.sck;
                if (cpha)
                    miso[i] = vif.miso;
                else begin
                    if (i != 0)
                        vif.mosi <= mosi[i-1];
                end
                if (i != 0)
                    #(period/2);
            end
            #tcsc;
            vif.ss_n[slv_sel] <= 1'b1;
        endtask : drive

        // executes one SPI transaction
        task drive_n(input int slv_sel = 0);
            bit [W-1:0] mosi_int;
            bit [W-1:0] miso_int;
            vif.ss_n[slv_sel] <= 1'b0;
            foreach(mosi_a[i]) begin
                mosi_int = mosi_a[i];
                miso_int = '0;
                if (!cpha)
                    vif.mosi <= mosi_int[W-1];
                #tcsc;
                foreach (mosi_int[j]) begin
                    vif.sck = ~vif.sck;
                    if (cpha)
                        vif.mosi <= mosi_int[j];
                    else
                        miso_int[j] = vif.miso;
                    #(period/2);
                    vif.sck <= ~vif.sck;
                    if (cpha)
                        miso_int[j] = vif.miso;
                    else begin
                        if (j != 0)
                            vif.mosi <= mosi_int[j-1];
                    end
                    if (j != 0)
                        #(period/2);
                    miso_a[i] = miso_int;
                end
                #tcsc;
            end
            vif.ss_n[slv_sel] <= 1'b1;
        endtask : drive_n

    endclass : spi_master


    //-------------------------------------------------------------------------------------
    // Parametrized CRC calculation and checking class
    // Parameters:
    //   DW = width of data on which CRC wil be calculated (default 16)
    //   PW = order of CRC polynomial vector               (default 8)
    //   P  = value of CRC polynomial vector               (default is CRC8 VDA CAN)
    //-------------------------------------------------------------------------------------
    // Example of using 6th order polynomial P on 32 bit data word:
    //   crc_value = crc#(32,6,P)::calc_crc(data);
    //   ok = crc#(32,6,P)::check_crc(data, crc_value);
    //-------------------------------------------------------------------------------------
    virtual class crc #(int DW = 16, PW = 8, logic [PW:0] P = 'b100011101);

        // calculates CRC value of input data word using polynomial P
        static function logic [PW-1:0] calc_crc (input logic [DW-1:0] data);
            logic [DW+PW-1:0] data_p;
            logic [PW-1:0] zeros;
            zeros  = '0;
            data_p = {data, zeros};
            for (int i = DW+PW-1; i >= PW; i--) begin
                if (data_p[i]) begin
                    for (int j=0; j <= PW; j++) begin
                        data_p[i-j] = data_p[i-j] ^ P[PW-j];
                    end
                end
            end
            calc_crc = data_p[PW-1:0];
        endfunction : calc_crc

        // checks if there are no CRC errors
        // returns 1 if no error, otherwise 0
        static function bit check_crc(input logic [DW-1:0] data, logic [PW-1:0] crc);
            logic [DW+PW-1:0] data_p;
            data_p = {data, crc};
            for (int i=DW+PW-1; i>=PW; i--) begin
                if (data_p[i]) begin
                    for (int j=0; j<=PW; j++) begin
                        data_p[i-j] = data_p[i-j] ^ P[PW-j];
                    end
                end
            end
            if (data_p[PW-1:0] != 0)
                check_crc = 0;
            else
                check_crc = 1;
        endfunction : check_crc

    endclass : crc


    //-------------------------------------------------------------------------------------
    // PWM class
    // Parameters:
    //   DT = dead time (default 3us)
    //-------------------------------------------------------------------------------------
    class pwm_c #(real DT = 3000.0);

        //----------------------------------------------
        // Properties
        //----------------------------------------------

        const real C_NS_TO_S = 1/1000000000.0;
        const real C_DT_AMP  = 0.43;
        const real C_PI      = 3.14159265359;

              real deadtime;
              real duty_cycle [3];
              real t_on       [3];
              real t_off      [3];
              real pwm_per;
              real pwm_stuck;
        rand  int  pwm_freq;
        rand  int  ele_freq;
        rand  t_pwm_status pwm_status;

        // constraints
        constraint ele_freq_c   { ele_freq inside {[0:1800]}; }
        constraint pwm_freq_c   { pwm_freq inside {10000, 16000, 20000}; }
        constraint pwm_status_c { pwm_status dist {PWM_OK:/100, PWM_NOK:/0}; }

        // interface
        virtual interface pwm_if #(DT) vif;

        //----------------------------------------------
        // Methods
        //----------------------------------------------

        // constructor
        function new (input virtual interface pwm_if #(DT) vif);
            //this.deadtime = 3000;
            this.pwm_stuck = 125000;
            this.vif       = vif;
            init_if();
        endfunction : new

        // post-randomize
        function void post_randomize();
            calc_pwm_per();
        endfunction : post_randomize

        // calculates PWM period
        function void calc_pwm_per();
            pwm_per = 1 / real'(pwm_freq) * 1000000000;
        endfunction : calc_pwm_per

        // initializes interface signals
        function void init_if ();
            vif.pwm_int   <= '0;
            vif.pwm_int_d <= '0;
            vif.pwm_i     <= '0;
            vif.pwm_fb    <= '0;
        endfunction : init_if

        // calculates duty cycle and on and off times of PWM signal for all three phases
        function void calc_t_and_dt();
            duty_cycle[2] = 0.5 + C_DT_AMP * sin(2*C_PI*ele_freq*$realtime*C_NS_TO_S);
            duty_cycle[1] = 0.5 + C_DT_AMP * sin(2*C_PI*ele_freq*$realtime*C_NS_TO_S - 2*C_PI/3);
            duty_cycle[0] = 0.5 + C_DT_AMP * sin(2*C_PI*ele_freq*$realtime*C_NS_TO_S - 4*C_PI/3);
            foreach (duty_cycle[i]) begin
                t_on [i] = pwm_per * duty_cycle[i];
                t_off[i] = (pwm_per - t_on[i])/2;
            end
        endfunction : calc_t_and_dt

        // drives HS and LS signals for one phase
        task pwm_cycle_phase (input int phase);
            vif.pwm_int[2*phase+1] <= 1'b0;
            vif.pwm_int[2*phase]   <= 1'b1;
            #(t_off[phase]);
            vif.pwm_int[2*phase+1] <= 1'b1;
            vif.pwm_int[2*phase]   <= 1'b0;
            #(t_on[phase]);
            vif.pwm_int[2*phase+1] <= 1'b0;
            vif.pwm_int[2*phase]   <= 1'b1;
            #(t_off[phase]);
        endtask : pwm_cycle_phase

        // drives HS and LS signals for all three phases
        task pwm_cycle ();
            calc_t_and_dt();
            if (pwm_status == PWM_OK) begin
                fork
                    pwm_cycle_phase(2);
                    pwm_cycle_phase(1);
                    pwm_cycle_phase(0);
                join
            end else #pwm_stuck;
        endtask : pwm_cycle

    endclass : pwm_c;

endpackage : bfi_tb_pkg